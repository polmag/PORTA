loadmodel -f test.h5
solve_jacobi -i 10 -c 1.0e-4
savemodel -f test_out.h5

loadmodel -o pmd -f test.pmd
solve_jacobi -i 10 -c 1.0e-4
savemodel -o pmd -f test_out.pmd

loadmodel -f test_out.h5
fs_surface -c sc -o psp -i 0 -a 0 -m 0 -M 0 -f test_out.psp
fs_surface -c sc -i 0 -a 0 -m 0 -M 0 -f test_out_psp.h5
fs_surface -o psp -i 0 -a 0 -m 0 -M 0 -f test_out_lc.psp
fs_surface -i 0 -a 0 -m 0 -M 0 -f test_out_lc_psp.h5

loadmodel -o pmd -f test_out.pmd
get_tau_pos -o tau -t 1.0 -i 0.0 -a 0.0 -m 0 -M 0 -f test_out.tau
get_tau_pos -t 1.0 -i 0.0 -a 0.0 -m 0 -M 0 -f test_out_tau.h5



