#!/usr/bin/env python
# -*- coding: utf-8 -*-

#####################
# contpol-pmd.py
#
# Ángel de Vicente 
#
# Writes a simple pmd file to test the continuum polarization module
#
#####################
#
# 27-11-2019 - First version (AdV)
#
#####################

import sys, math, os, struct, copy
import h5py
import numpy as np
import datetime

######################################################################

def get_atmo(atmoname):
    ''' Configures the atmospheric model
    '''

    # Initialize control flags
    atmo = {'check_atmo': False}

    # Get thermal part
    if atmoname == 'FALC':

        # Number of nodes
        atmo['nz'] = 70
        # Heights in km
        atmo['z'] = [2219.430,2217.880,2216.430,2215.080,2212.920, \
                     2210.750,2209.650,2208.530,2207.400,2206.270, \
                     2205.700,2205.180,2204.660,2204.130,2203.630, \
                     2203.160,2202.700,2202.220,2201.830,2201.560, \
                     2201.160,2200.840,2200.100,2199.000,2190.000, \
                     2168.000,2140.000,2110.000,2087.000,2075.000, \
                     2062.000,2043.000,2017.000,1980.000,1915.000, \
                     1860.000,1775.000,1670.000,1580.000,1475.000, \
                     1378.000,1278.000,1180.000,1065.000,980.000, \
                     905.000,855.000,805.000,755.000,705.000, \
                     650.000,600.000,560.000,525.000,490.000, \
                     450.000,400.000,350.000,300.000,250.000, \
                     200.000,150.000,100.000,50.000,0.000,-20.000, \
                     -40.000,-60.000,-80.000,-100.000]
        # Temperature in K
        atmo['T'] = [102770.00,98790.00,94800.00,90816.00,83891.00, \
                     75934.00,71336.00,66145.00,60170.00,53284.00, \
                     49385.00,45416.00,41178.00,36594.00,32145.00, \
                     27972.00,24056.00,20416.00,17925.00,16500.00, \
                     15000.00,14250.00,13500.00,13000.00,12000.00, \
                     11150.00,10550.00,9900.00,9450.00,9200.00, \
                     8950.00,8700.00,8400.00,8050.00,7650.00, \
                     7450.00,7250.00,7050.00,6900.00,6720.00, \
                     6560.00,6390.00,6230.00,6040.00,5900.00, \
                     5755.00,5650.00,5490.00,5280.00,5030.00, \
                     4750.00,4550.00,4430.00,4400.00,4410.00, \
                     4460.00,4560.00,4660.00,4770.00,4880.00, \
                     4990.00,5150.00,5410.00,5790.00,6520.00, \
                     6980.00,7590.00,8220.00,8860.00,9400.00]
        # Electron density in cm^-3
        atmo['Ne'] = [6.560E+09,6.810E+09,7.070E+09,7.360E+09, \
                      7.910E+09,8.660E+09,9.170E+09,9.820E+09, \
                      1.070E+10,1.190E+10,1.280E+10,1.380E+10, \
                      1.510E+10,1.670E+10,1.880E+10,2.120E+10, \
                      2.410E+10,2.770E+10,3.080E+10,3.290E+10, \
                      3.530E+10,3.650E+10,3.730E+10,3.720E+10, \
                      3.560E+10,3.810E+10,4.060E+10,4.240E+10, \
                      4.310E+10,4.340E+10,4.360E+10,4.370E+10, \
                      4.390E+10,4.420E+10,4.540E+10,4.700E+10, \
                      5.080E+10,5.740E+10,6.320E+10,7.040E+10, \
                      7.860E+10,8.760E+10,9.800E+10,1.100E+11, \
                      1.180E+11,1.230E+11,1.240E+11,1.080E+11, \
                      9.410E+10,8.280E+10,9.030E+10,1.260E+11, \
                      1.770E+11,2.410E+11,3.280E+11,4.660E+11, \
                      7.240E+11,1.120E+12,1.710E+12,2.600E+12, \
                      3.930E+12,6.040E+12,9.890E+12,1.970E+13, \
                      7.680E+13,1.730E+14,4.480E+14,1.050E+15, \
                      2.210E+15,3.870E+15]

        atmo['Nh'] = [5.480E+09,5.700E+09,5.940E+09,6.210E+09, \
                      6.731E+09,7.452E+09,7.944E+09,8.570E+09, \
                      9.433E+09,1.066E+10,1.149E+10,1.245E+10, \
                      1.373E+10,1.537E+10,1.747E+10,1.995E+10, \
                      2.304E+10,2.688E+10,3.045E+10,3.292E+10, \
                      3.610E+10,3.808E+10,4.059E+10,4.304E+10, \
                      5.010E+10,5.660E+10,6.400E+10,7.460E+10, \
                      8.430E+10,9.030E+10,9.710E+10,1.070E+11, \
                      1.219E+11,1.463E+11,1.989E+11,2.561E+11, \
                      3.797E+11,6.298E+11,9.896E+11,1.729E+12, \
                      2.967E+12,5.385E+12,1.002E+13,2.161E+13, \
                      3.931E+13,6.801E+13,9.931E+13,1.481E+14, \
                      2.271E+14,3.560E+14,6.030E+14,9.890E+14, \
                      1.480E+15,2.080E+15,2.900E+15,4.190E+15, \
                      6.540E+15,1.010E+16,1.540E+16,2.330E+16, \
                      3.470E+16,5.050E+16,7.100E+16,9.551E+16, \
                      1.181E+17,1.242E+17,1.284E+17,1.310E+17, \
                      1.322E+17,1.348E+17]

        # Reverse z axis and convert to cm
        atmo['z'] = atmo['z'][::-1]
        for i in range(len(atmo['z'])):
            atmo['z'][i] = atmo['z'][i]*1e5
        atmo['T'] = atmo['T'][::-1]
        atmo['Ne'] = atmo['Ne'][::-1]
        atmo['Nh'] = atmo['Nh'][::-1]
        atmo['check_atmo'] = True

    dz = np.array(atmo['z'])
    dz = np.absolute(dz[1:] - dz[:-1])
    atmo['mindz'] = np.amin(dz)

    return atmo

######################################################################



def main():
    ''' Writes a simple pmd file (binary and HDF5 versions) for the contpol module
    '''

    ###################################
    # Internal function: write_hdf5
    ###################################                    
    def write_hdf5(outname):
        try:
            import h5py
        except:
            msg = 'HDF5 package required for HDF5 file creation: skipping'
            sys.exit(msg)


        f = h5py.File(outname, 'w')

        # PORTA Head #
        # ------------
        f.attrs['PMD_MAGIC'] = np.string_('portapmd')
        f.attrs['PMD_VERSION'] = np.int32([2])

        now = datetime.datetime.now()
        date = np.int32([int(now.year - 1900.), int(now.month-1.), \
                int(now.day), int(now.hour), int(now.minute), \
                int(now.second)])
        f.attrs['CREATION_DATE'] = date
        f.attrs['PERIODICITY'] = np.int32([1,1])
        f.attrs['DOMAIN_SIZE'] = [Dx,Dy,Dz]
        f.attrs['DOMAIN_ORIGIN'] = [x0,y0,z0]
        f.attrs['GRID_DIMENSIONS'] = np.int32([nx,ny,nz])
        f.attrs['X_AXIS'] = x
        f.attrs['Y_AXIS'] = y
        f.attrs['Z_AXIS'] = z
        f.attrs['POLAR_NODES'] = np.int32([nth])
        f.attrs['AZIMUTH_NODES'] = np.int32([nph])
        f.attrs['MODULE_NAME'] = np.string_(m_name)

        comment = 'Continuum polarization test'
        f.attrs['MODULE_COMMENT'] = np.string_(comment)

        Head_size = int(4 + 8 + 8*nx*ny)
        Node_size = int(3*4 + 9*8)
        f.attrs['MODULE_HEADER_SIZE'] = np.int32([Head_size])

        # Not used in PORTA, really
        # f.attrs['Size Node'] = np.int32([Node_size])


        # Module head #
        # -------------
        f.create_group('Module') 

        f['Module'].attrs['MOD_VERSION'] = np.int32([m_version])
        f['Module'].attrs['FREQUENCY'] = [frequency]

        T0 = atmo['T'][0]
        f['Module/temp_bc'] = np.full((ny,nx),T0)

        # Grid Data #
        # -----------

        # Grid node structured array
        node_dt = np.dtype([ \
                        ('Nh',np.float32), \
                        ('Ne',np.float32), \
                        ('T',np.float32), \
                        ('jkq',np.float,9) \
        ])

        grid = f['Module'].create_dataset("g_data",(nz,ny,nx), dtype=node_dt)

        # For each height
        for iz in range(nz):

            n_T = atmo['T'][iz]
            n_Ne = atmo['Ne'][iz]
            n_Nh = atmo['Nh'][iz]
            
            # For each column to replicate
            grid[iz,:,:,'T'] = n_T
            grid[iz,:,:,'Ne'] = n_Ne
            grid[iz,:,:,'Nh'] = n_Nh
            grid[iz,:,:,'jkq'] = [0.0]*9

            # If there are perturbations, apply them
            if bPerx:
                if sys.argv.count('-Afx') > 0:
                    for ix in range(nx):
                        grid[iz,:,ix,'T'] += \
                            n_T*fVPerx*np.cos(fTPerx*x[ix])
                else:
                    for ix in range(nx):
                        grid[iz,:,ix,'T'] += \
                            fVPerx*np.cos(fTPerx*x[ix])
                    
            if bPery:
                if sys.argv.count('-Afy') > 0:
                    for iy in range(ny):
                        grid[iz,iy,:,'T'] += \
                            n_T*fVPery*np.cos(fTPery*y[iy])
                else:
                    for iy in range(ny):
                        grid[iz,iy,:,'T'] += \
                            fVPery*np.cos(fTPery*y[iy])

            
        f.close()

        # Closing message
        msg = '\nYour pmd file (HDF5 format) has been created and \n' + \
            '  stored in the file: {0}'
        print(msg.format(outname))

    # Internal function: write_hdf5 (end)
    ###################################
        
        
    ###################################
    # Internal function: write_pmd
    ###################################                    
    def write_pmd(outname):
    
        # Open pmd file pmd
        f = open(outname, 'wb')

        ##############
        # PORTA Head #
        ##############

        # Identifier string
        if oldpy:
            f.write(struct.pack('c'*8, *list('portapmd')))
        else:
            f.write(struct.pack('8s', b'portapmd'))
        # Endianess (0 = little)
        f.write(struct.pack('b', 0))
        # Integer size
        f.write(struct.pack('b', 4))
        # Double size
        f.write(struct.pack('b', 8))
        # PMD version
        f.write(struct.pack('i', 2))
        # Date
        now = datetime.datetime.now()
        date = [int(now.year - 1900.), int(now.month-1.), \
                int(now.day), int(now.hour), int(now.minute), \
                int(now.second)]
        f.write(struct.pack('i'*6, *date))
        # X periodicity
        f.write(struct.pack('b', 1))
        # Y periodicity
        f.write(struct.pack('b', 1))
        # Domain size in cm
        f.write(struct.pack('ddd', *[Dx,Dy,Dz]))
        # Domain origin in cm
        f.write(struct.pack('ddd', *[x0,y0,z0]))
        # Grid dimensions
        f.write(struct.pack('iii', *[nx,ny,nz]))
        # X axis
        f.write(struct.pack('d'*nx, *x))
        f.write(struct.pack('d'*(8192-nx), *np.zeros((8192-nx))))
        # Y axis
        f.write(struct.pack('d'*ny, *y))
        f.write(struct.pack('d'*(8192-ny), *np.zeros((8192-ny))))
        # Z axis
        f.write(struct.pack('d'*nz, *z))
        f.write(struct.pack('d'*(8192-nz), *np.zeros((8192-nz))))
        # Polar nodes per octant
        f.write(struct.pack('i', nth))
        # Azimuth nodes per octant
        f.write(struct.pack('i', nph))
        # Module name
        if oldpy:
            f.write(struct.pack('c'*m_nsize, *list(m_name)))
            f.write(struct.pack('c'*(1023-m_nsize), \
                                *(['\x00']*(1023-m_nsize))))
        else:
            f.write(struct.pack(str(m_nsize)+'s', \
                                bytes(m_name,encoding="utf-8")))
            f.write(struct.pack(str(1023-m_nsize)+'s', \
                    bytes('\x00'*(1023-m_nsize),encoding="utf-8")))
        # Comment
        comment = 'Continuum polarization test'
        c_size = len(list(comment))
        if oldpy:
            f.write(struct.pack('c'*c_size, *list(comment)))
            f.write(struct.pack('c'*(4096-c_size), \
                                *(['\0']*(4096-c_size))))
        else:
            f.write(struct.pack(str(c_size)+'s', \
                                bytes(comment,encoding="utf-8")))
            f.write(struct.pack(str(4096-c_size)+'s', \
                    bytes('\0'*(4096-c_size),encoding="utf-8")))
        # Sizes of header and node
        Head_size = int(4 + 8 + 8*nx*ny)
        Node_size = int(3*4 + 9*8)
        f.write(struct.pack('i', Head_size))
        f.write(struct.pack('i', Node_size))

        ###############
        # Module head #
        ###############

        # Module version
        f.write(struct.pack('i', m_version))
        f.write(struct.pack('d', frequency))

        # Temperature at the bottom layer
        T0 = atmo['T'][0]
        f.write(struct.pack('d'*nx*ny, *([T0]*nx*ny)))

        ###############
        # Module Grid #
        ###############

        # For each height
        for iz in range(nz):

            n_T = atmo['T'][iz]
            n_Ne = atmo['Ne'][iz]
            n_Nh = atmo['Nh'][iz]

            # For each column to replicate
            for iy in range(ny):

                # If perturbation in Y
                if bPery:
                    if sys.argv.count('-Afy') > 0:
                        dTy = n_T*fVPery*np.cos(fTPery*y[iy])
                    else:
                        dTy = fVPery*np.cos(fTPery*y[iy])
                else:
                    dTy = 0.0

                for ix in range(nx):

                    # If perturbation in X
                    if bPerx:
                        if sys.argv.count('-Afx') > 0:
                            dTx = n_T*fVPerx*np.cos(fTPerx*x[ix])
                        else:
                            dTx = fVPerx*np.cos(fTPerx*x[ix])
                    else:
                        dTx = 0.0


                    f.write(struct.pack('f', n_Nh))
                    f.write(struct.pack('f', n_Ne))
                    f.write(struct.pack('f', n_T+dTy+dTx))
                    f.write(struct.pack('d'*9, *([0.0]*9)))
                
        # Close pmd
        f.close()

        # Closing message
        msg = '\nYour pmd file (binary format) has been created and \n' + \
            '  stored in the file: {0}'
        print(msg.format(outname))
        
    # Internal function: write_pmd (end)
    ###################################

    
    ###################################
    # Main function start
            
    # Set defaults
    outname = 'test'
    lmbda = 3100.0
    fTPerx = -1.0
    fVPerx = 1.0
    bPerx = False
    fTPery = -1.0
    fVPery = 1.0
    bPery = False
    inx = -1
    iny = -1

    # Check if old version
    if sys.version_info[0] < 3:
        oldpy = True
    else:
        oldpy = False

    # Manage command line inputs
    if len(sys.argv) > 1:
        options = ['-h','-f', \
                   '-nx','-ny','-Px','-Py','-Ax','-Ay','-Afx','-Afy']
        if '-h' in sys.argv:
            msg = 'use: python contpol-pmd.py -h -f file -l freq'
            print(msg)
            msg = '-h == shows this help'
            print(msg)
            msg = '-f == name of output file'
            print(msg)
            msg = '-l == frequency (in Angstroms) [default: 3100 A]'
            print(msg)
            msg = '-nx == number of nodes in X axis'
            print(msg)
            msg = '-ny == number of nodes in Y axis'
            print(msg)
            msg = '-Px == period for temperature perturbation ' + \
                  'or domain size in X axis in km'
            print(msg)
            msg = '-Py == period for temperature perturbation ' + \
                  'or domain size in Y axis in km'
            print(msg)
            msg = '-Ax == amplitude for temperature perturbation ' + \
                  'in X axis (absolute)'
            print(msg)
            msg = '-Ay == amplitude for temperature perturbation ' + \
                  'in Y axis (absolute)'
            print(msg)
            msg = '-Afx == amplitude for temperature perturbation ' + \
                'in X axis \n    (as a fraction of model temperature)'
            print(msg)
            msg = '-Afy == amplitude for temperature perturbation ' + \
                'in Y axis \n    (as a fraction of model temperature)'
            print(msg)
            sys.exit()
        if '-f' in sys.argv:
            if sys.argv.count('-f') > 1:
                msg = 'Each option can be used only once, ' + \
                    'duplicated -f'
                sys.exit(msg)
            ii = sys.argv.index('-f') + 1
            if ii >= len(sys.argv):
                msg = 'No file name after -f'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No file name after -f'
                sys.exit(msg)
            outname = sys.argv[ii]
            try:
                f = open(outname, 'w')
                f.close()
            except:
                msg = 'Cannot open {0} for writing'
                sys.exit(msg.format(outname))
        if '-l' in sys.argv:
            if sys.argv.count('-l') > 1:
                msg = 'Each option can be used only once, ' + \
                    'duplicated -l'
                sys.exit(msg)
            ii = sys.argv.index('-l') + 1
            if ii >= len(sys.argv):
                msg = 'No frequency after -l'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No frequency after -l'
                sys.exit(msg)
            lmbda = sys.argv[ii]
        if '-Afx' in sys.argv:
            if sys.argv.count('-Afx') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Afx'
                sys.exit(msg)
            if sys.argv.count('-Ax') > 0:
                msg = 'Options -Afx and -Ax are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Afx') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude fraction after -Afx'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude fraction after -Afx'
                sys.exit(msg)
            fVPerx = float(sys.argv[ii])
            bPerx = True
            if np.absolute(fVPerx) <= 0.0:
                msg = 'X amplitude must be larger than 0'
                sys.exit(msg)
        if '-Ax' in sys.argv:
            if sys.argv.count('-Ax') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Ax'
                sys.exit(msg)
            if sys.argv.count('-Afx') > 0:
                msg = 'Options -Afx and -Ax are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Ax') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude after -Ax'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude after -Ax'
                sys.exit(msg)
            fVPerx = float(sys.argv[ii])
            bPerx = True
            if np.absolute(fVPerx) <= 0.0:
                msg = 'X amplitude must be larger than 0'
                sys.exit(msg)
        if '-Px' in sys.argv:
            if sys.argv.count('-Px') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Px'
                sys.exit(msg)
            ii = sys.argv.index('-Px') + 1
            if ii >= len(sys.argv):
                msg = 'No period after -Px'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No period after -Px'
                sys.exit(msg)
            fTPerx = float(sys.argv[ii])
            if fTPerx < 1e-6:
                msg = 'X period must be larger than 0'
                sys.exit(msg)
        if bPerx and fTPerx < 1e-6:
            msg = 'You must specify a period in X if you ' + \
                  'define the amplitude of the perturbation'
            sys.exit(msg)
        if '-nx' in sys.argv:
            if sys.argv.count('-nx') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -nx'
                sys.exit(msg)
            ii = sys.argv.index('-nx') + 1
            if ii >= len(sys.argv):
                msg = 'No number of nodes after -nx'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No number of nodes after -nx'
                sys.exit(msg)
            inx = int(sys.argv[ii])
            if inx < 2:
                msg = 'Number of X nodes must be at least 2'
                sys.exit(msg)
            if bPerx and inx < 5:
                msg = 'For the pertubed model, nodes must be at ' + \
                      'least 5 for one period in X'
                sys.exit(msg)
        if '-Afy' in sys.argv:
            if sys.argv.count('-Afy') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Afy'
                sys.exit(msg)
            if sys.argv.count('-Ay') > 0:
                msg = 'Options -Afy and -Ay are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Afy') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude fraction after -Afy'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude fraction after -Afy'
                sys.exit(msg)
            fVPery = float(sys.argv[ii])
            bPery = True
            if np.absolute(fVPery) <= 0.0:
                msg = 'Y amplitude must be larger than 0'
                sys.exit(msg)
        if '-Ay' in sys.argv:
            if sys.argv.count('-Ay') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Ay'
                sys.exit(msg)
            if sys.argv.count('-Afy') > 0:
                msg = 'Options -Afy and -Ay are mutually ' + \
                    'exclusive. Pick only one of them'
                sys.exit(msg)
            ii = sys.argv.index('-Ay') + 1
            if ii >= len(sys.argv):
                msg = 'No amplitude after -Ay'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No amplitude after -Ay'
                sys.exit(msg)
            fVPery = float(sys.argv[ii])
            bPery = True
            if np.absolute(fVPery) <= 0.0:
                msg = 'Y amplitude must be larger than 0'
                sys.exit(msg)
        if '-Py' in sys.argv:
            if sys.argv.count('-Py') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -Py'
                sys.exit(msg)
            ii = sys.argv.index('-Py') + 1
            if ii >= len(sys.argv):
                msg = 'No period after -Py'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No period after -Py'
                sys.exit(msg)
            fTPery = float(sys.argv[ii])
            bPery = True
            if fTPery < 1e-6:
                msg = 'Y period must be larger than 0'
                sys.exit(msg)
        if bPery and fTPery < 1e-6:
            msg = 'You must specify a period in Y if you ' + \
                  'define the amplitude of the perturbation'
            sys.exit(msg)
        if '-ny' in sys.argv:
            if sys.argv.count('-y') > 1:
                msg = 'Each option can be used only once, ' + \
                      'duplicated -ny'
                sys.exit(msg)
            ii = sys.argv.index('-ny') + 1
            if ii >= len(sys.argv):
                msg = 'No number of nodes after -ny'
                sys.exit(msg)
            if sys.argv[ii] in options:
                msg = 'No number of nodes after -ny'
                sys.exit(msg)
            iny = int(sys.argv[ii])
            if iny < 2:
                msg = 'Number of Y nodes must be at least 2'
                sys.exit(msg)
            if bPery and iny < 5:
                msg = 'For the pertubed model, nodes must be at ' + \
                      'least 5 for one period in Y'
                sys.exit(msg)
    else:
        msg = 'You can run with the -h option to get the list of ' + \
            'possible parameters'
        print(msg)

######################################################################

    # Check frequency is numeric
    try:
        lmbda = float(lmbda)
    except ValueError:
        msg = 'Frequency must be a number'
        print(msg)
        raise
    except:
        raise
            

    # Angular quadrature, nodes per octant
    nth = 4
    nph = 2
    # Minimum value of mu
    mu = np.polynomial.legendre.leggauss(4)[0]
    mu = 0.5*mu + 0.5
    minmu = np.amin(np.absolute(mu))

    # Module version and name    
    m_version = 1
    m_name = 'contpol'        

    # Atmosphere
    atmo = get_atmo('FALC')
    if not atmo['check_atmo']:
        msg = 'There was a problem setting the thermal part of ' + \
              'the atmospheric model'
        sys.exit(msg)

    # Horizontal nodes, a column will be replicated
    if inx < 0:
        if bPerx:
            nx = 5
        else:
            nx = 3
    else:
        nx = inx
    if iny < 0:
        if bPery:
            ny = 5
        else:
            ny = 3
    else:
        ny = iny


    # Configure horizontal geometry.

    # If no perturbed and domain not specified
    if not bPerx and fTPerx < 1e-6:
        # It is extended enough so every ray crosses the
        # bottom boundary for 1.5D
        x0 = 0.0
        x1a = float(nx)*atmo['mindz']*minmu*1.01 + x0
        x1b = np.amax(atmo['z']) - np.min(atmo['z']) + x0
        x1 = np.max([x1a,x1b])
    else:
        # The period is the domain, not the difference
        # between extremes
        fTPerx *= 1e5
        x0 = 0.0
        x1 = fTPerx*(1.0 - 1.0/float(nx))

    # If no perturbed and domain not specified
    if not bPery and fTPery < 1e-6:
        # It is extended enough so every ray crosses the
        # bottom boundary for 1.5D
        y0 = 0.0
        y1a = float(ny)*atmo['mindz']*minmu*1.01 + y0
        y1b = np.amax(atmo['z']) - np.min(atmo['z']) + y0
        y1 = np.max([y1a,y1b])
    else:
        # The period is the domain, not the difference
        # between extremes
        fTPery *= 1e5
        y0 = 0.0
        y1 = fTPery*(1.0 - 1.0/float(ny))
    
    
    # Build geometry
    x = np.linspace(x0,x1,nx,endpoint=True)
    Dx = x[-1] + x[1] - 2.*x[0]
    y = np.linspace(y0,y1,ny,endpoint=True)
    Dy = y[-1] + y[1] - 2.*y[0]
    z = atmo['z']
    nz = atmo['nz']
    z0 = z[0]
    Dz = z[-1] - z[0]

    # Check correct size if perturbed
    if bPerx:
        if np.absolute(Dx-fTPerx) > 1e-6:
            msg = 'Something went wrong defining the domain'
            msg += '{0} != {1}'.format(Dx,fTPerx)
            sys.exit(msg)
        fTPerx = 2.0*np.pi/fTPerx
    if bPery:
        if np.absolute(Dy-fTPery) > 1e-6:
            msg = 'Something went wrong defining the domain'
            msg += '{0} != {1}'.format(Dy,fTPery)
            sys.exit(msg)
        fTPery = 2.0*np.pi/fTPery

    
    # Get size module name
    m_nsize = len(list(m_name))

    frequency = 2.99792458e10/(lmbda * 1E-8)
    
    write_pmd(outname+'.pmd')
    write_hdf5(outname+'.h5')

    

######################################################################

if __name__ == "__main__":
    main()
