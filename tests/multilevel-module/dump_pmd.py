#!/usr/bin/env python
# -*- coding: utf-8 -*-

#===============================================================================
# Python3 code to read PORTA PMD files (in binary and HDF5 format)
#-------------------------------------------------------------------------------
#
# Defines functions:
#     dump_pmd
#
# Ángel de Vicente (angel.de.vicente@iac.es)
#
# Got help from
# https://www.devdungeon.com/content/working-binary-data-python
#===============================================================================

import argparse
import numpy as np
import struct

def dump_pmd_h5(f):
    import h5py
    
    ######################
    #READING PORTA HEADER#
    ######################
    
    print(f"Magic String: {f.attrs['PMD_MAGIC'].decode('utf-8')}")
    creation = f.attrs['CREATION_DATE']
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    [periodx,periody] = f.attrs['PERIODICITY']
    print(f"Periodicity: X: {periodx} Y: {periody}")

    [s_X,s_Y,s_Z] = f.attrs['DOMAIN_SIZE']
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = f.attrs['DOMAIN_ORIGIN']
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = f.attrs['GRID_DIMENSIONS']
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    x_array = f.attrs['X_AXIS']
    print(f"X array: \n{x_array}\n")

    y_array = f.attrs['Y_AXIS']
    print(f"Y array: \n{y_array}\n")

    z_array = f.attrs['Z_AXIS']
    print(f"Z array: \n{z_array}\n")


    inc_a,azi_a = f.attrs['POLAR_NODES'][0],f.attrs['AZIMUTH_NODES'][0]
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    print(f"Name of Atomic Module: {f.attrs['MODULE_NAME'].decode('utf-8')}")
    print(f"Model Comment: {f.attrs['MODULE_COMMENT'].decode('utf-8')}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    mod_v = f['Module'].attrs['ML_VERSION'][0]
    atom_mass = f['Module'].attrs['ATOM_MASS'][0]
    nl = f['Module'].attrs['NL'][0]
    nt = f['Module'].attrs['NT'][0]
    nr = f['Module'].attrs['NR'][0]
    ener = f['Module'].attrs['E']
    land = f['Module'].attrs['GL']
    j2 = f['Module'].attrs['J2']
    iul = f['Module'].attrs['IUL']
    aul = f['Module'].attrs['AUL']
    nfreq = f['Module'].attrs['NFREQ']
    nfreqc = f['Module'].attrs['NFREQ_C']
    dw = f['Module'].attrs['DW']
    dwc = f['Module'].attrs['DW_C']
    tref = f['Module'].attrs['TEMP_REF']
    kcut = f['Module'].attrs['K_CUT'][0]
    
    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

ML_VERSION: {mod_v}
ATOM_MASS: {atom_mass}
NL: {nl}
NT: {nt}
NR: {nr}
ENERGIES: {ener}
LANDE FACTORS: {land}
ANGULAR MOMENTA (x2): {j2}
UPPER LOWER INDICES: {iul}
EINSTEIN COEFFS. SPONTANEOUS EMISSION: {aul}
N. FREQUENCIES: {nfreq}
N. FREQUENCIES (CORE): {nfreqc}
DOPPLER WIDTHS: {dw}
DOPPLER WIDTHS (CORE): {dwc}
REFERENCE TEMPERARTURES: {tref}
MAX K MULTIPOLE: {kcut}""")

    temp_bc = np.asarray(f['Module']['temp_bc'])
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################
    NJKQ = 9*nr

    if kcut < 0:
        lkcut = 2000
        
    dm_size=0
    for ii in range(nl):
        dm_size += (min(j2[ii],lkcut) + 1)**2
    
    d_f = 'f'+str(8)

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('tmp',d_f),('density',d_f),('bx',d_f),('by',d_f),('bz',d_f),('vx',d_f),('vy',d_f),('vz',d_f),('vmi',d_f),('dm',d_f,dm_size),('jkq',d_f,NJKQ),('voigt_a',d_f,nr),('cul',d_f,nt),('dk',d_f,nl),('eta_c',d_f,nr),('eps_c',d_f,nr)])

    
    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['tmp'] = f['Module/g_data']['T'][ii,jj,kk]
                node_data[ii,jj,kk]['density'] = f['Module/g_data']['N'][ii,jj,kk]
                node_data[ii,jj,kk]['bx'] = f['Module/g_data']['B'][ii,jj,kk][0]
                node_data[ii,jj,kk]['by'] = f['Module/g_data']['B'][ii,jj,kk][1]
                node_data[ii,jj,kk]['bz'] = f['Module/g_data']['B'][ii,jj,kk][2]
                node_data[ii,jj,kk]['vx'] = f['Module/g_data']['v'][ii,jj,kk][0]
                node_data[ii,jj,kk]['vy'] = f['Module/g_data']['v'][ii,jj,kk][1]
                node_data[ii,jj,kk]['vz'] = f['Module/g_data']['v'][ii,jj,kk][2]
                node_data[ii,jj,kk]['vmi'] = f['Module/g_data']['vmi'][ii,jj,kk]
                
                node_data[ii,jj,kk]['dm']= f['Module/g_data']['rho'][ii,jj,kk]
                node_data[ii,jj,kk]['jkq'] = f['Module/g_data']['J'][ii,jj,kk]

                node_data[ii,jj,kk]['voigt_a'] = f['Module/g_data']['a_voigt'][ii,jj,kk]
                node_data[ii,jj,kk]['cul'] = f['Module/g_data']['Cul'][ii,jj,kk]
                node_data[ii,jj,kk]['dk'] = f['Module/g_data']['DK'][ii,jj,kk]
                node_data[ii,jj,kk]['eta_c'] = f['Module/g_data']['eta_c'][ii,jj,kk]
                node_data[ii,jj,kk]['eps_c'] = f['Module/g_data']['eps_c'][ii,jj,kk]

                print(f"""
NODE {ii},{jj},{kk}:
--------------------
temp:    {node_data[ii,jj,kk]['tmp']}
density: {node_data[ii,jj,kk]['density']}
B:       {node_data[ii,jj,kk]['bx']} {node_data[ii,jj,kk]['by']} {node_data[ii,jj,kk]['bz']}
V:       {node_data[ii,jj,kk]['vx']} {node_data[ii,jj,kk]['vy']} {node_data[ii,jj,kk]['vz']}
dm:      {node_data[ii,jj,kk]['dm']}
jkq:     {node_data[ii,jj,kk]['jkq']}
voigt_a: {node_data[ii,jj,kk]['voigt_a']}
cul:     {node_data[ii,jj,kk]['cul']}
dk:      {node_data[ii,jj,kk]['dk']}
eta_c:   {node_data[ii,jj,kk]['eta_c']}
eps_c:   {node_data[ii,jj,kk]['eps_c']}""")

    f.close()

    
def dump_pmd(filename):
    file = open(filename, 'rb')

    ######################
    #READING PORTA HEADER#
    ######################
    
    m_s = file.read(8).decode('utf-8') ; print("Magic String: ",m_s)
    endian = int.from_bytes(file.read(1),byteorder='little') ; print("Endianness: ", endian)
    if endian == 0:
        endian_s = "little"
        e_str    = '<'
    else:
        endian_s = "big"
        e_str    = '>'
        
    sizeint = int.from_bytes(file.read(1),byteorder=endian_s)      ; print("Size of integer: ", sizeint)
    sizedble = int.from_bytes(file.read(1),byteorder=endian_s)     ; print("Size of double: ", sizedble)
    vpmd = int.from_bytes(file.read(sizeint),byteorder=endian_s)   ; print("Version of PMD file: ", vpmd)

    creation = struct.unpack(e_str+'iiiiii', file.read(sizeint*6))
    print(f"Creation date: {creation[2]}/{creation[1]+1}/{creation[0]+1900} {creation[3]}:{creation[4]}:{creation[5]}")
    
    period = struct.unpack(e_str+'ss', file.read(2)) ;
    periodx = int.from_bytes(period[0],byteorder=endian_s)
    periody = int.from_bytes(period[1],byteorder=endian_s)
    print(f"Periodicity: X: {periodx} Y: {periody}")

    (s_X,s_Y,s_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain size: X: {s_X} Y: {s_Y} Z: {s_Z}")

    (o_X,o_Y,o_Z) = struct.unpack(e_str+'ddd', file.read(sizedble*3))
    print(f"Domain origin: X: {o_X} Y: {o_Y} Z: {o_Z}")

    (g_NX,g_NY,g_NZ) = struct.unpack(e_str+'iii', file.read(sizeint*3))
    print(f"Number of nodes: X: {g_NX} Y: {g_NY} Z: {g_NZ}")


    MAXDIM = 8192
    x_array = np.asarray(struct.unpack(e_str+'d'*g_NX, file.read(sizedble*g_NX)))
    print(f"X array: \n{x_array}\n")
    _ = file.read((MAXDIM-g_NX)*sizedble)

    y_array = np.asarray(struct.unpack(e_str+'d'*g_NY, file.read(sizedble*g_NY)))
    print(f"Y array: \n{y_array}\n")
    _ = file.read((MAXDIM-g_NY)*sizedble)

    z_array = np.asarray(struct.unpack(e_str+'d'*g_NZ, file.read(sizedble*g_NZ)))
    print(f"Z array: \n{z_array}\n")
    _ = file.read((MAXDIM-g_NZ)*sizedble)

    (inc_a,azi_a) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Number of angles: Inclination: {inc_a} Azimuthal: {azi_a}")

    name_module = file.read(1023).decode('utf-8') 
    name = name_module.split('\0',1)[0]
    print(f"Name of Atomic Module: {name}")

    comment_model = file.read(4096).decode('utf-8') 
    comment = comment_model.split('\0',1)[0]
    print(f"Model Comment: {comment}")

    (size_PMD_header,size_node) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Size of the module header in bytes: {size_PMD_header}")
    print(f"Size of the node data in bytes: {size_node}")

    #############################
    #READING ATOMIC MODEL HEADER#
    #############################

    (mod_v,atom_mass,nl,nt,nr) = struct.unpack(e_str+'idiii', file.read(sizeint*4+sizedble*1))
    ener = struct.unpack(e_str+'d'*nl,file.read(sizedble*nl))
    land = struct.unpack(e_str+nl*'d',file.read(sizedble*nl))
    j2 = struct.unpack(e_str+nl*'i',file.read(sizeint*nl))
    iul = struct.unpack(e_str+2*nt*'i',file.read(sizeint*2*nt))
    aul = struct.unpack(e_str+nr*'d',file.read(sizedble*nr))
    nfreq = struct.unpack(e_str+nr*'i',file.read(sizeint*nr))
    nfreqc = struct.unpack(e_str+nr*'i',file.read(sizeint*nr))
    dw = struct.unpack(e_str+nr*'d',file.read(sizedble*nr))
    dwc = struct.unpack(e_str+nr*'d',file.read(sizedble*nr))
    tref = struct.unpack(e_str+nr*'d',file.read(sizedble*nr))
    kcut = struct.unpack(e_str+'i',file.read(sizeint))[0]
    
    print(f"""

#############################
ATOMIC MODEL HEADER
#############################

ML_VERSION: {mod_v}
ATOM_MASS: {atom_mass}
NL: {nl}
NT: {nt}
NR: {nr}
ENERGIES: {ener}
LANDE FACTORS: {land}
ANGULAR MOMENTA (x2): {j2}
UPPER LOWER INDICES: {iul}
EINSTEIN COEFFS. SPONTANEOUS EMISSION: {aul}
N. FREQUENCIES: {nfreq}
N. FREQUENCIES (CORE): {nfreqc}
DOPPLER WIDTHS: {dw}
DOPPLER WIDTHS (CORE): {dwc}
REFERENCE TEMPERARTURES: {tref}
MAX K MULTIPOLE: {kcut}""")

    temp_bc = np.zeros((g_NY,g_NX), dtype=np.float)
    for ii in range(g_NY):
        for jj in range(g_NX):
            temp_bc[ii, jj] = struct.unpack('<d', file.read(sizedble))[0]
    print(f"BC Temp:: \n{temp_bc}\n")


    ####################
    #READING DATA NODES#
    ####################
    NJKQ = 9*nr

    if kcut < 0:
        lkcut = 2000
        
    dm_size=0
    for ii in range(nl):
        dm_size += (min(j2[ii],lkcut) + 1)**2
    
    d_f = 'f'+str(sizedble)

    node_data = np.zeros((g_NZ,g_NY,g_NX), dtype=[('tmp',d_f),('density',d_f),('bx',d_f),('by',d_f),('bz',d_f),('vx',d_f),('vy',d_f),('vz',d_f),('vmi',d_f),('dm',d_f,dm_size),('jkq',d_f,NJKQ),('voigt_a',d_f,nr),('cul',d_f,nt),('dk',d_f,nl),('eta_c',d_f,nr),('eps_c',d_f,nr)])

    print(f"""

#############################
NODES DATA
#############################
    """)

    
    for ii in range(g_NZ):
        for jj in range(g_NY):
            for kk in range(g_NX):
                node_data[ii,jj,kk]['tmp'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['density'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['bx'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['by'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['bz'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vx'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vy'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vz'] = struct.unpack(e_str+'d', file.read(sizedble))[0]
                node_data[ii,jj,kk]['vmi'] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for dmi in range(dm_size):
                    node_data[ii,jj,kk]['dm'][dmi] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for jkqi in range(NJKQ):    
                    node_data[ii,jj,kk]['jkq'][jkqi] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for vi in range(nr):
                    node_data[ii,jj,kk]['voigt_a'][vi] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for ci in range(nt):
                    node_data[ii,jj,kk]['cul'][ci] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for dki in range(nl):
                    node_data[ii,jj,kk]['dk'][dki] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for eti in range(nr):
                    node_data[ii,jj,kk]['eta_c'][eti] = struct.unpack(e_str+'d', file.read(sizedble))[0]

                for epi in range(nr):
                    node_data[ii,jj,kk]['eps_c'][epi] = struct.unpack(e_str+'d', file.read(sizedble))[0]


                print(f"""
NODE {ii},{jj},{kk}:
--------------------
temp:    {node_data[ii,jj,kk]['tmp']}
density: {node_data[ii,jj,kk]['density']}
B:       {node_data[ii,jj,kk]['bx']} {node_data[ii,jj,kk]['by']} {node_data[ii,jj,kk]['bz']}
V:       {node_data[ii,jj,kk]['vx']} {node_data[ii,jj,kk]['vy']} {node_data[ii,jj,kk]['vz']}
dm:      {node_data[ii,jj,kk]['dm']}
jkq:     {node_data[ii,jj,kk]['jkq']}
voigt_a: {node_data[ii,jj,kk]['voigt_a']}
cul:     {node_data[ii,jj,kk]['cul']}
dk:      {node_data[ii,jj,kk]['dk']}
eta_c:   {node_data[ii,jj,kk]['eta_c']}
eps_c:   {node_data[ii,jj,kk]['eps_c']}""")

#                 
    file.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="PORTA pmd  file")
    args = parser.parse_args()

    try:
        import h5py
        f = h5py.File(args.file,'r')
        dump_pmd_h5(f)
    except:
        # I can't open it as HDF5 file. Assume is a binary PMD file
        dump_pmd(args.file)

if __name__ == "__main__":
    main()

    
    

