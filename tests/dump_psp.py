#!/usr/bin/env python
# -*- coding: utf-8 -*-

#===============================================================================
# Pyhon3 code to read PORTA PSP files (in binary and HDF5 format)
#-------------------------------------------------------------------------------
#
# Defines functions:
#     dump_psp
#
# Ángel de Vicente (angel.de.vicente@iac.es)
#
# Got help from
# https://www.devdungeon.com/content/working-binary-data-python
#===============================================================================

import argparse
import numpy as np
import struct

def dump_psp_h5(f):
    import h5py
    
    ######################
    #READING PORTA HEADER#
    ######################
    
    print(f"Magic String: {f.attrs['IO_PSP_MAGIC'].decode('utf-8')}")
    print(f"Version of PSP file: {f.attrs['VERSION'][0]}")
    print(f"Comment: {f.attrs['COMMENT'].decode('utf-8')}")
    print(f"Line of sight angles: Theta: {f.attrs['LOS_THETA'][0]} Chi: {f.attrs['LOS_CHI'][0]}")
    print(f"Number of wavelengths: {f.attrs['NUMBER_WAVELENGTHS'][0]}")
    print(f"Wavelengths: \n{f.attrs['WAVELENGTHS']}\n")
    print(f"Orientation of atmospheric surface: {f.attrs['ORIENTATION_ATMOSPHERIC_SURFACE'][0]}")
    print(f"Domain dimensions [cm]: X: {f.attrs['DOMAIN_SIZE'][0]} Y: {f.attrs['DOMAIN_SIZE'][1]}")
    print(f"Periodicity: X: {f.attrs['PERIODICITY'][0]} Y: {f.attrs['PERIODICITY'][1]}")
    print(f"Number of nodes: X: {f.attrs['GRID_DIMENSIONS'][0]} Y: {f.attrs['GRID_DIMENSIONS'][1]}")
    
    print(f"X ordinates: \n{f.attrs['X_AXIS']}\n")
    print(f"Y ordinates: \n{f.attrs['Y_AXIS']}\n")

    print(f"STOKES I: \n{np.asarray(f['stokes_I'])}\n")
    print(f"STOKES Q: \n{np.asarray(f['stokes_Q'])}\n")
    print(f"STOKES U: \n{np.asarray(f['stokes_U'])}\n")
    print(f"STOKES V: \n{np.asarray(f['stokes_V'])}\n")
                    
    f.close()


def dump_psp(filename):
    
    file = open(filename, 'rb')
    
    ######################
    #READING PORTA HEADER#
    ######################
    endian_s = "little"
    e_str    = '<'
    sizeint = 4
    sizeflt = 4
    sizedble = 8
    stokesn = 4
    
    m_s = file.read(3).decode('utf-8') ; print("Magic String: ",m_s)
    vpmd = int.from_bytes(file.read(sizeint),byteorder=endian_s)   ; print("Version of PSP file: ", vpmd)

    comment_model = file.read(1023).decode('utf-8') 
    comment = comment_model.split('\0',1)[0]
    print(f"Comment: {comment}")

    (los_theta,los_chi) = struct.unpack(e_str+'dd', file.read(sizedble*2))
    print(f"Line of sight angles: Theta: {los_theta} Chi: {los_chi}")

    nwl = struct.unpack(e_str+'i', file.read(sizeint*1))[0]
    print(f"Number of wavelengths: {nwl}")

    wls = np.asarray(struct.unpack(e_str+'d'*nwl, file.read(sizedble*nwl)))
    print(f"Wavelengths: \n{wls}\n")

    ori = struct.unpack(e_str+'i', file.read(sizeint*1))[0]
    print(f"Orientation of atmospheric surface: {ori}")

    (x_dim,y_dim) = struct.unpack(e_str+'dd', file.read(sizedble*2))
    print(f"Domain dimensions [cm]: X: {x_dim} Y: {y_dim}")

    period = struct.unpack(e_str+'ss', file.read(2)) ;
    periodx = int.from_bytes(period[0],byteorder=endian_s)
    periody = int.from_bytes(period[1],byteorder=endian_s)
    print(f"Periodicity: X: {periodx} Y: {periody}")

    (nx,ny) = struct.unpack(e_str+'ii', file.read(sizeint*2))
    print(f"Number of nodes: X: {nx} Y: {ny}")


    x_array = np.asarray(struct.unpack(e_str+'d'*nx, file.read(sizedble*nx)))
    y_array = np.asarray(struct.unpack(e_str+'d'*ny, file.read(sizedble*ny)))
    print(f"X ordinates: \n{x_array}\n")
    print(f"Y ordinates: \n{y_array}\n")

    stokes = np.zeros((nwl,ny,nx,stokesn), dtype=np.float)
    for wi in range(nwl):
        for yi in range(ny):
            for xi in range(nx):
                for si in range(stokesn):
                    stokes[wi,yi,xi,si] = struct.unpack('<f', file.read(sizeflt))[0]
    print(f"STOKES I: \n{stokes[:,:,:,0]}\n")
    print(f"STOKES Q: \n{stokes[:,:,:,1]}\n")
    print(f"STOKES U: \n{stokes[:,:,:,2]}\n")
    print(f"STOKES V: \n{stokes[:,:,:,3]}\n")
                    
    file.close()



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="PORTA psp file")
    args = parser.parse_args()

    try:
        import h5py
        f = h5py.File(args.file,'r')
        dump_psp_h5(f)
    except:
        # I can't open it as HDF5 file. Assume is a binary PSP file
        print("binary")
        dump_psp(args.file)

if __name__ == "__main__":
    main()
    

