# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# pspedit.py                                                         #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Code to edit the content of a binary psp file                      #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Notes:                                                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  06/03/2020 - V1.0.0 - First version (TdPA)                        #
#                                                                    #
######################################################################
######################################################################
######################################################################

import sys,os,copy,struct
import numpy as np

######################################################################
######################################################################
######################################################################

def __vacuumtoair(wave,limit=None):
    ''' Convert from vacuum to air, VALD3
        http://www.astro.uu.se/valdwiki/Air-to-vacuum%20conversion
    '''

    if limit is None:
        limit = 2000.0

    s = 1e4/wave
    s2 = s*s
    ff = 1.0 + 8.34254e-5 + 2.406147e-2/(130.0 - s2) + \
                            1.5998e-4/(38.9 - s2)
    out = copy.deepcopy(wave)

    for i in range(len(wave)):
        if out[i] > limit:
            out[i] /= ff[i]

    return out

######################################################################
######################################################################

def __airtovacuum(wave,limit=None):
    ''' Convert from air to vacuum, VALD3
        http://www.astro.uu.se/valdwiki/Air-to-vacuum%20conversion
    '''

    if limit is None:
        limit = 2000.0

    s = 1e4/wave
    s2 = s*s
    ff = 1.0 + 8.336624212083e-5 + \
               2.408926869968e-2/(130.10659245522 - s2) + \
               1.599740894897e-4/(38.92568793293 - s2)
    out = copy.deepcopy(wave)

    for i in range(len(wave)):
        if out[i] > limit:
            out[i] *= ff[i]

    return out

######################################################################
######################################################################
######################################################################

def __error(msg=None, error=None):
    ''' Error print
    '''

    omsg = '\033[91m' + '\033[1m' + '##Error## ' + '\033[0m'

    if msg is not None:
        omsg += str(msg)

    if error is not None:
        for err in error:
            omsg += '\n  ' + str(err)

    print(omsg)
    sys.exit()

######################################################################
######################################################################
######################################################################

def __dub(key, array):
    ''' Checks duplicity, returns position in array
    '''

    if array.count(key) > 1:
        msg = 'Each option can be used only once, ' + \
              'duplicated {0}'.format(key)
        __error(msg)

    ind = array.index(key) + 1

    if ind >= len(array):
        msg = 'No argument after {0}'.format(key)
        __error(msg)

    return ind

######################################################################
######################################################################
######################################################################

def __arg(key, array, comms):
    ''' Checks argument
    '''

    if key in array:

        # Duplicity
        ii = __dub(key, array)

        # Argument
        if sys.argv[ii] in comms:
            msg = 'No argument name after {0}'.format(key)
            __error(msg)

        # Return value
        return sys.argv[ii]

    else:

        return None

######################################################################
######################################################################
######################################################################

def __print_help():

    print('Edits a psp file given the user inputs. First argument')
    print('must be the file name or the "-h" command. Commands:')
    print('  -o: Name of the output file if editting the data.')
    print(' --v: Returns the psp version of the selected file.')
    print('  -v: Changes the current version of the psp file to the')
    print('      selected version. Right now there is only one psp')
    print('      version: 1. Must be followed by an integer.')
    print('         e.g., python pspedit.py file -V 1')
    print(' --c: Returns the comment in the header of the selected')
    print('      file.')
    print('  -c: Overwrites the comment field with the strings that')
    print('      follow. It will scan up to the next command or the')
    print('      end of line, unless the string is within "')
    print('      characters.')
    print('         e.g., python pspedit.py file -c New comment')
    print('               python pspedit.py file -c "New comment"')
    print(' --t: Returns the theta angle of the line of sight in')
    print('      degrees..')
    print('  -t: Changes the theta angle of the line of sight. Must')
    print('      be followed by a number in degrees between 0 and')
    print('      180')
    print('         e.g., python pspedit.py file -t 30.0')
    print(' --a: Returns the azimuthal angle of the line of sight in')
    print('      degrees.')
    print('  -a: Changes the azimuthal angle of the line of sight.')
    print('      Must be followed by a number in degrees between 0')
    print('      and 360.')
    print('         e.g., python pspedit.py file -a 10.0')
    print('--nl: Returns the number of lambda wavelenghts.')
    print(' --l: Returns the array of lambda wavelenghts in ')
    print('      Angstroms.')
    print('-a2v: Converts wavelengths from air to vacuum.')
    print('-v2a: Converts wavelengths from vacuum to air.')
    print('-il0: Cuts the wavelengths array below the specified')
    print('      integer.')
    print('         e.g., python pspedit.py file -il0 11')
    print('-il1: Cuts the wavelengths array above the specified')
    print('      integer.')
    print('         e.g., python pspedit.py file -il1 88')
    print(' -l0: Cuts the wavelengths array below the specified')
    print('      wavelenght in Angstroms.')
    print('         e.g., python pspedit.py file -l0 5000.0')
    print(' -l1: Cuts the wavelengths array above the specified')
    print('      wavelength in Angstroms.')
    print('         e.g., python pspedit.py file -l1 6000.0')
    print('--dx: Returns the domain size in the X dimension in cm.')
    print('--dy: Returns the domain size in the Y dimension in cm.')
    print(' --P: Returns the periodicity condition in the X and Y')
    print('      boundaries.')
    print('--nx: Returns the number of nodes along the X dimension.')
    print('--ny: Returns the number of nodes along the Y dimension.')
    print(' --x: Returns the X axis in cm.')
    print(' --y: Returns the Y axis in cm.')
    print('-ix0: Cuts the X axis below the specified integer.')
    print('         e.g., python pspedit.py file -ix0 20')
    print('-ix1: Cuts the X axis above the specified integer.')
    print('         e.g., python pspedit.py file -ix1 40')
    print('-iy0: Cuts the Y axis below the specified integer.')
    print('         e.g., python pspedit.py file -iy0 20')
    print('-iy1: Cuts the Y axis above the specified integer.')
    print('         e.g., python pspedit.py file -iy1 40')
    print(' -x0: Cuts the X axis below the specified position')
    print('      in cm.')
    print('         e.g., python pspedit.py file -x0 2e8')
    print(' -x1: Cuts the X axis above the specified position')
    print('      in cm.')
    print('         e.g., python pspedit.py file -x1 90e8')
    print(' -y0: Cuts the Y axis below the specified position')
    print('      in cm.')
    print('         e.g., python pspedit.py file -y0 2e8')
    print(' -y1: Cuts the Y axis above the specified position')
    print('      in cm.')
    print('         e.g., python pspedit.py file -y1 90e8')
    sys.exit()

######################################################################
######################################################################
######################################################################

def main():
    ''' Edits a psp file given the user inputs. First argument must
        be the file name or the "-h" command. Commands:
            -o: Name of the output file if editting the data.
           --v: Returns the psp version of the selected file.
            -v: Changes the current version of the psp file to the
                selected version. Right now there is only one psp
                version: 1. Must be followed by an integer.
                   e.g., python pspedit.py file -V 1
           --c: Returns the comment in the header of the selected
                file.
            -c: Overwrites the comment field with the strings that
                follow. It will scan up to the next command or the
                end of line, unless the string is within " characters.
                   e.g., python pspedit.py file -c New comment
                         python pspedit.py file -c "New comment"
           --t: Returns the theta angle of the line of sight in
                degrees..
            -t: Changes the theta angle of the line of sight. Must
                be followed by a number in degrees between 0 and 180.
                   e.g., python pspedit.py file -t 30.0
           --a: Returns the azimuthal angle of the line of sight in
                degrees.
            -a: Changes the azimuthal angle of the line of sight.
                Must be followed by a number in degrees between 0.0
                and 360.0.
                   e.g., python pspedit.py file -a 10.0
          --nl: Returns the number of lambda wavelenghts.
           --l: Returns the array of lambda wavelenghts in Angstroms.
          -a2v: Converts wavelengths from air to vacuum.
          -v2a: Converts wavelengths from vacuum to air.
          -il0: Cuts the wavelengths array below the specified
                integer.
                   e.g., python pspedit.py file -il0 11
          -il1: Cuts the wavelengths array above the specified
                integer.
                   e.g., python pspedit.py file -il1 88
           -l0: Cuts the wavelengths array below the specified
                wavelenght in Angstroms.
                   e.g., python pspedit.py file -l0 5000.0
           -l1: Cuts the wavelengths array above the specified
                wavelength in Angstroms.
                   e.g., python pspedit.py file -l1 6000.0
          --dx: Returns the domain size in the X dimension in cm.
          --dy: Returns the domain size in the Y dimension in cm.
           --P: Returns the periodicity condition in the X and Y
                boundaries.
          --nx: Returns the number of nodes along the X dimension.
          --ny: Returns the number of nodes along the Y dimension.
           --x: Returns the X axis in cm.
           --y: Returns the Y axis in cm.
          -ix0: Cuts the X axis below the specified integer.
                   e.g., python pspedit.py file -ix0 20
          -ix1: Cuts the X axis above the specified integer.
                   e.g., python pspedit.py file -ix1 40
          -iy0: Cuts the Y axis below the specified integer.
                   e.g., python pspedit.py file -iy0 20
          -iy1: Cuts the Y axis above the specified integer.
                   e.g., python pspedit.py file -iy1 40
           -x0: Cuts the X axis below the specified position
                in cm.
                   e.g., python pspedit.py file -x0 2e8
           -x1: Cuts the X axis above the specified position
                in cm.
                   e.g., python pspedit.py file -x1 90e8
           -y0: Cuts the Y axis below the specified position
                in cm.
                   e.g., python pspedit.py file -y0 2e8
           -y1: Cuts the Y axis above the specified position
                in cm.
                   e.g., python pspedit.py file -y1 90e8
    '''

    # Check if old version
    if sys.version_info[0] < 3:
        oldpy = True
    else:
        oldpy = False

    # List of commands
    misc_comms = ['-o','-h']
    info_comms = ['--v','--c','--t','--a','--nl','--l', \
                  '--dx','--dy','--P','--nx','--ny','--x','--y']
    edit_comms = ['-v', '-c', '-t', '-a', \
                  '-il0','-il1','-l0','-l1','v2a','a2v', \
                  '-ix0','-ix1','-x0','-x1', \
                  '-iy0','-iy1','-y0','-y1']
    all_comms = misc_comms + info_comms + edit_comms

    # Defaults
    inputfile = None
    outfile = None
    edit = False
    info = False
    requ = {'v': False, 'c': False, 't': False, 'a': False, \
            'nl': False, 'l': False, 'dx': False, 'dy': False, \
            'P': False, 'nx': False, 'ny': False, 'x': False, \
            'y': False}
    news = {'v': None, 'c': None, 't': None, 'a': None, \
            'il0': None, 'il1': None, 'l0': None, 'l1': None, \
            'ix0': None, 'ix1': None, 'x0': None, 'x1': None, \
            'iy0': None, 'iy1': None, 'y0': None, 'y1': None, \
            'v2a': None, 'a2v': None}


    #####################
    # Command line init #
    #####################

    # Manage the command line inputs
    if len(sys.argv) > 1:

        # Input file
        inputfile = sys.argv[1]

        # Check if help
        if inputfile == '-h':
            __print_help()

        # Check if asking for help
        if '-h' in sys.argv:
            __print_help()

        # Check if file exists
        exists = os.path.isfile(inputfile)

        # If not exists, leave
        if not exists:
            msg = 'Invalid file path: {0}'.format(inputfile)
            __error(msg)

        # Check psp file
        try:
            f = open(inputfile, 'r')
            bytes = f.read(3)
            if sys.version_info[0] < 3:
                magic = ''.join(struct.unpack('ccc', bytes))
            else:
                magic = ''
                for ii in range(3):
                    byte = bytes[ii:ii+1].decode('utf-8')
                    magic += byte
            if magic != 'psp':
                msg = 'Invalid psp file: {0}'.format(inputfile)
                error = 'Key character {0} != psp'.format(magic)
                __error(msg,error)
        except:
            msg = 'Problem checking psp file: {0}'.format(inputfile)
            error = sys.exc_info()[0:2]
            __error(msg,error)

        #
        # Check output file name
        #
        outfile = __arg('-o', sys.argv, all_comms)

        #
        # Check info parameters
        #

        # For every info key
        for key in info_comms:

            # Check if in argument list
            if key in sys.argv:

                # Toggle the requirement to True
                requ[''.join(list(key)[2:])] = True

                # Initialize info output message and indicate
                # that info will be output
                if not info:
                    info = True
                    iout = []

        #
        # Check edit parameters
        #

        # For every info key
        for key in edit_comms:

            # Check if in argument list
            if key in sys.argv:

                edit = True
                eout = []
                cout = []
                break

        # If editting
        if edit:

            # Comments
            if '-c' in sys.argv:

                ii = __dub('-c', sys.argv)

                # Initialize
                news['c'] = sys.argv[ii]

                # Scan till the end
                while True:

                    # Advance index and check
                    ii += 1
                    if ii >= len(sys.argv):
                        break
                    if sys.argv[ii] in all_comms:
                        break

                    # Add to comm
                    news['c'] += ' {0}'.format(sys.argv[ii])

            # Air to vacuum
            if '-a2v' in sys.argv:

                if sys.argv.count('-a2v') > 1:
                    msg = 'Each option can be used only once, ' + \
                          'duplicated {0}'.format('-a2v')
                    __error(msg)

                news['a2v'] = True

            # Vacuum to air
            if '-v2a' in sys.argv:

                if news['a2v'] is not None:
                    msg = '-v2a and -a2v are incompatible commands'
                    __error(msg)

                if sys.argv.count('-v2a') > 1:
                    msg = 'Each option can be used only once, ' + \
                          'duplicated {0}'.format('-v2a')
                    __error(msg)

                news['v2a'] = True

            # Version
            arg = __arg('-v', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-v argument must be integer'
                    __error(msg)

                allowed = [1]

                if arg not in allowed:
                    msg = 'You chose a non-existent version of ' + \
                          'psp file.\n' + \
                          '          Allowed versions are:'
                    for allow in allowed:
                        msg += ' {0}'.format(allow)
                    __error(msg)

                news['v'] = arg

            # Theta
            arg = __arg('-t', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-t argument must be float'
                    __error(msg)

                # Bounds
                if arg < 0.0 or arg > 180.0:
                    msg = '-t argument must be in [0-180]'
                    __error(msg)

                news['t'] = arg

            # Azimuth
            arg = __arg('-a', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-a argument must be float'
                    __error(msg)

                # Bounds
                if arg < 0.0 or arg > 360.0:
                    msg = '-a argument must be in [0-360]'
                    __error(msg)

                news['a'] = arg

            # il0
            arg = __arg('-il0', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-il0 argument must be integer'
                    __error(msg)

                news['il0'] = arg

            # il1
            arg = __arg('-il1', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-il1 argument must be integer'
                    __error(msg)

                news['il1'] = arg

            # l0
            arg = __arg('-l0', sys.argv, all_comms)
            if arg is not None:

                if news['il0'] is not None:
                    msg = '-il0 and -l0 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-l0 argument must be float'
                    __error(msg)

                news['l0'] = arg

            # l1
            arg = __arg('-l1', sys.argv, all_comms)
            if arg is not None:

                if news['il1'] is not None:
                    msg = '-il1 and -l1 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-l1 argument must be float'
                    __error(msg)

                news['l1'] = arg

            # ix0
            arg = __arg('-ix0', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-ix0 argument must be integer'
                    __error(msg)

                news['ix0'] = arg

            # ix1
            arg = __arg('-ix1', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-ix1 argument must be integer'
                    __error(msg)

                news['ix1'] = arg

            # x0
            arg = __arg('-x0', sys.argv, all_comms)
            if arg is not None:

                if news['ix0'] is not None:
                    msg = '-ix0 and -x0 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-x0 argument must be float'
                    __error(msg)

                news['x0'] = arg

            # x1
            arg = __arg('-x1', sys.argv, all_comms)
            if arg is not None:

                if news['ix1'] is not None:
                    msg = '-ix1 and -x1 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-x1 argument must be float'
                    __error(msg)

                news['x1'] = arg

            # iy0
            arg = __arg('-iy0', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-iy0 argument must be integer'
                    __error(msg)

                news['iy0'] = arg

            # iy1
            arg = __arg('-iy1', sys.argv, all_comms)
            if arg is not None:

                try:
                    arg = int(arg)
                except ValueError:
                    msg = '-iy1 argument must be integer'
                    __error(msg)

                news['iy1'] = arg

            # y0
            arg = __arg('-y0', sys.argv, all_comms)
            if arg is not None:

                if news['iy0'] is not None:
                    msg = '-iy0 and -y0 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-y0 argument must be float'
                    __error(msg)

                news['y0'] = arg

            # y1
            arg = __arg('-y1', sys.argv, all_comms)
            if arg is not None:

                if news['iy1'] is not None:
                    msg = '-iy1 and -y1 cannot be requested ' + \
                          'together'
                    __error(msg)

                try:
                    arg = float(arg)
                except ValueError:
                    msg = '-y1 argument must be float'
                    __error(msg)

                news['y1'] = arg

    else:
        __print_help()

    ####################
    # Command line end #
    ####################

    #####################
    # Process input psp #
    #####################

    # Open input file
    fi = open(inputfile, 'rb')

    # Magic
    bytes = fi.read(3)
    if edit: eout.append(bytes)

    # Version
    bytes = fi.read(4)
    arg = int(struct.unpack('i', bytes)[0])
    if requ['v']:
        iout.append('PSP version: {0}'.format(arg))
    
    if news['v'] is not None:
        if arg == news['v']:
            msg = 'Requested change into the same version'
            __error(msg)
        else:
            msg = 'Code actually not ready to change versions'
            __error(msg)
        msg = 'Changed version from {0} to {1}'
        msg = msg.format(arg,news['v'])
        cout.append(msg)

    if edit: eout.append(bytes)

    # Comment
    bytes = fi.read(1023)

    if requ['c']:

        # Python 2
        if oldpy:
            comments = struct.unpack('c'*1023, bytes)
            arg = ['']
            for i in range(1023):
                if comments[i] == '\x00':
                    break
                else:
                    arg.append(comments[i])
            arg = ''.join(arg)
        # Python 3
        else:
            arg = ''
            for i in range(1023):
                byte = bytes[i:i+1].decode('utf-8')
                if byte == '\x00':
                    break
                else:
                    arg += byte
        # Check if everything in comment is a space
        if arg == ' '*len(list(arg)):
            arg = ' '
        # Strip spaces
        else:
            arg = arg.strip()

        iout.append('Comments: {0}'.format(arg))

    if news['c'] is not None:
        arg = news['c']
        if len(list(arg)) > 1023:
            msg = 'You comment cannot have more than 1023 characters'
            __error(msg)
        elif len(list(arg)) < 1023:
            arg = arg + ' '*(1023 - len(list(arg)))
        bytes = struct.pack(1023*'c', *arg)
        msg = 'Changed comment to "{0}"'.format(arg)
        cout.append(msg)

    if edit: eout.append(bytes)

    # Theta
    bytes = fi.read(8)

    if requ['t']:

        arg = float(struct.unpack('d', bytes)[0])
        iout.append('Theta angle: {0}'.format(arg))

    if news['t'] is not None:
        bytes = struct.pack('d', news['t'])
        cout.append('Changed theta to {0}'.format(news['t']))

    if edit: eout.append(bytes)

    # Azimuth
    bytes = fi.read(8)

    if requ['a']:

        arg = float(struct.unpack('d', bytes)[0])
        iout.append('Azimuth angle: {0}'.format(arg))

    if news['a'] is not None:
        bytes = struct.pack('d', news['a'])
        cout.append('Changed azimuth to {0}'.format(news['a']))

    if edit: eout.append(bytes)

    #
    # Wavelength
    #

    editl = news['il0'] is not None or \
            news['il1'] is not None or \
            news['l0'] is not None or \
            news['l1'] is not None or \
            news['v2a'] is not None or \
            news['a2v'] is not None

    bytes1 = fi.read(4)
    nwl = int(struct.unpack('i', bytes1)[0])
    bytes2 = fi.read(8*nwl)
    lamb = np.array(struct.unpack('d'*nwl, bytes2))
    nnwl = nwl
    il0 = 0
    il1 = nwl

    # If info
    if requ['nl']:
        msg = 'Wavelength nodes: {0}'.format(nwl)
        print(msg)
    if requ['l']:
        msg = 'Wavelength axis:'
        for l in lamb:
            msg += ' {0}'.format(l)
        print(msg)

    # If editting
    if editl:

        if news['l0'] is not None and news['il1'] is not None:
            msg = 'You cannot select index in one extreme and ' + \
                  'value at the other when cutting wavelength'
            __error(msg)
        if news['l1'] is not None and news['il0'] is not None:
            msg = 'You cannot select index in one extreme and ' + \
                  'value at the other when cutting wavelength'
            __error(msg)

        if lamb[0] > lamb[1]:
            reversel = True
        else:
            reversel = False

        if reversel and news['l0'] is not None or \
                        news['l1'] is not None:
            lamb = lamb[::-1]

        if news['l0'] is not None:
            il0 = np.argmin(np.absolute(lamb - news['l0']))
        if news['l1'] is not None:
            il1 = np.argmin(np.absolute(lamb - news['l1'])) + 1
        if news['il0'] is not None:
            il0 = news['il0']
            while il0 < 0:
                il0 += nwl
        if news['il1'] is not None:
            il1 = news['il1']
            while il1 < 0:
                il1 += nwl
            il1 += 1
        if il0 > nwl:
            msg = 'Lower index to cut wavelengths is larger than the ' + \
                  'last index: {0} > {1}'.format(il0, nwl-1)
            __error(msg)
        if il1 > nwl:
            msg = 'Upper index to cut wavelengths is larger than the ' + \
                  'last index: {0} > {1}'.format(il1-1, nwl-1)
            __error(msg)
        if il0 >= il1:
            msg = 'Upper index to cut wavelengths is smaller than ' + \
                  'the lower index: {0} < {1}'.format(il1-1, il0)
            __error(msg)

        # Get new lambda
        nnwl = il1 - il0
        if reversel and news['l0'] is not None or \
                        news['l1'] is not None:
            nlamb = lamb[il0:il1]
            iil1 = il1 - 1
            il1 = nwl - 1 - il0 + 1
            il0 = nwl - 1 - iil1


        # Changes log
        if nnwl != nwl:
            msg = 'Changed wavelength axis, new size is {0} '
            msg = msg.format(nnwl)
            msg += 'from {0} to {1} A'.format(nlamb[0],nlamb[-1])
            cout.append(msg)

        # Convert to vacuum
        if news['a2v']:
            nlamb = __airtovacuum(nlamb)
            msg = 'Converted from air to vacuum'
            cout.append(msg)
        # Convert to air
        if news['v2a']:
            nlamb = __vacuumtoair(nlamb)
            msg = 'Converted from vacuum to air'
            cout.append(msg)

        bytes1 = struct.pack('i', nnwl)
        bytes2 = struct.pack('d'*nnwl, *nlamb)

    # Editing
    if edit:
        eout.append(bytes1)
        eout.append(bytes2)

    # Surface
    bytes = fi.read(4)
    if edit: eout.append(bytes)

    #
    # Spatial axes
    #

    # Domain
    bytesdx = fi.read(8)
    dx = float(struct.unpack('d', bytesdx)[0])
    bytesdy = fi.read(8)
    dy = float(struct.unpack('d', bytesdy)[0])

    # Periodic
    bytesP = fi.read(2)
    if requ['P']:
        if oldpy:
            pp = struct.unpack('cc', bytes)
        else:
            pp = ''
            for i in range(2):
                byte = bytes[i:i+1].decode('utf-8')
                pp += byte
        iout.append('Periodic condition: {0}, {1}'.format(*list(pp)))

    # Sizes
    bytesnx = fi.read(4)
    nx = int(struct.unpack('i', bytesnx)[0])
    bytesny = fi.read(4)
    ny = int(struct.unpack('i', bytesny)[0])
    newnx = nx
    newny = ny

    # Arrays
    bytesx = fi.read(nx*8)
    x = np.array(struct.unpack('d'*nx, bytesx))
    bytesy = fi.read(ny*8)
    y = np.array(struct.unpack('d'*ny, bytesy))

    edits = news['ix0'] is not None or \
            news['ix1'] is not None or \
            news['x0'] is not None or \
            news['x1'] is not None or \
            news['iy0'] is not None or \
            news['iy1'] is not None or \
            news['y0'] is not None or \
            news['y1'] is not None

    # If info
    if requ['nx']:
        iout.append('Number of X nodes: {0}'.format(nx))
    if requ['dx']:
        iout.append('X domain: {0}'.format(dx))
    if requ['x']:
        msg = 'X axis:'
        for ele in x:
            msg += ' {0}'.format(ele)
        iout.append(msg)
    if requ['ny']:
        iout.append('Number of Y nodes: {0}'.format(ny))
    if requ['dy']:
        iout.append('Y domain: {0}'.format(dy))
    if requ['y']:
        msg = 'Y axis:'
        for ele in y:
            msg += ' {0}'.format(ele)
        iout.append(msg)

    # Initialize
    ix0 = 0
    ix1 = nx
    iy0 = 0
    iy1 = ny

    # If editing
    if edits:

        editx = news['ix0'] is not None or \
                news['ix1'] is not None or \
                news['x0'] is not None or \
                news['x1'] is not None
        edity = news['iy0'] is not None or \
                news['iy1'] is not None or \
                news['y0'] is not None or \
                news['y1'] is not None

        if editx:

            if news['x0'] is not None:
                ix0 = np.argmin(np.absolute(x - news['x0']))
            if news['x1'] is not None:
                ix1 = np.argmin(np.absolute(x - news['x1'])) + 1
            if news['ix0'] is not None:
                ix0 = news['ix0']
                while ix0 < 0:
                    ix0 += nx
            if news['ix1'] is not None:
                ix1 = news['ix1']
                while ix1 < 0:
                    ix1 += nx
                ix1 += 1
            if ix0 > nx:
                msg = 'Lower index to cut X axis is larger than the ' + \
                      'last index: {0} > {1}'.format(ix0, nx-1)
                __error(msg)
            if ix1 > nx:
                msg = 'Upper index to cut X axis is larger than the ' + \
                      'last index: {0} > {1}'.format(ix1-1, nx-1)
                __error(msg)
            if ix0 >= ix1:
                msg = 'Upper index to cut X axis is smaller than ' + \
                      'the lower index: {0} < {1}'.format(ix1-1, ix0)
                __error(msg)

            newx = x[ix0:ix1]
            newnx = ix1 - ix0
            newdx = newx[-1] + newx[1] - 2.0*newx[0]
            bytesdx = struct.pack('d', newdx)
            bytesnx = struct.pack('i', newnx)
            bytesx = struct.pack('d'*newnx, *newx)
            msg = 'Changed X axis. New domain is {0} with {1} nodes'
            msg = msg.format(newdx,newnx)
            cout.append(msg)

        if edity:

            if news['y0'] is not None:
                iy0 = np.argmin(np.absolute(y - news['y0']))
            if news['y1'] is not None:
                iy1 = np.argmin(np.absolute(y - news['y1'])) + 1
            if news['iy0'] is not None:
                iy0 = news['iy0']
                while iy0 < 0:
                    iy0 += ny
            if news['iy1'] is not None:
                iy1 = news['iy1']
                while iy1 < 0:
                    iy1 += ny
                iy1 += 1
            if iy0 > ny:
                msg = 'Lower index to cut Y axis is larger than the ' + \
                      'last index: {0} > {1}'.format(iy0, ny-1)
                __error(msg)
            if iy1 > ny:
                msg = 'Upper index to cut Y axis is larger than the ' + \
                      'last index: {0} > {1}'.format(iy1-1, ny-1)
                __error(msg)
            if iy0 >= iy1:
                msg = 'Upper index to cut Y axis is smaller than ' + \
                      'the lower index: {0} < {1}'.format(iy1-1, iy0)
                __error(msg)

            newy = y[iy0:iy1]
            newny = iy1 - iy0
            newdy = newy[-1] + newy[1] - 2.0*newy[0]
            bytesdy = struct.pack('d', newdy)
            bytesny = struct.pack('i', newny)
            bytesy = struct.pack('d'*newny, *newy)
            msg = 'Changed Y axis. New domain is {0} with {1} nodes'
            msg = msg.format(newdy,newny)
            cout.append(msg)

    if edit:
        eout.append(bytesdx)
        eout.append(bytesdy)
        eout.append(bytesP)
        eout.append(bytesnx)
        eout.append(bytesny)
        eout.append(bytesx)
        eout.append(bytesy)
        bytesdx = 0
        bytesdy = 0
        bytesP = 0
        bytesnx = 0
        bytesny = 0
        bytesx = 0
        bytesy = 0

    #
    # Dump info
    #
    if info:
        print('Requested info:')
        for line in iout:
            print(' - '+line)

    if edit:

        # Stokes
        insiz = 4*nwl*nx*ny
        bytes = fi.read(insiz*4)
        fi.close()

        # If cutting
        if editl or edits:

            ousiz = 4*nnwl*newnx*newny

            print il0,il1,il1-il0,nnwl
            print iy0,iy1,iy1-iy0,newny
            print ix0,ix1,ix1-ix0,newnx
            print (np.array( \
                    struct.unpack('f'*insiz,bytes) \
                              ).reshape((nwl,ny,nx,4))[il0:il1, \
                                                       iy0:iy1, \
                                                       ix0:ix1, \
                                                       :]).size

            bytes = struct.pack('f'*ousiz, \
                    *( \
                      np.array( \
                               struct.unpack('f'*insiz,bytes) \
                              ).reshape((nwl,ny,nx,4))[il0:il1, \
                                                       iy0:iy1, \
                                                       ix0:ix1, \
                                                       :] \
                     ).flatten())
        eout.append(bytes)
        bytes = 0

        #
        # Create new psp
        #
        if outfile is None:
            outfile = 'new.psp'
        f = open(outfile, 'wb')
        for line in eout:
            f.write(line)
        f.close()


        #
        # Dump edit info
        #
        if edit:
            if info:
                print('')
            print('Changes to data in {0}:'.format(outfile))
            for line in cout:
                print(' - '+line)
    

######################################################################
######################################################################
######################################################################

if __name__ == '__main__':
    main()
