# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# multilevel.py                                                      #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Manager for the multilevel module                                  #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  To do:                                                            #
#    Much of the code in get_data and get_data_h5 are the same.      #
#    We should collapse both versions.                               #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.4 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  24/01/2020 - V1.0.3 - Forgot to configure behavior of the saving  #
#                        vtk button (TdPA)                           #
#                                                                    #
#  23/01/2020 - V1.0.2 - Added option to store vtk files (TdPA)      #
#                                                                    #
#  03/12/2019 - V1.0.1 - Changed the way to do interpolation with    #
#                        hdf5. Now mimics the way for PMD (binary)   #
#                        so no issues with zoom (as with previous    #
#                        version (AdV)                               #
#                                                                    #
#  25/06/2018 - V1.0.0 - First version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.general
from source.loader import *
from source.tooltip import *
import h5py
from scipy.ndimage import zoom

######################################################################
######################################################################

class MODULE_class(Toplevel):
    ''' Class that defines the main body of the module class
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.configure(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.up = Frame(body)
        self.mid = Frame(body)
        self.do = Frame(body)
        self.bot = Frame(body)
        self.head = head
        self.box = body
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.buttons(self.do)

        self.display(self.mid)

        self.optionbox(self.up)

        self.progressbar(self.bot)

        self.up.pack(fill=BOTH,expand=1,side=TOP)
        self.mid.pack(fill=BOTH,expand=1,side=TOP)
        self.do.pack(fill=BOTH,expand=1,side=BOTTOM)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)


######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        global PMD
        PMD = source.general.PMD

        if PMD['head']['loaded']:
            tkMessageBox.showwarning("Load", \
                                     "You must unload the data " + \
                                     "before closing!",parent=self)
            return

        # Remove temporal quantities
        try:
            del PMD['siz']
        except KeyError:
            pass
        try:
            del PMD['selection']
        except KeyError:
            pass

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()


######################################################################
######################################################################
######################################################################
######################################################################

    def optionbox(self, box):
        ''' Defines all the control widgets of PMD_class class.
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # List the variables for this module
        PMD['vars'] = {0: 'Temperature', \
                       1: 'Density', \
                       2: 'B', \
                       3: 'V', \
                       4: 'Microturbulence', \
                       5: 'Density matrix', \
                       6: 'Radiation', \
                       7: 'Damping', \
                       8: 'Collisional rates', \
                       9: 'Depolarizing rates.', \
                       10: u'\u03B7 cont.', \
                       11: u'\u03B5 cont.'}
        PMD['file'] = {0: 'Temperature', \
                       1: 'Density', \
                       2: 'B', \
                       3: 'V', \
                       4: 'Microturbulence', \
                       5: 'Density matrix', \
                       6: 'Radiation', \
                       7: 'Damping', \
                       8: 'Collisions', \
                       9: 'Depolarizing coefficient', \
                       10: 'Absorptibity continuum', \
                       11: 'Emissivity continuum'}

        # Names of datasets in HDF5 files
        PMD['hdf5_dic'] = {0: 'T', \
                   1: 'N', \
                   2: 'B', \
                   3: 'v', \
                   4: 'vmi', \
                   5: 'rho', \
                   6: 'J', \
                   7: 'a_voigt', \
                   8: 'Cul', \
                   9: 'DK', \
                   10: 'eta_c', \
                   11: 'eps_c'}
        
        if 'read' not in PMD.keys():
            PMD['read'] = [False]*len(PMD['vars'].keys())
        PMD['sele'] = [0,0,0,0,0,3,3,1,1,1,1,1]
        PMD['scal'] = [True,True,False,False,True, \
                       False,False,False,False,False, \
                       False,False]
        PMD['type'] = ['n','n','n','n','n', \
                       'lkq','rkq', \
                       'r','t','l','r','r']

        # Get dimensions
        if 'siz' not in PMD.keys():

            # Try to read the data
            if PMD['head']['io'] == "hdf5":
                check = self.get_size_h5()
            else:
                check = self.get_size()

            if not check:
                tkMessageBox.showwarning("Load", \
                                         "Could not read " + \
                                         "from pmd file.",parent=self)
                self.close()

        # Initialize selection
        if 'selection' not in PMD.keys():

            PMD['selection'] = {}

            # For each variable
            for ii in PMD['vars'].keys():

                # If no selection, skip
                if PMD['sele'][ii] < 1:
                    continue

                # If more than one selection
                if PMD['sele'][ii] > 1:

                    # Density matrix
                    if PMD['type'][ii] == 'lkq':

                        PMD['selection'][ii] = []
                        for jj in range(PMD['siz']['l']):
                            Kmax = np.amin([PMD['Kcut'], \
                                            PMD['J2'][jj]])
                            for K in range(Kmax+1):
                                for Q in range(K+1):
                                    PMD['selection'][ii].append(True)
                                    if Q > 0:
                                        PMD['selection'][ii]. \
                                                         append(True)

                    # JKQ
                    if PMD['type'][ii] == 'rkq':

                        PMD['selection'][ii] = []
                        for jj in range(PMD['siz']['r']*9):
                            PMD['selection'][ii].append(True)

                # Just one quantity to selec
                else:

                    PMD['selection'][ii] = []
                    for jj in range(PMD['siz'][PMD['type'][ii]]):
                        PMD['selection'][ii].append(True)

        #
        # Properly starts
        #

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Load Manager", font=self.cFont, \
                    justify=CENTER)
        createToolTip(b0l,TOOLTIPS['1'])
        b0l.grid(row=row,column=col,columnspan=3,sticky=NSEW)
        # Loaded file
        # Status
        box00 = LabelFrame(box0)
        row += 1
        col = 0
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        Grid.rowconfigure(box00,0,weight=1)
        Grid.columnconfigure(box00,0,weight=1)
        if PMD['head']['loaded']:
            self.data0_name = Label(box00, text='Loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
        else:
            self.data0_name = Label(box00, text='Not loaded', \
                                    anchor=CENTER, \
                                    font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
        self.data0_name.grid(row=0,column=0,sticky=NSEW)
        box00.grid(row=row,column=col,sticky=NSEW)
        # Load button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_load = Button(box0,text="Load",font=self.cFont, \
                                 command=self.load0)
        createToolTip(self.butt0_load,TOOLTIPS['7'])
        self.butt0_load.grid(row=row,column=col,sticky=NSEW)
        # Unload button
        col += 1
        Grid.columnconfigure(box0,col,weight=1)
        self.butt0_unload = Button(box0,text="Unload", \
                                   font=self.cFont, \
                                   command=self.unload0)
        createToolTip(self.butt0_unload,TOOLTIPS['8'])
        self.butt0_unload.grid(row=row,column=col,sticky=NSEW)
        # Disable widgets if no data loaded
        if PMD['head']['loaded']:
            self.butt0_load.config(state='disabled')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
        else:
            self.butt0_load.config(state='normal')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)


        # Interpolation (box1)
        box1 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box1,row,weight=1)
        Grid.columnconfigure(box1,col,weight=1)
        b1l = Label(box1, text="Interpolation parameters", \
                    font=self.cFont, \
                    justify=CENTER)
        b1l.grid(row=row,column=col,columnspan=8,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = 0
        Grid.columnconfigure(box1,col,weight=1)
        self.int1_stat = IntVar()
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)
        self.int1_check = Checkbutton(box1, \
                                      text="Interpolate data " + \
                                           "while reading     ", \
                                      anchor=CENTER, \
                                      font=self.cFont, \
                                      variable=self.int1_stat, \
                                      command=self.check1)
        createToolTip(self.int1_check,text=TOOLTIPS['27'])
        self.int1_check.grid(row=row,column=col,sticky=NSEW)
        #
        # Entries
        #
        # X
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l1 = Label(box1, text='    NX:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l1.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['x']=PMD['head']['nodes0'][0]
        self.nx1_name = StringVar()
        self.nx1_name.set(CONF['nodes']['x'])
        self.nx1_name.trace('w',self.nx1_val)
        self.nx1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nx1_name)
        self.nx1_entry.grid(row=row,column=col,sticky=NSEW)
        # Y
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l2 = Label(box1, text='    NY:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l2.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['y']=PMD['head']['nodes0'][1]
        self.ny1_name = StringVar()
        self.ny1_name.set(CONF['nodes']['y'])
        self.ny1_name.trace('w',self.ny1_val)
        self.ny1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.ny1_name)
        self.ny1_entry.grid(row=row,column=col,sticky=NSEW)
        # Z
        # Label
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.b1l3 = Label(box1, text='    NZ:', \
                          justify=RIGHT,font=self.cFont, anchor=E)
        self.b1l3.grid(row=row,column=col,sticky=NSEW)
        # Entry
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        CONF['nodes']['z']=PMD['head']['nodes0'][2]
        self.nz1_name = StringVar()
        self.nz1_name.set(CONF['nodes']['z'])
        self.nz1_name.trace('w',self.nz1_val)
        self.nz1_entry = Entry(box1, font=self.cFont, \
                               textvariable=self.nz1_name)
        self.nz1_entry.grid(row=row,column=col,sticky=NSEW)
        # Interpolate button
        col += 1
        Grid.columnconfigure(box1,col,weight=1)
        self.butt1_int = Button(box1, text="Interpolate", \
                                font=self.cFont, command=self.int1)
        createToolTip(self.butt1_int,TOOLTIPS['40'])
        self.butt1_int.grid(row=row,column=col,sticky=NSEW)
        if PMD['head']['loaded']:
            self.butt1_int.config(state='normal')
            self.int1_check.config(state='disabled')
        else:
            self.butt1_int.config(state='disabled')
            self.int1_check.config(state='normal')
        # Pack the box
        box1.pack(fill=BOTH, expand=1, side = TOP)


        # Variables to read (box1)
        box2 = LabelFrame(box)
        row = 0
        col = 0
        jump = 8
        # General label
        Grid.rowconfigure(box2,row,weight=1)
        Grid.columnconfigure(box2,col,weight=1)
        b2l = Label(box2, text="Variables to read", \
                    font=self.cFont, \
                    justify=CENTER)
        b2l.grid(row=row,column=col,columnspan=jump,sticky=NSEW)
        # Check for interpolation
        row += 1
        col = -1
        self.var2_stat = []
        self.var2_check = []
        self.var2_button = []
        for ii in range(len(PMD['read'])):
            # Select button
            col += 1
            if col >= jump:
                row += 1
                col = 0
            if PMD['sele'][ii] < 1:
                text = '      '
            else:
                text = 'Select'
            self.var2_button.append(Button(box2, \
                                    text=text, \
                                    font=self.cFont, \
                                    command=lambda jj=ii: \
                                            self.select2(jj)))
            self.var2_button[ii].config(state='disabled')
            self.var2_button[ii].grid(row=row,column=col,sticky=NSEW)
            # Check button
            col += 1
            if col >= jump:
                row += 1
                col = 0
            self.var2_stat.append(IntVar())
            Grid.rowconfigure(box2,col,weight=1)
            Grid.columnconfigure(box2,col,weight=1)
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)
            self.var2_check.append(Checkbutton(box2, \
                                   text=PMD['vars'][ii], \
                                   anchor=W, \
                                   font=self.cFont, \
                                   variable=self.var2_stat[ii], \
                                   command=lambda jj=ii: \
                                           self.check2(jj)))
            createToolTip(self.var2_check[ii],text=TOOLTIPS['27'])
            self.var2_check[ii].grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box2.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################

    def display(self, box):
        ''' Initializes the display of the header
        '''

        global PMD
        PMD = source.general.PMD
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Column width (maybe this should not be hardcoded)
        col = 400

        # Create information label frame
        box0 = LabelFrame(box)
        Grid.rowconfigure(box0,0,weight=1)
        Grid.columnconfigure(box0,0,weight=1)
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                                    justify=CENTER, relief=FLAT, \
                                    bd=15, padx=3, pady=3, \
                                    font=self.cFont)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Atomic mass: {0}\n'.format(data['amass'])
            # Level energy
            ltext = ' Level energy: '
            for ilevel in range(data['nlevel']):
                if ilevel > 0:
                    ltext += ','
                ltext += ' {0:.3E}'.format(data['E'][ilevel])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Angular momentum
            ltext = ' Angular momentum: '
            for ilevel in range(data['nlevel']):
                J2 = data['J2'][ilevel]
                if ilevel > 0:
                    ltext += ','
                if data['J2'][ilevel] % 2 > 0:
                    ltext += ' {0:d}/2'.format(J2)
                else:
                    ltext += ' {0:d}'.format(J2/2)
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Lande factor
            ltext = ' Lande factor: '
            for ilevel in range(data['nlevel']):
                if ilevel > 0:
                    ltext += ','
                ltext += ' {0:4.2f}'.format(data['g'][ilevel])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Einstein coefficients
            ltext = ' Einstein coefficient: '
            for itran in range(data['rtran']):
                if itran > 0:
                    ltext += ','
                ltext += ' {0:.3E}'.format(data['Aul'][itran])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Kcut
            if data['Kcut'] > 0:
                text += 'K <= {0}'.format(data['Kcut'])
            text += '\n'
            self.data_label = Label(box0, text=text, anchor=CENTER, \
                              justify=LEFT, relief=FLAT, \
                              bd=15, padx=3, pady=3, \
                              font=self.cFont)
        self.data_label.grid(row=0,column=0,sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Plot button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_plot = Button(box0, text="Plot",  \
                                 font=self.cFont, \
                                 command=self.plot)
        createToolTip(self.butt1_plot,TOOLTIPS['42'])
        self.butt1_plot.grid(row=row,column=col,sticky=NSEW)
        # Save vtk
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_save = Button(box0, text="Save vtk",  \
                                 font=self.cFont, \
                                 command=self.save)
        createToolTip(self.butt1_save,TOOLTIPS['49'])
        self.butt1_save.grid(row=row,column=col,sticky=NSEW)
        # Close button
        row += 1
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        createToolTip(self.butt1_close,TOOLTIPS['11'])
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def progressbar(self, box):
        ''' Initializes the progress bar
        '''

        self.pgf = Frame(box)
        Grid.rowconfigure(self.pgf,0,weight=1)
        Grid.columnconfigure(self.pgf,0,weight=1)
        self.pg = Progressbar(self.pgf)
        self.pg.grid(row=0,column=0,sticky=NSEW)
        self.pgf.pack(fill=BOTH, expand=1, side = TOP)


######################################################################
######################################################################
######################################################################
######################################################################

    def load0(self):
        ''' Method that handles the Load button. Load the pmd file
            content from currently selected pmd file
        '''

        global PMD
        PMD = source.general.PMD

        # Update entries
        self.update_entry()

        # Check that you selected something
        if sum(PMD['read']) < 1:
            tkMessageBox.showwarning("Load", \
                                     "You must select at least " + \
                                     "one variable to read.", \
                                     parent=self)
            return

        # Signal in the GUI that we are loading
        self.data0_name.configure(text='Loading',font=self.cFont)
        self.data0_name.update_idletasks()

        # Try to read the data
        if PMD['head']['io'] == "hdf5":
            check = self.get_data_h5()
        else:
            check = self.get_data()
            
        if check:
            PMD['head']['loaded'] = True
        else:
            tkMessageBox.showwarning("Load", \
                                     "Could not read pmd file.", \
                                     parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def unload0(self):
        ''' Method that handles the Unload button. Unload the pmd
            data already loaded.
        '''

        global PMD
        PMD = source.general.PMD

        PMD['data'] = None
        PMD['head']['loaded'] = False
        try:
            del PMD['ndim']
        except KeyError:
            pass
        self.set_top()
        self.set_mid()

        return

######################################################################
######################################################################

    def check1(self):
        ''' Check to interpolate while reading
        '''

        global CONF
        CONF = source.general.CONF

        CONF['interpol'] = self.int1_stat.get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def int1(self):
        ''' Button to interpolate read data
        '''

        # Try to interpolate the data
        check = self.interpol_data()
        if not check:
            tkMessageBox.showwarning("Load", \
                                     "Could not interpolate " + \
                                     "pmd data.",parent=self)
            return

        self.set_top()
        self.set_mid()
        self.pg.step(-100)
        self.pg.update_idletasks()

        return

######################################################################
######################################################################

    def check2(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['read'][val] = self.var2_stat[val].get() == 1
        self.set_top()

        return

######################################################################
######################################################################

    def select2(self, val):
        ''' Check to limit what variables to read
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF

        if PMD['sele'][val] > 0:

            # Select density matrix
            if PMD['type'][val] == 'lkq':
                # Passive border
                self.configure(background=CONF['col_pas'])
                # Calls the selection window
                sel = RHOSELECT_class(self.master, \
                                title='Density matrix selection')
                # Active border
                self.configure(background=CONF['col_act'])

            # Select radiation field tensor
            elif PMD['type'][val] == 'rkq':
                # Passive border
                self.configure(background=CONF['col_pas'])
                # Calls the selection window
                sel = JSELECT_class(self.master, \
                                title='Radiation field selection')
                # Active border
                self.configure(background=CONF['col_act'])

            # Select anything else
            else:
                # Passive border
                self.configure(background=CONF['col_pas'])
                # Calls the selection window
                sel = SELECT_class(self.master, \
                                title='Variable selection', \
                                ind = val)
                # Active border
                self.configure(background=CONF['col_act'])

        self.set_top()

        return

######################################################################
######################################################################

    def plot(self):
        ''' Plot button
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF

        # Identify scalars and vectors
        scal = [0,1,4]
        vec = [2,3]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Open script file
        try:
            f = open('cache/dump.py','w')
            folder = 'cache/'
        except IOError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not write in " + \
                                     "cache folder. I am going " + \
                                     "to write in current " + \
                                     "directory, but this is not " + \
                                     "normal behavior.",parent=self)
            f = open('dump.py','w')
            folder = ''
        except:
            raise

        # Script header
        f.write('# -*- coding: utf-8 -*-\n\n')
        f.write('from numpy import array\n')
        f.write('try:\n')
        f.write('    engine = mayavi.engine\n')
        f.write('except NameError:\n')
        f.write('    from mayavi.api import Engine\n')
        f.write('    engine = Engine()\n')
        f.write('    engine.start()\n')
        f.write('if len(engine.scenes) == 0:\n')
        f.write('    engine.new_scene()\n')
        f.write('scene = engine.scenes[0]\n')

        # Initialize file names list
        files = []

        # Create KQ names
        if PMD['read'][5]:
            KQv = []
            Kcut = data['Kcut']
            if Kcut < 0:
                Kcut = 2000
            Kmax = np.amin([np.amax(data['J2']),Kcut])
            for K in range(Kmax+1):
                for Q in range(K+1):
                    for ii,R in zip(range(2),['  Real','  Imag']):
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])
        
        # For each variable
        for var in PMD['vars'].keys():

            # Skip if not reading
            if not PMD['read'][var]:
                continue

            d = copy.deepcopy(data[var])
            fn = PMD['file'][var]

            # Check type

            # Scalar or vector
            if PMD['type'][var] == 'n':

                # Scalar
                if PMD['rsiz'][var] == 1:
                    d = d[:,:,:,0]
                    pdata = vtk.PointData(vtk.Scalars(d.flatten(), \
                                                      name=fn))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+fn+'.vtk')
                    vtkdata.tofile(files[-1],'binary')
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

                # Vector
                else:
                    pdata = vtk.PointData(vtk.Vectors(d.flatten(), \
                                                      name=fn))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+fn+'.vtk')
                    vtkdata.tofile(files[-1],'binary')
                    f.write("vtk_file_reader = engine.open('" + \
                            files[-1] + "', scene)\n")

            # Other
            else:

                # Standard
                if PMD['type'][var] in list('lrt'):

                    if PMD['type'][var] == 'l':
                        label = 'level {0}'
                    else:
                        label = 'trans {0}'

                    jj = -1
                    for ii in range(PMD['siz'][PMD['type'][var]]):
                        if not PMD['selection'][var][ii]:
                            continue
                        jj += 1
                        dd = d[:,:,:,jj]
                        ff = fn + label.format(ii)
                        pdata = vtk.PointData( \
                                    vtk.Scalars(dd.flatten(), \
                                                name=ff))
                        vtkdata = vtk.VtkData(grid, pdata)
                        files.append(folder+ff+'.vtk')
                        vtkdata.tofile(files[-1],'binary')
                        f.write("vtk_file_reader = engine.open('" + \
                                files[-1] + "', scene)\n")

                # Density matrix
                if PMD['type'][var] == 'lkq':

                    label = 'level {0}, K {1}, Q {2}{3}'

                    jj = -1
                    kk = -1
                    Kcut = PMD['data']['Kcut']
                    if Kcut < 0:
                        Kcut = 2000
                    for ii in range(PMD['siz']['l']):
                        Kmax = np.amin([Kcut, PMD['data']['J2'][ii]])
                        for K in range(Kmax+1):
                            for Q in range(K+1):
                                if Q > 0:
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Real')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                        f.write("vtk_file_reader " + \
                                                "= engine.open('" + \
                                                files[-1] + \
                                                "', scene)\n")
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Imag')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                        f.write("vtk_file_reader " + \
                                                "= engine.open('" + \
                                                files[-1] + \
                                                "', scene)\n")
                                else:
                                    jj += 1
                                    if not PMD['selection'][var][jj]:
                                        continue
                                    kk += 1
                                    dd = d[:,:,:,kk]
                                    ff = fn + label.format(ii,K,Q,'')
                                    pdata = vtk.PointData( \
                                                vtk.Scalars( \
                                                    dd.flatten(), \
                                                    name=ff))
                                    vtkdata = vtk.VtkData(grid, pdata)
                                    files.append(folder+ff+'.vtk')
                                    vtkdata.tofile(files[-1],'binary')
                                    f.write("vtk_file_reader = " + \
                                            "engine.open('" + \
                                            files[-1] + "', scene)\n")

                # Radiation field
                if PMD['type'][var] == 'rkq':

                    label = 'trans {0}, K {1}, Q {2}{3}'

                    jj = -1
                    kk = -1
                    for ii in range(PMD['siz']['r']):
                        for K in range(3):
                            for Q in range(K+1):
                                if Q > 0:
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Real')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                        f.write("vtk_file_reader " + \
                                                "= engine.open('" + \
                                                files[-1] + \
                                                "', scene)\n")
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Imag')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                        f.write("vtk_file_reader " + \
                                                "= engine.open('" + \
                                                files[-1] + \
                                                "', scene)\n")
                                else:
                                    jj += 1
                                    if not PMD['selection'][var][jj]:
                                        continue
                                    kk += 1
                                    dd = d[:,:,:,kk]
                                    ff = fn + label.format(ii,K,Q,'')
                                    pdata = vtk.PointData( \
                                                vtk.Scalars( \
                                                    dd.flatten(), \
                                                    name=ff))
                                    vtkdata = vtk.VtkData(grid, pdata)
                                    files.append(folder+ff+'.vtk')
                                    vtkdata.tofile(files[-1],'binary')
                                    f.write("vtk_file_reader = " + \
                                            "engine.open('" + \
                                            files[-1] + "', scene)\n")

        # Close script
        f.close()

        # Make current passive
        self.config(background=CONF['col_pas'])

        # Call mayavi2
        try:
            subprocess.call(["mayavi2","cache/dump.py"])
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not open Mayavi2. " + \
                                     "Ensure that Mayavi2 is " + \
                                     "correctly installed and " + \
                                     "that the command mayavi2 " + \
                                     "opens it.",parent=self)
            return
        except:
            raise

        # Make current active
        self.configure(background=CONF['col_act'])


        # Remove script
        try:
            os.remove(folder+'dump.py')
        except OSError:
            tkMessageBox.showwarning("Plot", \
                                     "I could not find my python " + \
                                     "script to delete it. If " + \
                                     "you moved it, I will not " + \
                                     "touch it.",parent=self)
        except:
            raise

        # Delete vtk files
        for fil in files:
            try:
                os.remove(fil)
            except OSError:
                pass
            except:
                raise

        return

######################################################################
######################################################################

    def save(self):
        ''' Save button
        '''

        global PMD
        PMD = source.general.PMD

        # Identify scalars and vectors
        scal = [0,1,4]
        vec = [2,3]

        # Data pointer
        data = PMD['data']

        # Initialize list of load files
        d = []

        # Ask for folder
        folder = tkFileDialog.askdirectory(initialdir= \
                                            PATH['pmd_dir'], \
                                            parent=self)

        # If no folder, return
        if not isinstance(folder,str):
            return
        if not os.path.isdir(folder):
            return
        if list(folder)[-1] != '/':
            folder += '/'

        # Initialize file names list
        files = []

        # Create KQ names
        if PMD['read'][5]:
            KQv = []
            Kcut = data['Kcut']
            if Kcut < 0:
                Kcut = 2000
            Kmax = np.amin([np.amax(data['J2']),Kcut])
            for K in range(Kmax+1):
                for Q in range(K+1):
                    for ii,R in zip(range(2),['  Real','  Imag']):
                        KQv.append(' K='+str(K)+' Q='+str(Q)+R)

        # Create data files

        # Create grid once
        grid = vtk.RectilinearGrid(data['x'],data['y'],data['z'])
        
        # For each variable
        for var in PMD['vars'].keys():

            # Skip if not reading
            if not PMD['read'][var]:
                continue

            d = copy.deepcopy(data[var])
            fn = PMD['file'][var]

            # Check type

            # Scalar or vector
            if PMD['type'][var] == 'n':

                # Scalar
                if PMD['rsiz'][var] == 1:
                    d = d[:,:,:,0]
                    pdata = vtk.PointData(vtk.Scalars(d.flatten(), \
                                                      name=fn))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+fn+'.vtk')
                    vtkdata.tofile(files[-1],'binary')

                # Vector
                else:
                    pdata = vtk.PointData(vtk.Vectors(d.flatten(), \
                                                      name=fn))
                    vtkdata = vtk.VtkData(grid, pdata)
                    files.append(folder+fn+'.vtk')
                    vtkdata.tofile(files[-1],'binary')

            # Other
            else:

                # Standard
                if PMD['type'][var] in list('lrt'):

                    if PMD['type'][var] == 'l':
                        label = 'level {0}'
                    else:
                        label = 'trans {0}'

                    jj = -1
                    for ii in range(PMD['siz'][PMD['type'][var]]):
                        if not PMD['selection'][var][ii]:
                            continue
                        jj += 1
                        dd = d[:,:,:,jj]
                        ff = fn + label.format(ii)
                        pdata = vtk.PointData( \
                                    vtk.Scalars(dd.flatten(), \
                                                name=ff))
                        vtkdata = vtk.VtkData(grid, pdata)
                        files.append(folder+ff+'.vtk')
                        vtkdata.tofile(files[-1],'binary')

                # Density matrix
                if PMD['type'][var] == 'lkq':

                    label = 'level {0}, K {1}, Q {2}{3}'

                    jj = -1
                    kk = -1
                    Kcut = PMD['data']['Kcut']
                    if Kcut < 0:
                        Kcut = 2000
                    for ii in range(PMD['siz']['l']):
                        Kmax = np.amin([Kcut, PMD['data']['J2'][ii]])
                        for K in range(Kmax+1):
                            for Q in range(K+1):
                                if Q > 0:
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Real')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Imag')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                else:
                                    jj += 1
                                    if not PMD['selection'][var][jj]:
                                        continue
                                    kk += 1
                                    dd = d[:,:,:,kk]
                                    ff = fn + label.format(ii,K,Q,'')
                                    pdata = vtk.PointData( \
                                                vtk.Scalars( \
                                                    dd.flatten(), \
                                                    name=ff))
                                    vtkdata = vtk.VtkData(grid, pdata)
                                    files.append(folder+ff+'.vtk')
                                    vtkdata.tofile(files[-1],'binary')

                # Radiation field
                if PMD['type'][var] == 'rkq':

                    label = 'trans {0}, K {1}, Q {2}{3}'

                    jj = -1
                    kk = -1
                    for ii in range(PMD['siz']['r']):
                        for K in range(3):
                            for Q in range(K+1):
                                if Q > 0:
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Real')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                    jj += 1
                                    if PMD['selection'][var][jj]:
                                        kk += 1
                                        dd = d[:,:,:,kk]
                                        ff = fn + label.format(ii, \
                                                              K,Q, \
                                                              ' Imag')
                                        pdata = vtk.PointData( \
                                                   vtk.Scalars( \
                                                      dd.flatten(), \
                                                      name=ff))
                                        vtkdata = vtk.VtkData(grid, \
                                                              pdata)
                                        files.append(folder+ff+'.vtk')
                                        vtkdata.tofile(files[-1], \
                                                       'binary')
                                else:
                                    jj += 1
                                    if not PMD['selection'][var][jj]:
                                        continue
                                    kk += 1
                                    dd = d[:,:,:,kk]
                                    ff = fn + label.format(ii,K,Q,'')
                                    pdata = vtk.PointData( \
                                                vtk.Scalars( \
                                                    dd.flatten(), \
                                                    name=ff))
                                    vtkdata = vtk.VtkData(grid, pdata)
                                    files.append(folder+ff+'.vtk')
                                    vtkdata.tofile(files[-1],'binary')

        return

######################################################################
######################################################################
######################################################################
######################################################################

    def nx1_val(self, *dumm):
        ''' Validates nx entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nx1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['x'] = val
        except ValueError:
            if val!="":
                self.nx1_name.set(CONF['nodes']['x'])
            else:
                CONF['nodes']['x'] = 0
        except:
            raise

######################################################################
######################################################################

    def ny1_val(self, *dumm):
        ''' Validates ny entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.ny1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['y'] = val
        except ValueError:
            if val!="":
                self.ny1_name.set(CONF['nodes']['y'])
            else:
                CONF['nodes']['y'] = 0
        except:
            raise

######################################################################
######################################################################

    def nz1_val(self, *dumm):
        ''' Validates nz entry
        '''

        global CONF
        CONF = source.general.CONF

        val = self.nz1_name.get()
        try:
            num = int(val)
            if num < 0:
                raise ValueError
            CONF['nodes']['z'] = val
        except ValueError:
            if val!="":
                self.nz1_name.set(CONF['nodes']['z'])
            else:
                CONF['nodes']['z'] = 0
        except:
            raise

######################################################################
######################################################################

    def update_entry(self, event=None):
        ''' Really updates entries
        '''

        global CONF
        CONF = source.general.CONF

        # nx variable
        val = self.nx1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['x'] = val
        except ValueError:
            pass
        except:
            raise
        # ny variable
        val = self.ny1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['y'] = val
        except ValueError:
            pass
        except:
            raise
        # nz variable
        val = self.nz1_name.get()
        try:
            num = int(val)
            if num > 0:
                CONF['nodes']['z'] = val
        except ValueError:
            pass
        except:
            raise

######################################################################
######################################################################

    def set_top(self):
        ''' Reconfigures the top of the widget window
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF
        global TOOLTIPS
        TOOLTIPS = source.general.TOOLTIPS

        # Status
        if PMD['head']['loaded']:
            self.data0_name.config(text='Loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['5'])
            self.butt0_load.config(state='disabled')
            self.int1_check.config(state='disabled')
            self.butt1_int.config(state='normal')
            self.butt1_close.config(state='disabled')
            self.butt1_plot.config(state='normal')
            self.butt1_save.config(state='normal')
            self.butt0_unload.config(state='normal')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='disabled')
                self.var2_button[ii].config(state='disabled')
        else:
            self.data0_name.config(text='Not loaded', \
                                   font=self.cFont)
            createToolTip(self.data0_name, TOOLTIPS['6'])
            self.butt0_load.config(state='normal')
            self.int1_check.config(state='normal')
            self.butt1_int.config(state='disabled')
            self.butt1_close.config(state='normal')
            self.butt1_plot.config(state='disabled')
            self.butt1_save.config(state='disabled')
            self.butt0_unload.config(state='disabled')
            for ii in range(len(PMD['read'])):
                self.var2_check[ii].config(state='normal')
                if PMD['read'][ii]:
                    if PMD['sele'][ii] > 0:
                        self.var2_button[ii].config(state='normal')
                else:
                    if PMD['sele'][ii] > 0:
                        self.var2_button[ii].config(state='disabled')

        # Interpolation
        if CONF['interpol']:
            self.int1_stat.set(1)
        else:
            self.int1_stat.set(0)

        # Variables to read
        for ii in range(len(PMD['read'])):
            if PMD['read'][ii]:
                self.var2_stat[ii].set(1)
            else:
                self.var2_stat[ii].set(0)

        return

######################################################################
######################################################################

    def set_mid(self):
        ''' Reconfigures the display of the widget window
        '''

        global PMD
        PMD = source.general.PMD

        # Column width (maybe this should not be hardcoded)
        col = 400

        # Update information label frame
        if not PMD['head']['loaded']:
            text = '\n\nLoad the pmd file\n\n'
            self.data_label.config(justify=CENTER)
        else:
            data = PMD['data']
            module = PMD['head']['module']
            text = '\n'
            text += ' Module {0} version {1}\n'.format(module, \
                                               PMD['head']['pmd_ver'])
            text += ' Atomic mass: {0}\n'.format(data['amass'])
            # Level energy
            ltext = ' Level energy: '
            for ilevel in range(data['nlevel']):
                if ilevel > 0:
                    ltext += ','
                ltext += ' {0:.3E}'.format(data['E'][ilevel])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Angular momentum
            ltext = ' Angular momentum: '
            for ilevel in range(data['nlevel']):
                if ilevel > 0:
                    ltext += ','
                J2 = data['J2'][ilevel]
                if data['J2'][ilevel] % 2 > 0:
                    ltext += ' {0:d}/2'.format(J2)
                else:
                    ltext += ' {0:d}'.format(J2/2)
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Lande factor
            ltext = ' Lande factor: '
            for ilevel in range(data['nlevel']):
                if ilevel > 0:
                    ltext += ','
                ltext += ' {0:4.2f}'.format(data['g'][ilevel])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Einstein coefficients
            ltext = ' Einstein coefficient: '
            for itran in range(data['rtran']):
                if itran > 0:
                    ltext += ','
                ltext += ' {0:.3E}'.format(data['Aul'][itran])
            while len(list(ltext)) > col:
                icut = col
                for icol in range(col,-1,-1):
                    if list(ltext)[icol] == ' ':
                        icut = icol
                        break
                text += ltext[:icut] + '\n'
                ltext = ltext[icut+1:]
            if len(list(ltext)) > 0:
                text += ltext + '\n'
            # Kcut
            if data['Kcut'] > 0:
                text += 'K <= {0}'.format(data['Kcut'])
            text += '\n'
            self.data_label.config(justify=LEFT)
        self.data_label.config(text=text)

        return

######################################################################
######################################################################
######################################################################
######################################################################

    def get_size(self):
        ''' Reads just dimensions for the initializer
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Get dimensions
        try:
            with open(PMD['head']['name'],'rb') as f:

                # Skip to module head
                bytes = f.seek(PMD['head']['size'])
                # Get module version
                bytes = f.read(4)
                mod_ver = struct.unpack( \
                                  CONF['endian'] + 'I', bytes)[0]
                if mod_ver != 1:
                    tkMessageBox.showwarning("Load", \
                                             "Incompatible " + \
                                             "module version.", \
                                             parent=self)
                    return False
                # Atomic mass
                bytes = f.seek(8, 1)
                # Atom dimensions
                bytes = f.read(4)
                nlevel = int(struct.unpack( \
                                CONF['endian'] + 'i', bytes)[0])
                bytes = f.read(4)
                ntran = int(struct.unpack( \
                               CONF['endian'] + 'i', bytes)[0])
                bytes = f.read(4)
                rtran = int(struct.unpack( \
                               CONF['endian'] + 'i', bytes)[0])
                jump = nlevel*8*2
                f.seek(jump, 1)
                # Angular momentum
                bytes = f.read(4*nlevel)
                PMD['J2'] = np.array(struct.unpack( \
                                     CONF['endian'] + 'i'*nlevel, \
                                     bytes), dtype=np.int32)
                jump = ntran*8 + rtran*4*10
                f.seek(jump, 1)
                bytes = f.read(4)
                PMD['Kcut'] = int(struct.unpack( \
                              CONF['endian'] + 'i', bytes)[0])
                if PMD['Kcut'] < 0:
                    PMD['Kcut'] = 2000
                PMD['siz'] = {}
                PMD['siz']['l'] = nlevel
                PMD['siz']['r'] = rtran
                PMD['siz']['t'] = ntran

            f.close()
            return True
        except:
            return False

######################################################################
######################################################################

    def get_size_h5(self):
        ''' Reads just dimensions for the initializer from hdf5 file
        '''

        global PMD
        PMD = source.general.PMD
        global CONF
        CONF = source.general.CONF

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        try:

            with h5py.File(PMD['head']['name'],'r') as f:

                PMD['J2'] = f['Module'].attrs['J2']
                PMD['Kcut'] = f['Module'].attrs['K_CUT'][0]
                if PMD['Kcut'] < 0:
                    PMD['Kcut'] = 2000
                PMD['siz'] = {}
                PMD['siz']['l'] = f['Module'].attrs['NL'][0]
                PMD['siz']['t'] = f['Module'].attrs['NT'][0]
                PMD['siz']['r'] = f['Module'].attrs['NR'][0]

            f.close()
            return True

        except:
            return False

######################################################################
######################################################################

    def get_data(self):
        ''' Reads the pmd for the multilevel module given the header
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with open(PMD['head']['name'],'rb') as f:

                # Skip to module head
                bytes = f.seek(PMD['head']['size'])
                # Get module version
                bytes = f.read(4)
                inpt['mod_ver'] = struct.unpack( \
                                  CONF['endian'] + 'I', bytes)[0]
                if inpt['mod_ver'] != 1:
                    tkMessageBox.showwarning("Load", \
                                             "Incompatible " + \
                                             "module version.", \
                                             parent=self)
                    return False
                # Atomic mass
                bytes = f.read(8)
                inpt['amass'] = float(struct.unpack( \
                                CONF['endian'] + 'd', bytes)[0])
                # Atom dimensions
                bytes = f.read(4)
                inpt['nlevel'] = int(struct.unpack( \
                                 CONF['endian'] + 'i', bytes)[0])
                bytes = f.read(4)
                inpt['ntran'] = int(struct.unpack( \
                                CONF['endian'] + 'i', bytes)[0])
                bytes = f.read(4)
                inpt['rtran'] = int(struct.unpack( \
                                CONF['endian'] + 'i', bytes)[0])
                # Energy
                bytes = f.read(8*inpt['nlevel'])
                inpt['E'] = np.array(struct.unpack( \
                                     CONF['endian'] + \
                                     'd'*inpt['nlevel'], \
                                     bytes), dtype=np.float32)
                # Lande
                bytes = f.read(8*inpt['nlevel'])
                inpt['g'] = np.array(struct.unpack( \
                                     CONF['endian'] + \
                                     'd'*inpt['nlevel'], \
                                     bytes), dtype=np.float32)
                # Angular momentum
                bytes = f.read(4*inpt['nlevel'])
                inpt['J2'] = np.array(struct.unpack( \
                                      CONF['endian'] + \
                                      'i'*inpt['nlevel'], \
                                      bytes), dtype=np.int32)
                # Pairs of levels
                bytes = f.read(4*inpt['ntran']*2)
                inpt['iul'] = np.array(struct.unpack( \
                                       CONF['endian'] + \
                                       'i'*2*inpt['ntran'], \
                                       bytes), dtype=np.int32). \
                                       reshape((inpt['ntran'],2))
                # Aul
                bytes = f.read(8*inpt['rtran'])
                inpt['Aul'] = np.array(struct.unpack( \
                                       CONF['endian'] + \
                                       'd'*inpt['rtran'], \
                                       bytes), dtype=np.float32)
                # nfreq
                bytes = f.read(4*inpt['rtran'])
                inpt['nfreq'] = np.array(struct.unpack( \
                                         CONF['endian'] + \
                                         'i'*inpt['rtran'], \
                                         bytes), dtype=np.int32)
                # nfreqc
                bytes = f.read(4*inpt['rtran'])
                inpt['nfreqc'] = np.array(struct.unpack( \
                                          CONF['endian'] + \
                                          'i'*inpt['rtran'], \
                                          bytes), dtype=np.int32)
                # Width
                bytes = f.read(8*inpt['rtran'])
                inpt['Dw'] = np.array(struct.unpack( \
                                      CONF['endian'] + \
                                      'd'*inpt['rtran'], \
                                      bytes), dtype=np.float32)
                # Widthc
                bytes = f.read(8*inpt['rtran'])
                inpt['Dwc'] = np.array(struct.unpack( \
                                       CONF['endian'] + \
                                       'd'*inpt['rtran'], \
                                       bytes), dtype=np.float32)
                # Tref
                bytes = f.read(8*inpt['rtran'])
                inpt['Tref'] = np.array(struct.unpack( \
                                        CONF['endian'] + \
                                        'd'*inpt['rtran'], \
                                        bytes), dtype=np.float32)
                # Kcut
                bytes = f.read(4)
                inpt['Kcut'] = int(struct.unpack( \
                                   CONF['endian'] + \
                                   'i', bytes)[0])
                # Store nodes
                inpt['nodes0'] = copy.deepcopy(PMD['head']['nodes0'])
                inpt['nodes'] = copy.deepcopy(PMD['head']['nodes0'])
                # Axes
                inpt['x']=PMD['head']['x'][0:PMD['head']['nodes0'][0]]
                inpt['y']=PMD['head']['y'][0:PMD['head']['nodes0'][1]]
                inpt['z']=PMD['head']['z'][0:PMD['head']['nodes0'][2]]
                # T bottom
                bytes = f.read(8*inpt['nodes'][0]*
                                 inpt['nodes'][1])
                inpt['tempo'] = np.array(struct.unpack( \
                                CONF['endian'] + 'd'* \
                                inpt['nodes'][0]* \
                                inpt['nodes'][1], bytes)). \
                                reshape(inpt['nodes'][1], \
                                        inpt['nodes'][0])

                # Compute density matrix size
                inpt['ndim'] = 0
                Kc = inpt['Kcut']
                if Kc < 0:
                    Kc = 2000
                for ilevel in range(inpt['nlevel']):
                    Kmax = np.amin([Kc,inpt['J2'][ilevel]])
                    for K in range(Kmax+1):
                        for Q in range(K+1):
                            inpt['ndim'] += 1
                            if Q > 0:
                                inpt['ndim'] += 1
                PMD['ndim'] = inpt['ndim']

                # Calculate plane size
                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            8*(9 + inpt['ndim'] + \
                                   inpt['rtran']*12 + \
                                   inpt['ntran'] + \
                                   inpt['nlevel'])

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]

                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8

                # Identify scalars and vectors
                scal = [0,1,4]
                vec = [2,3]

                # Initialize real sizes
                PMD['rsiz'] = {}

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    # If reading variable
                    if read:

                        # Scalars and vectors
                        if PMD['type'][ii] == 'n':

                            if ii in scal:
                                inpt[ii] = \
                                         np.empty([inpt['nodes'][2], \
                                                   inpt['nodes'][1], \
                                                   inpt['nodes'][0], \
                                                   1])
                                PMD['rsiz'][ii] = 1
                            elif ii in vec:
                                inpt[ii] = \
                                         np.empty([inpt['nodes'][2], \
                                                   inpt['nodes'][1], \
                                                   inpt['nodes'][0], \
                                                   3])
                                PMD['rsiz'][ii] = 3

                        # Other type
                        else:
                            siz = 0
                            for cc in PMD['selection'][ii]:
                                if cc:
                                    siz += 1
                            PMD['rsiz'][ii] = siz
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 siz])
                    # If not reading
                    else:
                        inpt[ii] = -1
                        PMD['rsiz'][ii] = -1

                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = self.skip_plane(f, planesize)
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane(f)
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane(f)

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in PMD['vars'].keys():
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in \
                                            range(PMD['rsiz'][var]):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in PMD['vars'].keys():
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in \
                                            range(PMD['rsiz'][var]):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True

                                # Interpolate in vertical
                                w1 = (zz - zz0)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in PMD['vars'].keys():
                                    if not PMD['read'][var]:
                                        continue
                                    for ii in \
                                            range(PMD['rsiz'][var]):
                                        inpt[var][izp,:,:,ii] = \
                                                w1*P0[var][:,:,ii] + \
                                                w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                # Do not interpolate
                else:

                    for iz in range(inpt['nodes'][2]):

                        P0 = self.get_plane(f)

                        for var in PMD['vars']:
                            if PMD['read'][var]:
                                for ii in range(PMD['rsiz'][var]):
                                    inpt[var][iz,:,:,:] = P0[var]

                f.close()

                PMD['data'] = inpt
                return True

            PMD['data'] = inpt
            return True

        except:

            raise
            return False

######################################################################
######################################################################

    def get_data_h5(self):
        ''' Reads the pmd (in HDF5) for the multilevel module given
            the header.
            Returns true if the reading went well and false otherwise.
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Check if there is a file at all
        if not os.path.isfile(PMD['head']['name']):
            return False

        # Initialize output
        inpt = {}

        # Read it as a pmd file
        try:

            self.pg.update_idletasks()

            with h5py.File(PMD['head']['name'],'r') as f:

                inpt['mod_ver'] = f['Module'].attrs['ML_VERSION'][0]
                inpt['amass'] = f['Module'].attrs['ATOM_MASS'][0]
                inpt['nlevel'] = f['Module'].attrs['NL'][0]
                inpt['ntran'] = f['Module'].attrs['NT'][0]
                inpt['rtran'] = f['Module'].attrs['NR'][0]
                inpt['E'] = f['Module'].attrs['E']
                inpt['g'] = f['Module'].attrs['GL']
                inpt['J2'] = f['Module'].attrs['J2']
                inpt['iul'] = f['Module'].attrs['IUL']
                inpt['iul'] = inpt['iul'].reshape((inpt['ntran'],2))
                inpt['Aul'] = f['Module'].attrs['AUL']
                inpt['nfreq'] = f['Module'].attrs['NFREQ']
                inpt['nfreqc'] = f['Module'].attrs['NFREQ_C']
                inpt['Dw'] = f['Module'].attrs['DW']
                inpt['Dwc'] = f['Module'].attrs['DW_C']
                inpt['Tref'] = f['Module'].attrs['TEMP_REF']
                inpt['Kcut'] = f['Module'].attrs['K_CUT']
                inpt['tempo'] = np.asarray(f['Module']['temp_bc'])

                # Compute density matrix size
                inpt['ndim'] = 0
                Kc = inpt['Kcut']
                if Kc < 0:
                    Kc = 2000
                for ilevel in range(inpt['nlevel']):
                    Kmax = np.amin([Kc,inpt['J2'][ilevel]])
                    for K in range(Kmax+1):
                        for Q in range(K+1):
                            inpt['ndim'] += 1
                            if Q > 0:
                                inpt['ndim'] += 1
                PMD['ndim'] = inpt['ndim']

                # Store nodes
                inpt['nodes0'] = copy.deepcopy(PMD['head']['nodes0'])
                inpt['nodes'] = copy.deepcopy(PMD['head']['nodes0'])
                # Axes
                inpt['x']=PMD['head']['x'][0:PMD['head']['nodes0'][0]]
                inpt['y']=PMD['head']['y'][0:PMD['head']['nodes0'][1]]
                inpt['z']=PMD['head']['z'][0:PMD['head']['nodes0'][2]]

                planesize = PMD['head']['nodes0'][0]* \
                            PMD['head']['nodes0'][1]* \
                            8*(9 + inpt['ndim'] + \
                                   inpt['rtran']*12 + \
                                   inpt['ntran'] + \
                                   inpt['nlevel'])

                # Create geometry axis
                if CONF['interpol']:

                    # Change number of nodes
                    inpt['nodes'] = [int(CONF['nodes']['x']), \
                                     int(CONF['nodes']['y']), \
                                     int(CONF['nodes']['z'])]

                    # Check sizes
                    for ii in range(len(inpt['nodes'])):
                        if inpt['nodes'][ii] > inpt['nodes0'][ii] or \
                           inpt['nodes'][ii] <= 1:
                            tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                            return False

                    # Original grid in Mm
                    temp = {}
                    temp['x'] = np.array(inpt['x'])*1e-8
                    temp['y'] = np.array(inpt['y'])*1e-8
                    temp['z'] = np.array(inpt['z'])*1e-8

                    # Final grid
                    inpt['x'] = np.linspace(min(temp['x']), \
                                            max(temp['x']), \
                                            inpt['nodes'][0])
                    inpt['y'] = np.linspace(min(temp['y']), \
                                            max(temp['y']), \
                                            inpt['nodes'][1])
                    inpt['z'] = np.linspace(min(temp['z']), \
                                            max(temp['z']), \
                                            inpt['nodes'][2])

                    # Check what planes do we really need to read
                    Flags = np.zeros(inpt['nodes0'][2], dtype=bool)
                    liz1 = 0
                    for iz in range(inpt['nodes0'][2]-1):

                        for iz1 in range(liz1,inpt['nodes'][2]):

                            if inpt['z'][iz1] > temp['z'][iz+1]:
                                break

                            liz1 = iz1

                            if inpt['z'][iz1] >= temp['z'][iz]:
                                Flags[iz] = Flags[iz] or True
                                Flags[iz+1] = Flags[iz+1] or True

                else:

                    # Flag all planes for read
                    Flags = np.ones(inpt['nodes0'][2], dtype=bool)

                    # Transform into Mm
                    inpt['x'] = np.array(inpt['x'])*1e-8
                    inpt['y'] = np.array(inpt['y'])*1e-8
                    inpt['z'] = np.array(inpt['z'])*1e-8

                # Identify scalars and vectors
                scal = [0,1,4]
                vec = [2,3]

                # Initialize real sizes
                PMD['rsiz'] = {}

                # Create space to read data
                for ii,read in \
                    zip(range(len(PMD['read'])),PMD['read']):

                    # If reading variable
                    if read:

                        # Scalars and vectors
                        if PMD['type'][ii] == 'n':

                            if ii in scal:
                                inpt[ii] = \
                                         np.empty([inpt['nodes'][2], \
                                                   inpt['nodes'][1], \
                                                   inpt['nodes'][0], \
                                                   1])
                                PMD['rsiz'][ii] = 1
                            elif ii in vec:
                                inpt[ii] = \
                                         np.empty([inpt['nodes'][2], \
                                                   inpt['nodes'][1], \
                                                   inpt['nodes'][0], \
                                                   3])
                                PMD['rsiz'][ii] = 3

                        # Other type
                        else:
                            siz = 0
                            for cc in PMD['selection'][ii]:
                                if cc:
                                    siz += 1
                            PMD['rsiz'][ii] = siz
                            inpt[ii] = np.empty([inpt['nodes'][2], \
                                                 inpt['nodes'][1], \
                                                 inpt['nodes'][0], \
                                                 siz])
                    # If not reading
                    else:
                        inpt[ii] = -1
                        PMD['rsiz'][ii] = -1
                        
                # Interpolating
                if CONF['interpol']:

                    # Auxiliar initialization
                    izps = 1
                    NX = inpt['nodes'][0]
                    NY = inpt['nodes'][1]
                    P0 = {}
                    P1 = {}

                    # With HDF5 I don't read the data sequentially as
                    # for the binary file, but I can read a whole
                    # plane easily if we know the correspoding iz, so
                    # izt is a counter to keep track of the plane to
                    # be read or skipped
                    izt = 0
                    
                    for iz in range(inpt['nodes0'][2] - 1):

                        # If the next two planes are not needed,
                        # skip the current plane if have not been
                        # read
                        if not Flags[iz] or not Flags[iz+1]:
                            try:
                                check = P1['done']
                                P1 = {}
                            except KeyError:
                                P0 = {}  # nothing to be done for
                                         # HDF5, but the izt counter
                                         # has to go up
                                izt = izt + 1
                            except :
                                raise
                            continue

                        # Get P0
                        try:
                            check = P0['done']
                        except KeyError:
                            P0 = self.get_plane_h5(f,izt)
                            izt = izt + 1
                        except:
                            raise

                        # Get P1
                        P1 = self.get_plane_h5(f,izt)
                        izt = izt + 1

                        zz0 = temp['z'][iz]
                        zz1 = temp['z'][iz+1]

                        for izp in range(izps-1,inpt['nodes'][2]):

                            izps = izp + 1
                            zz = inpt['z'][izp]

                            if zz > zz1:
                                break

                            if zz0 <= zz and zz1 >= zz:

                                # Interpolate in lower plane
                                if not P0['done']:
                                    for var in PMD['vars'].keys():
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in \
                                            range(PMD['rsiz'][var]):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P0[var][:,:,ii])
                                            P0[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P0[var] = P0[var][0:NY,0:NX,:]
                                    P0['done'] = True

                                # Interpolate in upper plane
                                if not P1['done']:
                                    for var in PMD['vars'].keys():
                                        if not PMD['read'][var]:
                                            continue
                                        for ii in \
                                            range(PMD['rsiz'][var]):
                                            r = interpolate. \
                                                 interp2d( \
                                                    temp['y'], \
                                                    temp['x'], \
                                                    P1[var][:,:,ii])
                                            P1[var][0:NY,0:NX,ii] = \
                                                    r(inpt['x'], \
                                                      inpt['y'])
                                        P1[var] = P1[var][0:NY,0:NX,:]
                                    P1['done'] = True

                                # Interpolate in vertical
                                w1 = (zz - zz0)/(zz1 - zz0)
                                w2 = 1 - w1
                                for var in PMD['vars'].keys():
                                    if not PMD['read'][var]:
                                        continue
                                    for ii in \
                                            range(PMD['rsiz'][var]):
                                        inpt[var][izp,:,:,ii] = \
                                                w1*P0[var][:,:,ii] + \
                                                w2*P1[var][:,:,ii]

                        if iz < inpt['nodes0'][2] - 2:

                            # Shift planes
                            if Flags[iz+2]:
                                P0 = copy.deepcopy(P1)
                            else:
                                P0 = {}
                        
                        else:

                            P1 = {}
                            P0 = {}

                # Do not interpolate
                else:

                    # For each variable
                    for var in PMD['vars']:

                        # If reading
                        if not PMD['read'][var]:
                            continue

                        nvar = PMD['hdf5_dic'][var]

                        # If a scalar
                        if var in scal:
                            inpt[var][:,:,:,0] = np.asarray( \
                                             f['Module/g_data'][nvar])

                        # Not a scalar
                        else:

                            # Normal array
                            if PMD['type'][var] in list('lrt'):
                                siz = PMD['siz'][PMD['type'][var]]
                                sel = PMD['selection'][var]
                            # Density matrix
                            elif PMD['type'][var] == 'lkq':
                                siz = PMD['ndim']
                                sel = PMD['selection'][var]
                            # Radiation field tensor
                            elif PMD['type'][var] == 'rkq':
                                siz = PMD['siz']['r']*9
                                sel = PMD['selection'][var]
                            # Vector
                            else :
                                siz = 3
                                sel = [True]*siz

                            # Loop
                            kk = -1
                            for jj in range(siz):
                                if not sel[jj]:
                                    continue
                                kk += 1
                                inpt[var][:,:,:,kk] = np.asarray( \
                                   f['Module/g_data'][nvar][:,:,:,jj])

                f.close()

                PMD['data'] = inpt
                return True

        except:

            raise
            return False
        
######################################################################
######################################################################

    def get_plane(self, f):
        ''' Load the data from a plane in the pmd file
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(12):
            T[ii] = []
        T['done'] = False
        dim = PMD['head']['nodes0'][1]*PMD['head']['nodes0'][0]

        #
        # Read plane
        #
        var = PMD['vars'].keys()
        sorted(var)

        # For each point within the plane
        for ixy in range(dim):

            # For each variable
            for ii in var:

                # If scalar or vector
                if PMD['type'][ii] == 'n':
                    if PMD['scal'][ii]:
                        bytes = f.read(8)
                        if PMD['read'][ii]:
                            T[ii].append(struct.unpack( \
                                         CONF['endian'] + 'd',\
                                         bytes)[0])
                    else:
                        for jj in range(3):
                            bytes = f.read(8)
                            if PMD['read'][ii]:
                                T[ii].append(struct.unpack( \
                                             CONF['endian'] + 'd',\
                                             bytes)[0])
                # Not scalar or vector
                else:

                    # Shortcut
                    sel = PMD['selection'][ii]

                    # Normal array
                    if PMD['type'][ii] in list('lrt'):
                        siz = PMD['siz'][PMD['type'][ii]]
                    # Density matrix
                    elif PMD['type'][ii] == 'lkq':
                        siz = PMD['ndim']
                    # Radiation field tensor
                    elif PMD['type'][ii] == 'rkq':
                        siz = 9*PMD['siz']['r']

                    for jj in range(siz):
                        bytes = f.read(8)
                        if PMD['read'][ii]:
                            if sel[jj]:
                                T[ii].append(struct.unpack( \
                                             CONF['endian'] + 'd',\
                                             bytes)[0])
        # For each variable
        for ii in var:
            if PMD['read'][ii]:
                T[ii] = np.array(T[ii]). \
                        reshape(PMD['head']['nodes0'][1], \
                                PMD['head']['nodes0'][0], \
                                PMD['rsiz'][ii])

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T


######################################################################
######################################################################

    def get_plane_h5(self, f, izt):
        ''' Load the data from plane izt in the pmd (HDF5) file
        '''

        global PMD
        PMD = source.general.PMD

        # Initialize plane
        T = {}
        for ii in range(12):
            T[ii] = []
        T['done'] = False

        # Read plane
        #
        var = PMD['vars'].keys()
        sorted(var)

        
        # For each variable
        for ii in var:
            nvar = PMD['hdf5_dic'][ii]
            
            # If scalar or vector
            if PMD['type'][ii] == 'n':
                if PMD['scal'][ii]:
                    if PMD['read'][ii]:
                        T[ii] = \
                            np.empty([PMD['head']['nodes0'][1], \
                                      PMD['head']['nodes0'][0], \
                                      1])
                        T[ii][:,:,0] = np.asarray(f['Module/g_data'] \
                                                   [nvar][izt,:,:])

                else:
                    if PMD['read'][ii]:
                        T[ii] = np.asarray(f['Module/g_data'] \
                                            [nvar][izt,:,:,:])

            # Not scalar or vector
            else:

                # Shortcut
                sel = PMD['selection'][ii]
                rsiz = 0
                for cc in sel:
                    if cc:
                        rsiz += 1

                T[ii] = np.empty([PMD['head']['nodes0'][1], \
                                  PMD['head']['nodes0'][0], \
                                  rsiz])

                        
                # Normal array
                if PMD['type'][ii] in list('lrt'):
                    siz = PMD['siz'][PMD['type'][ii]]
                # Density matrix
                elif PMD['type'][ii] == 'lkq':
                    siz = PMD['ndim']
                # Radiation field tensor
                elif PMD['type'][ii] == 'rkq':
                    siz = 9*PMD['siz']['r']
                    
                # Loop
                kk = -1
                if PMD['read'][ii]:
                    for jj in range(siz):
                        if sel[jj]:
                            kk += 1
                            T[ii][:,:,kk] = np.asarray( \
                                               f['Module/g_data'] \
                                                [nvar][izt,:,:,jj])

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return T


######################################################################
######################################################################

    def interpol_data(self):
        ''' Interpolates the pmd data that is loaded
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Initialize progress bar
        self.pgc = 0
        self.pgl = 0

        # Initialize auxiliar dictionary
        inpt = {}

        # Change number of nodes
        inpt['nodes'] = [int(CONF['nodes']['x']), \
                         int(CONF['nodes']['y']), \
                         int(CONF['nodes']['z'])]

        # Check sizes
        for ii in range(len(inpt['nodes'])):
            if inpt['nodes'][ii] > PMD['data']['nodes0'][ii] or \
               inpt['nodes'][ii] <= 1:
                tkMessageBox.showwarning("Load", \
                                         "The dimension of the " + \
                                         "interpolated data " + \
                                         "cannot be larger than " + \
                                         "the original or " + \
                                         "smaller than 2.", \
                                         parent=self)
                return False

        # Original grid in Mm
        temp = {}
        temp['x'] = PMD['data']['x']
        temp['y'] = PMD['data']['y']
        temp['z'] = PMD['data']['z']

        # Final grid
        inpt['x'] = np.linspace(min(temp['x']), \
                                max(temp['x']), \
                                inpt['nodes'][0])
        inpt['y'] = np.linspace(min(temp['y']), \
                                max(temp['y']), \
                                inpt['nodes'][1])
        inpt['z'] = np.linspace(min(temp['z']), \
                                max(temp['z']), \
                                inpt['nodes'][2])

        # Check that axes are different
        if np.array_equal(temp['x'],inpt['x']) and \
           np.array_equal(temp['y'],inpt['y']) and \
           np.array_equal(temp['z'],inpt['z']):
            return

        # Data pointer
        data = PMD['data']
        NX,NY,NZ = inpt['nodes']

        # Save axis
        data['x'] = inpt['x']
        data['y'] = inpt['y']
        data['z'] = inpt['z']
        data['nodes'] = inpt['nodes']
        inpt = 0

        # Identify scalars and vectors
        scal = [0,1,2,8,9,10,11]
        vec = [3,4]

        #
        # Variables are loaded, different system to save memory
        #

        # Get a mesh of the final grid
        zz,yy,xx = np.meshgrid(data['z'],data['y'],data['x'], \
                               indexing='ij')


        # For each variable
        for var in PMD['vars'].keys():
            if PMD['read'][var]:
                aux = data[var]
                for ii in range(PMD['rsiz'][var]):
                    r = interpolate.RegularGridInterpolator( \
                            (temp['z'],temp['y'],temp['x']), \
                             aux[:,:,:,ii])
                    data[var][:NZ,:NY,:NX,ii] = r((zz,yy,xx))
                    # Update bar
                    self.pgc += 100./float(len(PMD['read']))
                    pgc = int(self.pgc)
                    if pgc > self.pgl:
                        diff = pgc - self.pgl
                        for ii in range(diff):
                            if pgc <= 99:
                                self.pg.step()
                                self.pg.update_idletasks()
                        self.pgl += diff
                data[var] = data[var][:NZ,:NY,:NX,:]
        return True


######################################################################
######################################################################

    def skip_plane(self, f, skip):
        ''' Skip a plane
        '''

        bytes = f.seek(skip, 1)

        self.pgc += 100./float(PMD['head']['nodes0'][2])
        pgc = int(self.pgc)
        if pgc > self.pgl:
            diff = pgc - self.pgl
            for ii in range(diff):
                if pgc <= 99:
                    self.pg.step()
                    self.pg.update_idletasks()
            self.pgl += diff

        return {}

######################################################################
######################################################################
######################################################################
######################################################################

class RHOSELECT_class(Toplevel):
    ''' Class that defines the main body of the selector class for
        the density matrix
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.config(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.box = Frame(body)
        self.bot = Frame(body)
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.configure(self.box)
        self.button(self.bot)

        self.box.pack(fill=BOTH,expand=1,side=TOP)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################

    def configure(self, box):
        ''' Method that fills the window
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Grid of selects
        ind = PMD['type'].index('lkq')
        self.ind = ind
        '''
        ndim = len(PMD['selection'][ind])
        nx = int(np.sqrt(ndim))
        ny = nx
        while nx*ny < ndim:
            nx += 1
        '''
        nx = PMD['siz']['l']
        KMax = 0
        for ii in range(PMD['siz']['l']):
            Kmax = np.amin([PMD['Kcut'],PMD['J2'][ii]])
            if Kmax > KMax:
                KMax = Kmax
        ny = KMax*KMax + 2*KMax + 1

        # Data to load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Select density matrix elements",\
                    font=self.cFont, justify=CENTER)
        b0l.grid(row=row,column=col,columnspan=nx,sticky=NSEW)

        # Create grid of checks
        '''
        row = 0
        col = 0
        '''
        self.var0_stat = []
        self.var0_check = []
        jj = -1
        for ii in range(PMD['siz']['l']):
            Kmax = np.amin([PMD['Kcut'],PMD['J2'][ii]])
            col = ii
            row = 0
            for K in range(Kmax+1):
                for Q in range(K+1):
                    text = 'Level: {0}, K:{1}, Q:{2}'
                    text = text.format(ii+1,K,Q)

                    # Complex
                    if Q > 0:

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + ' Real', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + ' Imag', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)

                    # Real
                    else:

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + '     ', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def button(self, box):
        ''' Defines the close button of the widget
        '''

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Close button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def check0(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['selection'][self.ind][val] = \
                                        self.var0_stat[val].get() == 1

        return

######################################################################
######################################################################
######################################################################
######################################################################

class JSELECT_class(Toplevel):
    ''' Class that defines the main body of the selector class for
        the radiation field tensor
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.config(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.box = Frame(body)
        self.bot = Frame(body)
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.configure(self.box)
        self.button(self.bot)

        self.box.pack(fill=BOTH,expand=1,side=TOP)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################

    def configure(self, box):
        ''' Method that fills the window
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Grid of selects
        ind = PMD['type'].index('rkq')
        self.ind = ind
        '''
        ndim = PMD['siz']['r']*9
        nx = int(np.sqrt(ndim))
        ny = nx
        while nx*ny < ndim:
            nx += 1
        '''
        nx = PMD['siz']['r']
        ny = 9

        # Data to load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Select radiation field elements",\
                    font=self.cFont, justify=CENTER)
        b0l.grid(row=row,column=col,columnspan=nx,sticky=NSEW)

        # Create grid of checks
        row = 0
        col = 0
        self.var0_stat = []
        self.var0_check = []
        jj = -1
        for ii in range(PMD['siz']['r']):
            col = ii
            row = 0
            for K in range(3):
                for Q in range(K+1):
                    text = 'Trans: {0}, K:{1}, Q:{2}'
                    text = text.format(ii+1,K,Q)

                    # Complex
                    if Q > 0:

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + ' Real', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + ' Imag', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)

                    # Real
                    else:

                        jj += 1
                        row += 1
                        '''
                        row += 1
                        if row > ny:
                            col += 1
                            row = 1
                        '''

                        self.var0_stat.append(IntVar())
                        if PMD['selection'][ind][jj]:
                            self.var0_stat[jj].set(1)
                        else:
                            self.var0_stat[jj].set(0)
                        self.var0_check.append(Checkbutton(box0, \
                                        text=text + '     ', \
                                        anchor=W, \
                                        font=self.cFont, \
                                        variable=self.var0_stat[jj], \
                                        command=lambda kk=jj: \
                                                self.check0(kk)))
                        self.var0_check[jj].grid(row=row, \
                                                 column=col, \
                                                 sticky=NSEW)
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def button(self, box):
        ''' Defines the close button of the widget
        '''

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Close button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def check0(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['selection'][self.ind][val] = \
                                        self.var0_stat[val].get() == 1

        return


######################################################################
######################################################################
######################################################################
######################################################################

class SELECT_class(Toplevel):
    ''' Class that defines the main body of the selector class for
        the radiation field tensor
    '''

######################################################################
######################################################################

    def __init__(self, parent, title = None, ind = None):
        ''' Initializes the widget single tab
        '''

        global CONF
        CONF = source.general.CONF

        if ind is None:
            self.close()
        self.ind = ind

        # Initialized a support window.
        Toplevel.__init__(self, parent)
        self.transient(parent)

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Set the title if any
        if title:
            self.title(title)

        # Active border
        self.config(background=CONF['col_act'])

        self.parent = parent
        self.result = None
        self.plt_action = None

        head = Frame(self)
        body = Frame(head)
        self.box = Frame(body)
        self.bot = Frame(body)
        self.initial_focus = self.body(body)
        head.pack(padx=CONF['b_pad'], pady=CONF['b_pad'])
        body.pack(fill=BOTH,expand=1)

        self.grab_set()
        self.lift()
        
        if not self.initial_focus:
            self.initial_focus = self

        self.configure(self.box)
        self.button(self.bot)

        self.box.pack(fill=BOTH,expand=1,side=TOP)
        self.bot.pack(fill=BOTH,expand=1,side=BOTTOM)
        body.pack(fill=BOTH,expand=1)
        head.pack(fill=BOTH,expand=1)

        self.protocol("WM_DELETE_WINDOW", self.close)
        self.bind("<Escape>", self.close)

        self.geometry("+%d+%d" % (parent.winfo_rootx()+50,
                                  parent.winfo_rooty()+50))

        self.initial_focus.focus_set()

        self.wait_window(self)

######################################################################
######################################################################

    def body(self, master):
        ''' Creates the dialog body and set initial focus.
        '''

        return master

######################################################################
######################################################################

    def close(self, event=None):
        ''' Method that handles the window closing.
        '''

        # put focus back to the parent window
        self.parent.focus_set()
        self.destroy()

######################################################################
######################################################################

    def configure(self, box):
        ''' Method that fills the window
        '''

        global CONF
        CONF = source.general.CONF
        global PMD
        PMD = source.general.PMD

        # Grid of selects
        ind = self.ind
        ndim = PMD['siz'][PMD['type'][self.ind]]
        nx = int(np.sqrt(ndim))
        ny = nx
        while nx*ny < ndim:
            nx += 1

        # Data to load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # General label
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        b0l = Label(box0, text="Select "+PMD['vars'][self.ind],\
                    font=self.cFont, justify=CENTER)
        b0l.grid(row=row,column=col,columnspan=nx,sticky=NSEW)

        # Create grid of checks
        row = 0
        col = 0
        self.var0_stat = []
        self.var0_check = []
        jj = -1
        if PMD['type'][self.ind] == 'r' or \
           PMD['type'][self.ind] == 't':
            base = 'Trans: '
        elif PMD['type'][self.ind] == 'l':
            base = 'Level: '
        else:
            base = '       '
        for ii in range(PMD['siz'][PMD['type'][self.ind]]):
            text = '{0}: {1}'
            text = text.format(base,ii+1)

            jj += 1
            row += 1
            if row > ny:
                col += 1
                row = 1

            self.var0_stat.append(IntVar())
            if PMD['selection'][ind][jj]:
                self.var0_stat[jj].set(1)
            else:
                self.var0_stat[jj].set(0)
            self.var0_check.append(Checkbutton(box0, \
                            text=text, \
                            anchor=W, \
                            font=self.cFont, \
                            variable=self.var0_stat[jj], \
                            command=lambda kk=jj: \
                                    self.check0(kk)))
            self.var0_check[jj].grid(row=row, \
                                     column=col,sticky=NSEW)

        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def button(self, box):
        ''' Defines the close button of the widget
        '''

        # Data load (box0)
        box0 = LabelFrame(box)
        row = 0
        col = 0
        # Close button
        Grid.rowconfigure(box0,row,weight=1)
        Grid.columnconfigure(box0,col,weight=1)
        self.butt1_close = Button(box0, text="Close", \
                                  font=self.cFont, command=self.close)
        self.butt1_close.grid(row=row,column=col,sticky=NSEW)
        # Pack the box
        box0.pack(fill=BOTH, expand=1, side = TOP)

######################################################################
######################################################################

    def check0(self, val):
        ''' Check to read a variable
        '''

        global PMD
        PMD = source.general.PMD

        PMD['selection'][self.ind][val] = \
                                        self.var0_stat[val].get() == 1

        return


######################################################################
######################################################################
######################################################################
######################################################################
