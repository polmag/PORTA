# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# colors.py                                                          #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Define color maps                                                  #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  12/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
######################################################################
######################################################################
######################################################################

from source.loader import *

######################################################################
######################################################################

def color_def(precolo, string, preleve = None, minv = 0, maxv = 255):
    ''' Defines the color bar given the color indixes in a list of
        tuples, the list of positions and the minimum a maximum of
        relative to the positions you gave. It takes also as argument
        the name that the scale will have.
    '''
    
    bit_rgb = np.linspace(0,1,256)
    if preleve is None:
        prelevel = np.linspace(0, 255, len(precolo))
    # Introduce the extremes of the data
    level = [minv]
    if minv >= preleve[0]:
        if minv == preleve[0]:
            color = [precolo[0]]
            ini = 1
        else:
            ini = -1
            for i in range(len(preleve) - 1):
                if minv >= preleve[i] and minv < preleve[i+1]:
                    R = [precolo[i][0], precolo[i+1][0]]
                    G = [precolo[i][1], precolo[i+1][1]]
                    B = [precolo[i][2], precolo[i+1][2]]
                    color = [(np.interp([minv], [preleve[i], \
                                                 preleve[i+1]],R)[0],
                              np.interp([minv], [preleve[i], \
                                                 preleve[i+1]],G)[0],
                              np.interp([minv], [preleve[i], \
                                                 preleve[i+1]],B)[0])]
                    ini = i+1
                    break
    else:
        color = [(  0,  0,  0)]
        ini = 0
    if maxv < preleve[len(preleve) - 1]:
        for j in range(len(preleve[ini:]) - 1):
            if maxv > preleve[ini+j] and maxv <= preleve[ini+j+1]:
                for k in range(len(preleve[ini:ini+j+1])):
                    level.append(preleve[k+ini])
                    color.append(precolo[k+ini])
                level.append(maxv)
                fin = ini+j
                R = [precolo[fin][0], precolo[fin+1][0]]
                G = [precolo[fin][1], precolo[fin+1][1]]
                B = [precolo[fin][2], precolo[fin+1][2]]
                color.append((np.interp([maxv], [preleve[fin], \
                                               preleve[fin+1]],R)[0],
                              np.interp([maxv], [preleve[fin], \
                                               preleve[fin+1]],G)[0],
                              np.interp([maxv], [preleve[fin], \
                                               preleve[fin+1]],B)[0]))
    else:
        for j in range(len(preleve[ini:])):
            level.append(preleve[ini+j])
            color.append(precolo[ini+j])
            fin = ini+j
        if maxv != preleve[len(preleve)-1]:
            level.append(maxv)
            color.append((precolo[-1][0], precolo[-1][1], \
                          precolo[-1][2]))
    # Normalize the height scale
    a = 1./(maxv - minv)
    b = minv/(minv - maxv)
    for i in range(len(level)):
        level[i] = a*level[i] + b
    level[0] = 0
    level[len(level)-1]=1
    # Transform from bits to 0-1
    for i in range(len(color)):
        color[i] = (bit_rgb[color[i][0]],
                    bit_rgb[color[i][1]],
                    bit_rgb[color[i][2]])
    # Generate the dictionary    
    cdict = {'red':[], 'green':[], 'blue':[]}
    for lv, col in zip(level, color):
        cdict[ 'red' ].append((lv, col[0], col[0]))
        cdict['green'].append((lv, col[1], col[1]))
        cdict[ 'blue'].append((lv, col[2], col[2]))
    # Define map and return    
    cmap = LinearSegmentedColormap(string, cdict, 256)    
    return cmap

