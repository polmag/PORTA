# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# cache.py                                                           #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Controls cache                                                     #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  12/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.loader
from source.general import *

######################################################################
######################################################################

def read_cache():
    ''' Looks for a cache file and if it exists it reads the last
        data stored
    '''

    global PATH
    PATH = source.general.PATH
    global CONF_def
    CONF_def = source.general.CONF_def
    global CONF
    CONF = source.general.CONF
    global CONF_read
    CONF_read = source.general.CONF_read

    try:
        with open(os.path.join(str(os.getcwd()),'cache', \
                  'cache'), "rb") as cache:
            CACHE = pickle.load(cache)
            for key in CONF_read:
                CONF[key] = CACHE['CONF'][key]
            for key in PATH:
                if CACHE['PATH'][key] is not None:
                    if os.path.isdir(CACHE['PATH'][key]):
                        PATH[key] = CACHE['PATH'][key]
                    else:
                        PATH[key] = None
                else:
                    PATH[key] = None
    except KeyError:
        for key in CONF_def:
            CONF[key] = CONF_def[key]
    except IOError:
        for key in CONF_def:
            CONF[key] = CONF_def[key]
    except EOFError:
        for key in CONF_def:
            CONF[key] = CONF[key]
    except:
        raise

    VARS['format_list'] = plt.gcf().canvas. \
                          get_supported_filetypes().keys()
    if CONF['format'] == 'None':
        if u'eps' in VARS['format_list']:
            CONF['format'] = u'eps'
        elif u'ps' in VARS['format_list']:
            CONF['format'] = u'png'
        elif u'png' in VARS['format_list']:
            CONF['format'] = u'png'
        else:
            CONF['format'] = VARS['format_list'][0]


######################################################################
######################################################################

def write_cache():
    ''' Creates a cache file and stokes the options data
    '''

    global PATH
    PATH = source.general.PATH
    global CONF
    CONF = source.general.CONF

    try:
        with open(os.path.join(str(os.getcwd()),'cache', \
                  'cache'), "wb") as cache:
           pass
           CACHE = {'CONF' : CONF, 'PATH' : PATH}
           pickle.dump(CACHE, cache)
    except IOError:
        pass
    except EOFError:
        pass
    except:
        raise

