/*-*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# extractor.c                                                        #
#                                                                    #
# Tanausú del Pino Alemán                                        #
#   Instituto de Astrofísica de Canarias                           #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# This code to extract data from a pmd and convert it to vtk for     #
# mayavi                                                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Notes:                                                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  05/09/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
####################################################################*/

/* Includes */
#include <complex.h>
#include <limits.h>
#include <math.h>
#include <memory.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Definitions */
#define IS_BIG_ENDIAN ((unsigned char)(*(uint16_t *)"\0\xff" < 0x100))
#define MAGICLEN      8          /**< @brief length of identifier*/
#define MAGICSTRING   "portapmd" /**< @brief identifies*/
#define PMD_VERSION   2          /**< @brief understandable PMD version*/
#define MODULENAMELEN                                                          \
    1023                     /**< @brief expected size of                      \
                                          module name */
#define PMD_COMMENT_LEN 4096 /**< @brief size reserved for comments*/
#define ERR                                                                    \
    {                                                                          \
        fprintf(stderr, "ERROR\n");                                            \
        exit(1);                                                               \
    } /**< @brief Error                                                        \
           handler*/

/* Define 3D vector */
typedef struct t_vec_3d_class {
    double x, y, z;
} t_vec_3d;

/* Define two-level head */
typedef struct t_head_class {
    int nl; /**< @brief number of lower level components */
    int nu; /**< @brief number of upper level components */
} t_head;

/* Define two-level node */
typedef struct t_node_class {
    double eps;        /**< @brief photon destruction probability*/
    double T;          /**< @brief Temperature [K]*/
    double N;          /**< @brief Density of ion [cm^-3]*/
    double Bx, By, Bz; /**< @brief Cartesian components of the
                                   magnetic field vector [G]*/
    double vx, vy, vz; /**< @brief Cartesian components of the
                                   velocity vector [cm/s]*/
    double *rhol;      /**< @brief density matrix lower level*/
    double *rhou;      /**< @brief density matrix upper level*/
    double  J[9];      /**< @brief Radiation field tensors [erg/cm^2/s/Hz]*/
    double  a_voigt;   /**< @brief Damping parameter of Voigt profile*/
    double  d2;        /**< @brief Elastic collisional rate */
    double  eta_c;     /**< @brief Absorptivity of the continuum at the
                                   line center frequency [cm^-1]*/
    double eps_c;      /**< @brief Thermal emissivity of the continuum at
                                   the line center frequency
                                   [erg/cm^3/s/str/Hz]*/
} t_node;

/* Define multi-level node */
typedef struct m_Node_class {
    /* PMD file data */
    double T;          /**< @brief Temperature [K]*/
    double N;          /**< @brief Density of ion [cm^-3]*/
    double Bx, By, Bz; /**< @brief Cartesian components of the
                                   magnetic field vector [G]*/
    double vx, vy, vz; /**< @brief Cartesian components of the
                                   velocity vector [cm/s]*/
    double  vmi;       /**< @brief Microturbulent velocity [cm/s]*/
    double *rho;       /**< @brief Density matrix components*/
    double *J;         /**< @brief Radiation field tensors [erg/cm^2/s/Hz]*/
    double *a_voigt;   /**< @brief Damping parameter of Voigt profile*/
    double *Cul;       /**< @brief Inelastic collisional rate upper->lower
                                   [s^-1]*/
    double *DK;        /**< @brief Depolarizing elastic collisional rate
                                   for each level [s^-1]*/
    double *eta_c;     /**< @brief Absorptivity of the continuum at the
                                   line center frequency [cm^-1]*/
    double *eps_c;     /**< @brief Thermal emissivity of the continuum at
                                   the line center frequency
                                   [erg/cm^3/s/str/Hz]*/
} m_Node;

/* Array of files */
typedef struct files_class {
    FILE *f; /**< @brief file pointer*/
} files;

/* Commons */
int    g_NX, g_NY, g_NZ;                /**< @brief Grid sizes*/
int    g_mod;                           /**< @brief module name index*/
double g_X[8192], g_Y[8192], g_Z[8192]; /**< @brief coordinates*/
char   g_end;                           /**< @brief endianess*/
int    g_int, g_double, g_ver;          /**< @brief byte sizes and version*/
char   g_period[2];                     /**< @brief period*/
double g_dimensions[6];                 /**< @brief [0-2]: size, [3-5]: origin*/
int    g_nxyz[3];                       /**< @brief # of nodes per axes */
int    g_nangles[2]; /**< @brief # of inclination and azimuth angles */
char   g_modname[MODULENAMELEN + 1];   /**< @brief module name*/
char   g_comment[PMD_COMMENT_LEN + 1]; /**< @brief pmd comment*/
int    g_mod_header_size, g_node_size; /**< @brief size of the module
                                                    header and the node
                                                    data size */
t_node **g_plane_low, **g_plane_up;    /**<@brief model planes*/
t_head   g_head;

/*******************************************************************/
/***********************  End of preliminars ***********************/
/*******************************************************************/

/****************************************************************************/
/**
 * Compare two strings
 *
 * @brief Compare strings
 * @param Two strings
 * @return int 0 if different, 1 if equal
 *
 ****************************************************************************/
int checkstring(char *str1, char *str2)
{
    char c1, c2;
    int  out;

    out = 0;

    for (int i = 0; i >= 0; i++) {
        c1 = str1[i];
        c2 = str2[i];

        if (c1 == '\0' || c2 == '\0') {
            if (c1 == '\0' && c2 == '\0') {
                return 1;
            } else if (c1 == '\0' && c2 == ' ') {
                return 1;
            } else if (c1 == ' ' && c2 == '\0') {
                return 1;
            } else {
                return 0;
            }
        }
        if (c1 != c2)
            return 0;
    }
}

/****************************************************************************/
/**
 * Loads the header data from a pmd file
 *
 * @brief Loads header data
 * @param f [in] File to read from
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
int ReadHeader(FILE *f_in)
{
    char   magic[MAGICLEN + 1];
    int    i, j;
    double t;

    /* magic string */
    if (fread(magic, 1, MAGICLEN, f_in) != MAGICLEN)
        ERR;
    magic[MAGICLEN] = 0;
    if (strcmp(magic, MAGICSTRING))
        ERR;

    /* endians */
    if (fread(&g_end, 1, 1, f_in) != 1)
        ERR;

    /* type sizes */
    if (fread(&g_int, 1, 1, f_in) != 1)
        ERR;
    if (g_int != sizeof(int)) {
        fprintf(stderr, "\nSORRY, the integer size is "
                        "incompatible.\n");
        return 0;
    }
    if (fread(&g_double, 1, 1, f_in) != 1)
        ERR;
    if (g_double != sizeof(double)) {
        fprintf(stderr, "\nSORRY, the double-precision size is "
                        "incompatible.\n");
        return 0;
    }
    /* PMD version */
    if (fread(&g_ver, sizeof(int), 1, f_in) != 1)
        ERR;
    if (g_ver != PMD_VERSION) {
        fprintf(stderr, "\nSORRY, the PMD version is "
                        "incompatible.\n");
        return 0;
    }

    /* time: skip */
    for (i = 0; i < 6; i++) {
        int tmp;
        if (fread(&tmp, sizeof(int), 1, f_in) != 1)
            ERR;
    }

    /* periodicity */
    if (fread(g_period, 1, 2, f_in) != 2)
        ERR;

    /* dimensions */
    if (fread(g_dimensions, sizeof(double), 6, f_in) != 6)
        ERR;

    /* number of nodes per axes */
    if (fread(g_nxyz, sizeof(int), 3, f_in) != 3)
        ERR;
    g_NX = g_nxyz[0];
    g_NY = g_nxyz[1];
    g_NZ = g_nxyz[2];

    /* Allocate planes */
    g_plane_low    = (t_node **)malloc(g_NY * sizeof(t_node *));
    g_plane_low[0] = (t_node *)malloc(g_NX * g_NY * sizeof(t_node));
    for (i = 1; i < g_NY; i++)
        g_plane_low[i] = g_plane_low[0] + i * g_NX;

    g_plane_up    = (t_node **)malloc(g_NY * sizeof(t_node *));
    g_plane_up[0] = (t_node *)malloc(g_NX * g_NY * sizeof(t_node));
    for (i = 1; i < g_NY; i++)
        g_plane_up[i] = g_plane_up[0] + i * g_NX;

    /* read coordinates */
    if (fread(g_X, sizeof(double), 8192, f_in) != 8192)
        ERR;
    if (fread(g_Y, sizeof(double), 8192, f_in) != 8192)
        ERR;
    if (fread(g_Z, sizeof(double), 8192, f_in) != 8192)
        ERR;

    /* number of ray angles */
    if (fread(g_nangles, sizeof(int), 2, f_in) != 2)
        ERR;

    /* module name */
    memset(g_modname, 0, MODULENAMELEN + 1);
    if (fread(g_modname, 1, MODULENAMELEN, f_in) != MODULENAMELEN)
        ERR;

    /* comment */
    memset(g_comment, 1, PMD_COMMENT_LEN + 1);
    if (fread(g_comment, 1, PMD_COMMENT_LEN, f_in) != PMD_COMMENT_LEN)
        ERR;

    /* module header size */
    if (fread(&g_mod_header_size, sizeof(int), 1, f_in) != 1)
        ERR;

    /* node data size */
    if (fread(&g_node_size, sizeof(int), 1, f_in) != 1)
        ERR;

    /*                  */
    /* Read module head */
    /*                  */

    /* If two-level */
    if (checkstring(g_modname, "twolevel")) {
        /**********************/
        /*  Two Level header  */
        /*  PMD module header:
        ------------------
        int TwoLevel_VERSION      // version of the module (==1)
        double amass              // atomic mass
        double Aul                // einstein coefficient
        double Eul                // energy diff
        int Jl2                   // double of lower level momentum
        int Ju2                   // double of upper level momentum
        double gl                 // lower level weight
        double gu                 // upper level weight
        double T_ref              // Temp of reference
        int nx                    // number of x mesh points
        int ny                    // number of y mesh points
        double x[nx]              // x-points coordinates
        double y[ny]              // y-points coordinates
        double temp[ny][nx]       // 2D array of kinetic temperature at
                                     the ground (iz=0)
        */

        /* Index of module */
        g_mod = 0;
        /* Version */
        j = fread(&i, 4, 1, f_in);
        /* Mass */
        j = fread(&t, 8, 1, f_in);
        /* Aul */
        j = fread(&t, 8, 1, f_in);
        /* Eul */
        j = fread(&t, 8, 1, f_in);
        /* Jl2, compute size */
        j         = fread(&i, 4, 1, f_in);
        g_head.nl = 2.0 * (i + 1) * (i + 1);
        /* Ju2, compute size */
        j         = fread(&i, 4, 1, f_in);
        g_head.nu = 2.0 * (i + 1) * (i + 1);
        /* gl */
        j = fread(&t, 8, 1, f_in);
        /* gu */
        j = fread(&t, 8, 1, f_in);
        /* T_ref */
        j = fread(&t, 8, 1, f_in);
        /* nx */
        j = fread(&i, 4, 1, f_in);
        /* ny */
        j = fread(&i, 4, 1, f_in);
        /* Read and throw dimensions and T_ground */
        for (i = 0; i < g_NX + g_NY + g_NX * g_NY; i++)
            j = fread(&t, 8, 1, f_in);
        return 1;
    } else {
        fprintf(stderr,
                "\nSORRY, the module %s is not included "
                "in the extractor.c source",
                g_modname);
        return 0;
    }
}

/****************************************************************************/
/**
 * Reads a node from a pmd file from the two-level module
 *
 * @brief Loads node
 * @param f [in] File to read from
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
t_node t_ReadNode(FILE *f, t_node *nd)
{
    /* Grid two-level node data storage:
    ------------------------------------
     double eps;
     double temp;
     double density;
     double Bx;
     double By;
     double Bz;
     double vx;
     double vy;
     double vz;
     double* rhol;
     double* rhou;
     double jkq[9];
     double a_voigt;
     double d2;
     double etac;
     double epsc;
    */

    float  t;
    double d;
    int    i, j;

    /* Photon destruction */
    j = fread(&nd->eps, 8, 1, f);
    /* Temperature */
    j = fread(&nd->T, 8, 1, f);
    /* Density */
    j = fread(&nd->N, 8, 1, f);
    /* Magnetic field */
    j = fread(&nd->Bx, 8, 1, f);
    j = fread(&nd->By, 8, 1, f);
    j = fread(&nd->Bz, 8, 1, f);
    /* velocity */
    j = fread(&nd->vx, 8, 1, f);
    j = fread(&nd->vy, 8, 1, f);
    j = fread(&nd->vz, 8, 1, f);
    /* Lower level density matrix */
    for (i = 0; i < g_head.nl; i++)
        j = fread(&nd->rhol[i], 8, 1, f);
    for (i = 0; i < g_head.nu; i++)
        j = fread(&nd->rhou[i], 8, 1, f);
    for (i = 0; i < 9; i++)
        j = fread(&nd->J[i], 8, 1, f);
    /* Voigt parameter */
    j = fread(&nd->a_voigt, 8, 1, f);
    /* Elastic collisions */
    j = fread(&nd->d2, 8, 1, f);
    /* Continuum absorption */
    j = fread(&nd->eta_c, 8, 1, f);
    /* Continuum emission */
    j = fread(&nd->eps_c, 8, 1, f);
}

/****************************************************************************/
/**
 * Allocates in two-level node
 *
 * @brief Allocates t-l node
 * @param node
 *
 ****************************************************************************/
void allocnode(t_node *nd)
{
    /* Allocate node info */
    if (g_mod == 0) {
        nd->rhol = (double *)malloc(g_head.nl * sizeof(double));
        nd->rhou = (double *)malloc(g_head.nu * sizeof(double));
    }
}

/****************************************************************************/
/**
 * Frees memory in a two-level node
 *
 * @brief Frees t-l node
 * @param node
 *
 ****************************************************************************/
void freenode(t_node *nd)
{
    if (g_mod == 0) {
        free(nd->rhol);
        free(nd->rhou);
    }
}

/****************************************************************************/
/**
 * Reads a plane from a pmd file from the two-level module
 *
 * @brief Loads node
 * @param f [in] File to read from
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
void ReadPlane(FILE *f, t_node **plane)
{
    int x, y;
    if (g_mod == 0) {
        for (y = 0; y < g_NY; y++)
            for (x = 0; x < g_NX; x++) {
                allocnode(&plane[y][x]);
                t_ReadNode(f, &plane[y][x]);
            }
    }
}

/****************************************************************************/
/**
 * Copy between two planes
 *
 * @brief Processes the pmd file
 * @param f [in] File to read from
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
void CopyPlane(t_node **src, t_node **dst)
{
    int m, n;
    for (m = 0; m < g_NY; m++) {
        for (n = 0; n < g_NX; n++) {
            memcpy(&dst[m][n], &src[m][n], sizeof(t_node));
            freenode(&src[m][n]);
        }
    }
}

/****************************************************************************/
/**
 * Completely frees a plane
 *
 * @brief Frees plane
 * @param Plane to free
 *
 ****************************************************************************/
void freeplane(t_node **plane)
{
    int m, n;
    for (m = 0; m < g_NY; m++) {
        for (n = 0; n < g_NX; n++) {
            freenode(&plane[m][n]);
        }
    }
    free(plane);
}

/****************************************************************************/
/**
 * Processes the pmd two-level file
 *
 * @brief Processes the pmd file
 * @param f [in] File to read from
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
void t_Process(FILE *f, int argc, char *argv[])
{
    /* Extract final dimensions */
    int inx, iny, inz, verbose;
    verbose = atoi(argv[2]);
    inx     = atoi(argv[3]);
    iny     = atoi(argv[4]);
    inz     = atoi(argv[5]);

    char   filename[100];
    int    debug = 0;
    int    out[12];
    int    i, j, iz, izp, izps, ixp, ixps, iyp, iyps, iy, ix, cnt, cntx;
    int    cnty, cntz;
    double aux, z1, minz, maxz, dz, dzps;
    double auxxlm, auxxlp, auxxum, auxxup, auxl, auxu;
    double x1, y1, minx, miny, maxx, maxy, dx, dy, dxps, dyps;
    double dnz, dnx, dny;
    double vl00, vl10, vl01, vl11, vu00, vu10, vu01, vu11;
    t_node l00, l10, l01, l11, u00, u10, u01, u11;
    files  F[12];
    files  FJ[9];
    files *Fl;
    files *Fu;

    dnx = inx;
    dny = iny;
    dnz = inz;

    /* Get limits */
    minz = g_Z[0];
    maxz = g_Z[g_NZ - 1] - 1e-5;
    minx = g_X[0];
    maxx = g_X[g_NX - 1] - 1e-5;
    miny = g_Y[0];
    maxy = g_Y[g_NY - 1] - 1e-5;

    /* Get steps */
    dz = (maxz - minz) / (dnz - 1e0);
    dx = (maxx - minx) / (dnx - 1e0);
    dy = (maxy - miny) / (dny - 1e0);

    /* Reference indexes
     0: eps
     1: T
     2: N
     3: B
     4: v
     5: rhol
     6: rhou
     7: J
     8: a_voigt
     9: d2
    10: eta cont
    11: eps cont */

    /* Initialize out */
    for (i = 0; i < 12; i++)
        out[i] = 0.0;

    /* Bool variables to output */
    for (i = 6; i < argc; i++) {
        j      = atoi(argv[i]);
        out[j] = 1;
    }

    /*         */
    /* HEADERS */
    /*         */

    /* Scalars round 1 */
    for (i = 0; i <= 2; i++) {
        if (out[i]) {
            if (i == 0)
                F[i].f = fopen("epsilon.vtk", "w");
            if (i == 1)
                F[i].f = fopen("Temperature.vtk", "w");
            if (i == 2)
                F[i].f = fopen("Density.vtk", "w");

            fprintf(F[i].f, "# vtk DataFile Version 2.0\n");
            if (i == 0)
                fprintf(F[i].f, "Epsilon\n");
            if (i == 1)
                fprintf(F[i].f, "Temperature\n");
            if (i == 2)
                fprintf(F[i].f, "Density\n");
            fprintf(F[i].f, "ASCII\n");
            fprintf(F[i].f, "DATASET STRUCTURED_POINTS\n");
            fprintf(F[i].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
            fprintf(F[i].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                    miny * 1e-8, minz * 1e-8);
            fprintf(F[i].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8, dy * 1e-8,
                    dz * 1e-8);
            fprintf(F[i].f, "POINT_DATA   %d\n", inx * iny * inz);
            fprintf(F[i].f, "SCALARS scalars float\n");
            fprintf(F[i].f, "LOOKUP_TABLE default\n");
        }
    }

    /* Scalars round 2 */
    for (i = 8; i < 12; i++) {
        if (out[i]) {
            if (i == 8)
                F[i].f = fopen("Damping parameter.vtk", "w");
            if (i == 9)
                F[i].f = fopen("Depolarizing coefficient.vtk", "w");
            if (i == 10)
                F[i].f = fopen("Absorptivity continuum.vtk", "w");
            if (i == 11)
                F[i].f = fopen("Emissivity continuum.vtk", "w");
            fprintf(F[i].f, "# vtk DataFile Version 2.0\n");
            if (i == 8)
                fprintf(F[i].f, "Damping parameter\n");
            if (i == 9)
                fprintf(F[i].f, "Depolarizing coefficient\n");
            if (i == 10)
                fprintf(F[i].f, "# Absorptivity continuum\n");
            if (i == 11)
                fprintf(F[i].f, "# Emissivity continuum\n");
            fprintf(F[i].f, "ASCII\n");
            fprintf(F[i].f, "DATASET STRUCTURED_POINTS\n");
            fprintf(F[i].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
            fprintf(F[i].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                    miny * 1e-8, minz * 1e-8);
            fprintf(F[i].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8, dy * 1e-8,
                    dz * 1e-8);
            fprintf(F[i].f, "POINT_DATA   %d\n", inx * iny * inz);
            fprintf(F[i].f, "SCALARS scalars float\n");
            fprintf(F[i].f, "LOOKUP_TABLE default\n");
        }
    }

    /* Vectors */
    for (i = 3; i <= 4; i++) {
        if (out[i]) {
            if (i == 3)
                F[i].f = fopen("B.vtk", "w");
            if (i == 4)
                F[i].f = fopen("V.vtk", "w");
            fprintf(F[i].f, "# vtk DataFile Version 2.0\n");
            if (i == 3)
                fprintf(F[i].f, "# Magnetic field\n");
            if (i == 4)
                fprintf(F[i].f, "# Velocity field\n");
            fprintf(F[i].f, "ASCII\n");
            fprintf(F[i].f, "DATASET STRUCTURED_POINTS\n");
            fprintf(F[i].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
            fprintf(F[i].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                    miny * 1e-8, minz * 1e-8);
            fprintf(F[i].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8, dy * 1e-8,
                    dz * 1e-8);
            fprintf(F[i].f, "POINT_DATA   %d\n", inx * iny * inz);
            if (i == 3)
                fprintf(F[i].f, "VECTORS B float\n");
            if (i == 4)
                fprintf(F[i].f, "VECTORS V float\n");
        }
    }

    /* rhol */
    if (out[5]) {
        Fl = (files *)malloc(g_head.nl * sizeof(files));

        /* Initialize index */
        j = -1;
        for (int K = 0; K < 100; K++) {
            for (int Q = -K; Q <= K; Q++) {
                for (int R = 1; R >= 0; R--) {
                    /* Advance index */
                    j++;
                    if (R == 1) {
                        sprintf(filename,
                                "Lower level density matrix "
                                "K=%d Q=%d  Real.vtk",
                                K, Q);
                    } else {
                        sprintf(filename,
                                "Lower level density matrix "
                                "K=%d Q=%d  Imag.vtk",
                                K, Q);
                    }
                    Fl[j].f = fopen(filename, "w");
                    fprintf(Fl[j].f, "# vtk DataFile Version 2.0\n");
                    fprintf(Fl[j].f, "Lower level density matrix\n");
                    fprintf(Fl[j].f, "ASCII\n");
                    fprintf(Fl[j].f, "DATASET STRUCTURED_POINTS\n");
                    fprintf(Fl[j].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
                    fprintf(Fl[j].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                            miny * 1e-8, minz * 1e-8);
                    fprintf(Fl[j].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8,
                            dy * 1e-8, dz * 1e-8);
                    fprintf(Fl[j].f, "POINT_DATA   %d\n", inx * iny * inz);
                    fprintf(Fl[j].f, "SCALARS scalars float\n");
                    fprintf(Fl[j].f, "LOOKUP_TABLE default\n");
                }
            }
            if (j + 1 >= g_head.nl)
                break;
        }
    }

    /* rhou */
    if (out[6]) {
        Fu = (files *)malloc(g_head.nu * sizeof(files));

        /* Initialize index */
        j = -1;
        for (int K = 0; K < 100; K++) {
            for (int Q = -K; Q <= K; Q++) {
                for (int R = 1; R >= 0; R--) {
                    /* Advance index */
                    j++;
                    if (R == 1) {
                        sprintf(filename,
                                "Upper level density matrix "
                                "K=%d Q=%d  Real.vtk",
                                K, Q);
                    } else {
                        sprintf(filename,
                                "Upper level density matrix "
                                "K=%d Q=%d  Imag.vtk",
                                K, Q);
                    }
                    Fu[j].f = fopen(filename, "w");
                    fprintf(Fu[j].f, "# vtk DataFile Version 2.0\n");
                    fprintf(Fu[j].f, "Upper level density matrix\n");
                    fprintf(Fu[j].f, "ASCII\n");
                    fprintf(Fu[j].f, "DATASET STRUCTURED_POINTS\n");
                    fprintf(Fu[j].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
                    fprintf(Fu[j].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                            miny * 1e-8, minz * 1e-8);
                    fprintf(Fu[j].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8,
                            dy * 1e-8, dz * 1e-8);
                    fprintf(Fu[j].f, "POINT_DATA   %d\n", inx * iny * inz);
                    fprintf(Fu[j].f, "SCALARS scalars float\n");
                    fprintf(Fu[j].f, "LOOKUP_TABLE default\n");
                }
            }
            if (j + 1 >= g_head.nu)
                break;
        }
    }

    /* J */
    if (out[7]) {
        /* Initialize index */
        j = -1;
        for (int K = 0; K <= 2; K++) {
            for (int Q = 0; Q <= K; Q++) {
                for (int R = 1; R >= 0; R--) {
                    /* Skip non-existing imaginary parts */
                    if (Q == 0 && R == 0)
                        continue;

                    /* Advance index */
                    j++;
                    if (R == 1) {
                        sprintf(filename,
                                "Radiation field "
                                "K=%d Q=%d  Real.vtk",
                                K, Q);
                    } else {
                        sprintf(filename,
                                "Radiation field "
                                "K=%d Q=%d  Imag.vtk",
                                K, Q);
                    }
                    FJ[j].f = fopen(filename, "w");
                    fprintf(FJ[j].f, "# vtk DataFile Version 2.0\n");
                    fprintf(FJ[j].f, "Radiation field\n");
                    fprintf(FJ[j].f, "ASCII\n");
                    fprintf(FJ[j].f, "DATASET STRUCTURED_POINTS\n");
                    fprintf(FJ[j].f, "DIMENSIONS %d %d %d \n", inx, iny, inz);
                    fprintf(FJ[j].f, "ORIGIN    %e   %e   %e\n", minx * 1e-8,
                            miny * 1e-8, minz * 1e-8);
                    fprintf(FJ[j].f, "SPACING    %e   %e   %e\n\n", dx * 1e-8,
                            dy * 1e-8, dz * 1e-8);
                    fprintf(FJ[j].f, "POINT_DATA   %d\n", inx * iny * inz);
                    fprintf(FJ[j].f, "SCALARS scalars float\n");
                    fprintf(FJ[j].f, "LOOKUP_TABLE default\n");
                }
            }
        }
    }

    /* Read first two planes */
    ReadPlane(f, g_plane_low);
    ReadPlane(f, g_plane_up);

    /*                      */
    /* Read and interpolate */
    /*                      */

    /* Initialize indexes */
    izps = 1;
    cnt  = 0;
    cntz = 0;

    /* For each height in the pmd */
    for (iz = 0; iz < g_NZ - 1; iz++) {
        if (debug) {
            fprintf(stderr, "iz: %d, z: %e, z+: %e\n", iz, g_Z[iz],
                    g_Z[iz + 1]);
        }

        /* Initialize for first point in interpolated grid (Z) */
        dzps = izps - 1e0;
        z1   = minz - dz + dz * dzps;

        /* Initialize counters */
        cnty = 0;
        cntx = 0;

        /* For each height in the interpolated grid */
        for (izp = izps - 1; izp < inz; izp++) {
            /* Update last initial izp */
            izps = izp;

            /* Advance interpolate grid (Z) */
            z1 = z1 + dz;

            if (debug) {
                fprintf(stderr, "izp: %d, zp: %e\n", izp, z1);
            }

            /* If within the two read planes */
            if (g_Z[iz] <= z1 && g_Z[iz + 1] > z1) {
                /* Advance counter */
                ++cntz;

                /* Compute linear weights */
                double w1 = (g_Z[iz + 1] - z1) / (g_Z[iz + 1] - g_Z[iz]);
                double w2 = 1.0 - w1;

                if (debug) {
                    fprintf(stderr,
                            "izp: %d, iz:%d, count: %d, "
                            "zp: %e between z: %e and z: %e\n",
                            izp, iz, cnt, z1, g_Z[iz], g_Z[iz + 1]);
                }

                /* Initialize indexes */
                iyps = 1;
                cnty = 0;

                /* For each Y in the plane */
                for (iy = 0; iy < g_NY - 1; iy++) {
                    /* Initialize for first point in
                       interpolated grid (Y) */
                    dyps = iyps - 1e0;
                    y1   = miny - dy + dy * dyps;

                    /* For each Y in interpolated grid */
                    for (iyp = iyps - 1; iyp < iny; iyp++) {
                        /* Update last initial iyp */
                        iyps = iyp;

                        /* Advance interpolate grid (Y) */
                        y1 = y1 + dy;

                        /* Initialize counter */
                        cntx = 0;

                        /* If interpolated Y within limits */
                        if (g_Y[iy] <= y1 && g_Y[iy + 1] > y1) {
                            /* Advance counter */
                            ++cnty;

                            /* Compute linear weights */
                            double w1y =
                                (g_Y[iy + 1] - y1) / (g_Y[iy + 1] - g_Y[iy]);
                            double w2y = 1.0 - w1y;

                            /* Initialize indexes */
                            ixps = 1;
                            cntx = 0;

                            /* For each X in the plane */
                            for (ix = 0; ix < g_NX - 1; ix++) {
                                /* Initialize for first point in
                                   interpolated grid (X) */
                                dxps = ixps - 1e0;
                                x1   = minx - dx + dx * dxps;

                                /* For each X in interpolated grid */
                                for (ixp = ixps - 1; ixp < inx; ixp++) {
                                    /* Update last initial ixp */
                                    ixps = ixp;

                                    /* Advance interpolate grid (X) */
                                    x1 = x1 + dx;

                                    /* If interpolated X within
                                       limits */
                                    if (g_X[ix] <= x1 && g_X[ix + 1] > x1) {
                                        /* Advance counters */
                                        ++cntx;
                                        ++cnt;

                                        /* Compute linear weights */
                                        double w1x = (g_X[ix + 1] - x1) /
                                                     (g_X[ix + 1] - g_X[ix]);
                                        double w2x = 1.0 - w1x;

                                        /* Reset indentation */

                                        /* Pointers */
                                        l00 = g_plane_low[iy][ix];
                                        l10 = g_plane_low[iy + 1][ix];
                                        l01 = g_plane_low[iy][ix + 1];
                                        l11 = g_plane_low[iy + 1][ix + 1];
                                        u00 = g_plane_up[iy][ix];
                                        u10 = g_plane_up[iy + 1][ix];
                                        u01 = g_plane_up[iy][ix + 1];
                                        u11 = g_plane_up[iy + 1][ix + 1];

                                        /* Scalars round 1 */
                                        for (i = 0; i < 3; i++) {
                                            if (out[i] == 0)
                                                continue;

                                            if (i == 0) {
                                                vl00 = l00.eps;
                                                vl10 = l10.eps;
                                                vl01 = l01.eps;
                                                vl11 = l11.eps;
                                                vu00 = u00.eps;
                                                vu10 = u10.eps;
                                                vu01 = u01.eps;
                                                vu11 = u11.eps;
                                            } else if (i == 1) {
                                                vl00 = l00.T;
                                                vl10 = l10.T;
                                                vl01 = l01.T;
                                                vl11 = l11.T;
                                                vu00 = u00.T;
                                                vu10 = u10.T;
                                                vu01 = u01.T;
                                                vu11 = u11.T;
                                            } else if (i == 2) {
                                                vl00 = l00.N;
                                                vl10 = l10.N;
                                                vl01 = l01.N;
                                                vl11 = l11.N;
                                                vu00 = u00.N;
                                                vu10 = u10.N;
                                                vu01 = u01.N;
                                                vu11 = u11.N;
                                            }

                                            auxxlm = w1y * vl00 + w2y * vl10;
                                            auxxlp = w1y * vl01 + w2y * vl11;
                                            auxxum = w1y * vu00 + w2y * vu10;
                                            auxxup = w1y * vu01 + w2y * vu11;
                                            auxl = w1x * auxxlm + w2x * auxxlp;
                                            auxu = w1x * auxxum + w2x * auxxup;
                                            aux  = w1 * auxl + w2 * auxu;
                                            fprintf(F[i].f, "%8.8e ", aux);
                                        }

                                        /* Scalars round 2 */
                                        for (i = 8; i < 12; i++) {
                                            if (out[i] == 0)
                                                continue;

                                            if (i == 8) {
                                                vl00 = l00.a_voigt;
                                                vl10 = l10.a_voigt;
                                                vl01 = l01.a_voigt;
                                                vl11 = l11.a_voigt;
                                                vu00 = u00.a_voigt;
                                                vu10 = u10.a_voigt;
                                                vu01 = u01.a_voigt;
                                                vu11 = u11.a_voigt;
                                            } else if (i == 9) {
                                                vl00 = l00.d2;
                                                vl10 = l10.d2;
                                                vl01 = l01.d2;
                                                vl11 = l11.d2;
                                                vu00 = u00.d2;
                                                vu10 = u10.d2;
                                                vu01 = u01.d2;
                                                vu11 = u11.d2;
                                            } else if (i == 10) {
                                                vl00 = l00.eta_c;
                                                vl10 = l10.eta_c;
                                                vl01 = l01.eta_c;
                                                vl11 = l11.eta_c;
                                                vu00 = u00.eta_c;
                                                vu10 = u10.eta_c;
                                                vu01 = u01.eta_c;
                                                vu11 = u11.eta_c;
                                            } else if (i == 11) {
                                                vl00 = l00.eps_c;
                                                vl10 = l10.eps_c;
                                                vl01 = l01.eps_c;
                                                vl11 = l11.eps_c;
                                                vu00 = u00.eps_c;
                                                vu10 = u10.eps_c;
                                                vu01 = u01.eps_c;
                                                vu11 = u11.eps_c;
                                            }

                                            auxxlm = w1y * vl00 + w2y * vl10;
                                            auxxlp = w1y * vl01 + w2y * vl11;
                                            auxxum = w1y * vu00 + w2y * vu10;
                                            auxxup = w1y * vu01 + w2y * vu11;
                                            auxl = w1x * auxxlm + w2x * auxxlp;
                                            auxu = w1x * auxxum + w2x * auxxup;
                                            aux  = w1 * auxl + w2 * auxu;
                                            fprintf(F[i].f, "%8.8e ", aux);
                                        }

                                        /* B */
                                        if (out[3]) {
                                            for (j = 0; j < 3; j++) {
                                                if (j == 0) {
                                                    vl00 = l00.Bx;
                                                    vl10 = l10.Bx;
                                                    vl01 = l01.Bx;
                                                    vl11 = l11.Bx;
                                                    vu00 = u00.Bx;
                                                    vu10 = u10.Bx;
                                                    vu01 = u01.Bx;
                                                    vu11 = u11.Bx;
                                                } else if (j == 1) {
                                                    vl00 = l00.By;
                                                    vl10 = l10.By;
                                                    vl01 = l01.By;
                                                    vl11 = l11.By;
                                                    vu00 = u00.By;
                                                    vu10 = u10.By;
                                                    vu01 = u01.By;
                                                    vu11 = u11.By;
                                                } else if (j == 2) {
                                                    vl00 = l00.Bz;
                                                    vl10 = l10.Bz;
                                                    vl01 = l01.Bz;
                                                    vl11 = l11.Bz;
                                                    vu00 = u00.Bz;
                                                    vu10 = u10.Bz;
                                                    vu01 = u01.Bz;
                                                    vu11 = u11.Bz;
                                                }

                                                auxxlm =
                                                    w1y * vl00 + w2y * vl10;
                                                auxxlp =
                                                    w1y * vl01 + w2y * vl11;
                                                auxxum =
                                                    w1y * vu00 + w2y * vu10;
                                                auxxup =
                                                    w1y * vu01 + w2y * vu11;
                                                auxl =
                                                    w1x * auxxlm + w2x * auxxlp;
                                                auxu =
                                                    w1x * auxxum + w2x * auxxup;
                                                aux = w1 * auxl + w2 * auxu;
                                                fprintf(F[3].f, "%8.8e ", aux);
                                            }
                                        }

                                        /* V */
                                        if (out[4]) {
                                            for (j = 0; j < 3; j++) {
                                                if (j == 0) {
                                                    vl00 = l00.vx;
                                                    vl10 = l10.vx;
                                                    vl01 = l01.vx;
                                                    vl11 = l11.vx;
                                                    vu00 = u00.vx;
                                                    vu10 = u10.vx;
                                                    vu01 = u01.vx;
                                                    vu11 = u11.vx;
                                                } else if (j == 1) {
                                                    vl00 = l00.vy;
                                                    vl10 = l10.vy;
                                                    vl01 = l01.vy;
                                                    vl11 = l11.vy;
                                                    vu00 = u00.vy;
                                                    vu10 = u10.vy;
                                                    vu01 = u01.vy;
                                                    vu11 = u11.vy;
                                                } else if (j == 2) {
                                                    vl00 = l00.vz;
                                                    vl10 = l10.vz;
                                                    vl01 = l01.vz;
                                                    vl11 = l11.vz;
                                                    vu00 = u00.vz;
                                                    vu10 = u10.vz;
                                                    vu01 = u01.vz;
                                                    vu11 = u11.vz;
                                                }

                                                auxxlm =
                                                    w1y * vl00 + w2y * vl10;
                                                auxxlp =
                                                    w1y * vl01 + w2y * vl11;
                                                auxxum =
                                                    w1y * vu00 + w2y * vu10;
                                                auxxup =
                                                    w1y * vu01 + w2y * vu11;
                                                auxl =
                                                    w1x * auxxlm + w2x * auxxlp;
                                                auxu =
                                                    w1x * auxxum + w2x * auxxup;
                                                aux = w1 * auxl + w2 * auxu;
                                                fprintf(F[4].f, "%8.8e ", aux);
                                            }
                                        }

                                        /* rhol */
                                        if (out[5]) {
                                            for (j = 0; j < g_head.nl; j++) {
                                                auxxlm = w1y * l00.rhol[j] +
                                                         w2y * l10.rhol[j];
                                                auxxlp = w1y * l01.rhol[j] +
                                                         w2y * l11.rhol[j];
                                                auxxum = w1y * u00.rhol[j] +
                                                         w2y * u10.rhol[j];
                                                auxxup = w1y * u01.rhol[j] +
                                                         w2y * u11.rhol[j];
                                                auxl =
                                                    w1x * auxxlm + w2x * auxxlp;
                                                auxu =
                                                    w1x * auxxum + w2x * auxxup;
                                                aux = w1 * auxl + w2 * auxu;
                                                fprintf(Fl[j].f, "%8.8e ", aux);
                                            }
                                        }

                                        /* rhou */
                                        if (out[6]) {
                                            for (j = 0; j < g_head.nu; j++) {
                                                auxxlm = w1y * l00.rhou[j] +
                                                         w2y * l10.rhou[j];
                                                auxxlp = w1y * l01.rhou[j] +
                                                         w2y * l11.rhou[j];
                                                auxxum = w1y * u00.rhou[j] +
                                                         w2y * u10.rhou[j];
                                                auxxup = w1y * u01.rhou[j] +
                                                         w2y * u11.rhou[j];
                                                auxl =
                                                    w1x * auxxlm + w2x * auxxlp;
                                                auxu =
                                                    w1x * auxxum + w2x * auxxup;
                                                aux = w1 * auxl + w2 * auxu;
                                                fprintf(Fu[j].f, "%8.8e ", aux);
                                            }
                                        }

                                        /* J */
                                        if (out[7]) {
                                            for (j = 0; j < 9; j++) {
                                                auxxlm = w1y * l00.J[j] +
                                                         w2y * l10.J[j];
                                                auxxlp = w1y * l01.J[j] +
                                                         w2y * l11.J[j];
                                                auxxum = w1y * u00.J[j] +
                                                         w2y * u10.J[j];
                                                auxxup = w1y * u01.J[j] +
                                                         w2y * u11.J[j];
                                                auxl =
                                                    w1x * auxxlm + w2x * auxxlp;
                                                auxu =
                                                    w1x * auxxum + w2x * auxxup;
                                                aux = w1 * auxl + w2 * auxu;
                                                fprintf(FJ[j].f, "%8.8e ", aux);
                                            }
                                        }

                                        /*                    */
                                        /* Recover identation */
                                        /*                    */
                                    }

                                    /* If out of node, break */
                                    if (x1 >= g_X[ix + 1]) {
                                        break;
                                    }
                                }
                            }
                        }
                        /* If out of node, break */
                        if (y1 >= g_Y[iy + 1]) {
                            break;
                        }
                    }
                }
            }
            /* If out of node, break */
            if (z1 >= g_Z[iz + 1]) {
                break;
            }
        }

        /* Print progress */
        if (verbose) {
            fprintf(stderr,
                    "iz = %d/%d (count: "
                    "%d/%d|%d/%d|%d/%d|%d/%d)\n",
                    iz, g_NZ - 1, cntx, inx, cnty, iny, cntz, inz, cnt,
                    inx * iny * inz);
        }

        if (iz > g_NZ - 3) {
            break;
        }

        /* Copy plane up to low */
        CopyPlane(g_plane_up, g_plane_low);
        /* Read new plane */
        ReadPlane(f, g_plane_up);
    }

    /* Close files */
    for (i = 0; i < 12; i++) {
        if (out[i]) {
            if (i <= 4 && i >= 8) {
                fclose(F[i].f);
            } else if (i == 5) {
                for (j = 0; j < g_head.nl; j++)
                    fclose(Fl[j].f);
            } else if (i == 6) {
                for (j = 0; j < g_head.nu; j++)
                    fclose(Fu[j].f);
            } else if (i == 7) {
                for (j = 0; j < 9; j++)
                    fclose(FJ[j].f);
            }
        }
    }

    /* Free memory */
    if (out[5])
        free(Fl);
    if (out[6])
        free(Fu);

    return;
}

/*******************************************************************/
/*******************************************************************/
/*******************************************************************/

/****************************************************************************/
/**
 * Main
 *
 * @brief Main routine
 * @return int 0 if success, 1 if failed
 *
 ****************************************************************************/
void main(int argc, char *argv[])
{
    FILE *f;

    /* Check arguments */
    if (argc < 7) {
        fprintf(stderr, "SYNTAX: extractor file.pmd nx ny nz *vars");
        return;
    }

    /* Open pmd file */
    if (!(f = fopen(argv[1], "rb"))) {
        fprintf(stderr, "ERROR: cannot open file %s\n", argv[1]);
        return;
    }

    /* Read Header */
    ReadHeader(f);

    /*              */
    /* Process file */
    /*              */
    t_Process(f, argc, argv);

    /* Free planes */
    /*freeplane(g_plane_low);
      freeplane(g_plane_up);*/

    return;
}
