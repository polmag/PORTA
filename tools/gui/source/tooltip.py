# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# tooltip.py                                                         #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Class to show tooltips                                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Source code from:                                                  #
# http://www.voidspace.org.uk/python/weblog/arch_d7_2006_07_01.shtml #
# How to use:                                                        #
#    createToolTip(widget, "This is a tooltip")                      #
#    widget.bind('<Return>', command)                                #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  22/03/2018 - V1.0.0 - Deactivate the tooltips. (TdPA)             #
#                                                                    #
#  12/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

import source.loader
from source.general import *

######################################################################
######################################################################

class ToolTip(object):


    def __init__(self, widget):
        ''' Initialization
        '''

        global CONF
        CONF = source.general.CONF

        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0
        self.cfont = CONF['cfont_size']


    def showtip(self, text):
        ''' Display text in tooltip window
        '''

        return
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 27
        y = y + cy + self.widget.winfo_rooty() +27
        self.tipwindow = tw = Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        try:
            # For Mac OS
            tw.tk.call("::tk::unsupported::MacWindowStyle",
            "style", tw._w,
            "help", "noActivates")
        except TclError:
            pass
        label = Label(tw, text=self.text, justify=LEFT,
        background="#ffffe0", relief=SOLID, borderwidth=1,
        font=("tahoma", self.cfont, "normal"))
        label.pack(ipadx=1)


    def hidetip(self):
        ''' Remove the tooltip
        '''

        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()


def createToolTip(widget, text):
    ''' Create the tooltip
    '''

    return
    toolTip = ToolTip(widget)


    def enter(event):
        ''' Show the tooltip
        '''

        return
        toolTip.showtip(text)


    def leave(event):
        ''' Stop showing the tooltip
        '''

        return
        toolTip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)

