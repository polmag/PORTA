# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# loader.py                                                          #
#                                                                    #
# Tanausú del Pino Alemán                                            #
#   Instituto de Astrofísica de Canarias                             #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Import packages for the GUI                                        #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  20/08/2019 - V1.0.4 - Compatibility with old library versions     #
#                        regarding NaviationToolbar2Tk (TdPA)        #
#                                                                    #
#  31/05/2019 - V1.0.3 - The matplotlib package in ubuntu is version #
#                        2.1.1, so I am adding a backup import for   #
#                        outdated matplotlib packages (TdPA)         #
#                                                                    #
#  20/05/2019 - V1.0.2 - NavigationToolbar2TkAgg has been deprecated #
#                        since v 2.2 of Matplotlib. Moving back to   #
#                        NavigationToolbar2Tk (AdV)                  #
#                                                                    #
#  24/04/2018 - V1.0.1 - Compatibility with python 3+. (TdPA)        #
#                                                                    #
#  12/03/2018 - V1.0.0 - First Version. (TdPA)                       #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

import sys, os, struct
import numpy as np
from scipy import interpolate
import matplotlib
#import FileDialog
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
try:
    from matplotlib.backends.backend_tkagg import \
                                           NavigationToolbar2Tk as \
                                           NavigationToolbar
except ImportError:
    from matplotlib.backends.backend_tkagg import \
                                          NavigationToolbar2TkAgg as \
                                          NavigationToolbar
except:
    raise
from matplotlib.backend_bases import key_press_handler
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import ScalarFormatter
from matplotlib.ticker import FormatStrFormatter
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
if sys.version_info[0] < 3:
    # for Python2
    from ttk import *
    from Tkinter import *
    import tkFont
    import cPickle as pickle
    import tkMessageBox, tkFileDialog
else:
    # for Python3
    from tkinter import ttk
    from tkinter import *
    import tkinter.font as tkFont
    import pickle as pickle
    import tkinter.messagebox as tkMessageBox
    import tkinter.filedialog as tkFileDialog
    Progressbar = ttk.Progressbar
import base64
import subprocess
import copy
import pyvtk as vtk
