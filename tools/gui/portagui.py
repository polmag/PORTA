# -*- coding: utf-8 -*-

######################################################################
######################################################################
######################################################################
#                                                                    #
# portagui.py                                                        #
#                                                                    #
# Tanausú del Pino Alemán                                            #
# Ángel de Vicente                                                   #
#   Instituto de Astrofísica de Canarias                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# This code is a GUI to visualize the PORTA files                    #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
# Notes:                                                             #
#                                                                    #
######################################################################
######################################################################
#                                                                    #
#  10/03/2020 - V1.0.2 - Added colors to indicate focus (TdPA)       #
#                      - Solved focus problems (AdV)                 #
#                                                                    #
#  22/03/2018 - V1.0.1 - Added settings. Now you can change the font #
#                        size of the GUI. (TdPA)                     #
#                                                                    #
#  12/03/2018 - V1.0.0 - First version.  (TdPA)                      #
#                                                                    #
#  07/03/2018 - V0.0.0 - Start Code. (TdPA)                          #
#                                                                    #
######################################################################
######################################################################
######################################################################

from source.loader import *
from source.cache import *
from source.general import *
from source.tooltip import *
from source.colors import *
from source.pmd import *
from source.psp import *
from source.settings import *

######################################################################
######################################################################
######################################################################


def confirm_quit():
    ''' Function that handles the close protocol when the closing
        button of the window is pressed
    '''

    if tkMessageBox.askokcancel("Close", \
                                "Do you really want to close?"):
        write_cache()
        root.destroy()
        root.quit()


######################################################################
######################################################################


class GUI_class:
    ''' Class that defines the main body of the GUI, where you can
        call the different GUI applications
    '''

    def __init__(self, master):
        ''' Initializes the widget tree for the main GUI window
        '''

        global CONF
        CONF = source.general.CONF

        # Creates a font to use in the widget
        self.cFont = tkFont.Font(family="Helvetica", \
                                 size=CONF['wfont_size'])

        # Frame master
        self.master = master
        self.master.configure(background=CONF['col_act'])
        self.toolbar = Frame(master)

        # Create the buttons layout
        self.buttons(self.toolbar)

        self.toolbar.pack(side=TOP, fill=BOTH, expand=1, \
                          padx=CONF['b_pad'], pady=CONF['b_pad'])

######################################################################
######################################################################
######################################################################
######################################################################

    def quit(self, instance):
        ''' Method that handles the Close button. Just closes the GUI
        '''

        if tkMessageBox.askokcancel("Close", \
                                    "Do you really want to close?", \
                                    parent=self):
            write_cache()
            instance.destroy()

######################################################################
######################################################################

    def close(self):
        ''' Method that handles the Close button. Just closes the GUI
        '''

        write_cache()
        self.toolbar.quit()

######################################################################
######################################################################
######################################################################
######################################################################

    def buttons(self, box):
        ''' Defines the control buttons of the widget
        '''

        b = Button(self.toolbar, text="Visualize pmd files", \
                   width=40, font=self.cFont, command=self.pmd)
        createToolTip(b, "Model atmosphere visualizer")
        b.pack(fill=BOTH, padx=2, expand=1)
        b = Button(self.toolbar, text="Visualize psp files", \
                   width=40, font=self.cFont, command=self.psp)
        createToolTip(b, "Emergent Stokes visualizer")
        b.pack(fill=BOTH, padx=2, expand=1)
        b = Button(self.toolbar, text="Settings", \
                   width=40, font=self.cFont, command=self.settings)
        createToolTip(b, "Change general GUI settings")
        b.pack(fill=BOTH, padx=2, expand=1)
        b = Button(self.toolbar, text="Close", width=40, \
                   font=self.cFont, command=self.close)
        createToolTip(b, "Exit the GUI application")
        b.pack(fill=BOTH, padx=2, expand=1)

######################################################################
######################################################################
######################################################################
######################################################################

    def pmd(self):
        ''' Method that handles the button PMD. It calls a
            support window of the PMD_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Make current passive
        self.master.config(background=CONF['col_pas'])

        # Calls the input window
        pmd = PMD_class(self.master,title='PMD View')

        # Make current active
        self.master.configure(background=CONF['col_act'])

######################################################################
######################################################################

    def psp(self):
        ''' Method that handles the button PSP. It calls a
            support window of the PSP_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Make current passive
        self.master.config(background=CONF['col_pas'])

        psp = PSP_class(self.master,title='PSP View')

        # Make current active
        self.master.configure(background=CONF['col_act'])

######################################################################
######################################################################

    def settings(self):
        ''' Method that handles the button Settings. It calls a
            support window of the SETTINGS_class class.
        '''

        global CONF
        CONF = source.general.CONF

        # Current size
        pfsize = CONF['wfont_size']
        pbsize = CONF['b_pad']

        # Make current passive
        self.master.config(background=CONF['col_pas'])

        # Calls the settings window
        settings = SETTINGS_class(self.master,title='Options')

        # Make current active
        self.master.configure(background=CONF['col_act'])

        # New size
        nfsize = CONF['wfont_size']
        nbsize = CONF['b_pad']

        # If size changed
        if pfsize != nfsize or pbsize != nbsize:

            if pfsize != nfsize:
                # Creates a font to use in the widget
                self.cFont = tkFont.Font(family="Helvetica", \
                                         size=CONF['wfont_size'])
                self.master.option_add('*Dialog.msg.font', \
                                       'Helvetica ' + \
                                       str(CONF['wfont_size']))
            self.redraw()

######################################################################
######################################################################
######################################################################
######################################################################

    def redraw(self):
        ''' Redraws the whole window
        '''

        global CONF
        CONF = source.general.CONF

        self.toolbar.destroy()
        self.toolbar = Frame(self.master)
        self.buttons(self.toolbar)
        self.toolbar.pack(side=TOP, fill=BOTH, expand=1, \
                          padx=CONF['b_pad'], pady=CONF['b_pad'])


######################################################################
############################ MAIN ####################################
######################################################################

# Initialize Tkinter and the close window protocol
root = Tk()
root.wm_title('PORTA GUI')
root.protocol("WM_DELETE_WINDOW", confirm_quit)

# Defines a custom color map
plt.register_cmap(cmap = color_def([(0, 0, 255), (255, 255, 0), \
                                    (255, 0, 0), (255, 255, 255)], \
                                   'RedYellowBlue', \
                                   preleve = [ 0, 1./3., 2./3., 1], \
                                   minv = 0, maxv = 1))

read_cache()

# Run the GUI
GUI = GUI_class(root)
# Set the fonts of the dialogs
root.option_add('*Dialog.msg.font', 'Helvetica ' + \
                str(CONF['wfont_size']))
root.mainloop()

