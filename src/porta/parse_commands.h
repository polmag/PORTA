#ifndef PARSE_COMMANDS_H
#define PARSE_COMMANDS_H

#define MAX_ARGS 64
#define N_COMMANDS 14

static const char cCommand[N_COMMANDS][MAX_IDEN_LEN + 1] = {
    "help",
    "echo",
    "set",
    "get",
    "version",
    "exit",
    "fs_surface",    /* performs FS along a general ray direction for all
                        frequencies and stores the emergent radiation into a
                        PSP disk file */
    "fs_clv_freq",      /* FS for calculation of the center-to-limb variation at
                           a given discrete frequency of radiation, spatially
                           averaged WARNING: ONLY FOR HORZIONTALLY EQUIDISTANT
                           GRIDS! */
    "fs_clv_freq_av",   /* FS for calculation of the center-to-limb variation at
                           a given discrete frequency of radiation, spatially
                           averaged, azimuth averaged WARNING: ONLY FOR
                           HORZIONTALLY EQUIDISTANT GRIDS! */
    "fs_azaver",         /* FS averaged over all grid points and all azimuths;
                            results in a single stokes profile */
    "get_tau_pos",       /* for a general ray direction calculates geometrical
                            coordinates of points with a given tau distance from
                            a surface (izmax) nodes; stores the data in a *.TAU
                            file */
    "solve_jacobi",      /* solve the non-LTE problem in the finest grid using
                            Jacobi iteration */
    "savemodel",         /* save the curent model into a file */
    "loadmodel"         /* load a file (PMD or HDF5) */
};


/* abreviations of the commands (must be consistent with cCommand!) */
enum cCommandID {
    CC_HELP = 0,
    CC_ECHO,
    CC_SET,
    CC_GET,
    CC_VERSION,
    CC_EXIT,
    CC_FSSURFACE,
    CC_FSCLVFREQ,
    CC_FSCLVFREQAV,
    CC_FSAZAVER,
    CC_GETTAU,
    CC_SOLVE_JACOBI,
    CC_SAVEMODEL,
    CC_LOADMODEL,
};


extern int argc;
extern char *argv[MAX_ARGS];
extern char tmps_module[MAX_STRING+1];
static  char sep[5]  = " -- ";

/*   CC_GETTAU   */
/*****************/
static struct option tau_long_options[] = {
  {"io",       required_argument,  0,  'o' },
  {"tau",      required_argument,  0,  't' },
  {"incl",     required_argument,  0,  'i' },
  {"azim",     required_argument,  0,  'a' },
  {"fr_i_min", required_argument,  0,  'm' },
  {"fr_i_max", required_argument,  0,  'M' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char tau_short_options[] = "o:t:i:a:m:M:f:";

typedef struct Stau_ps {
  char o[MAX_STRING+1],f[MAX_STRING+1];
  double t,i,a;
  int m,M;
} Stau_ps;

extern Stau_ps tau_ps;

/*   CC_FSSURFACE   */
/********************/
static struct option surface_long_options[] = {
  {"io",       required_argument,  0,  'o' },
  {"method",   required_argument,  0,  'c' },
  {"incl",     required_argument,  0,  'i' },
  {"azim",     required_argument,  0,  'a' },
  {"fr_i_min", required_argument,  0,  'm' },
  {"fr_i_max", required_argument,  0,  'M' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char surface_short_options[] = "o:c:i:a:m:M:f:";

typedef struct Ssurface_ps {
  char o[MAX_STRING+1],f[MAX_STRING+1],c[MAX_STRING+1];
  double i,a;
  int m,M;
} Ssurface_ps;

extern Ssurface_ps surface_ps;

/*   CC_SOLVE_JACOBI   */
/***********************/
static struct option jacobi_long_options[] = {
  {"max_it",   required_argument,  0,  'i' },
  {"max_rc",   required_argument,  0,  'c' },
  {0,          0,                  0,  0   }
};

static char jacobi_short_options[] = "i:c:";

typedef struct Sjacobi_ps {
  double c;
  int i;
} Sjacobi_ps;

extern Sjacobi_ps jacobi_ps;

/*   CC_LOADMODEL || CC_SAVEMODEL  */
/***********************************/
static struct option model_io_long_options[] = {
  {"io",       required_argument,  0,  'o' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char model_io_short_options[] = "o:f:";

typedef struct Smodel_io_ps {
  char o[MAX_STRING+1],f[MAX_STRING+1];
} Smodel_io_ps;

extern Smodel_io_ps model_io_ps;

/*   CC_FSCLVFREQAV   */
/**********************/
static struct option clv_av_long_options[] = {
  {"method",   required_argument,  0,  'c' },  
  {"mu_min",   required_argument,  0,  'm' },
  {"mu_max",   required_argument,  0,  'M' },
  {"n_dirs",   required_argument,  0,  'd' },
  {"n_azim",   required_argument,  0,  'a' },
  {"freq_i",   required_argument,  0,  'i' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char clv_av_short_options[] = "c:m:M:d:a:i:f:";

typedef struct Sclv_av_ps {
  char f[MAX_STRING+1],c[MAX_STRING+1];
  double m,M;
  int d,a,i;
} Sclv_av_ps;

extern Sclv_av_ps clv_av_ps;

/*   CC_FSCLVFREQ   */
/********************/
static struct option clv_long_options[] = {
  {"method",   required_argument,  0,  'c' },  
  {"mu_min",   required_argument,  0,  'm' },
  {"mu_max",   required_argument,  0,  'M' },
  {"n_dirs",   required_argument,  0,  'd' },
  {"azim",     required_argument,  0,  'a' },
  {"freq_i",   required_argument,  0,  'i' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char clv_short_options[] = "c:m:M:d:a:i:f:";

typedef struct Sclv_ps {
  char f[MAX_STRING+1],c[MAX_STRING+1];
  double a,m,M;
  int d,i;
} Sclv_ps;

extern Sclv_ps clv_ps;

/*   CC_FSAZAVER   */
/*******************/
static struct option azaver_long_options[] = {
  {"method",   required_argument,  0,  'c' },  
  {"incl",     required_argument,  0,  't' },
  {"n_azim",   required_argument,  0,  'a' },
  {"fr_i_min", required_argument,  0,  'm' },
  {"fr_i_max", required_argument,  0,  'M' },
  {"file",     required_argument,  0,  'f' },
  {0,          0,                  0,  0   }
};

static char azaver_short_options[] = "c:t:a:m:M:f:";

typedef struct Sazaver_ps {
  char f[MAX_STRING+1],c[MAX_STRING+1];
  double t;
  int a,m,M;
} Sazaver_ps;

extern Sazaver_ps azaver_ps;

/*   CC_SET   */
/**************/
static struct option set_long_options[] = {
  {"n_incl",   required_argument,  0,  't' },  
  {"n_azim",   required_argument,  0,  'a' },
  {"stack",    required_argument,  0,  's' },
  {"quad",     required_argument,  0,  'q' },
  {"ori_x",    required_argument,  0,  'x' },
  {"ori_y",    required_argument,  0,  'y' },
  {"ori_z",    required_argument,  0,  'z' },
  {"dim_x",    required_argument,  0,  'i' },
  {"dim_y",    required_argument,  0,  'j' },
  {"dim_z",    required_argument,  0,  'k' },  
  {0,          0,                  0,  0   }
};

static char set_short_options[] = "t:a:s:d:q:x:y:z:i:j:k:";

typedef struct Sset_ps {
  double x,y,z,i,j,k;
  int t,a,s;
  char q[MAX_STRING+1];
} Sset_ps;

extern Sset_ps set_ps;

/*   CC_GET   */
/**************/
static struct option get_long_options[] = {
  {"n_incl",   no_argument,  0,  't' },  
  {"n_azim",   no_argument,  0,  'a' },
  {"stack",    no_argument,  0,  's' },
  {"n_freq",   no_argument,  0,  'f' },
  {"n_nodes",  no_argument,  0,  'n' },
  {"domain",   no_argument,  0,  'd' },
  {"period",   no_argument,  0,  'p' },
  {"wls",      no_argument,  0,  'w' },
  {"quad",     no_argument,  0,  'q' },
  {0,          0,            0,  0   }
};

static char get_short_options[] = "tasfndpwq";



/**********************/
extern void parse_getopt(int, char*, int*, char *argv[MAX_ARGS]);
extern void module_parameters(char *tmps, char *tmps_module);
extern void reset_ps(int cmd);

extern void parse_main_parameters(int cmd, int argc, char *argv[MAX_ARGS]);

extern void parse_tau_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_surface_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_jacobi_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_model_io_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_clv_av_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_clv_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_azaver_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_get_parameters(int argc, char *argv[MAX_ARGS]);
extern void parse_set_parameters(int argc, char *argv[MAX_ARGS]);

#endif
