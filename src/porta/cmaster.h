/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

/* I/O user console of the master process
 * it reads the usen input (script file) in an infinite loop and sends signals
 * to the slave nodes */

#ifndef CMASTER_H_
#define CMASTER_H_

/* an infinite loop on the master process: sends signals to slave-nodes' console
 */
extern void RunConsoleMaster(void);

#endif /*CMASTER_H_*/
