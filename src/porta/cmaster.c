/* Copyright (C) 2013 by Jiří Štěpán, stepan@asu.cas.cz */

#include "portal/def.h"
#include "portal/directions.h"
#include "portal/error.h"
#include "portal/ese.h"
#include "portal/fs.h"
#include "portal/global.h"
#include "portal/grid.h"
#include "portal/io.h"
#include "portal/matika.h"
#include "portal/mem.h"
#include "portal/modules.h"
#include "portal/plot.h"
#include "portal/portal.h"
#include "portal/process.h"
#include "portal/slv.h"
#include "portal/stack.h"
#include "portal/thread_io.h"
#include "portal/tools.h"
#include "portal/topology.h"
#include "portal/voigt.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <getopt.h>
#include "cmaster.h"
#include "parse_commands.h"

/* skip rest of the line in G_POR */
#define SKIPLINE                                                               \
    {                                                                          \
        int i;                                                                 \
        do                                                                     \
            i = fgetc(G_POR);                                                  \
        while (i != '\n' && i != EOF);                                         \
    }
/* whitespace test */
#define WHITE(c) ((c) == ' ' || (c) == '\t' || (c) == '\n')


#define CONSOLE_CHEAT "q"


/* global variables (global.c) */
extern t_global_vars *g_global;
extern FILE *         G_POR; /* the model script file */

/*******************************************************************************/
/* reads command from G_POR
 * return: -1 if unknown command, otherwise index number of command
 *        in cCommand, and N_COMMANDS if the line is commented-out
 */
static int ReadCommand(void)
{
    char cmd[MAX_IDEN_LEN + 1];
    int  i;

    if (feof(G_POR))
        return CC_EXIT;

    if (fscanf(G_POR, "%" POR_TOSTRING(MAX_IDEN_LEN) "s", cmd) <= 0)
        return CC_EXIT;

    if (cmd[0] == COMMENT_CHAR) {
        SKIPLINE;
        return N_COMMANDS;
    }
    /* "the secret command" closing the console and allowing to execute the
     * remaining code in main() */
    if (!strcmp(cmd, CONSOLE_CHEAT)) {
        fprintf(stderr, "*** entering wild mode ***\n");
        return -2;
    }
    /* browse available commands */
    for (i = 0; i < N_COMMANDS; i++) {
        if (!strcmp(cmd, cCommand[i]))
            return i;
    }
    fprintf(stderr, "unknown command: %s\n", cmd);
    SKIPLINE;
    return -1;
}


/*******************************************************************************/
/* reads string until end of line/eof, of maximum length MAX_STRING
 * sets str array
 * return: 0 if error, 1 otherwise
 * note: leading and final whitespaces are skipped
 */
static int ReadString(char str[])
{ /* WARNING: reads also the final \n : don't use SKIPLINE at the end of the
     line reading otherwise you skip the next command line */
    int st;
    if (!fgets(str, MAX_STRING, G_POR)) {
        fprintf(stderr, "can't read string\n");
        return 0;
    }
    /* skip leading whitespaces: */
    st               = 0;
    str[strlen(str)] = 0;
    while (st < strlen(str) && WHITE(str[st]))
        ++st;
    if (st > 0) {
        memmove(&str[0], &str[0] + st, strlen(str) - st + 1);
    }
    /* remove the final whitespaces */
    while (strlen(str) > 0 && WHITE(str[strlen(str) - 1]))
        str[strlen(str) - 1] = 0;
    return 1;
}

/*******************************************************************************/
#ifdef UNDEF
/* removes whitespace from the end of the string */
static void CutRestWhite(char str[])
{
    int i = strlen(str) - 1;
    while (i >= 0 && WHITE(str[i]))
        str[i--] = 0;
}
#endif

/*******************************************************************************/
/* start the console */
void RunConsoleMaster(void)
{
    int        cmd, par, tmp, tmp2, tmp3, i1, i2, i3;
    char       tmps[MAX_STRING + 1];
    double     d1, d2, d3;
    t_vec_3d   v1, v2;
    MPI_Status status;

    /* to branch in hdf5/binary */
    int signal;

    if (G_MY_MPI_RANK)
        IERR; /* only master node can run this loop */

    do {
        /* if invalid command-> skip the rest of the command line: */
        if ((cmd = ReadCommand()) < 0 && (cmd != -2))
            continue;
        /* exit console */
        if (cmd == -2)
            return;
        /* comment */
        if (cmd == N_COMMANDS)
            continue;
        /* process arguments of the command: */


        switch (cmd) {
        case CC_HELP:
	  ReadString(tmps);
	  if (tmps[0] == '\0') {
	    command_help(NULL);
	  } else {
	    command_help(tmps);
	  }
	  break;
        case CC_ECHO:
            if (!ReadString(tmps))
                continue;
            fprintf(stderr, "%s\n", tmps);
            fflush(stderr);
            break;
            /*****************************************/
        case CC_SET:
	  ReadString(tmps);

	  parse_getopt(CC_SET,tmps,&argc,argv);
	  module_parameters(tmps,tmps_module);
	  reset_ps(CC_SET);
	  parse_main_parameters(CC_SET,argc,argv);

	  if (set_ps.t != INT_MIN) {
	    tmp = g_global->dirs.n_incl_oct;
            g_global->dirs.n_incl_oct = set_ps.t;
	    if (g_global->dirs.n_incl_oct * g_global->dirs.n_azim_oct * 8 >
		MAXNDIRS ||
		g_global->dirs.n_incl_oct < 1) {
	      g_global->dirs.n_incl_oct = tmp;
	      fprintf(stderr, "invalid number of rays\n");
	    } else {
	      if (g_global->dirs.n_azim_oct == -1) {
		dir_MakeDirections(g_global->dirs.n_incl_oct,
				   DEF_NDI_AZIM, &g_global->dirs);
	      } else {
		dir_MakeDirections(g_global->dirs.n_incl_oct,
				   g_global->dirs.n_azim_oct,
				   &g_global->dirs);
	      }
	      if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
		  !pro_M2S_SendDirs()) {
		Error(E_ERROR, POR_AT,
		      "cannot synchronize direction quadrature");
	      }
	    }
	  }

	  if (set_ps.a != INT_MIN) {
	    tmp = g_global->dirs.n_azim_oct;
	    g_global->dirs.n_azim_oct = set_ps.a;
	    if (g_global->dirs.n_incl_oct * g_global->dirs.n_azim_oct * 8 >
		MAXNDIRS ||
		g_global->dirs.n_azim_oct < 1) {
	      g_global->dirs.n_azim_oct = tmp;
	      fprintf(stderr, "invalid number of rays\n");
	    } else {
	      dir_MakeDirections(g_global->dirs.n_incl_oct,
				 g_global->dirs.n_azim_oct,
				 &g_global->dirs);
	      if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
		  !pro_M2S_SendDirs()) {
		Error(E_ERROR, POR_AT,
		      "cannot synchronize direction quadrature");
	      }
	    }
	  }


	  if (set_ps.s != INT_MIN) {
	    tmp = g_global->stack_output;
            g_global->stack_output = set_ps.s;
	    if (g_global->stack_output < SOUT_OFF ||
		g_global->stack_output > SOUT_MAX) {
	      g_global->stack_output = tmp;
	      fprintf(stderr, "invalid stack output\n");
	    }
	  }


	  if (set_ps.q[0] != '\0') {
	    for (i1 = 0; i1 < N_QUADS; i1++) {
	      if (!strcmp(set_ps.q, quad_sets[i1]))
		break;
	    }
	    if (i1 == N_QUADS) {
	      Error(E_ERROR, POR_AT, "the input set is not defined");
	    } else {
	      g_global->dirs.n_incl_oct = i1;
	      g_global->dirs.n_azim_oct = -1;
	      dir_MakeDirectionsQuadSet(i1, &g_global->dirs);
	      if (!pro_SignalToAllSlaves(SIG_DIRECTIONS) ||
		  !pro_M2S_SendDirs()) {
		Error(E_ERROR, POR_AT,
		      "cannot synchronize direction quadrature");
	      }
	    }
	  }


	  if (set_ps.x != INT_MIN) {
	    /* in parse_set_parameters we check that all or none are provided,
	       so it is sufficient to check just for one */

	    v1.x = set_ps.x ; v1.y = set_ps.y ; v1.z = set_ps.z;
	    v2.x = set_ps.i ; v2.y = set_ps.j ; v2.z = set_ps.k;
	    grd_SetDomainGeometry(v1, v2);
	    fprintf(stderr,
		    "domain geometry: origin=[%3.3e,%3.3e,%3.3e] cm, "
		    "dimensions=[%3.3e,%3.3e,%3.3e] cm\n",
		    g_global->domain_origin.x, g_global->domain_origin.y,
		    g_global->domain_origin.z, g_global->domain_size.x,
		    g_global->domain_size.y, g_global->domain_size.z);
	  }
          

            break;
	    
            /*****************************************/
        case CC_GET:
	  ReadString(tmps);

	  parse_getopt(CC_GET,tmps,&argc,argv);
	  module_parameters(tmps,tmps_module);
	  reset_ps(CC_GET);
	  parse_main_parameters(CC_GET,argc,argv);

	  break;

            /*****************************************/
        case CC_VERSION:
            /* print version of Porta */
            fprintf(stderr, "%s %d.%d.%d %s, Copyright (C) %s\n", POR_LABEL,
                    POR_VER, POR_SUBVER, POR_PATCH, POR_EXTRAVER, POR_AUTHOR);
            break;
        case CC_EXIT:
            pro_SignalToAllSlaves(SIG_EXIT);
            ExitPorta(0);
            /* Free-up communicators */
            MPI_Comm_free(&zband_comm);
            MPI_Comm_free(&freqband_comm);

#ifdef THREAD_IO
            end_io_comm(&io_comm);
#endif

            MPI_Finalize(); /* this is to be called by each process */
            exit(EXIT_SUCCESS);
            break;
        case CC_FSSURFACE:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else {
	      ReadString(tmps);

	      parse_getopt(CC_FSSURFACE,tmps,&argc,argv);
	      module_parameters(tmps,tmps_module);
	      reset_ps(CC_FSSURFACE);
	      parse_main_parameters(CC_FSSURFACE,argc,argv);

	      if (!strcmp(surface_ps.o,"h5")) {
		too_SurfaceMap_fp = &too_SurfaceMap_h5;
	      } else {
		too_SurfaceMap_fp = &too_SurfaceMap;
	      }
	      
	      char cmt[IO_PSP_COMMENT_LEN + 1] = "";

	      // TODO: this trick is used in order to avoid exactly vertical
	      // rays, which can give problems for rotations
	      if (surface_ps.i < 1.0e-6)
		surface_ps.i = 1.0e-6; 

	      if (surface_ps.m < 0 || surface_ps.M >= PorNFreqs() || surface_ps.M < surface_ps.m) {
		Error(E_ERROR, POR_AT, "invalid wavelength interval");
		goto L_FSSURFACE_END;
	      }
	      if ((g_global->period[0] || g_global->period[1]) &&
		  fabs(90.0 - surface_ps.i) < 3.0) {
		Error(E_WARNING, POR_AT,
		      "too inclined rays in the periodic medium lead "
		      "to very long "
		      "solution times (inclination=%e deg)",
		      surface_ps.i);
	      }


	      if (!strcmp(surface_ps.c,"sc")) {
		// SC
		stk_Add(
			1, POR_AT,
			"calculating formal solution (SC) at the model surface...");
		if (!pro_SignalToAllSlaves(SIG_FSRAY))
		  IERR;
	      } else {
		// LC
		stk_Add(
			1, POR_AT,
			"calculating formal solution (LC) at the model surface...");
		if (!pro_SignalToAllSlaves(SIG_LC_FSRAY))
		  IERR;
	      }


	      if (MPI_SUCCESS !=
		  MPI_Bcast(&surface_ps.i, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&surface_ps.a, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&surface_ps.m, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&surface_ps.M, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (!(*too_SurfaceMap_fp)(surface_ps.i, surface_ps.a, surface_ps.m, surface_ps.M, surface_ps.f, cmt)) {
		Error(E_ERROR, POR_AT,
		      "formal solution was unsuccessful");
	      }
	    }
	
        L_FSSURFACE_END:
            break;

            /**************************************************/

        case CC_FSCLVFREQ:
	  if (!PorMeshInitialized()) {
	    Error(E_ERROR, POR_AT, "uninitialized grid");
	  } else if (!mod_IsModuleLoaded()) {
	    Error(E_ERROR, POR_AT, "no module loaded");
	  } else if (!g_global->period[0] || !g_global->period[1]) {
	    Error(E_ERROR, POR_AT,
		  "this function can only be used in horizontally periodic "
		  "models");
	  } else {
	    ReadString(tmps);

	    parse_getopt(CC_FSCLVFREQ,tmps,&argc,argv);
	    module_parameters(tmps,tmps_module);
	    reset_ps(CC_FSCLVFREQ);
	    parse_main_parameters(CC_FSCLVFREQ,argc,argv);

	    if (clv_ps.i < 0 || clv_ps.i >= PorNFreqs()) {
	      Error(E_ERROR, POR_AT, "invalid wavelength index");
	      goto L_FSCLV_END;
	    }
	    if (clv_ps.m <= 0.0 || clv_ps.M > 1.0 || clv_ps.m >= clv_ps.M) {
	      Error(E_ERROR, POR_AT, "invalid inclination interval");
	      goto L_FSCLV_END;
	    }
	    if (clv_ps.d < 1) {
	      Error(E_ERROR, POR_AT,
		    "invalid number of CLV directions");
	      goto L_FSCLV_END;
	    }
	    if ((g_global->period[0] || g_global->period[1]) &&
		fabs(90.0 - acos(clv_ps.m) / M_PI * 180.0) < 3.0) {
	      Error(E_WARNING, POR_AT,
		    "too inclined rays in the periodic medium lead "
		    "to very long "
		    "solution times (inclination=%e deg)",
		    acos(clv_ps.m) / M_PI * 180.0);
	    }

	    /* TODO: this trick is used in order to avoid exactly
	     * vertical rays:
	     */
	    if (clv_ps.M > 0.999999) {
	      clv_ps.M = 0.999999;
	      if (clv_ps.m >= clv_ps.M)
		clv_ps.m = clv_ps.M * 0.999999;
	    }

	    if (!strcmp(clv_ps.c,"sc")) {
		// SC
		stk_Add(
			1, POR_AT,
			"calculating CLV (SC) of the spatially averaged signal...");
		if (!pro_SignalToAllSlaves(SIG_FSCLV))
		  IERR;
	      } else {
		// LC
		stk_Add(
			1, POR_AT,
			"calculating CLV (SC) of the spatially averaged signal...");
		if (!pro_SignalToAllSlaves(SIG_LC_FSCLV))
		  IERR;
	      }

	    if (MPI_SUCCESS !=
		MPI_Bcast(&clv_ps.a, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&clv_ps.m, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&clv_ps.M, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&clv_ps.d, 1, MPI_INT, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&clv_ps.i, 1, MPI_INT, 0, MPI_COMM_WORLD))
	      CERR;
	    if (!too_CLVFreq(clv_ps.a, clv_ps.m, clv_ps.M, clv_ps.d, clv_ps.i, clv_ps.f)) {
	      Error(E_ERROR, POR_AT,
		    "formal solution was unsuccessful");
	    }
	  }

        L_FSCLV_END:
	  break;

            /**************************************************/

        case CC_FSCLVFREQAV:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "uninitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else if (!g_global->period[0] || !g_global->period[1]) {
                Error(E_ERROR, POR_AT,
                      "this function can only be used in horizontally periodic "
                      "models");
            } else {
	      ReadString(tmps);

	      parse_getopt(CC_FSCLVFREQAV,tmps,&argc,argv);
	      module_parameters(tmps,tmps_module);
	      reset_ps(CC_FSCLVFREQAV);
	      parse_main_parameters(CC_FSCLVFREQAV,argc,argv);

	      
	      if (clv_av_ps.i < 0 || clv_av_ps.i >= PorNFreqs()) {
		Error(E_ERROR, POR_AT, "invalid wavelength index");
		goto L_FSCLVAV_END;
	      }
	      if (clv_av_ps.m <= 0.0 || clv_av_ps.M > 1.0 || clv_av_ps.m >= clv_av_ps.M) {
		Error(E_ERROR, POR_AT, "invalid inclination interval");
		goto L_FSCLVAV_END;
	      }
	      if (clv_av_ps.d < 1) {
		Error(E_ERROR, POR_AT,
		      "invalid number of CLV directions");
		goto L_FSCLVAV_END;
	      }
	      if (clv_av_ps.a < 1) {
		Error(E_ERROR, POR_AT,
		      "number of azimuths should be significantly "
		      "larger than 1");
		goto L_FSCLVAV_END;
	      }
	      if ((g_global->period[0] || g_global->period[1]) &&
		  fabs(90.0 - acos(clv_av_ps.m) / M_PI * 180.0) < 3.0) {
		Error(E_WARNING, POR_AT,
		      "too inclined rays in the periodic medium lead "
		      "to very long "
		      "solution times (inclination=%e deg)",
		      acos(clv_av_ps.m) / M_PI * 180.0);
	      }

	      /* TODO: this trick is used in order to avoid exactly
	       * vertical rays:
	       */
	      if (clv_av_ps.M > 0.999999) {
		clv_av_ps.M = 0.999999;
		if (clv_av_ps.m >= clv_av_ps.M)
		  clv_av_ps.m = clv_av_ps.M * 0.999999;
	      }

	      if (!strcmp(clv_av_ps.c,"sc")) {
		// SC
		stk_Add(1, POR_AT,
			"calculating CLV (SC) of the spatially- and "
			"azimuth-averaged signal...");
		if (!pro_SignalToAllSlaves(SIG_FSCLVAV))
		  IERR;
	      } else {
		// LC
		stk_Add(1, POR_AT,
			"calculating CLV (LC) of the spatially- and "
			"azimuth-averaged signal...");
		if (!pro_SignalToAllSlaves(SIG_FSCLVAV_LC))
		  IERR;
	      }

	      
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&clv_av_ps.m, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&clv_av_ps.M, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&clv_av_ps.d, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&clv_av_ps.a, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&clv_av_ps.i, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (!too_CLVFreqAV(clv_av_ps.m, clv_av_ps.M, clv_av_ps.d, clv_av_ps.a, clv_av_ps.i, clv_av_ps.f)) {
		Error(E_ERROR, POR_AT,
		      "formal solution was unsuccessful");
	      }
	    }

        L_FSCLVAV_END:
            break;

            /**************************************************/

        case CC_FSAZAVER:
	  if (!PorMeshInitialized()) {
	    Error(E_ERROR, POR_AT, "uninitialized grid");
	  } else if (!mod_IsModuleLoaded()) {
	    Error(E_ERROR, POR_AT, "no module loaded");
	  } else if (!g_global->period[0] || !g_global->period[1]) {
	    Error(E_ERROR, POR_AT,
		  "this function can only be used in horizontally periodic "
		  "models");
	  } else {
	    ReadString(tmps);

	    parse_getopt(CC_FSAZAVER,tmps,&argc,argv);
	    module_parameters(tmps,tmps_module);
	    reset_ps(CC_FSAZAVER);
	    parse_main_parameters(CC_FSAZAVER,argc,argv);

	    if (azaver_ps.m < 0 || azaver_ps.M >= PorNFreqs() || azaver_ps.m > azaver_ps.M) {
	      Error(E_ERROR, POR_AT, "invalid wavelength indices");
	      goto L_FSAZAV_END;
	    }
	    if (azaver_ps.t < 0.0 || azaver_ps.t >= 90.0) {
	      Error(E_ERROR, POR_AT, "invalid inclination");
	      goto L_FSAZAV_END;
	    }
	    if (azaver_ps.a < 1) {
	      Error(E_ERROR, POR_AT,
		    "number of azimuths should be significantly "
		    "larger than 1");
	      goto L_FSAZAV_END;
	    }
	    if ((g_global->period[0] || g_global->period[1]) &&
		fabs(90.0 - azaver_ps.t) < 3.0) {
	      Error(E_WARNING, POR_AT,
		    "too inclined rays in the periodic medium lead "
		    "to very long "
		    "solution times (inclination=%e deg)",
		    azaver_ps.t);
	    }

	    if (azaver_ps.t < 1.0e-6)
	      azaver_ps.t = 1.0e-6; /* TODO: this trick is used in order to
			      avoid exactly vertical rays: */

	    if (!strcmp(azaver_ps.c,"sc")) {
	      // SC
	      stk_Add(
		      1, POR_AT,
		      "calculating formal solution (SC) for the spatially- and "
		      "azimuth-averaged signal...");
	      if (!pro_SignalToAllSlaves(SIG_FSAZAV))
		IERR;
	    } else {
	      // LC
	      stk_Add(
		      1, POR_AT,
		      "calculating formal solution (LC) for the spatially- and "
		      "azimuth-averaged signal...");
	      if (!pro_SignalToAllSlaves(SIG_LC_FSAZAV))
		IERR;
	    }
	    
	    if (MPI_SUCCESS !=
		MPI_Bcast(&azaver_ps.t, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&azaver_ps.a, 1, MPI_INT, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&azaver_ps.m, 1, MPI_INT, 0, MPI_COMM_WORLD))
	      CERR;
	    if (MPI_SUCCESS !=
		MPI_Bcast(&azaver_ps.M, 1, MPI_INT, 0, MPI_COMM_WORLD))
	      CERR;
	    if (!too_FSAZAV(azaver_ps.t, azaver_ps.a, azaver_ps.m, azaver_ps.M, azaver_ps.f)) {
	      Error(E_ERROR, POR_AT,
		    "formal solution was unsuccessful");
	    }
	  }
	
	L_FSAZAV_END:
	  break;

            /**************************************************/

        case CC_GETTAU:
            if (!PorMeshInitialized()) {
                Error(E_ERROR, POR_AT, "unitialized grid");
            } else if (!mod_IsModuleLoaded()) {
                Error(E_ERROR, POR_AT, "no module loaded");
            } else {
	      ReadString(tmps);

	      parse_getopt(CC_GETTAU,tmps,&argc,argv);
	      module_parameters(tmps,tmps_module);
	      reset_ps(CC_GETTAU);
	      parse_main_parameters(CC_GETTAU,argc,argv);

	      if (!strcmp(tau_ps.o,"h5")) {
		too_SurfaceTau_fp = &too_SurfaceTau_h5;
	      } else {
		too_SurfaceTau_fp = &too_SurfaceTau;
	      }
	      
	      char cmt[IO_TAU_COMMENT_LEN + 1] = "";
	      // TODO: this trick is used in order to avoid exactly
	      // vertical rays
	      if (tau_ps.i < 1.0e-6)
		tau_ps.i = 1.0e-6;

	      /* // TODO: this trick is used in order to avoid very small
	       * taus */
	      /* if (tau_ps.t<1.0e-6) tau_ps.t = 1.0e-6; */
	      
	      if (tau_ps.m < 0 || tau_ps.M >= PorNFreqs() || tau_ps.M < tau_ps.m) {
		Error(E_ERROR, POR_AT, "invalid wavelength interval");
		goto L_GETTAU_END;
	      }
	      if ((g_global->period[0] || g_global->period[1]) &&
		  fabs(90.0 - tau_ps.i) < 3.0) {
		Error(E_WARNING, POR_AT,
		      "too inclined rays in the periodic medium lead "
		      "to very long "
		      "solution times (inclination=%e deg)",
		      tau_ps.i);
	      }
	      stk_Add(1, POR_AT, "calculating positions where tau=%e...",
		      tau_ps.t);

	      if (!pro_SignalToAllSlaves(SIG_GETTAU))
		IERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&tau_ps.t, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&tau_ps.i, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&tau_ps.a, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&tau_ps.m, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      if (MPI_SUCCESS !=
		  MPI_Bcast(&tau_ps.M, 1, MPI_INT, 0, MPI_COMM_WORLD))
		CERR;
	      g_global->afunc.F_SyncParams();
	      if (!(*too_SurfaceTau_fp)(tau_ps.t, tau_ps.i, tau_ps.a, tau_ps.m, tau_ps.M, tau_ps.f, cmt)) {
		Error(E_ERROR, POR_AT, "get tau was unsuccessful");
	      }
	    }
	
        L_GETTAU_END:
            break;

        case CC_SOLVE_JACOBI:
	  if (!PorMeshInitialized()) {
	    Error(E_ERROR, POR_AT, "uninitialized grid");
	  } else if (!mod_IsModuleLoaded()) {
	    Error(E_ERROR, POR_AT, "no module loaded");
	  } else {

	    ReadString(tmps);

	    parse_getopt(CC_SOLVE_JACOBI,tmps,&argc,argv);
	    module_parameters(tmps,tmps_module);
	    reset_ps(CC_SOLVE_JACOBI);
	    parse_main_parameters(CC_SOLVE_JACOBI,argc,argv);
	    
	    if (!pro_SignalToAllSlaves(SIG_JACOBI)) 
	      CERR;
	      
	      /* send info on # of iterations and maximum relative change */
	    MPI_Bcast(&jacobi_ps.i, 1, MPI_INT, 0, MPI_COMM_WORLD);
	    MPI_Bcast(&jacobi_ps.c, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	    /* --== here the non-LTE problem is solved by the slaves! ==-- */
	    if (MPI_SUCCESS !=
		MPI_Recv(&jacobi_ps.c, 1, MPI_DOUBLE, 1, 0, MPI_COMM_WORLD, &status))
	      CERR; /* get max. relative change from process #1 */
	    PrintfErr("Jacobi iteration finished. Maximum relative change of "
		      "population: %6.6e.\n",
		      jacobi_ps.c);
	    break;
	  }
	  
        case CC_SAVEMODEL:
	  if (!PorMeshInitialized()) {
	    Error(E_ERROR, POR_AT, "uninitialized grid");
	  } else if (!mod_IsModuleLoaded()) {
	    Error(E_ERROR, POR_AT, "no module loaded");
	  } else {
	    
	    ReadString(tmps);

	    parse_getopt(CC_SAVEMODEL,tmps,&argc,argv);
	    module_parameters(tmps,tmps_module);
	    reset_ps(CC_SAVEMODEL);
	    parse_main_parameters(CC_SAVEMODEL,argc,argv);

	    if (!strcmp(model_io_ps.o,"h5")) {
	      io_SaveGridMaster_fp = &io_SaveGridMaster_h5;
	      signal               = SIG_SAVEGRID_H5;
	    } else {
	      io_SaveGridMaster_fp = &io_SaveGridMaster;
	      signal               = SIG_SAVEGRID;
	    }

	    if (!pro_SignalToAllSlaves(signal)) 
	      CERR;

	    if (!(*io_SaveGridMaster_fp)(model_io_ps.f)) {
	      Error(E_ERROR, POR_AT, "the grid could not be saved to %s",model_io_ps.f);
	    } else {
	      stk_Add(1, "the grid has been saved to", model_io_ps.f);
            }
	  }
	  break;

        case CC_LOADMODEL:
	  ReadString(tmps);

	  parse_getopt(CC_LOADMODEL,tmps,&argc,argv);
	  module_parameters(tmps,tmps_module);
	  reset_ps(CC_LOADMODEL);
	  parse_main_parameters(CC_LOADMODEL,argc,argv);
	  
	  if (!strcmp(model_io_ps.o,"h5")) {
	    io_LoadGridMaster_fp = &io_LoadGridMaster_h5;
	    signal               = SIG_LOADGRID_H5;
	  } else {
	    io_LoadGridMaster_fp = &io_LoadGridMaster;
	    signal               = SIG_LOADGRID;
	  }
	  
	  if (!pro_SignalToAllSlaves(signal)) 
	    CERR;
	  
	  if (!(*io_LoadGridMaster_fp)(model_io_ps.f)) {
	    Error(E_ERROR, POR_AT, "the grid could not be loaded from %s",
		  model_io_ps.f);
	  } else {
	    stk_Add(1, "the grid has been loaded from", model_io_ps.f);
	  }
	  
	  break;

        default:
	  IERR;
	  break;
        }
    } while (1);
}
