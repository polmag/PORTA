#include "portal/def.h"
#include "portal/error.h"
#include "portal/modules.h"
#include "portal/global.h"
#include <string.h>
#include <getopt.h>
#include "parse_commands.h"

int argc;
char *argv[MAX_ARGS];
char tmps_module[MAX_STRING+1];

Sset_ps set_ps;
Sazaver_ps azaver_ps;
Sclv_av_ps clv_av_ps;
Sclv_ps clv_ps;
Sjacobi_ps jacobi_ps;
Smodel_io_ps model_io_ps;
Ssurface_ps surface_ps;
Stau_ps tau_ps;

void parse_getopt(int cmd, char *tmps_t, int *argc, char *argv[MAX_ARGS]) {
  char tmps[MAX_STRING+1];
  strcpy(tmps,tmps_t);

  *argc = 1;
  switch (cmd) {
  case CC_GETTAU:
    argv[0] = "get_tau_pos";
    break;
  case CC_FSSURFACE: 
    argv[0] = "fs_surface";
    break;
  case CC_SOLVE_JACOBI: 
    argv[0] = "solve_jacobi";
    break;
  case CC_LOADMODEL: /* fall through */
  case CC_SAVEMODEL:
    argv[0] = "model_io";
    break;
  case CC_FSCLVFREQAV:
    argv[0] = "fs_clv_freq_av";
    break;
  case CC_FSCLVFREQ:
    argv[0] = "fs_clv_freq";
    break;
  case CC_FSAZAVER:
    argv[0] = "fs_azaver";
    break;
  case CC_GET:
    argv[0] = "get";
    break;
  case CC_SET:
    argv[0] = "set";
    break;    
  }
  
  char *p2 = strtok(tmps, " ");
  while (p2 && *argc < MAX_ARGS)
    {
      argv[(*argc)++] = p2;
      p2 = strtok(NULL, " ");
    }
  argv[*argc] = 0;
}


/* This functions is used to read what will be passed to the module
   (for those commands that accept module parameters */
void module_parameters(char *tmps, char *tmps_module) {
  char *ret;

  ret = strstr(tmps, sep);
  if (ret) {
    strcpy(tmps_module,ret+4);
  } else {
    tmps_module[0] = '\0';
  }
}


void reset_ps(int cmd) {
    // for the "undefined" variables (the user must provide them),
    // (for numeric values: INT_MIN, for strings, first position: '\0'
  
  switch (cmd) {
  case CC_GETTAU:
    // "undefined", so user must provide them (for numeric values: INT_MIN, for strings: '\0')
    tau_ps.f[0] = '\0';
    tau_ps.t = tau_ps.i = tau_ps.a = INT_MIN;
    tau_ps.m = tau_ps.M = INT_MIN;

    // default values
    strcpy(tau_ps.o,"h5");
    // how to get the max number of frequencies?

    break;

  case CC_FSSURFACE:
    // "undefined"
    surface_ps.f[0] = '\0';
    surface_ps.i = surface_ps.a = INT_MIN;
    surface_ps.m = surface_ps.M = INT_MIN;
    
    // default values
    strcpy(surface_ps.o,"h5");
    strcpy(surface_ps.c,"lc");
    // how to get the max number of frequencies?

    break;

  case CC_SOLVE_JACOBI:
    // "undefined"
    jacobi_ps.i = INT_MIN;
    jacobi_ps.c = INT_MIN;
    
    // default values

    break;

  case CC_LOADMODEL: /* fall through */
  case CC_SAVEMODEL:
    // "undefined"
    model_io_ps.f[0] = '\0';
    
    // default values
    strcpy(model_io_ps.o,"h5");

    break;

  case CC_FSCLVFREQAV:
    // "undefined"
    clv_av_ps.f[0] = '\0';
    clv_av_ps.m = clv_av_ps.M = INT_MIN;
    clv_av_ps.d = clv_av_ps.a = clv_av_ps.i = INT_MIN;

    //default values
    strcpy(clv_av_ps.c,"lc");

    break;

  case CC_FSCLVFREQ:
    // "undefined"
    clv_ps.f[0] = '\0';
    clv_ps.m = clv_ps.M = clv_ps.a = INT_MIN;
    clv_ps.d = clv_ps.i = INT_MIN;

    //default values
    strcpy(clv_ps.c,"lc");

    break;

  case CC_FSAZAVER:
    // "undefined"
    azaver_ps.f[0] = '\0';
    azaver_ps.t = INT_MIN;
    azaver_ps.a = azaver_ps.m = azaver_ps.M = INT_MIN;

    //default values
    strcpy(azaver_ps.c,"lc");

    break;

  case CC_GET:
    // get just prints information, we don't need to save any values.
    break;

  case CC_SET:
    set_ps.t = set_ps.a = set_ps.s = set_ps.i = set_ps.j = set_ps.k = INT_MIN;
    set_ps.x = set_ps.y = set_ps.z = INT_MIN;
    set_ps.q[0] = '\0';
    break;

  }
}


void parse_main_parameters(int cmd, int argc, char *argv[MAX_ARGS]) {

  // This should be most likely be set to 1, but it looks like there is a bug in the GNU implementation
  //  so it actually works better if set to 0.
  // https://stackoverflow.com/questions/65291405/bug-in-getopt-c-for-non-global-use
  optind = 0; // this is a global from getopth.h
              // (need to reset it to 1 to start from beginning for every command)

  switch (cmd) {
  case CC_GETTAU:
    parse_tau_parameters(argc, argv);
    break;

  case CC_FSSURFACE:
    parse_surface_parameters(argc, argv);
    break;

  case CC_SOLVE_JACOBI:
    parse_jacobi_parameters(argc, argv);
    break;

  case CC_LOADMODEL: /* fall through as both have same parameters */
  case CC_SAVEMODEL:
    parse_model_io_parameters(argc, argv);
    break;    

  case CC_FSCLVFREQAV:
    parse_clv_av_parameters(argc, argv);
    break;

  case CC_FSCLVFREQ:
    parse_clv_parameters(argc, argv);
    break;

  case CC_FSAZAVER:
    parse_azaver_parameters(argc, argv);
    break;

  case CC_GET:
    parse_get_parameters(argc, argv);
    break;

  case CC_SET:
    parse_set_parameters(argc, argv);
    break;
  }
}


/*   CC_GETTAU   */
/*****************/
void parse_tau_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, tau_short_options, tau_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'o':
	if (!sscanf(optarg,"%s",(char*)&(tau_ps.o)))
	  Error(E_ERROR, POR_AT, "argument for o must be a string\n");
	break;
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(tau_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case 't':
	if (!sscanf(optarg,"%lf",&(tau_ps.t)))
	  Error(E_ERROR, POR_AT, "argument for t must be double\n");
	break;
      case 'i':
	if (!sscanf(optarg,"%lf",&(tau_ps.i)))
	  Error(E_ERROR, POR_AT, "argument for i must be double\n");
	break;
      case 'a':
	if (!sscanf(optarg,"%lf",&(tau_ps.a)))
	  Error(E_ERROR, POR_AT, "argument for a must be double\n");
	break;
      case 'm':
	tau_ps.m = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for m must be integer\n");
	break;
      case 'M':
	tau_ps.M = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for M must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (tau_ps.t == INT_MIN || tau_ps.i == INT_MIN || tau_ps.a == INT_MIN || \
      tau_ps.m == INT_MIN || tau_ps.M == INT_MIN ||			\
      tau_ps.o[0] == '\0' || tau_ps.f[0] == '\0') 
    Error(E_ERROR, POR_AT, "t,i,a,m,M,o and f are mandatory in get_tau_pos command\n");

  if (strcmp(tau_ps.o,"h5") && strcmp(tau_ps.o,"tau")) {
    Error(E_ERROR, POR_AT, "m must be one of [h5,tau]\n");
  }

  /*  printf("\nArguments [PORTA Library] for get_tau_pos:\n");
  printf("[io:%s ; tau:%3.2f ; incl:%3.2f ; azim:%3.2f ; fr_i_min:%d ; fr_i_max:%d ; file:%s]\n", \
  tau_ps.o,tau_ps.t,tau_ps.i,tau_ps.a,tau_ps.m,tau_ps.M,tau_ps.f); */
  
  if (mod_IsModuleLoaded()) {
    parse_getopt(CC_GETTAU,tmps_module, &argc, argv);
    g_global->afunc.parse_params(argc,argv);
  }

}


/*   CC_FSSURFACE   */
/********************/
void parse_surface_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, surface_short_options, surface_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'o':
	if (!sscanf(optarg,"%s",(char*)&(surface_ps.o)))
	  Error(E_ERROR, POR_AT, "argument for o must be a string\n");
	break;
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(surface_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case 'c':
	if (!sscanf(optarg,"%s",(char*)&(surface_ps.c)))
	  Error(E_ERROR, POR_AT, "argument for c must be a string\n");
	break;
      case 'i':
	if (!sscanf(optarg,"%lf",&(surface_ps.i)))
	  Error(E_ERROR, POR_AT, "argument for i must be double\n");
	break;
      case 'a':
	if (!sscanf(optarg,"%lf",&(surface_ps.a)))
	  Error(E_ERROR, POR_AT, "argument for a must be double\n");
	break;
      case 'm':
	surface_ps.m = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for m must be integer\n");
	break;
      case 'M':
	surface_ps.M = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for M must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (surface_ps.i == INT_MIN || surface_ps.a == INT_MIN || \
      surface_ps.m == INT_MIN || surface_ps.M == INT_MIN || \
      surface_ps.o[0] == '\0' || surface_ps.f[0] == '\0' || \
      surface_ps.c[0] == '\0') 
    Error(E_ERROR, POR_AT, "c,i,a,m,M,o and f are mandatory in get_surface_pos command\n");

  if (strcmp(surface_ps.o,"h5") && strcmp(surface_ps.o,"psp")) {
    Error(E_ERROR, POR_AT, "m must be one of [h5,psp]\n");
  }

  if (strcmp(surface_ps.c,"lc") && strcmp(surface_ps.c,"sc")) {
    Error(E_ERROR, POR_AT, "c must be one of [sc,lc]\n");
  }

  
  /*printf("\nArguments [PORTA Library] for get_surface_pos:\n");
  printf("[io:%s ; method:%s ; incl:%3.2f ; azim:%3.2f ; fr_i_min:%d ; fr_i_max:%d ; file:%s]\n", \
  surface_ps.o,surface_ps.c,surface_ps.i,surface_ps.a,surface_ps.m,surface_ps.M,surface_ps.f);*/

  /* If the module accepts arguments uncomment and implement */
  
  /* if (mod_IsModuleLoaded()) { */
  /*   parse_getopt(CC_FSSURFACE,tmps_module, &argc, argv); */
  /*   g_global->afunc.parse_params(argc,argv); */
  /* } */

}


/*   CC_SOLVE_JACOBI   */
/***********************/
void parse_jacobi_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, jacobi_short_options, jacobi_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'c':
	if (!sscanf(optarg,"%lf",&(jacobi_ps.c)))
	  Error(E_ERROR, POR_AT, "argument for c must be double\n");
	break;
      case 'i':
	jacobi_ps.i = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for i must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (jacobi_ps.i == INT_MIN || jacobi_ps.c == INT_MIN)
    Error(E_ERROR, POR_AT, "c and i are mandatory in solve_jacobi command\n");

  /* printf("\nArguments [PORTA Library] for solve_jacobi:\n");
  printf("[max_rc:%3.2f ; max_it:%d\n", \
  jacobi_ps.c,jacobi_ps.i); */

  /* If the module accepts arguments uncomment and implement */
  
  /* if (mod_IsModuleLoaded()) { */
  /*   parse_getopt(CC_SOLVE_JACOBI,tmps_module, &argc, argv); */
  /*   g_global->afunc.parse_params(argc,argv); */
  /* } */

}


/*   CC_LOADMODEL || CC_SAVEMODEL   */
/************************************/
void parse_model_io_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, model_io_short_options, model_io_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'o':
	if (!sscanf(optarg,"%s",(char*)&(model_io_ps.o)))
	  Error(E_ERROR, POR_AT, "argument for o must be a string\n");
	break;
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(model_io_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (model_io_ps.o[0] == '\0' || model_io_ps.f[0] == '\0')
    Error(E_ERROR, POR_AT, "o and f are mandatory in loadmodel/savemodel commands\n");

  if (strcmp(model_io_ps.o,"h5") && strcmp(model_io_ps.o,"pmd")) {
    Error(E_ERROR, POR_AT, "m must be one of [h5,pmd]\n");
  }

  /*printf("\nArguments [PORTA Library] for load/save_model:\n");
  printf("[io:%s ; file:%s]\n", \
  model_io_ps.o,model_io_ps.f); */

}


/*   CC_FSCLVFREQAV   */
/**********************/
void parse_clv_av_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, clv_av_short_options, clv_av_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(clv_av_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case 'c':
	if (!sscanf(optarg,"%s",(char*)&(clv_av_ps.c)))
	  Error(E_ERROR, POR_AT, "argument for c must be a string\n");
	break;
      case 'm':
	if (!sscanf(optarg,"%lf",&(clv_av_ps.m)))
	  Error(E_ERROR, POR_AT, "argument for m must be double\n");
	break;
      case 'M':
	if (!sscanf(optarg,"%lf",&(clv_av_ps.M)))
	  Error(E_ERROR, POR_AT, "argument for M must be double\n");
	break;
      case 'd':
	clv_av_ps.d = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for d must be integer\n");
	break;
      case 'a':
	clv_av_ps.a = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for a must be integer\n");
	break;
      case 'i':
	clv_av_ps.i = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for i must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (clv_av_ps.m == INT_MIN || clv_av_ps.M == INT_MIN || \
      clv_av_ps.d == INT_MIN || clv_av_ps.a == INT_MIN || clv_av_ps.i == INT_MIN || \
      clv_av_ps.f[0] == '\0') 
    Error(E_ERROR, POR_AT, "m,M,d,a,i and f are mandatory in fs_clv_freq_av command\n");

  if (strcmp(clv_av_ps.c,"lc") && strcmp(clv_av_ps.c,"sc")) {
    Error(E_ERROR, POR_AT, "c must be one of [sc,lc]\n");
  }
  
  /*printf("\nArguments [PORTA Library] for fs_clv_freq_av:\n");
  printf("[method:%s ; mu_min:%3.2f ; mu_max:%3.2f ; n_dirs:%d ; n_azim:%d ; freq_i:%d ; file:%s]\n", \
  clv_av_ps.c,clv_av_ps.m,clv_av_ps.M,clv_av_ps.d,clv_av_ps.a,clv_av_ps.i,clv_av_ps.f); */

  /* If the module accepts arguments uncomment and implement */
  
  /* if (mod_IsModuleLoaded()) { */
  /*   parse_getopt(CC_FSCLVFREQAV,tmps_module, &argc, argv); */
  /*   g_global->afunc.parse_params(argc,argv); */
  /* } */

}


/*   CC_FSCLVFREQ   */
/********************/
void parse_clv_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, clv_short_options, clv_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(clv_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case 'c':
	if (!sscanf(optarg,"%s",(char*)&(clv_ps.c)))
	  Error(E_ERROR, POR_AT, "argument for c must be a string\n");
	break;
      case 'm':
	if (!sscanf(optarg,"%lf",&(clv_ps.m)))
	  Error(E_ERROR, POR_AT, "argument for m must be double\n");
	break;
      case 'M':
	if (!sscanf(optarg,"%lf",&(clv_ps.M)))
	  Error(E_ERROR, POR_AT, "argument for M must be double\n");
	break;
      case 'a':
	if (!sscanf(optarg,"%lf",&(clv_ps.a)))
	  Error(E_ERROR, POR_AT, "argument for a must be double\n");
	break;
      case 'd':
	clv_ps.d = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for d must be integer\n");
	break;
      case 'i':
	clv_ps.i = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for i must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (clv_ps.m == INT_MIN || clv_ps.M == INT_MIN || \
      clv_ps.d == INT_MIN || clv_ps.a == INT_MIN || clv_ps.i == INT_MIN || \
      clv_ps.f[0] == '\0') 
    Error(E_ERROR, POR_AT, "m,M,d,a,i and f are mandatory in fs_clv_freq command\n");

  if (strcmp(clv_ps.c,"lc") && strcmp(clv_ps.c,"sc")) {
    Error(E_ERROR, POR_AT, "c must be one of [sc,lc]\n");
  }

  /* printf("\nArguments [PORTA Library] for fs_clv_freq:\n");
  printf("[method:%s ; mu_min:%3.2f ; mu_max:%3.2f ; n_dirs:%d ; azim:%3.2f ; freq_i:%d ; file:%s]\n", \
  clv_ps.c,clv_ps.m,clv_ps.M,clv_ps.d,clv_ps.a,clv_ps.i,clv_ps.f); */

  /* If the module accepts arguments uncomment and implement */
  
  /* if (mod_IsModuleLoaded()) { */
  /*   parse_getopt(CC_FSCLVFREQ,tmps_module, &argc, argv); */
  /*   g_global->afunc.parse_params(argc,argv); */
  /* } */

}


/*   CC_FSAZAVER   */
/*******************/
void parse_azaver_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;
  
  while((opt = getopt_long(argc, argv, azaver_short_options, azaver_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 'f':
	if (!sscanf(optarg,"%s",(char*)&(azaver_ps.f)))
	  Error(E_ERROR, POR_AT, "argument for f must be a string\n");
	break;
      case 'c':
	if (!sscanf(optarg,"%s",(char*)&(azaver_ps.c)))
	  Error(E_ERROR, POR_AT, "argument for c must be a string\n");
	break;
      case 't':
	if (!sscanf(optarg,"%lf",&(azaver_ps.t)))
	  Error(E_ERROR, POR_AT, "argument for t must be double\n");
	break;
      case 'a':
	azaver_ps.a = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for a must be integer\n");
	break;
      case 'm':
	azaver_ps.m = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for m must be integer\n");
	break;
      case 'M':
	azaver_ps.M = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for M must be integer\n");
	break;
      case '?':
	printf("wrong arguments to command\n"); exit(EXIT_FAILURE);
	break;
      }
    }
  
  if (azaver_ps.m == INT_MIN || azaver_ps.M == INT_MIN || azaver_ps.a == INT_MIN || \
      azaver_ps.t == INT_MIN || \
      azaver_ps.c[0] == '\0' ||	azaver_ps.f[0] == '\0') 
    Error(E_ERROR, POR_AT, "c,t,a,m,M and f are mandatory in fs_azaver command\n");

  if (strcmp(azaver_ps.c,"lc") && strcmp(azaver_ps.c,"sc")) {
    Error(E_ERROR, POR_AT, "c must be one of [sc,lc]\n");
  }

  /* printf("\nArguments [PORTA Library] for fs_azaver:\n");
  printf("[method:%s ; incl:%3.2f ; n_azim:%d ; fr_i_min:%d ; fr_i_max:%d ; file:%s]\n", \
  azaver_ps.c,azaver_ps.t,azaver_ps.a,azaver_ps.m,azaver_ps.M,azaver_ps.f); */

  /* If the module accepts arguments uncomment and implement */
  
  /* if (mod_IsModuleLoaded()) { */
  /*   parse_getopt(CC_FSAZAVER,tmps_module, &argc, argv); */
  /*   g_global->afunc.parse_params(argc,argv); */
  /* } */

}


/*   CC_GET   */
/**************/
void parse_get_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;

  while((opt = getopt_long(argc, argv, get_short_options, get_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 't':
	if (g_global->dirs.n_azim_oct == -1)
	  Error(E_ERROR, POR_AT, "no number of inclinations set");
	fprintf(stderr, "%d\n", g_global->dirs.n_incl_oct);
	break;
      case 'a':
	if (g_global->dirs.n_azim_oct == -1)
	  Error(E_ERROR, POR_AT, "no number of azimuths set");
	fprintf(stderr, "%d\n", g_global->dirs.n_azim_oct);
	break;
      case 's':
	fprintf(stderr, "%d\n", g_global->stack_output);
	break;
      case 'f':
	fprintf(stderr, "%d\n", PorNFreqs());
	break;
      case 'n':
	fprintf(stderr, "%d\n", PorNNodes());
	break;
      case 'd':
	fprintf(stderr,
		"%17.17e %17.17e %17.17e    %17.17e %17.17e %17.17e\n",
		g_global->domain_origin.x, g_global->domain_origin.y,
		g_global->domain_origin.z, g_global->domain_size.x,
		g_global->domain_size.y, g_global->domain_size.z);
	break;
      case 'p':
	fprintf(stderr, "X:%d Y:%d\n", g_global->period[0],
		g_global->period[1]);
	break;
      case 'w':
	if (PorNFreqs() <= 0) {
	  Error(E_ERROR, POR_AT, "no radiation wavelengths set");
	} else {
	  const double *freq  = PorFreqs();
	  int           nfreq = PorNFreqs(), i;
	  for (i = nfreq - 1; i >= 0; i--) {
	    fprintf(stderr, "%d\t%10.10e\n", i,
		    C_C / freq[i] * 1.0e8);
	  }
	  fflush(stderr);
	}
	break;
      case 'q':
	if (g_global->dirs.n_azim_oct != -1) {
	  Error(E_ERROR, POR_AT, "no quadrature set");
	}
	fprintf(stderr, "%s\n", quad_sets[g_global->dirs.n_incl_oct]);
	break;
      case '?':
	printf("wrong arguments to command get\n"); exit(EXIT_FAILURE);
	break;
      }
    }
}


/*   CC_SET   */
/**************/
void parse_set_parameters(int argc, char *argv[MAX_ARGS]) {
  int opt, option_index;
  char *tail;

  while((opt = getopt_long(argc, argv, set_short_options, set_long_options, &option_index)) != -1)
    {
      switch(opt) {
      case 't':
	set_ps.t = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for t must be integer\n");
	break;
      case 'a':
	set_ps.a = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for a must be integer\n");
	break;
      case 's':
	set_ps.s = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for s must be integer\n");
	break;
      case 'q':
	if (!sscanf(optarg,"%s",(char*)&(set_ps.q)))
	  Error(E_ERROR, POR_AT, "argument for q must be a string\n");
	break;
      case 'i':
	set_ps.i = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for i must be integer\n");
	break;
      case 'j':
	set_ps.j = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for j must be integer\n");
	break;
      case 'k':
	set_ps.k = strtol(optarg, &tail, 0);
	if (tail[0] != '\0') 
	  Error(E_ERROR, POR_AT, "argument for k must be integer\n");
	break;
      case 'x':
	if (!sscanf(optarg,"%lf",&(set_ps.x)))
	  Error(E_ERROR, POR_AT, "argument for x must be double\n");
	break;
      case 'y':
	if (!sscanf(optarg,"%lf",&(set_ps.y)))
	  Error(E_ERROR, POR_AT, "argument for y must be double\n");
	break;
      case 'z':
	if (!sscanf(optarg,"%lf",&(set_ps.z)))
	  Error(E_ERROR, POR_AT, "argument for z must be double\n");
	break;
      case '?':
	printf("wrong arguments to command set\n"); exit(EXIT_FAILURE);
	break;
      }
    }

  if (set_ps.x != INT_MIN || set_ps.y != INT_MIN || set_ps.z != INT_MIN || \
      set_ps.i != INT_MIN || set_ps.j != INT_MIN || set_ps.k != INT_MIN) {

    if (set_ps.x == INT_MIN || set_ps.y == INT_MIN || set_ps.z == INT_MIN || \
	set_ps.i == INT_MIN || set_ps.j == INT_MIN || set_ps.k == INT_MIN) {

      Error(E_ERROR, POR_AT, "x,y,z,i,j,k must be provided all or none\n");
    }
  }
}
