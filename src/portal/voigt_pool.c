/*
   Pool of Voigt profiles for interpolation within the atomic modules.

   The module has to provide the minimum and maximum line Doppler width
   and the minimum and maximum a-parameter (you can find min. and max. Gamma
   of the profiles and divide them by max. and min. Doppler width).

   The output profiles are bilinearly interpolated, hence they are normalized to
   unity.

 */

#include "error.h"
#include "matika.h"
#include "mem.h"
#include "stack.h"
#include "voigt.h"

#define A_STEP                                                                 \
    1.03 /* 1.01 ? */ /* multiplicative factor for the a-parameter (must be >  \
                       * 1)                                                    \
                       */
#define A_0_NSTEPS                                                             \
    100 /* if min. a is 0, then the 0-a_max interval is divided into           \
           A_0_NSTEPS equidistant steps */
#define MAX_A_STEPS 300 /* 1000 ?*/ /* limit for the number of a-parameters */
#define DOPP_STEP                                                              \
    1.03 /* 1.01 ? */ /* Doppler with multiplication factor (must be > 1) */

typedef struct t_profpool {
    int id; /* id of the profile from 0 to MAXN_PROFILES-1 provided by a module
             */
    int     n_freq;
    double *freq; /* frequencies of the abscissas */
    double *wi;   /* weights of the abscissas */
    double  nu0;
    double  nu_d_min;
    double  nu_d_max;
    double  a_min;
    double  a_max;
    int     dispersion_on;
    int norm_warning; /* warning on inaccurate profile normalization has been
                         displayed */

    int     n_d; /* # of discrete Doppler widths */
    double *da;  /* array of Doppler widths */
    int     n_a; /* # of discrete a-parameters */
    double *aa;  /* array of a-paramters */

    /* matrix n_d (rows) X n_a (columns) of pointers to profiles */
    t_absprofile ***pool;
} t_profpool;

static int        g_n_profiles = 0;      /* # of allocated profile pools */
static t_profpool g_pool[MAXN_PROFILES]; /* array of profile pools */

/**********************************************************/
void vgt_PoolClear(void)
{
    int i;

    if (!g_n_profiles)
        return;
    stk_Add(0, POR_AT, "clearing %d Voigt pool profiles", g_n_profiles);
    for (i = 0; i < MAXN_PROFILES; i++) {
        int m, n;
        if (g_pool[i].id < 0)
            continue;
        g_pool[i].id = -1;
        free(g_pool[i].freq);
        free(g_pool[i].wi);
        for (m = 0; m < g_pool[i].n_d; m++) {
            for (n = 0; n < g_pool[i].n_a; n++) {
                free(g_pool[i].pool[m][n]->phi);
                if (g_pool[i].dispersion_on)
                    free(g_pool[i].pool[m][n]->psi);
                free(g_pool[i].pool[m][n]);
            }
            free(g_pool[i].pool[m]);
        }
        free(g_pool[i].pool);
        free(g_pool[i].da);
        free(g_pool[i].aa);
    }
    g_n_profiles = 0;
}

/**********************************************************/
/* return array of a-parameters, n_a is set to the number of elements in this
 * array */
static double *SuggestAs(double a_min, double a_max, int *n_a)
{
    double *as = NULL;
    int     i;

    if (a_min > a_max || a_min < 0)
        Error(E_ERROR, POR_AT, "invalid range of a-parameters (%e, %e)", a_min,
              a_max);
    if (a_min == a_max) {
        *n_a  = 1;
        as    = ArrayNew(1);
        as[0] = a_min;
        return as;
    }
    if (a_min == 0.0) {
        double step  = a_max / (A_0_NSTEPS - 1);
        *n_a         = A_0_NSTEPS;
        as           = ArrayNew(*n_a);
        as[0]        = 0.0;
        as[*n_a - 1] = a_max;
        for (i = 1; i < *n_a - 1; i++)
            as[i] = as[i - 1] + step;
    } else if (log10(a_max / a_min) / log10(A_STEP) >=
               MAX_A_STEPS) /* too huge number of a's */
    {
        double q = log(a_max / a_min) / (MAX_A_STEPS - 1);
        *n_a     = MAX_A_STEPS;
        as       = ArrayNew(*n_a);
        as[0]    = a_min;
        for (i = 1; i < *n_a - 1; i++)
            as[i] = a_min * exp(q * i);
        as[*n_a - 1] = a_max;
    } else {
        *n_a  = 2;
        as    = ArrayNew(*n_a);
        as[0] = a_min;
        as[1] = as[0] * A_STEP;
        while (as[*n_a - 1] < a_max) {
            as       = (double *)Realloc(as, DBLSIZE * (*n_a + 1));
            as[*n_a] = as[*n_a - 1] * A_STEP;
            *n_a += 1;
        }
        as[*n_a - 1] = a_max;
    }
    return as;
}

/**********************************************************/
/* return array of Doppler widths, n_d is set to the number of elements in this
 * array */
static double *SuggestDopplers(double nu_d_min, double nu_d_max, int *n_d)
{
    double *dw = NULL;
    if (nu_d_min > nu_d_max || nu_d_min <= 0)
        Error(E_ERROR, POR_AT, "invalid range of Doppler widths (%e, %e)",
              nu_d_min, nu_d_max);
    if (nu_d_min == nu_d_max) {
        *n_d  = 1;
        dw    = ArrayNew(1);
        dw[0] = nu_d_min;
        return dw;
    }
    dw    = ArrayNew(2);
    dw[0] = nu_d_min;
    dw[1] = dw[0] * DOPP_STEP;
    *n_d  = 2;
    while (dw[*n_d - 1] < nu_d_max) {
        dw       = (double *)Realloc(dw, DBLSIZE * (*n_d + 1));
        dw[*n_d] = dw[*n_d - 1] * DOPP_STEP;
        *n_d += 1;
        if (*n_d == 1000)
            Error(E_ERROR, POR_AT, "too high number of Doppler profiles");
    }
    dw[*n_d - 1] = nu_d_max;
    return dw;
}

/**********************************************************/
void vgt_PoolBuild(int id_transition, int n, const double *freqs, double nu0,
                   double nu_d_min, double nu_d_max, double a_min, double a_max,
                   int dispersion_on)
{
    int         i, j;
    t_profpool *pp;

    stk_Add(0, POR_AT, "initializing profile pool #%d", id_transition);
    if (!g_n_profiles) {
        for (i = 0; i < MAXN_PROFILES; i++) {
            g_pool[i].id = -1;
        }
    }
    if (id_transition < 0 || id_transition >= MAXN_PROFILES) {
        Error(E_ERROR, POR_AT, "invalid profile pool index %d", id_transition);
    }
    pp = &g_pool[id_transition];
    if (pp->id >= 0) {
        Error(E_ERROR, POR_AT,
              "the profile pool #%d has been already initialized",
              id_transition);
    }
    if (n > MAXNFREQ || n < 1) {
        Error(E_ERROR, POR_AT, "invalid number of frequencies: %d", n);
    }
    pp->id            = id_transition;
    pp->a_max         = a_max;
    pp->a_min         = a_min;
    pp->dispersion_on = dispersion_on;
    pp->norm_warning  = 0;
    pp->n_freq        = n;
    pp->freq          = ArrayNew(pp->n_freq);
    Memcpy(pp->freq, freqs, DBLSIZE * pp->n_freq);
    pp->wi = ArrayNew(pp->n_freq);
    if (pp->n_freq >= 2) {
        pp->wi[0] = 0.5 * (pp->freq[1] - pp->freq[0]);
        pp->wi[pp->n_freq - 1] =
            0.5 * (pp->freq[pp->n_freq - 1] - pp->freq[pp->n_freq - 2]);
        for (i = 1; i < pp->n_freq - 1; i++)
            pp->wi[i] = 0.5 * (pp->freq[i + 1] - pp->freq[i - 1]);
    } else {
        pp->wi[0] = 1.0;
    }
    pp->nu0      = nu0;
    pp->nu_d_min = nu_d_min;
    pp->nu_d_max = nu_d_max;
    pp->da       = SuggestDopplers(pp->nu_d_min, pp->nu_d_max, &pp->n_d);
    pp->aa       = SuggestAs(pp->a_min, pp->a_max, &pp->n_a);
    pp->pool     = (t_absprofile ***)Malloc(pp->n_d * sizeof(t_absprofile **));
    for (i = 0; i < pp->n_d; i++) {
        pp->pool[i] = (t_absprofile **)Malloc(pp->n_a * sizeof(t_absprofile *));
        for (j = 0; j < pp->n_a; j++) {
            pp->pool[i][j] =
                vgt_VoigtProfile(pp->n_freq, pp->freq, pp->nu0, pp->da[i],
                                 pp->aa[j], pp->dispersion_on);
        }
    }
    stk_Add(
        0, POR_AT,
        "  the Voigt pool #%d has been initialized with %dx%d = %d profiles",
        id_transition, pp->n_d, pp->n_a, pp->n_d * pp->n_a);
    g_n_profiles++;
}

/**********************************************************/
double vgt_PoolGetProfile(int id_transition, int ifreq, double nu_d, double a,
                          double shift, double prof[2], int calc_norm)
{
    int           id, ia;
    t_profpool *  pp;
    t_absprofile *p00, *p01, *p10, *p11;
    double        td, ta, w_left, w_right, v_ymin, v_ymax;
    static double phi_tmp[MAXNFREQ],
        psi_tmp[MAXNFREQ]; /* arrays to store the non-shifted line profiles */

#ifdef DEBUG
    if (id_transition < 0 || id_transition >= MAXN_PROFILES) {
        Error(E_ERROR, POR_AT, "id of the transition is out of range");
        return 0;
    }
    if (nu_d < g_pool[id_transition].nu_d_min ||
        nu_d > g_pool[id_transition].nu_d_max) {
        Error(E_ERROR, POR_AT, "Doppler width (%e) out of range (%e, %e)", nu_d,
              g_pool[id_transition].nu_d_min, g_pool[id_transition].nu_d_max);
        return 0;
    }
    if (a < g_pool[id_transition].a_min || a > g_pool[id_transition].a_max) {
        Error(E_ERROR, POR_AT, "a-parameter (%e) out of range (%e, %e)", a,
              g_pool[id_transition].a_min, g_pool[id_transition].a_max);
        return 0;
    }
    if (g_pool[id_transition].id != id_transition) {
        Error(E_ERROR, POR_AT, "invalid transition id");
        return 0;
    }
#endif
    prof[0] = prof[1] = 0.0;
    pp                = &g_pool[id_transition];
    if (pp->n_d == 1 || pp->n_a == 1)
        Error(E_ERROR, POR_AT, "sorry, not yet implemented (nd: %d, na: %d)!!!",
              pp->n_d,
              pp->n_a); /* FIXME: implement also the cases n_d=1 and/or n_a=1 */
    id = mat_ArraySearch(pp->n_d, pp->da, nu_d);
    ia = mat_ArraySearch(pp->n_a, pp->aa, a);
#ifdef DEBUG
    if (id < 0 || ia < 0)
        IERR;
#endif
    p00 = pp->pool[id][ia];
    p01 = pp->pool[id][ia + 1];
    p10 = pp->pool[id + 1][ia];
    p11 = pp->pool[id + 1][ia + 1];
    /* interpolation coefficients in the Doppler x Voigt-a space: */
    td = (nu_d - pp->da[id]) / (pp->da[id + 1] - pp->da[id]);
    ta = (a - pp->aa[ia]) / (pp->aa[ia + 1] - pp->aa[ia]);
#ifdef DEBUG
    if (td < 0.0 || td > 1.0 || ta < 0.0 || ta > 1.0)
        IERR;
#endif
    if (!calc_norm && shift != 0.0) /* there is a frequency shift but no
                                       renormalization requested (= fast) */
    {
        int    i;
        double phi_i, psi_i, phi_i1, psi_i1;
        /* find freq[i] in the shifted profile which is just below freq[ifreq]:
         */
        if (shift > 0.0) {
            if (pp->freq[0] + shift > pp->freq[ifreq])
                return 1.0; /* profile out of the requested frequency */
            i = ifreq - 1;
            while (pp->freq[i] + shift > pp->freq[ifreq])
                i--; /* this loop should be fast for a small frequency shift */
        } else {
            if (pp->freq[pp->n_freq - 1] + shift < pp->freq[ifreq])
                return 1.0; /* profile out of the requested frequency */
            i = ifreq;
            while (pp->freq[i] + shift < pp->freq[ifreq])
                i++; /* this loop should be fast for a small frequency shift */
            i--;
        }
        /* frequency weights */
        w_right = (pp->freq[ifreq] - (pp->freq[i] + shift)) /
                  (pp->freq[i + 1] - pp->freq[i]);
        w_left = 1.0 - w_right;
        /* get profile at [i] */
        v_ymin = (p01->phi[i] - p00->phi[i]) * ta + p00->phi[i];
        v_ymax = (p11->phi[i] - p10->phi[i]) * ta + p10->phi[i];
        phi_i =
            (v_ymax - v_ymin) * td + v_ymin; /* profile value at frequency i: */
        /* get profile at [i+1] */
        v_ymin = (p01->phi[i + 1] - p00->phi[i + 1]) * ta + p00->phi[i + 1];
        v_ymax = (p11->phi[i + 1] - p10->phi[i + 1]) * ta + p10->phi[i + 1];
        phi_i1 = (v_ymax - v_ymin) * td +
                 v_ymin; /* profile value at frequency i+1: */
        /* interpolate profile from [i] and [i+1] */
        prof[0] = w_left * phi_i +
                  w_right * phi_i1; /* interpolate the profile in frequency */
        if (pp->dispersion_on) {
            v_ymin = (p01->psi[i] - p00->psi[i]) * ta + p00->psi[i];
            v_ymax = (p11->psi[i] - p10->psi[i]) * ta + p10->psi[i];
            psi_i  = (v_ymax - v_ymin) * td + v_ymin;
            v_ymin = (p01->psi[i + 1] - p00->psi[i + 1]) * ta + p00->psi[i + 1];
            v_ymax = (p11->psi[i + 1] - p10->psi[i + 1]) * ta + p10->psi[i + 1];
            psi_i1 = (v_ymax - v_ymin) * td + v_ymin;
            prof[1] = w_left * psi_i + w_right * psi_i1;
        }
        return 1.0;
    } else if (calc_norm &&
               shift != 0.0) /* if there is a shift then calculate profile but
                                also the normalization factor (requires
                                integration of the profile --> slow) */
    {
        int    i, i_min, i_max, j;
        double norm = 0;
        /* profile completely out of domain? */
        if (pp->freq[0] + shift > pp->freq[pp->n_freq - 1] ||
            pp->freq[pp->n_freq - 1] + shift < pp->freq[0]) {
            Error(E_ERROR, POR_AT, "profile completly out of domain");
            return 0;
        }
        /* get the whole profile with a given nu_d and a: */
        mat_BilinearInterpolation(pp->n_freq, ta, td, phi_tmp, 0.0, 1.0, 0.0,
                                  1.0, p00->phi, p01->phi, p10->phi, p11->phi);
        if (pp->dispersion_on)
            mat_BilinearInterpolation(pp->n_freq, ta, td, psi_tmp, 0.0, 1.0,
                                      0.0, 1.0, p00->psi, p01->psi, p10->psi,
                                      p11->psi);
        /* find the boundary indices in the lab-frame non-zero profile */
        i_min = 0;
        while (pp->freq[i_min] < pp->freq[0] + shift)
            i_min++; /* shift to the right => zero profile on the left */
        i_max = pp->n_freq - 1;
        while (pp->freq[i_max] > pp->freq[pp->n_freq - 1] + shift)
            i_max--; /* shift to the left => zero profile on the right */
        j = 0;
        /* for every i from i_min to i_max find the profile by interpolation in
         * frequencies from phi_tmp[] */
        for (i = i_min; i <= i_max; i++) {
            double fr_left, fr_right;
            while (j < pp->n_freq - 1 && pp->freq[j + 1] + shift < pp->freq[i])
                j++;
#ifdef DEBUG
            if (j == pp->n_freq - 1)
                IERR;
#endif
            fr_left  = pp->freq[j] + shift;
            fr_right = pp->freq[j + 1] + shift;
#ifdef DEBUG
            if (pp->freq[i] < fr_left || pp->freq[i] > fr_right)
                IERR;
#endif
            w_left  = (fr_right - pp->freq[i]) / (fr_right - fr_left);
            w_right = 1.0 - w_left;
            norm +=
                (w_left * phi_tmp[j] + w_right * phi_tmp[j + 1]) * pp->wi[i];
            if (i == ifreq) {
                prof[0] = w_left * phi_tmp[j] + w_right * phi_tmp[j + 1];
                if (pp->dispersion_on)
                    prof[1] = w_left * psi_tmp[j] + w_right * psi_tmp[j + 1];
            }
        }
        if (fabs(norm - 1.0) > 0.75 && !g_pool[id_transition].norm_warning) {
            Error(E_WARNING, POR_AT,
                  "inaccurate discretization (id_tr=%d, %3.3e, shift=%3.3e, "
                  "nud=%e, "
                  "a=%e)",
                  id_transition, norm, shift, nu_d, a);
            g_pool[id_transition].norm_warning = 1;
        }
        return (1.0 / norm); /* return inverse integral */
    } else /* if shift==0 then => life is much easier without the need to
              renormalize */
    {
        /* do the bilinear interpolation by hand: */
        v_ymin  = (p01->phi[ifreq] - p00->phi[ifreq]) * ta + p00->phi[ifreq];
        v_ymax  = (p11->phi[ifreq] - p10->phi[ifreq]) * ta + p10->phi[ifreq];
        prof[0] = (v_ymax - v_ymin) * td + v_ymin;
        if (pp->dispersion_on) {
            v_ymin = (p01->psi[ifreq] - p00->psi[ifreq]) * ta + p00->psi[ifreq];
            v_ymax = (p11->psi[ifreq] - p10->psi[ifreq]) * ta + p10->psi[ifreq];
            prof[1] = (v_ymax - v_ymin) * td + v_ymin;
        }
        return 1.0;
    }
}

/**********************************************************************/
double vgt_PoolGetQuadrature(int id_transition, int ifreq)
{
#ifdef DEBUG
    if (id_transition < 0 || id_transition >= MAXN_PROFILES) {
        Error(E_ERROR, POR_AT, "id of the transition is out of range");
        return 0;
    }
    if (ifreq < 0 || ifreq >= g_pool[id_transition].n_freq) {
        Error(E_ERROR, POR_AT, "invalid frequency index (%d) in the pool %d",
              ifreq, id_transition);
        return 0;
    }
#endif
    return g_pool[id_transition].wi[ifreq];
}
