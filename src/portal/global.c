#include "global.h"
#include "def.h"
#include "directions.h"
#include "error.h"
#include "grid.h"
#include "mem.h"
#include "process.h"
#include "stack.h"
#include "thread_io.h"
#include <string.h>
#include <time.h>

int        G_MY_MPI_RANK; /* rank of the current MPI process */
int        G_MPI_NPROCS;  /* total number of MPI processes = mpi_L*mpi_M+1*/
MPI_Status G_MPI_STATUS;

int G_MPI_L; /* number of horizontal slices (default: DEF_MPI_PROCS_L) */
int G_MPI_M; /* number of wavelength blocks per horizontal slice (default:
                DEF_MPI_PROCS_M) */

int G_MY_ZBAND_ID;    /* Z-band of my process */
int G_MY_FREQBAND_ID; /* Frequency-band of my process */

/* zband_comm:   communicator with all processes sharing the same Z-band as my
   process feqband_comm: communicator with all processes sharing the same
   Frequency-band as my process */
MPI_Comm zband_comm, freqband_comm, slaves_comm;

#ifdef THREAD_IO
MPI_Comm io_comm;
#endif

FILE *G_POR = NULL; /* the model script file */

t_global_vars *g_global = NULL;

static time_t g_start_time;

/* returns number of seconds which lasted since initialization of the portal
 * library */
double LibRunningTime(void)
{
    time_t now = time(NULL);
    return fabs(difftime(now, g_start_time));
}

void InitPorta(void)
{
    int i;
#ifdef DEBUG
    srand(0);
#else
    srand((unsigned)time(NULL));
#endif

    g_start_time = time(NULL);

    if (G_MY_MPI_RANK == 0) {
        PrintfErr("%c%c %s %d.%d.%d %s (build %s, %s"
#ifdef DEBUG
                  ", DEBUGGING BUILD"
#endif
                  ")\n",
                  COMMENT_CHAR, COMMENT_CHAR, POR_LABEL, POR_VER, POR_SUBVER,
                  POR_PATCH, POR_EXTRAVER, __DATE__, __TIME__);
        PrintfErr("%c%c Running %d slave processes (%d subdomains, %d "
                  "frequency bands)\n",
                  COMMENT_CHAR, COMMENT_CHAR, G_MPI_NPROCS - 1, G_MPI_L,
                  G_MPI_M);
    }
    stk_Init();
    g_global = (t_global_vars *)Malloc(sizeof(t_global_vars));

#ifdef THREAD_IO
    g_global->logfile = tmpfile();
    if (!g_global->logfile) {
        fprintf(stderr, "WARNING(%s): cannot open a log-file\n", POR_AT);
    }
#else
    g_global->logfile = fopen(LOG_FILENAME, "w");
    if (!g_global->logfile) {
        fprintf(stderr, "WARNING(%s): cannot open a log-file\n", POR_AT);
    }
#endif
    g_global->stack_output = DEF_SOUT;
    stk_Add(1, POR_AT, "initializing global variables");
    dir_MakeDirections(DEF_NDI_INCL, DEF_NDI_AZIM, &g_global->dirs);
    g_global->n_freq          = 0;
    g_global->max_n_grids     = 1;
    g_global->current_n_grids = 0;
    g_global->period[0] = g_global->period[1] = 1;
    g_global->domain_size.x                   = g_global->domain_size.y =
        g_global->domain_size.z               = 0.0;
    g_global->domain_origin.x                 = g_global->domain_origin.y =
        g_global->domain_origin.z             = 0.0;
    for (i = 0; i < MAX_N_GRIDS; i++) {
        g_global->grid[i].node = NULL;
        g_global->grid[i].nx = g_global->grid[i].ny = g_global->grid[i].nz = 0;
    }
    g_global->module_header_fsize = -1;
    g_global->module_node_fsize   = -1;
#ifdef DEF_MONOTONIC_OPACITY
    g_global->mono_opacity = DEF_MONOTONIC_OPACITY;
#else
    g_global->mono_opacity = 0;
#endif
    /* note that the following default values may be inconsistent with the total
     * number of processes */
    memset(g_global->comment, 0, PMD_COMMENT_LEN + 1);
    PorResetAFunc();
}

void ExitPorta(int stop)
{
    if (!G_MY_MPI_RANK)
        stk_Add(1, POR_AT, "exiting %s", POR_LABEL);
    grd_ReleaseDomain();
    /*CleanBath();*/
    stk_Free();
    if (fclose(g_global->logfile)) {
        fprintf(stderr, "ERROR(%s): cannot close a log-file", POR_AT);
    }
    free(g_global);
    g_global = NULL;
    if (!G_MY_MPI_RANK)
        PrintfErr("%c%c bye\n", COMMENT_CHAR, COMMENT_CHAR);
    /* TODO: use MPI_Abort() */
    if (stop) {
        MPI_Abort(MPI_COMM_WORLD, EXIT_SUCCESS);
        exit(EXIT_SUCCESS);
    }
}


/*********************************************************/
/*                                                       */
/* Command help                                          */
/*                                                       */
/*********************************************************/

#define COMMAND_LIST_HELP						\
  "\nList of PORTA commands: \n\n"					\
  "help:            Prints the list of the available commands and options. \n" \
  "                   Followed by one of the command names below, it provides \n" \
  "                   detailed information about that command\n"	\
  "version:         Prints the PORTA version information \n"		\
  "echo:            Prints (‘echoes’) some arbitrary text\n"		\
  "exit:            Stops processing the commands script\n"		\
  "get:             Prints the value of some variables (number of angles, wavelengths, etc.)\n" \
  "set:             Sets the value of some variables (number of angles, etc.)\n" \
  "loadmodel:       Loads a model from a file\n"			\
  "savemodel:       Saves the curent model into a file\n"		\
  "solve_jacobi:    Solves the non-LTE problem using Jacobi iteration\n" \
  "fs_surface:      Performs the formal solution along a general ray direction\n" \
  "                   for all/some frequencies and stores the emergent radiation in a file\n" \
  "fs_clv_freq:     Performs the formal solution for the calculation of the center-to-limb variation\n" \
  "                   at a given discrete frequency of radiation, spatially averaged\n" \
  "fs_clv_freq_av:  Performs the formal solution for the calculation of the center-to-limb variation\n" \
  "                   at a given discrete frequency of radiation, spatially and azimuth averaged\n" \
  "fs_azaver:       Performs the formal solution, averaged over all grid points and all azimuths.\n" \
  "                   It results in a single Stokes profile\n"		\
  "get_tau_pos:     For a general ray direction it calculates the geometrical coordinates of points\n" \
  "                   with a given tau distance from the surface nodes\n\n" \
  "For module specific command options, check the PORTA manual\n"

#define VERSION_HELP					\
  "\nPORTA command: version\n\n"			\
  "Prints the PORTA version information \n"		
  
#define ECHO_HELP					\
  "\nPORTA command: echo \"text\" \n\n"				\
  "Prints (‘echoes’) some arbitrary \"text\" \n"		
  
#define EXIT_HELP					    \
  "\nPORTA command: exit\n\n"				    \
  "exit:            Stops processing the commands script\n" 
  
#define GET_HELP							\
  "\nPORTA command: get\n\n"						\
  "Prints the value of some variables (number of angles, wavelengths, etc.)\n\n" \
  "Options: \n"								\
  "-------- \n"								\
  "--n_incl|-t:  number of inclination angles per octant \n"		\
  "--n_azim|-a:  number of azimuthal angles per octant \n"		\
  "--stack|-s:   stack output \n"					\
  "--n_freq|-f:  number of radiation frequencies  \n"			\
  "--n_nodes|-n: total number of grid nodes in the grid \n"		\
  "--domain|-d:  domain origin and dimensions \n"			\
  "--period|-p:  horizontal periodicity of the domain on/off \n"	\
  "--wls|-w:     print all the radiation wavelengths to the standard output \n" \
  "--quad|-q:    near optimal angular quadrature set \n" 


#define SET_HELP							\
  "\nPORTA command: set\n\n"						\
  "Sets the value of some variables (number of angles, etc.)\n"		\
  "  The variables representing the domain (ori_ and dim_)\n"		\
  "  must be given all or none. \n\n"					\
  "Options: \n"								\
  "-------- \n"								\
  "--n_incl|-t: number of inclination angles per octant \n"		\
  "--n_azim|-a: number of azimuthal angles per octant \n"		\
  "--stack|-s:  stack output \n"					\
  "--quad|-q:   near optimal angular quadrature set \n"			\
  "--ori_x|-x:  domain origin (X) \n"					\
  "--ori_y|-y:  domain origin (Y) \n"					\
  "--ori_z|-z:  domain origin (Z) \n"					\
  "--dim_x|-i:  domain dimension (X) \n"				\
  "--dim_y|-j:  domain dimension (Y) \n"				\
  "--dim_z|-k:  domain dimension (Z) \n" 


#define LOADMODEL_HELP				\
  "\nPORTA command: loadmodel\n\n"		\
  "Loads a PORTA Model File (PMD) \n\n"		\
  "Options: \n"					\
  "-------- \n"								\
  "--io|-o:       format of the PMD file. Values: [pmd|h5]  (Default: h5) \n" \
  "--file|-f:     input file (PORTA PMD file) \n"

#define SAVEMODEL_HELP				\
  "\nPORTA command: savemodel\n\n"		\
  "Saves the curent model into a PMD file\n\n"	\
  "Options: \n"					\
  "-------- \n"								\
  "--io|-o:       format of the generated PMD file. Values: [pmd|h5]  (Default: h5) \n" \
  "--file|-f:     output file (PORTA PMD file) \n"
  
#define SOLVE_JACOBI_HELP				\
  "\nPORTA command: solve_jacobi\n\n"					\
  "Solves the non-LTE problem using Jacobi iteration. PORTA will continue performing \n" \
  "  Jacobi iterations until one of the criteria below (max_it or max_rc) is reached, \n" \
  "  at which point PORTA will stop\n\n"				\
  "Options: \n"								\
  "-------- \n"								\
  "--max_it|-i: maximum number of Jacobi iterations to perform \n"	\
  "--max_rc|-c: threshold for the relative change of atomic populations between \n" \
  "               consecutive iterations\n"

#define FS_SURFACE_HELP							\
  "\nPORTA command: fs_surface\n\n"					\
  "Performs the formal solution along a general ray direction\n"	\
  "  for all/some frequencies and stores in a file the emergent Stokes \n" \
  "  profiles for each point of the model’s surface. It uses monotonic \n" \
  "  Bézier interpolation \n\n"						\
  "Options: \n"								\
  "-------- \n"								\
  "--io|-o:       format of the generated PSP file. Values: [psp|h5]  (Default: h5) \n" \
  "--method|-c:   characteristics method to use.    Values: [sc|lc]   (Default: lc) \n" \
  "--incl|-i:     inclination angle (in degrees), of the line of sight \n" \
  "                 for which the emergent radiation will be computed \n" \
  "--azim|-a:     azimuthal angle (in degrees) of the line of sight \n"	\
  "                 for which the emergent radiation will be computed \n" \
  "--fr_i_min|-m: starting frequency index \n"				\
  "--fr_i_max|-M: ending frequency index \n"				\
  "--file|-f:     output file (PORTA PSP file) \n"


#define FS_CLV_FREQ_HELP						\
  "\nPORTA command: fs_clv_freq\n\n"					\
  "Performs the formal solution for the calculation of the center-to-limb variation\n" \
  "  of the emergent Stokes profiles at a given discrete frequency of radiation, \n" \
  "spatially averaged\n\n"						\
  "Options: \n"								\
  "-------- \n"								\
  "--method|-c: characteristics method to use. Values: [sc|lc] (Default: lc) \n" \
  "--mu_min|-m: minimum mu value (mu = 0 being the limb and mu = 1 the disk center) \n"	\
  "--mu_max|-M: maximum mu value \n"					\
  "--n_dirs|-d: number of directions to take, equally spaced, in the range of (mu_min,mu_max)\n" \
  "--azim|-a:   azimuth angle (in degrees) \n"				\
  "--freq_i|-i: frequency index for which the center-to-limb variation will be computed \n" \
  "--file|-f:   output file. The center-to-limb variation results will be stored as a plain \n" \
  "               text file, with n_dirs lines, where each one will have five values: \n" \
  "               the mu value and the four Stokes values, where Q,U and V are given as \n" \
  "               a percentage over I. That, is, each line in the file will be given by:\n" \
  "               mu I Q/I[%%] U/I[%%] V/I[%%] \n"


#define FS_CLV_FREQ_AV_HELP						\
  "\nPORTA command: fs_clv_freq_av\n\n"					\
  "Performs the formal solution for the calculation of the center-to-limb variation\n" \
  "  at a given discrete frequency of radiation, spatially and azimuth averaged\n\n" \
  "Options: \n"								\
  "-------- \n"								\
  "--method|-c: characteristics method to use. Values: [sc|lc] (Default: lc) \n" \
  "--mu_min|-m: minimum mu value (mu = 0 being the limb and mu = 1 the disk center) \n"	\
  "--mu_max|-M: maximum mu value \n"					\
  "--n_dirs|-d: number of directions to take, equally spaced, in the range of (mu_min,mu_max)\n" \
  "--n_azim|-a: number of equally spaced azimuth angles to be considered for the calculation \n" \
  "--freq_i|-i: frequency index for which the center-to-limb variation will be computed \n" \
  "--file|-f:   output file. The center-to-limb variation results will be stored as a plain \n" \
  "               text file, with n_dirs lines, where each one will have five values: \n" \
  "               the mu value and the four Stokes values, where Q,U and V are given as \n" \
  "               a percentage over I. That, is, each line in the file will be given by:\n" \
  "               mu I Q/I[%%] U/I[%%] V/I[%%] \n"


#define FS_AZAVER_HELP							\
  "\nPORTA command: fs_azaver\n\n"					\
  "Performs the formal solution, averaged over all grid points and all azimuths.\n" \
  "  It results in a single Stokes profile\n\n"				\
  "Options: \n"								\
  "-------- \n"								\
  "--method|-c:   characteristics method to use. Values: [sc|lc] (Default: lc) \n" \
  "--incl|-t:     inclination angle (in degrees), of the line of sight \n" \
  "--n_azim|-a:   number of equally spaced azimuth angles to be considered for the calculation \n" \
  "--fr_i_min|-m: starting frequency index \n"				\
  "--fr_i_max|-M: ending frequency index \n"				\
  "--file|-f:     output file. The output will be stored as a plain text file, \n" \
  "                 where each line will have five values: the frequency value and \n" \
  "                 the four Stokes values. That, is, each line in the file will be given by:\n" \
  "                 \"labmda I Q U V\" \n"

#define GET_TAU_POS_HELP						\
  "\nPORTA command: get_tau_pos\n\n"					\
  "For a general ray direction it calculates the geometrical coordinates of points\n" \
  "  with a given tau distance from the surface nodes. It stores the data in a TAU file\n\n" \
  "Options: \n"								\
  "-------- \n"								\
  "--io|-o:       format of the generated TAU file. Values: [tau|h5] (Default: h5) \n" \
  "--tau|-t:      optical distance from the surface nodes \n"		\
  "--incl|-i:     inclination angle (in degrees) of the line of sight for which the coordinates \n" \
  "                 of distance = tau are to be calculated.\n"		\
  "--azim|-a:     azimuthal angle (in degrees) of the line of sight for which the coordinates \n"	\
  "                 of distance = tau are to be calculated.\n"		\
  "--fr_i_min|-m: starting frequency index for which the tau coordinates are to be calculated.\n" \
  "--fr_i_max|-M: ending frequency index for which the tau coordinates are to be calculated.\n"	\
  "--file|-f:     output file (PORTA TAU file), which will store, for each wavelength, \n" \
  "                 the actual tau value and the 3D coordinates for distance = tau\n"


void command_help(char *cmd) {
  int tmp;

  if (!cmd) {
    fprintf(stderr, COMMAND_LIST_HELP);
  } else {
    if (!strcmp(cmd,"version")) {
      fprintf(stderr, VERSION_HELP);
    } else if (!strcmp(cmd,"echo")) {
      fprintf(stderr, ECHO_HELP);
    } else if (!strcmp(cmd,"exit")) {
      fprintf(stderr, EXIT_HELP);
    } else if (!strcmp(cmd,"get")) {
      fprintf(stderr, GET_HELP);
    } else if (!strcmp(cmd,"set")) {
      fprintf(stderr, SET_HELP);
    } else if (!strcmp(cmd,"loadmodel")) {
      fprintf(stderr, LOADMODEL_HELP);
    } else if (!strcmp(cmd,"savemodel")) {
      fprintf(stderr, SAVEMODEL_HELP);
    } else if (!strcmp(cmd,"solve_jacobi")) {
      fprintf(stderr, SOLVE_JACOBI_HELP);
    } else if (!strcmp(cmd,"fs_surface")) {
      fprintf(stderr, FS_SURFACE_HELP);
    } else if (!strcmp(cmd,"fs_clv_freq")) {
      fprintf(stderr, FS_CLV_FREQ_HELP);
    } else if (!strcmp(cmd,"fs_clv_freq_av")) {
      fprintf(stderr, FS_CLV_FREQ_AV_HELP);
    } else if (!strcmp(cmd,"fs_azaver")) {
      fprintf(stderr, FS_AZAVER_HELP);
    } else if (!strcmp(cmd,"get_tau_pos")) {
      fprintf(stderr, GET_TAU_POS_HELP);
    } else {
      fprintf(stderr, "Unrecognized command \"%s\". Run \"porta -h\" to see list of available commands\n",cmd);
    }
  }
}
