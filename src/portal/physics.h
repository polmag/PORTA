#ifndef PHCONST_H_
#define PHCONST_H_

/* physical constants (CGS units) */

#define C_C       (299792458.0e2)      /* speed of light [cm s^-2]      */
#define C_H       (6.62606896e-27)     /* Planck constant [erg s]      */
#define C_KB      (1.3806504e-16)      /* Boltzmann constant [erg K^-1] */
#define C_EV      (1.602176565e-12)    /* 1 ev in ergs */
#define C_AU_MASS (1.660538922e-24)    /* atomic mass unit [g] */
#define C_EL_MASS (9.10938188e-28)     /* electron mass [g] */
#define C_RYDBERG (13.60569253 * C_EV) /* energy of 1 Rydberg [erg] */
#define C_BOHRMAG (9.27400968e-21)     /* Bohr magneton [erg.G-1] */

/* this is a trick that makes it sure the optical distance between two points on
 * the short characteristic are not exactly coinciding; such coincidence would
 * require additional testing in the formal solver and to a slower code;
 * assuming that opacity of the vacuum is 10^(-34) does not affect results of
 * the realistic models (it corresponds to optical thickness of 10^(-10) per
 * 10^6 light years) and make the formal solution robust even in the absence of
 * any material; setting VACUUM_OPACITY to 0 allows to avoid the trick but the
 * solution becomes less robust; see fs.c::FSQuad() and fs.c::FSLin() for
 * implementation details;
 */
#define VACUUM_OPACITY (1.0e-34)

/* Planck's law, units: erg cm^-2 s^-1 Hz^-1 srad^-1 */
extern double phy_PlanckFunction(double temp, double freq);

/* Larmor frequency nu_L [s^-1] for magnetic field B [G] */
extern double phy_Larmor(double B);

#endif /* PHCONST_H_ */
