/* Topology of the octree cartesian grids. Don't modify this file unless you are
 * absolutely sure what you are doing. */

#ifndef TOPOLOGY_H_
#define TOPOLOGY_H_

#define BOUNDARY_NONE 0 /* must always be equal to 0 */
#define BOUNDARY_MIN  1 /* boundary at the -axis     */
#define BOUNDARY_MAX  2 /* boundary at the +axis     */

/* 6 cartesian directions */
#define CRIGHT 0
#define CLEFT  1
#define CFRONT 2
#define CBACK  3
#define CUP    4
#define CDOWN  5

/* indices of cell nodes in each of 6 cube faces
 * [face id (0-5)][id of cell node]
 *
 * order of nodes makes a cycle, vertical faces start cycling horizontally
 * (along positive axis direction) at the bottom
 *
 * the same array gives indices of subcels having a boundary face
 * in a given direction
 */
static const int g_c_cube_face[6][4] = {
    {1, 2, 6, 5}, /* face CRIGHT */
    {0, 3, 7, 4}, /* face CLEFT  */
    {3, 2, 6, 7}, /* face CFRONT */
    {0, 1, 5, 4}, /* face CBACK  */
    {4, 5, 6, 7}, /* face CUP    */
    {0, 1, 2, 3}  /* face CDOWN  */
};

#define X_AXIS_DIR(f)                                                          \
    ((f) == CRIGHT || (f) == CLEFT) /* possible hack: (!(f)|((f)==1)) */
#define Y_AXIS_DIR(f)                                                          \
    ((f) == CFRONT || (f) == CBACK) /* possible hack: (!(((f)>>1)^1)) */
#define Z_AXIS_DIR(f)                                                          \
    ((f) == CUP || (f) == CDOWN) /* possible hack: ( ((f)&4) | !((f)-5)) */
#define HORIZONTAL_FACE(f) (Z_AXIS_DIR(f))
#define VERTICAL_FACE(f)   (!Z_AXIS_DIR(f))

/* opposite directions to the cartesian directions */
static const int g_c_opposite_cart_dir[6] = {1, 0, 3, 2, 5, 4};
#define OPP_CARTES(d) g_c_opposite_cart_dir[d]

#ifdef UNDEF
/* complementary directions to the horizontal directions: CRIGHT<->CFRONT,
 * CLEFT<->CBACK */
static const int g_c_complementary_cartesian[4] = {CFRONT, CBACK, CRIGHT,
                                                   CLEFT};
#define COMPL_CARTES(d) g_c_complementary_cartesian[d]
#endif

/* indices of subcells neighboring in given direction
 * [subcell index 0-7][direction of the neighbor 0-6]
 * -1 is set if there is no inner neighbor
 */
static const int g_c_subcell_neighbor[8][6] = {
    {1, -1, 3, -1, 4, -1}, /* subcell 0 */
    {-1, 0, 2, -1, 5, -1}, /* subcell 1 */
    {-1, 3, -1, 1, 6, -1}, /* subcell 2 */
    {2, -1, -1, 0, 7, -1}, /* subcell 3 */
    {5, -1, 7, -1, -1, 0}, /* subcell 4 */
    {-1, 4, 6, -1, -1, 1}, /* subcell 5 */
    {-1, 7, -1, 5, -1, 2}, /* subcell 6 */
    {6, -1, -1, 4, -1, 3}  /* subcell 7 */
};

/* 19 cube subnodes; the 3 indices correspond to x, y, z and their value
 * defines one of 3 possible possitions in the cell
 * (for example, 0=leftmost, 1=center, 2=rightmost in the case of x)
 */
static const int g_c_cube_subnodes[19][3] = {
    {0, 1, 0}, /* 0  */
    {1, 1, 0}, /* 1  */
    {2, 1, 0}, /* 2  */
    {1, 0, 0}, /* 3  */
    {1, 2, 0}, /* 4  */
    {0, 1, 1}, /* 5  */
    {1, 1, 1}, /* 6  */
    {2, 1, 1}, /* 7  */
    {1, 0, 1}, /* 8  */
    {1, 2, 1}, /* 9  */
    {0, 0, 1}, /* 10 */
    {2, 0, 1}, /* 11 */
    {2, 2, 1}, /* 12 */
    {0, 2, 1}, /* 13 */
    {0, 1, 2}, /* 14 */
    {1, 1, 2}, /* 15 */
    {2, 1, 2}, /* 16 */
    {1, 0, 2}, /* 17 */
    {1, 2, 2}  /* 18 */
};

/* 19 subnodes, each containing indices of other subnodes in all 6 directions;
 * if there is no connection in given
 * direction, -1 is put instead of subnode index
 */
static const int g_c_subnode_neighbors[19][6] = {
    {1, -1, -1, -1, 5, -1},  /* 0  */
    {2, 0, 4, 3, 6, -1},     /* 1  */
    {-1, 1, -1, -1, 7, -1},  /* 2  */
    {-1, -1, 1, -1, 8, -1},  /* 3  */
    {-1, -1, -1, 1, 9, -1},  /* 4  */
    {6, -1, 13, 10, 14, 0},  /* 5  */
    {7, 5, 9, 8, 15, 1},     /* 6  */
    {-1, 6, 12, 11, 16, 2},  /* 7  */
    {11, 10, 6, -1, 17, 3},  /* 8  */
    {12, 13, -1, 6, 18, 4},  /* 9  */
    {8, -1, 5, -1, -1, -1},  /* 10 */
    {-1, 8, 7, -1, -1, -1},  /* 11 */
    {-1, 9, -1, 7, -1, -1},  /* 12 */
    {9, -1, -1, 5, -1, -1},  /* 13 */
    {15, -1, -1, -1, -1, 5}, /* 14 */
    {16, 14, 18, 17, -1, 6}, /* 15 */
    {-1, 15, -1, -1, -1, 7}, /* 16 */
    {-1, -1, 15, -1, -1, 8}, /* 17 */
    {-1, -1, -1, 15, -1, 9}  /* 18 */
};

/*
 * connections for corner nodes to the subnodes
 */
static const int g_c_node_subnode[8][6] = {
    {3, -1, 0, -1, 10, -1},   /* 0 */
    {-1, 3, 2, -1, 11, -1},   /* 1 */
    {-1, 4, -1, 2, 12, -1},   /* 2 */
    {4, -1, -1, 0, 13, -1},   /* 3 */
    {17, -1, 14, -1, -1, 10}, /* 4 */
    {-1, 17, 16, -1, -1, 11}, /* 5 */
    {-1, 18, -1, 16, -1, 12}, /* 6 */
    {18, -1, -1, 14, -1, 13}, /* 7 */
};

/*
 * we have 2 cells, each with 8 subcels, what subcells may neighbor those
 * of the second cell?
 * [id of subcell in cell 0-7][direction 0-5]: gives index of subcell in the
 * neighbor
 */
static const int g_c_subcell_subneighbor[8][6] = {
    {-1, 1, -1, 3, -1, 4}, /* subcell 0 */
    {0, -1, -1, 2, -1, 5}, /* subcell 1 */
    {3, -1, 1, -1, -1, 6}, /* subcell 2 */
    {-1, 2, 0, -1, -1, 7}, /* subcell 3 */
    {-1, 5, -1, 7, 0, -1}, /* subcell 4 */
    {4, -1, -1, 6, 1, -1}, /* subcell 5 */
    {7, -1, 5, -1, 2, -1}, /* subcell 6 */
    {-1, 6, 4, -1, 3, -1}  /* subcell 7 */
};

/*
 * node indices in 8 chlidren cells (the corner ones are +19)
 * [8 subcells][8 nodes each]
 * order of subcells inside a cell is the same as the order of corner nodes
 */
static const int g_c_subcell_nodes[8][8] = {
    {19 + 0, 3, 1, 0, 10, 8, 6, 5},    /* subcell 0 */
    {3, 19 + 1, 2, 1, 8, 11, 7, 6},    /* subcell 1 */
    {1, 2, 19 + 2, 4, 6, 7, 12, 9},    /* subcell 2 */
    {0, 1, 4, 19 + 3, 5, 6, 9, 13},    /* subcell 3 */
    {10, 8, 6, 5, 19 + 4, 17, 15, 14}, /* subcell 4 */
    {8, 11, 7, 6, 17, 19 + 5, 16, 15}, /* subcell 5 */
    {6, 7, 12, 9, 15, 16, 19 + 6, 18}, /* subcell 6 */
    {5, 6, 9, 13, 14, 15, 18, 19 + 7}  /* subcell 7 */
};

/* 3 indices of the faces opposite to every node in a cell (= to a given octant
 * index)
 *
 * the order of the cells must always be: [x=const, y=const., z=const.]
 */
static const int g_c_node_opposite_face[8][3] = {
    {0, 2, 4}, /* node/oc-direction 0 */
    {1, 2, 4}, /* node/oc-direction 1 */
    {1, 3, 4}, /* node/oc-direction 2 */
    {0, 3, 4}, /* node/oc-direction 3 */
    {0, 2, 5}, /* node/oc-direction 4 */
    {1, 2, 5}, /* node/oc-direction 5 */
    {1, 3, 5}, /* node/oc-direction 6 */
    {0, 3, 5}  /* node/oc-direction 7 */
};

/* [index of node in the subcells indexing][octant index]
 * indicates whether the octant pointer of a node is changed (>=0)
 * and to which of the subcells (0-7)
 */
static const int g_c_octant_index[19][8] = {
    {-1, -1, -1, -1, -1, 0, 3, -1}, /* subnode 0    */
    {-1, -1, -1, -1, 0, 1, 2, 3},   /* subnode 1    */
    {-1, -1, -1, -1, 1, -1, -1, 2}, /* subnode 2    */
    {-1, -1, -1, -1, -1, -1, 1, 0}, /* subnode 3    */
    {-1, -1, -1, -1, 3, 2, -1, -1}, /* subnode 4    */
    {-1, 0, 3, -1, -1, 4, 7, -1},   /* subnode 5    */
    {0, 1, 2, 3, 4, 5, 6, 7},       /* subnode 6    */
    {1, -1, -1, 2, 5, -1, -1, 6},   /* subnode 7    */
    {-1, -1, 1, 0, -1, -1, 5, 4},   /* subnode 8    */
    {3, 2, -1, -1, 7, 6, -1, -1},   /* subnode 9    */
    {-1, -1, 0, -1, -1, -1, 4, -1}, /* subnode 10   */
    {-1, -1, -1, 1, -1, -1, -1, 5}, /* subnode 11   */
    {2, -1, -1, -1, 6, -1, -1, -1}, /* subnode 12   */
    {-1, 3, -1, -1, -1, 7, -1, -1}, /* subnode 13   */
    {-1, 4, 7, -1, -1, -1, -1, -1}, /* subnode 14   */
    {4, 5, 6, 7, -1, -1, -1, -1},   /* subnode 15   */
    {5, -1, -1, 6, -1, -1, -1, -1}, /* subnode 16   */
    {-1, -1, 5, 4, -1, -1, -1, -1}, /* subnode 17   */
    {7, 6, -1, -1, -1, -1, -1, -1}  /* subnode 18   */
};

/* [index of node in the subcells indexing][octant index]
 * indicates oct-directions which are set to be pointing
 * at the cell neighbor of the face-center new nodes
 * contains index of the neighboring cell in one of 6 directions
 */
static const int g_c_octant_face_center[19][8] = {
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 0: edge              */
    {5, 5, 5, 5, -1, -1, -1, -1},     /* new subnode 1: face direction 5  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 2: edge              */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 3: edge              */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 4: edge              */
    {1, -1, -1, 1, 1, -1, -1, 1},     /* new subnode 5: face direction 1  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 6: cell center       */
    {-1, 0, 0, -1, -1, 0, 0, -1},     /* new subnode 7: face direction 0  */
    {3, 3, -1, -1, 3, 3, -1, -1},     /* new subnode 8: face direction 3  */
    {-1, -1, 2, 2, -1, -1, 2, 2},     /* new subnode 9: face direction 2  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 10: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 11: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 12: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 13: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 14: edge             */
    {-1, -1, -1, -1, 4, 4, 4, 4},     /* new subnode 15: face direction 4 */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 16: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* new subnode 17: edge             */
    {-1, -1, -1, -1, -1, -1, -1, -1}  /* new subnode 18: edge             */
};

/* aliases for 8 octant directions: flipping the vertical direction = change by
 * 4 */
#define OCT_LBD 0 /* Left (-x) Back (-y) Down (-z) : octant 0 */
#define OCT_RBD 1
#define OCT_RFD 2
#define OCT_LFD 3
#define OCT_LBU 4
#define OCT_RBU 5
#define OCT_RFU 6 /* Right (+x) Front (+y) Up (+z) : octant 6 */
#define OCT_LFU 7

static const int g_c_opposite_oc_direction[8] = {
    OCT_RFU, OCT_LFU, OCT_LBU, OCT_RBU, OCT_RFD, OCT_LFD, OCT_LBD, OCT_RBD};
#define OPP_OCT(d) g_c_opposite_oc_direction[d]

/* [subnode index 0-18 (but only edges matter)][0-7:
 * 0=first face neighbor
 * 1=second face neighbor
 * 2,3=ids of octants to be set with 1st neighbor
 * 4,5=ids of octants to be set with 2nd neighbor
 * 6,7=ids of octants to be set with 3nd neighbor (=going from 1st in dir. of
 * 2nd)]
 */
static const int g_c_edge_octants[19][8] = {
    {5, 1, OCT_RBD, OCT_RFD, OCT_LBU, OCT_LFU, OCT_LBD,
     OCT_LFD},                        /* subnode 0  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 1  */
    {5, 0, OCT_LBD, OCT_LFD, OCT_RBU, OCT_RFU, OCT_RBD,
     OCT_RFD}, /* subnode 2  */
    {5, 3, OCT_LFD, OCT_RFD, OCT_LBU, OCT_RBU, OCT_LBD,
     OCT_RBD}, /* subnode 3  */
    {5, 2, OCT_LBD, OCT_RBD, OCT_LFU, OCT_RFU, OCT_LFD,
     OCT_RFD},                        /* subnode 4  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 5  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 6  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 7  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 8  */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 9  */
    {1, 3, OCT_LFU, OCT_LFD, OCT_RBU, OCT_RBD, OCT_LBU,
     OCT_LBD}, /* subnode 10 */
    {3, 0, OCT_LBU, OCT_LBD, OCT_RFU, OCT_RFD, OCT_RBU,
     OCT_RBD}, /* subnode 11 */
    {0, 2, OCT_RBU, OCT_RBD, OCT_LFU, OCT_LFD, OCT_RFU,
     OCT_RFD}, /* subnode 12 */
    {1, 2, OCT_LBU, OCT_LBD, OCT_RFU, OCT_RFD, OCT_LFU,
     OCT_LFD}, /* subnode 13 */
    {1, 4, OCT_LBD, OCT_LFD, OCT_RBU, OCT_RFU, OCT_LBU,
     OCT_LFU},                        /* subnode 14 */
    {-1, -1, -1, -1, -1, -1, -1, -1}, /* subnode 15 */
    {0, 4, OCT_RBD, OCT_RFD, OCT_LBU, OCT_LFU, OCT_RBU,
     OCT_RFU}, /* subnode 16 */
    {3, 4, OCT_LBD, OCT_RBD, OCT_LFU, OCT_RFU, OCT_LBU,
     OCT_RBU}, /* subnode 17 */
    {2, 4, OCT_LFD, OCT_RFD, OCT_LBU, OCT_RBU, OCT_LFU,
     OCT_RFU} /* subnode 18 */
};

/* index of the given node in a neighboring cell (of the same generation /
 * "mirrored") in a given cartesian direction (to be used mainly by PBC) */
static const int g_c_mirrornodeindex[6][8] = {
    /* 0  1  2  3  4  5  6  7 */
    {-1, 0, 3, -1, -1, 4, 7, -1}, /* CRIGHT */
    {1, -1, -1, 2, 5, -1, -1, 6}, /* CLEFT  */
    {-1, -1, 1, 0, -1, -1, 5, 4}, /* FRONT  */
    {3, 2, -1, -1, 7, 6, -1, -1}, /* CBACK  */
    {-1, -1, -1, -1, 0, 1, 2, 3}, /* CUP    */
    {4, 5, 6, 7, -1, -1, -1, -1}  /* CDOWN  */
};

/* 3D vector components in the directions corresponding to 8 oct-directions */
static const double g_c_oct_vec[8][3] = {
    {-1.0, -1.0, -1.0}, /* 0 */
    {1.0, -1.0, -1.0},  /* 1 */
    {1.0, 1.0, -1.0},   /* 2 */
    {-1.0, 1.0, -1.0},  /* 3 */
    {-1.0, -1.0, 1.0},  /* 4 */
    {1.0, -1.0, 1.0},   /* 5 */
    {1.0, 1.0, 1.0},    /* 6 */
    {-1.0, 1.0, 1.0}    /* 7 */
};

#endif /* TOPOLOGY_H_ */
