/* Encapsulated postscript output via the Cgraph library */

#ifndef PLOT_H_
#define PLOT_H_

/*#include <cgraph.h>*/
#include "def.h"

/* contains a 2D data that can be used for scatter plots (like domain-surface
 * intensities etc.) */
typedef struct t_scatter_plot {
    int      n_points;
    int      n_data;
    double * x;    /* array of n_points x-coordinates */
    double * y;    /* array of n_points y-coordinates */
    double **data; /* matrix of n_points X n_data function values */
} t_scatter_plot;

/* initialized an empty scatter plot */
extern void plt_InitScatterPlot(t_scatter_plot *sp);

/* frees memory allocated for a scatter plot and initializes it */
extern void plt_ResetScatterPlot(t_scatter_plot *sp);

/* add a new data point to the scatter plot structure */
extern void plt_AddScatterPoint(t_scatter_plot *sp, double x, double y,
                                int n_data, double *data);

/* store the scatter plot to a text file
 * index: index of in the data array to be saved
 * fname: file name where to save the plot data
 * NOTE: the coordinates are in kilometers
 */
extern int plt_ScatterPlotSave(t_scatter_plot *sp, int index,
                               const char *fname);

#ifdef UNDEF
/* TODO: add a limit of max. generation to be plotted */
/* draw the grid mesh into an EPS file */
extern int PlotMeshEPS(char *filename, t_vector_3d viewpoint,
                       t_vector_3d lookat, double perspective);
#endif

#ifdef UNDEF
/* IFRIT *.bin file for <J00> for a given transition, i.e. element in the module
 * array; returns nonzero on success */
extern int plt_J00_3D_(int transition_id, char file_name[]);
#endif

#endif /* PLOT_H_ */
