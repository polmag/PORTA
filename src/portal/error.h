/* Error handling */

#ifndef ERROR_H_
#define ERROR_H_

#include <stdio.h>

#if __STDC_VERSION__ < 199901L
#if __GNUC__ >= 2
#define __func__ __FUNCTION__
#else
#define __func__ "<unknown>"
#endif
#else
#define __func__ "<unknown>"
#endif

#define POR_STRINGIFY(x) #x
#define POR_TOSTRING(x)  POR_STRINGIFY(x)
/* #define POR_AT __FILE__ ":" __func__ ":" POR_TOSTRING(__LINE__) */
#define POR_AT __FILE__ ":" POR_TOSTRING(__LINE__)

#define IERR Error(E_ERROR, POR_AT, "internal error")
#define CERR Error(E_ERROR, POR_AT, "communication error");

enum t_error_level { E_MESSAGE = 0, E_WARNING = 1, E_ERROR = 2 };

extern void Error(enum t_error_level level, char *location, char *err, ...);

extern void Printf(char *fmt, ...);

extern void PrintfErr(char *fmt, ...);

extern void SPrintf(char *destination, char *fmt, ...);

extern int FClose(FILE *stream);

#endif /* ERROR_H_ */
