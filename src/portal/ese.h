/* general routines for solution of the statistical equilibrium equations */

#ifndef ESE_H_
#define ESE_H_

#include "def.h"

/* max. rel. change of populations of a given density matrix */
extern double ese_MaxRelativeChange(int n, const int *pops,
                                    const double *old_dm, const double *new_dm);

/* solve ESE in every node of the subdomain (but NOT in the virtual layers
 * min_iz-1 & max_iz+1; use pro_SynchronizeOverlappingDM()) returns maximum
 * relative change of the density matrix populations
 */
extern double ese_SolveGridPrecondESE(t_grid *g);

#endif /* ESE_H_ */
