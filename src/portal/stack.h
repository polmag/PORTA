/* Porta message stack */

#ifndef STACK_H_
#define STACK_H_

/* initializes stack; must be called at the very beginning of the program */
extern void stk_Init(void);

/* release memory allocated for the stack; to be called at the end of Porta */
extern void stk_Free(void);

/* adds record to the stack; if (print) then the message is also send to stdout
 */
extern void stk_Add(int print, char *location, char *fnct, ...);

/* prints stack to stderr */
extern void stk_Dump(void);

#endif /* STACK_H_ */
