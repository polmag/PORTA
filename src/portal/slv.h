/* several full non-LTE solution algorithms that can be invoked from console */

#ifndef SOLVERS_H_
#define SOLVERS_H_

/* (slv_jacobi.c) solution of the non-LTE problem in a given grid via the Jacobi
 * iteration; called by every slave process
 *
 * p_grid: pointer to the grid to use
 * max_it: max. number of iterations
 * max_rc: max. relative change of populations
 *
 * return value: maximum relative change of populations in the whole domain
 */
extern double slv_Jacobi(t_grid *grid, int max_it, double max_rc);

/* uses g_global->max_n_grids for coarse grids
 * (implementation: mg.c)
 */
extern void slv_SolveStandardMG(int n_v_cycles, int n_presmooth,
                                int n_postsmooth);

/* uses g_global->max_n_grids for coarse grids
 * accuracy: maximum relative change in the finest grid
 * OR
 * max_v_cycles: maximum number of V-cycles to perform
 * (implementation: mg.c)
 */
extern void slv_SolveNestedMG(int n_presmooth, int n_postsmooth,
                              double accuracy, int max_v_cycles);

#endif /* SOLVERS_H_ */
