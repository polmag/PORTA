#include "stack.h"
#include "def.h"
#include "global.h"
#include "mem.h"
#include "thread_io.h"
#include <fcntl.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

typedef struct {
    int    n_record;
    int    last;
    char **record;
} t_stack;

static t_stack stack;

void stk_Init(void)
{
    extern t_stack stack;
    stack.n_record = 0;
    stack.record   = NULL;
    stack.last     = -1;
#if defined(STACK_ON)
    stack.record = (char **)Malloc(sizeof(char *) * STACK_SIZE);
    memset(stack.record, 0, sizeof(char *) * STACK_SIZE);
#endif
}

void stk_Free(void)
{
#if defined(STACK_ON)
    extern t_stack stack;
    int            i;
    for (i = 0; i < stack.n_record; i++)
        if (stack.record[i])
            free(stack.record[i]);
    free(stack.record);
    stack.record   = NULL;
    stack.n_record = 0;
    stack.last     = -1;
#endif
}

void stk_Add(int print, char *location, char *fnct, ...)
{
#if !defined(STACK_ON)
    return;
#endif
    extern t_stack stack;
    va_list        argptr;
    char           text[STACK_MAX_LEN];
    t_stack *      s    = &stack;
    static char    dv[] = " :: ";
    int            len1, len2, len3, len, l;

#ifdef THREAD_IO
    char buf[IO_MAX_LEN];
#endif

    va_start(argptr, fnct);
    vsprintf(text, fnct, argptr);
    va_end(argptr);
    if (strlen(text) > sizeof(text)) {
#ifdef THREAD_IO
        l = sprintf(buf, "%c%c STACK ERROR: memory overwrite\n", COMMENT_CHAR,
                    COMMENT_CHAR);
        print_t0(io_comm, buf, l + 1,
                 SERR_IO); // l+1 so we also send the null character
#else
        fprintf(stderr, "%c%c STACK ERROR: memory overwrite\n", COMMENT_CHAR,
                COMMENT_CHAR);
#endif
        exit(EXIT_FAILURE);
    }
    s->last = (s->last + 1) % STACK_SIZE;
    if (s->record[s->last])
        free(s->record[s->last]);
    len1               = strlen(location);
    len2               = strlen(dv);
    len3               = strlen(text);
    len                = len1 + len2 + len3;
    s->record[s->last] = (char *)Malloc(CHRSIZE * (len + 1));
    memcpy(s->record[s->last], location, len1);
    memcpy(s->record[s->last] + len1, dv, len2);
    memcpy(s->record[s->last] + len1 + len2, text, len3);
    *(s->record[s->last] + len) = (char)0;
    if ((G_MY_MPI_RANK == 0 || G_MY_MPI_RANK == 1) && print &&
        g_global->stack_output != SOUT_OFF) {
        char *ms = strstr(s->record[s->last], dv);
        if (g_global->stack_output & SOUT_STD) {
#ifdef THREAD_IO
            l = sprintf(buf, "%c%c %s\n", COMMENT_CHAR, COMMENT_CHAR,
                        ms + len2);
            print_t0(io_comm, buf, l + 1,
                     SOUT_IO); // l+1 so we also send the null character
#else
            fprintf(stdout, "%c%c [%d] %s\n", COMMENT_CHAR, COMMENT_CHAR,
                    G_MY_MPI_RANK, ms + len2);
#endif
        }
        if (g_global->stack_output & SOUT_ERR) {
#ifdef THREAD_IO
            l = sprintf(buf, "%c%c %s\n", COMMENT_CHAR, COMMENT_CHAR,
                        ms + len2);
            print_t0(io_comm, buf, l + 1,
                     SERR_IO); // l+1 so we also send the null character
#else
            fprintf(stderr, "%c%c [%d] %s\n", COMMENT_CHAR, COMMENT_CHAR,
                    G_MY_MPI_RANK, ms + len2);
#endif
        }
        if ((g_global->stack_output & SOUT_LOG) && g_global->logfile) {
            time_t     thetime;
            struct tm *dt;
            char       cas[128];
            time(&thetime);
            dt = localtime(&thetime);
            strftime((char *)&cas, 127, "%a %b %d %H:%M:%S %Y", dt);
#ifdef THREAD_IO
            l = sprintf(buf, "@ %s: %s\n", (char *)&cas, ms + len2);
            print_t0(io_comm, buf, l + 1,
                     SLOG_IO); // l+1 so we also send the null character
#else
            if (fprintf(g_global->logfile, "@ %s: ", (char *)&cas) < 0) {
                fprintf(stderr, "STACK ERROR: cannot write to log-file");
            }
            if (fprintf(g_global->logfile, "%s\n", ms + len2) < 0) {
                fprintf(stderr, "STACK ERROR: cannot write to log-file");
            }
#endif
        }
    }
    /* logging of everything */
    else if (!print && (g_global->stack_output | SOUT_LOG) &&
             g_global->logfile) {
        char *     ms = strstr(s->record[s->last], dv);
        time_t     thetime;
        struct tm *dt;
        char       cas[128];
        time(&thetime);
        dt = localtime(&thetime);
        strftime((char *)&cas, 127, "%a %b %d %H:%M:%S %Y", dt);
#ifdef THREAD_IO
        l = sprintf(buf, "@ %s: %s\n", (char *)&cas, ms + len2);
        print_t0(io_comm, buf, l + 1,
                 SLOG_IO); // l+1 so we also send the null character
#else
        if (fprintf(g_global->logfile, "@ %s: ", (char *)&cas) < 0) {
            fprintf(stderr, "STACK ERROR: cannot write to log-file");
        }
        if (fprintf(g_global->logfile, "%s\n", ms + len2) < 0) {
            fprintf(stderr, "STACK ERROR: cannot write to log-file");
        }
#endif
    }
    s->n_record = (s->n_record == STACK_SIZE ? STACK_SIZE : s->n_record + 1);
}

void stk_Dump(void)
{
#if !defined(STACK_ON)
    return;
#endif
    extern t_stack stack;
    int            i, cnt, l;
    t_stack *      s = &stack;

#ifdef THREAD_IO
    char buf[IO_MAX_LEN];
#endif

    if (!s->n_record)
        return;
    if (s->n_record == STACK_SIZE) {
        cnt = 0;
        for (i = s->last; cnt++ < STACK_SIZE;
             i = (i - 1 + STACK_SIZE) % STACK_SIZE) {
#ifdef THREAD_IO
            l = sprintf(buf, "%s\n", s->record[i]);
            print_t0(io_comm, buf, l + 1,
                     SERR_IO); // l+1 so we also send the null character
            print_t0(io_comm, buf, l + 1,
                     SLOG_IO); // l+1 so we also send the null character
#else
            fprintf(stderr, "%s\n", s->record[i]);
            if (fprintf(g_global->logfile, "%s\n", s->record[i]) < 0) {
                fprintf(stderr, "STACK ERROR: cannot write to log-file");
            }

#endif
        }
    } else {
        for (i = s->last; i >= 0; i--) {
#ifdef THREAD_IO
            l = sprintf(buf, "%s\n", s->record[i]);
            print_t0(io_comm, buf, l + 1,
                     SERR_IO); // l+1 so we also send the null character
            print_t0(io_comm, buf, l + 1,
                     SLOG_IO); // l+1 so we also send the null character
#else
            fprintf(stderr, "%s\n", s->record[i]);
            if (fprintf(g_global->logfile, "%s\n", s->record[i]) < 0) {
                fprintf(stderr, "STACK ERROR: cannot write to log-file");
            }
#endif
        }
    }
}
