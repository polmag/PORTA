#include "grid.h"
#include "error.h"
#include "global.h"
#include "matika.h"
#include "mem.h"
#include "modules.h"
#include "process.h"
#include "stack.h"
#include "topology.h"
#include <float.h>
#include <stdlib.h>

extern t_global_vars *g_global;

/* function to give the x coordinate inside the domain of an arbitrary x
   coordinate grd_XCycl takes as input an index, this one takes as input a
   coordinate */
double grd_cycle_x(const t_grid *g, double x)
{
    double domain_end_x;

    if (x < g_global->domain_origin.x) {
        // we are below the minimum value in X
        domain_end_x = g_global->domain_origin.x + g_global->domain_size.x;
        return domain_end_x - fmod(domain_end_x - x, g_global->domain_size.x);

    } else if (x > g_global->domain_origin.x + g_global->domain_size.x) {
        // we are above the maximum value in X
        return g_global->domain_origin.x +
               fmod(x - g_global->domain_origin.x, g_global->domain_size.x);
    } else {
        // we are inside the domain limits of the model
        return x;
    }
}

/* function to give the y coordinate inside the domain of an arbitrary y
   coordinate grd_YCycl takes as input an index, this one takes as input a
   coordinate */
double grd_cycle_y(const t_grid *g, double y)
{
    double domain_end_y;

    if (y < g_global->domain_origin.y) {
        // we are below the minimum value in Y
        domain_end_y = g_global->domain_origin.y + g_global->domain_size.y;
        return domain_end_y - fmod(domain_end_y - y, g_global->domain_size.y);

    } else if (y > g_global->domain_origin.y + g_global->domain_size.y) {
        // we are above the maximum value in Y
        return g_global->domain_origin.y +
               fmod(y - g_global->domain_origin.y, g_global->domain_size.y);
    } else {
        // we are inside the domain limits of the model
        return y;
    }
}

/*******************************************************************************/
double grd_XCycl(const t_grid *g, int ix)
{
    /* the % operator for negative numbers is machine dependent in C =>
     * transfrom to positive operands */
    if (ix >= 0) {
        return g_global->domain_origin.x + (g->x[ix % g->nx] - g->x[0]) +
               g_global->domain_size.x * (ix / g->nx);
    } else {
        /* TODO: CHECK THIS!!!!!! */
        return g_global->domain_origin.x +
               (g->x[g->nx - 1 - ((-ix - 1) % g->nx)] - g->x[0]) +
               g_global->domain_size.x * ((ix + 1) / g->nx - 1);
    }
}

/*******************************************************************************/
double grd_YCycl(const t_grid *g, int iy)
{
    /* the % operator for negative numbers is machine dependent in C =>
     * transfrom to positive operands */
    if (iy >= 0) {
        return g_global->domain_origin.y + (g->y[iy % g->ny] - g->y[0]) +
               g_global->domain_size.y * (iy / g->ny);
    } else {
        /* TODO: CHECK THIS!!!!!! */
        return g_global->domain_origin.y +
               (g->y[g->ny - 1 - ((-iy - 1) % g->ny)] - g->y[0]) +
               g_global->domain_size.y * ((iy + 1) / g->ny - 1);
    }
}

/*******************************************************************************/
void grd_SetDomainGeometry(t_vec_3d origin, t_vec_3d dim)
{
    if (dim.x <= 0 || dim.y <= 0 || dim.z <= 0) {
        Error(E_ERROR, POR_AT, "invalid domain dimensions (%e, %e, %e)", dim.x,
              dim.y, dim.z);
        return;
    }
    if (g_global->domain_size.x * g_global->domain_size.y *
            g_global->domain_size.z ==
        0) {
        if (g_global->grid[0].node)
            Error(E_ERROR, POR_AT,
                  "inconsistent setup: uninitialized grid geometry vs. "
                  "initialized "
                  "grid nodes");
        g_global->domain_size.x   = dim.x;
        g_global->domain_size.y   = dim.y;
        g_global->domain_size.z   = dim.z;
        g_global->domain_origin.x = origin.x;
        g_global->domain_origin.y = origin.y;
        g_global->domain_origin.z = origin.z;
    } else {
        int ig;
        for (ig = 0; ig < g_global->current_n_grids; ig++) {
            int     ncx, ncy, ncz, n;
            double *pcx, *pcy, *pcz, a, b;
            ncx = g_global->grid[ig].nx;
            ncy = g_global->grid[ig].ny;
            ncz = g_global->grid[ig].nz;
            pcx = g_global->grid[ig].x;
            pcy = g_global->grid[ig].y;
            pcz = g_global->grid[ig].z;
            a   = dim.x / g_global->domain_size.x;
            b   = origin.x - a * g_global->domain_origin.x;
            for (n = 0; n < ncx; n++)
                pcx[n] = a * pcx[n] + b;
            a = dim.y / g_global->domain_size.y;
            b = origin.y - a * g_global->domain_origin.y;
            for (n = 0; n < ncy; n++)
                pcy[n] = a * pcy[n] + b;
            a = dim.z / g_global->domain_size.z;
            b = origin.z - a * g_global->domain_origin.z;
            for (n = 0; n < ncz; n++)
                pcz[n] = a * pcz[n] + b;
        }
    }
    g_global->domain_size.x   = dim.x;
    g_global->domain_size.y   = dim.y;
    g_global->domain_size.z   = dim.z;
    g_global->domain_origin.x = origin.x;
    g_global->domain_origin.y = origin.y;
    g_global->domain_origin.z = origin.z;
}

/*******************************************************************************/
/* intersection providing the 9-point (biquadratic) or 4-point (bilinear)
 * stencil for use in the FS interpolation */
void grd_FindIntersection(const t_node *p_node, t_vec_3d *dir, int horizontal,
                          t_intersect *inters, int *cross_x, int *cross_y)
{
    double  tx, ty, tz, x_inters, y_inters, z_inters;
    int     dix, diy, diz, ix_inters, iy_inters, iz_inters;
    t_grid *g;

#ifdef DEBUG
    int min_iz, max_iz;
    pro_MinMax_IZ(G_MY_MPI_RANK, p_node->p_grid->nz, &min_iz, &max_iz);
#endif

#ifdef DEBUG
    if (!p_node || !dir || !inters)
        IERR;
#endif

    *cross_x = *cross_y = 0;
    g                   = p_node->p_grid;
    dix                 = (dir->x >= 0 ? 1 : -1);
    diy                 = (dir->y >= 0 ? 1 : -1);
    diz                 = (dir->z >= 0 ? 1 : -1);
    ix_inters           = p_node->ix + dix;
    iy_inters           = p_node->iy + diy;
    iz_inters           = p_node->iz + diz;
#ifdef DEBUG
#ifdef FS_LIN
    if (iz_inters < min_iz || iz_inters > max_iz)
        IERR;
#else
    if (iz_inters < min_iz - 1 || iz_inters > max_iz + 1 || iz_inters < 0 ||
        iz_inters >= g->nz)
        IERR;
#endif
#endif
    x_inters = grd_XCycl(g, ix_inters);
    y_inters = grd_YCycl(g, iy_inters);
    z_inters = g->z[iz_inters];
    if (horizontal) {
        double xn, yn;
        int    ixn, iyn;
        inters->iz1 = iz_inters;
        if (fabs(dir->z) < DBL_EPSILON)
            IERR;
        tz = (z_inters - NODE_Z(p_node)) / dir->z;
#ifdef DEBUG
        if (tz <= 0)
            IERR;
#endif
        inters->orientation = ORI_XY;
        inters->distance    = tz;
        inters->inters.x    = NODE_X(p_node) + inters->distance * dir->x;
        inters->inters.y    = NODE_Y(p_node) + inters->distance * dir->y;
        inters->inters.z    = z_inters;

        ixn = p_node->ix + dix;
        xn  = grd_XCycl(g, ixn);
        while ((inters->inters.x - xn) * dix > 0) {
            ixn += dix;
            xn = grd_XCycl(g, ixn);
        }
        *cross_x = abs(ixn - p_node->ix) - 1;

        inters->ix1 =
            ixn - (dix + 1) / 2; /* equivalent to:    if (dix>0) inters->ix1 =
                                    ixn-1; else inters->ix1 = ixn */

        iyn = p_node->iy + diy;
        yn  = grd_YCycl(g, iyn);
        while ((inters->inters.y - yn) * diy > 0) {
            iyn += diy;
            yn = grd_YCycl(g, iyn);
        }
        *cross_y = abs(iyn - p_node->iy) - 1;

        inters->iy1 =
            iyn - (diy + 1) / 2; /* equivalent to:    if (diy>0) inters->iy1 =
                                    iyn-1; else inters->iy1 = iyn */
        return;
    }
    tx =
        (fabs(dir->x) > DBL_EPSILON
             ? (x_inters - NODE_X(p_node)) / dir->x
             : DBL_MAX); /* allows for characteristics parallel to the X axis */
    ty = (fabs(dir->y) > DBL_EPSILON ? (y_inters - NODE_Y(p_node)) / dir->y
                                     : DBL_MAX);
    tz = (fabs(dir->z) > DBL_EPSILON ? (z_inters - NODE_Z(p_node)) / dir->z
                                     : DBL_MAX);
    /*
    tx = (x_inters - NODE_X(p_node)) / dir->x;
    ty = (y_inters - NODE_Y(p_node)) / dir->y;
    tz = (z_inters - NODE_Z(p_node)) / dir->z;
    */
#ifdef DEBUG
    if (tx <= 0 || ty <= 0 || tz <= 0) {
        Error(E_ERROR, POR_AT,
              "invalid t-s: %e %e %e (%d, %d, %d) (%e, %e, %e) ", tx, ty, tz,
              p_node->ix, p_node->iy, p_node->iz, dir->x, dir->y, dir->z);
    }
#endif
    if (tz <= tx && tz <= ty) {
        inters->orientation = ORI_XY;
        inters->distance    = tz;
        inters->inters.x    = NODE_X(p_node) + inters->distance * dir->x;
        inters->inters.y    = NODE_Y(p_node) + inters->distance * dir->y;
        inters->inters.z    = z_inters;
        inters->iz1         = iz_inters;

        inters->ix1 = p_node->ix + (dix - 1) / 2;
        inters->iy1 = p_node->iy + (diy - 1) / 2;
    } else if (tx <= ty && tx <= tz) {
        inters->orientation = ORI_YZ;
        inters->distance    = tx;
        inters->inters.x    = x_inters;
        inters->inters.y    = NODE_Y(p_node) + inters->distance * dir->y;
        inters->inters.z    = NODE_Z(p_node) + inters->distance * dir->z;
        inters->ix1         = ix_inters;

        inters->iy1 = p_node->iy + (diy - 1) / 2;
        inters->iz1 = p_node->iz + (diz - 1) / 2;
    } else {
        inters->orientation = ORI_XZ;
        inters->distance    = ty;
        inters->inters.x    = NODE_X(p_node) + inters->distance * dir->x;
        inters->inters.y    = y_inters;
        inters->inters.z    = NODE_Z(p_node) + inters->distance * dir->z;
        inters->iy1         = iy_inters;

        inters->ix1 = p_node->ix + (dix - 1) / 2;
        inters->iz1 = p_node->iz + (diz - 1) / 2;
    }
#ifdef DEBUG
    if ((inters->iz1 < min_iz - 1 || inters->iz1 >= max_iz + 1) &&
        inters->orientation != ORI_XY) {
        Error(E_ERROR, POR_AT, "invalid iz1=%d", inters->iz1);
    }
#endif
}

/*******************************************************************************/
/* WARNING: p_data must be set to NULL for a new node before calling this
 * function (do it while allocating) */
static void InitNode(t_grid *grid, int ix, int iy, int iz)
{
    t_node *p;
    p         = &grid->node[iz][iy][ix];
    p->p_grid = grid;
    p->ix     = ix;
    p->iy     = iy;
    p->iz     = iz;
    if (mod_IsModuleLoaded()) {
        g_global->afunc.F_Init_p_data(p);
    } else {
        p->p_data = NULL;
    }
}

/*******************************************************************************/
void grd_NewGrid(int grid_id, int nx, int ny, int nz)
{
    int i;
    int min_iz, max_iz;
    stk_Add(IAM_MASTER, POR_AT, "creating a new grid with %d x %d x %d nodes",
            nx, ny, nz);
    if (grid_id < 0 || grid_id >= MAX_N_GRIDS) {
        Error(E_ERROR, POR_AT, "invalid grid level %d", grid_id);
    }
    if (g_global->grid[grid_id].node) {
        Error(E_ERROR, POR_AT, "grid already initialized");
    }
    if (nz < 4 || ny < 3 || nx < 3) {
        Error(E_ERROR, POR_AT,
              "invalid number of grid nodes (nx: %d, ny: %d, nz: %d)", nx, ny,
              nz);
    }
    if (g_global->current_n_grids != grid_id) {
        Error(E_ERROR, POR_AT, "grids must be initialized sequantially!");
    } else if (grid_id >= g_global->max_n_grids) {
        Error(E_ERROR, POR_AT, "maximum number of grids exceeded (%d)",
              g_global->max_n_grids);
    }
    if (g_global->domain_size.x <= 0.0 || g_global->domain_size.y <= 0.0 ||
        g_global->domain_size.z <= 0.0) {
        Error(E_ERROR, POR_AT, "the domain dimensions are not initialized");
        return;
    }
    g_global->grid[grid_id].nx = nx;
    g_global->grid[grid_id].ny = ny;
    g_global->grid[grid_id].nz = nz;
    ArraySet(MAX_N_ORDINATES, g_global->grid[grid_id].x, 0.0);
    ArraySet(MAX_N_ORDINATES, g_global->grid[grid_id].y, 0.0);
    ArraySet(MAX_N_ORDINATES, g_global->grid[grid_id].z, 0.0);
    for (i = 0; i < nx; i++)
        g_global->grid[grid_id].x[i] =
            g_global->domain_origin.x + g_global->domain_size.x * i / nx;
    for (i = 0; i < ny; i++)
        g_global->grid[grid_id].y[i] =
            g_global->domain_origin.y + g_global->domain_size.y * i / ny;
    for (i = 0; i < nz; i++)
        g_global->grid[grid_id].z[i] =
            g_global->domain_origin.z + g_global->domain_size.z * i / (nz - 1);
    if (!pro_MinMax_IZ(G_MY_MPI_RANK, nz, &min_iz, &max_iz) && G_MY_MPI_RANK) {
        Error(E_ERROR, POR_AT, "cannot decompose the domain");
        return;
    }
    if (!IAM_MASTER)
        stk_Add(0, POR_AT, "subdomain z-interval: %d -- %d", min_iz, max_iz);
    g_global->grid[grid_id].node = (t_node ***)Malloc(sizeof(t_node **) * nz);
    if (G_MY_MPI_RANK >= 1 && G_MY_MPI_RANK <= G_MPI_L)
        PrintfErr("%c%c Subdomain [%d] z-indices: %d--%d (%d in total)\n",
                  COMMENT_CHAR, COMMENT_CHAR, G_MY_MPI_RANK, min_iz, max_iz,
                  max_iz - min_iz + 1);

    if (!IAM_MASTER && max_iz - min_iz < 1) {
        Error(
            E_ERROR, POR_AT,
            "\n\n===========================\nError in process: %d\nEach "
            "subdomain needs at least 2 z-indices. \nYou should run PORTA with "
            "fewer L domains\n===========================\n\n",
            G_MY_MPI_RANK);
        ExitPorta(1);
        return;
    }
    for (i = 0; i < nz; i++) {
        int iy;
        g_global->grid[grid_id].node[i] = NULL;
        /*
         * note that min_iz-1 and max_iz+1 planes are needed for the multigrid
         * method as a storage of atomic state and, in the case of quadratic FS,
         * also for the downwind points of the short characteristics
         */
        if (G_MY_MPI_RANK && i >= min_iz - 1 &&
            i <= max_iz + 1) /* master node does not allocate any nodes */
        {
            g_global->grid[grid_id].node[i] =
                (t_node **)Malloc(sizeof(t_node *) * ny);
            for (iy = 0; iy < ny; iy++) {
                int ix;
                g_global->grid[grid_id].node[i][iy] =
                    (t_node *)Malloc(sizeof(t_node) * nx);
                for (ix = 0; ix < nx; ix++) {
                    g_global->grid[grid_id].node[i][iy][ix].p_data = NULL;
                    InitNode(&g_global->grid[grid_id], ix, iy, i);
                }
            }
        }
    }
    g_global->current_n_grids++;
}

/*******************************************************************************/
void grd_NewGridC(int grid_id, int nx, int ny, int nz, double x[], double y[],
                  double z[])
{
    int i;
    if (!x || !y || !z)
        IERR;
    if (x[nx - 1] >= g_global->domain_origin.x + g_global->domain_size.x ||
        fabs(x[0] - g_global->domain_origin.x) >
            DBL_EPSILON * g_global->domain_size.x ||
        y[ny - 1] >= g_global->domain_origin.y + g_global->domain_size.y ||
        fabs(y[0] - g_global->domain_origin.y) >
            DBL_EPSILON * g_global->domain_size.y) {
        Error(E_ERROR, POR_AT, "X or Y coordinates do not fit the domain");
    }
    if (fabs(z[nz - 1] -
             (g_global->domain_origin.z + g_global->domain_size.z)) >
            DBL_EPSILON * g_global->domain_size.z ||
        fabs(z[0] - g_global->domain_origin.z) >
            DBL_EPSILON * g_global->domain_size.z) {
        Error(E_ERROR, POR_AT, "Z coordinates do not fit the domain (%e, %e)",
              z[0], z[nz - 1]);
    }
    for (i = 0; i < nx - 1; i++)
        if (x[i + 1] <= x[i])
            Error(E_ERROR, POR_AT, "invalid ordering of the X coordinates");
    for (i = 0; i < ny - 1; i++)
        if (y[i + 1] <= y[i])
            Error(E_ERROR, POR_AT, "invalid ordering of the Y coordinates");
    for (i = 0; i < nz - 1; i++)
        if (z[i + 1] <= z[i])
            Error(E_ERROR, POR_AT, "invalid ordering of the Z coordinates");

    // In most cases if periodic X/Y, we want the domain size to be the distance
    // from x[0] to x[nx-1] plus an extra cell, the size of x[1] - x[0]. This is
    // not required in any way, but it is the usual way we define grids, so we
    // give a WARNING in case this is not the case and it was a mistake when
    // creating the input file grid.
    if (IAM_MASTER) {
        if (fabs(x[nx - 1] + x[1] - 2 * x[0] - g_global->domain_size.x) >
            DBL_EPSILON * g_global->domain_size.x) {
            Error(E_WARNING, POR_AT,
                  "Last X cell size (%e) is greater than first X cell size "
                  "(%e). Is "
                  "this intended?",
                  x[0] + g_global->domain_size.x - x[nx - 1], x[1] - x[0]);
        }

        if (fabs(y[ny - 1] + y[1] - 2 * y[0] - g_global->domain_size.y) >
            DBL_EPSILON * g_global->domain_size.y) {
            Error(E_WARNING, POR_AT,
                  "Last Y cell size (%e) is greater than first Y cell size "
                  "(%e). Is "
                  "this intended?",
                  y[0] + g_global->domain_size.y - y[ny - 1], y[1] - y[0]);
        }
    }

    grd_NewGrid(grid_id, nx, ny, nz);
    stk_Add(G_MY_MPI_RANK == 1, POR_AT,
            "domain dimensions: %6.6e x %6.6e x %6.6e km^3",
            g_global->domain_size.x / 1e5, g_global->domain_size.y / 1e5,
            g_global->domain_size.z / 1e5);
    Memcpy(g_global->grid[grid_id].x, x, nx * DBLSIZE);
    Memcpy(g_global->grid[grid_id].y, y, ny * DBLSIZE);
    Memcpy(g_global->grid[grid_id].z, z, nz * DBLSIZE);
}

/*******************************************************************************/
void grd_ReleaseGrid(int ig)
{
    int     ix, iy, iz;
    t_grid *g;

    stk_Add(IAM_MASTER, POR_AT, "releasing grid %d", ig);
    if (ig >= g_global->current_n_grids) {
        stk_Add(0, POR_AT, "grid %d does not exist", ig);
        return;
    }
    g = &g_global->grid[ig];
    if (!g->node)
        IERR;
    for (iz = 0; iz < g->nz; iz++) {
        if (G_MY_MPI_RANK &&
            g->node[iz]) /* only realease the z-planes which were actually
                            allocated in the current sub-domain */
        {
            for (iy = 0; iy < g->ny; iy++) {
                for (ix = 0; ix < g->nx; ix++) {
                    t_node *p = &g->node[iz][iy][ix];
                    if (p->p_data) {
                        g_global->afunc.F_Free_p_data(p->p_data);
                        p->p_data = NULL;
                    }
                }
                free(g->node[iz][iy]);
            }
            free(g->node[iz]);
        }
    }
    free(g->node);
    g->node = NULL;
    g->nx = g->ny = g->nz = 0;
    g_global->current_n_grids--;
}

/*******************************************************************************/
void grd_ReleaseDomain(void)
{
    int i;
    stk_Add(0, POR_AT, "releasing the memory allocated for the grids");
    g_global->domain_origin.x       = g_global->domain_origin.y =
        g_global->domain_origin.z   = g_global->domain_size.x =
            g_global->domain_size.y = g_global->domain_size.z = 0.0;
    if (!g_global->current_n_grids) {
        stk_Add(0, POR_AT, "no grids need to be released");
        return;
    }
    for (i = g_global->current_n_grids - 1; i >= 0; i--) {
        grd_ReleaseGrid(i);
    }
    if (mod_IsModuleLoaded()) {
        mod_UnlinkModule();
    }
}
