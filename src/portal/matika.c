#include "matika.h"
#include "mem.h"
#include "memoization_ja.h"
#include <float.h>

/* Initialization 6J jagged arrays */
static strip6D A6D;
static int     stripA6D_set = 0;
/* Initialization 3J jagged arrays */
static strip6D B6D;
static int     stripB6D_set = 0;

double mat_Minimum4(double a, double b, double c, double d)
{
    double abc = a < b ? (a < c ? a : c) : (b < c ? b : c);
    return d < abc ? d : abc;
}

double mat_AExp(double x)
{
#if defined(DEBUG)
    if (x < 0.0)
        Error(E_ERROR, POR_AT, "negative arguments not allowed: x = %6.6e", x);
#endif
    if (x > 0.08) {
        return exp(-x);
    } else {
        return 1.0 +
               x * (-1.0 +
                    x * (0.5 +
                         x * (-0.16666666666666666667 +
                              x * (4.1666666666666666667e-2 +
                                   x * (-8.3333333333333333333e-3 +
                                        x * (1.3888888888888888889e-3 +
                                             (-1.984126984126984127e-4 +
                                              2.4801587301587301587e-5 * x) *
                                                 x))))));
    }
}

double mat_MExp(double x)
{
#if defined(DEBUG)
    if (x < 0.0) {
        Error(E_ERROR, POR_AT, "negative arguments not allowed: x = %6.6e", x);
    }
#endif
    if (x > 0.07) {
        return 1.0 - exp(-x);
    } else {
        return x *
               (1.0 +
                x * (-0.5 + x * (1.6666666666666666667e-1 +
                                 x * (-4.1666666666666666667e-2 +
                                      x * (8.3333333333333333333e-3 +
                                           x * (-1.3888888888888888889e-3 +
                                                (1.984126984126984127e-4 -
                                                 2.4801587301587301587e-5 * x) *
                                                    x))))));
    }
}

double mat_PowN(double x, int n)
{
    double       ex;
    unsigned int i;
#if defined(DEBUG)
    if (n < 0)
        Error(E_ERROR, POR_AT, "only non-negative powers allowed: n=%d", n);
#endif
    if (!n)
        return 1.0;
    ex = x;
    for (i = 2; i <= n; i++)
        ex *= x;
    return ex;
}

double *ArrayNew(int n)
{
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid number of elements n=%d", n);
#endif
    return (double *)Malloc(DBLSIZE * n);
}

void ArraySet(int n, double *x, double a)
{
    unsigned int i;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid array length: %d", n);
    if (!x)
        Error(E_ERROR, POR_AT, "array not initialized");
#endif
    for (i = 0; i < n; i++)
        x[i] = a;
}

void ArrayMultD(int n, double *x, double a)
{
    double *x1 = x + n - 1;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid array length: %d", n);
    if (!x)
        Error(E_ERROR, POR_AT, "array not initialized");
#endif
    while (x1 >= x) {
        *x1 *= a;
        --x1;
    }
}

void ArrayAdd(int n, double *x, const double *y)
{
    int i;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid array length: %d", n);
    if ((!x) || (!y))
        Error(E_ERROR, POR_AT, "array not initialized");
#endif
    for (i = 0; i < n; i++)
        x[i] += y[i];
}

void ArraySub(int n, double *x, double *y)
{
    double *x1 = x + n - 1, *y1 = y + n - 1;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid array length: %d", n);
    if ((!x) || (!y))
        Error(E_ERROR, POR_AT, "array not initialized");
#endif
    while (x1 >= x) {
        *x1 -= *y1;
        --x1;
        --y1;
    }
}

double *ArrayDupl(int n, double *a)
{
    double *b;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid number of elements: %d", n);
    if (!a)
        Error(E_ERROR, POR_AT, "array uninitialized");
#endif
    b = (double *)Malloc(DBLSIZE * n);
    Memcpy(b, a, DBLSIZE * n);
    return b;
}

double DotProduct(int n, double *a, double *b)
{
    double dp = 0.0;
#if defined(DEBUG)
    if (!a || !b)
        Error(E_ERROR, POR_AT, "vector not allocated");
    if (n < 0)
        Error(E_ERROR, POR_AT, "invalid number of elements");
#endif
    while (n > 0) {
        dp += (*a) * (*b);
        ++a;
        ++b;
        --n;
    };
    return dp;
}

double **MatrixNew(int nrows, int ncols)
{
    int      r = nrows;
    double **m;
#if defined(DEBUG)
    if (nrows < 1)
        Error(E_ERROR, POR_AT, "invalid number of rows: %d", nrows);
    if (ncols < 1)
        Error(E_ERROR, POR_AT, "invalid number of columns: %d", ncols);
#endif
    m = (double **)Malloc(sizeof(double *) * nrows);
    while (--r >= 0)
        m[r] = (double *)Malloc(DBLSIZE * ncols);
    return m;
}

double **MatrixDupl(int nrows, int ncols, double **a)
{
    int      r = nrows, rsize;
    double **m;
#if defined(DEBUG)
    if (nrows < 1)
        Error(E_ERROR, POR_AT, "invalid number of rows: %d", nrows);
    if (ncols < 1)
        Error(E_ERROR, POR_AT, "invalid number of columns: %d", ncols);
    if (!a)
        Error(E_ERROR, POR_AT, "uninitialized matrix");
#endif
    m     = MatrixNew(nrows, ncols);
    rsize = DBLSIZE * ncols;
    while (--r >= 0)
        Memcpy(m[r], a[r], rsize);
    return m;
}

void MatrixCopy(int nrows, int ncols, double **a, double **b)
{
    int m;
#ifdef DEBUG
    if (nrows < 1 || ncols < 1)
        Error(E_ERROR, POR_AT,
              "invalid matrix dimensions (nrows=%d, ncolumns=%d)", nrows,
              ncols);
    if (!a || !b)
        Error(E_ERROR, POR_AT, "uninitialized matrix/vector");
#endif
    for (m = 0; m < nrows; m++)
        Memcpy(b[m], a[m], ncols * DBLSIZE);
}

void MatrixFree(int nrows, int ncols, double **m)
{
    int r = nrows;

    if (!nrows || !ncols || !m)
        return;
    while (--r >= 0) {
#if defined(DEBUG)
        if (!m[r])
            Error(E_ERROR, POR_AT, "missing matrix row");
#endif
        free(m[r]);
    }
    free(m);
}

t_complex **MatrixNewC(int nrows, int ncols)
{
    int         r = nrows;
    t_complex **m;
#if defined(DEBUG)
    if (nrows < 1)
        Error(E_ERROR, POR_AT, "invalid number of rows: %d", nrows);
    if (ncols < 1)
        Error(E_ERROR, POR_AT, "invalid number of columns: %d", ncols);
#endif
    m = (t_complex **)Malloc(sizeof(t_complex *) * nrows);
    while (--r >= 0)
        m[r] = (t_complex *)Malloc(CLXSIZE * ncols);
    return m;
}

void MatrixFreeC(int nrows, int ncols, t_complex **m)
{
    int r = nrows;

    if (!nrows || !ncols || !m)
        return;
    while (--r >= 0) {
#if defined(DEBUG)
        if (!m[r])
            Error(E_ERROR, POR_AT, "missing matrix row");
#endif
        free(m[r]);
    }
    free(m);
}

int mat_ArraySearch(int n, const double *xi, double x)
{
    int il, ir, ic;
#if defined(DEBUG)
    if (n < 2)
        Error(E_ERROR, POR_AT, "invalid size of array: %d", n);
    if (!xi)
        Error(E_ERROR, POR_AT, "array not allocated");
#endif
    if ((x > xi[n - 1]) || (x < xi[0]))
        return -1;
    else if (n == 2)
        return 0;
    il = 0;
    ir = n - 1;
    do {
        ic = (il + ir) / 2;
        if (xi[ic] >= x)
            ir = ic;
        else
            il = ic;
    } while (ir - il > 1);
    return il;
}

/*******************************************************
 *     vector manipulation, computational geometry     *
 *******************************************************/

void VecUnit2Ang(const t_vec_3d n, double *theta, double *chi)
{
    double xy;
#if defined(DEBUG)
    if (!VEC_IS_UNIT_3D(n)) {
        Error(E_WARNING, POR_AT,
              "vector (%e, %e, %e) is not "
              "properly normalized",
              n.x, n.y, n.z);
    }
#endif
    *theta = acos(n.z);
    xy     = sqrt(n.x * n.x + n.y * n.y);
    *chi   = (xy != 0.0 ? acos(n.x / xy) : 0.0);
    *chi   = (n.y >= 0.0 ? *chi : 2.0 * M_PI - (*chi));
}

void Ang2VecUnit(double theta, double chi, t_vec_3d v)
{
    double xy;
#if defined(DEBUG)
    if (theta < 0.0 || theta > M_PI) {
        Error(E_WARNING, POR_AT, "theta out of range");
    }
    if (chi < 0.0 || chi > 2.0 * M_PI) {
        Error(E_WARNING, POR_AT, "chi out of range");
    }
#endif
    v.z = cos(theta);
    xy  = sin(theta);
    v.x = xy * cos(chi);
    v.y = xy * sin(chi);
}

/************************************************
 *              special functions               *
 ************************************************/

double AssocLegendre(int l, int m, double x)
{
    double fact, pll = 0.0, pmm, pmmp1, somx2;
    int    i, ll;
#if defined(DEBUG)
    if (m < 0 || m > l || fabs(x) > 1.0) {
        Error(E_ERROR, POR_AT, "invalid arguments (%d, %d, %e)", l, m, x);
    }
#endif
    pmm = 1.0;
    if (m > 0) {
        somx2 = sqrt((1.0 - x) * (1.0 + x));
        fact  = 1.0;
        for (i = 1; i <= m; i++) {
            pmm *= -fact * somx2;
            fact += 2.0;
        }
    }
    if (l == m) {
        return pmm;
    } else {
        pmmp1 = x * (2 * m + 1) * pmm;
        if (l == (m + 1)) {
            return pmmp1;
        } else {
            for (ll = m + 2; ll <= l; ll++) {
                pll =
                    (x * (2 * ll - 1) * pmmp1 - (ll + m - 1) * pmm) / (ll - m);
                pmm   = pmmp1;
                pmmp1 = pll;
            }
            return pll;
        }
    }
}

#ifdef UNDEF
/**
 * normalization coefficient of the real spherical harmonics N^\a m_\a l
 */
static double NormYReal(int l, int m)
{
    static double save_ml[NTOT_Y_L];
    static int    calculated = 0;
    int           el, em, i;
    /* precalculate normalization factors */

    if (!calculated) {
        for (el = 0; el <= MAX_Y_L; el++) {
            for (em = -el; em <= el; em++) {
                i = YLM_INDEX(el, em);
                save_ml[i] =
                    sqrt(((double)(2 * el + 1)) * mat_Factorial(el - em) /
                         mat_Factorial(el + em) / 4.0 / M_PI);
            }
        }
        calculated = 1;
    }

    if (l <= MAX_Y_L) {
        return save_ml[YLM_INDEX(l, m)];
    } else {
        return sqrt(((double)(2 * l + 1)) * mat_Factorial(l - m) /
                    mat_Factorial(l + m) / 4.0 / M_PI);
    }
}

double YReal(int l, int m, double theta, double chi)
{
    double norm, leg;
#if defined(DEBUG)
    if (m < -l || m > l) {
        Error(E_ERROR, POR_AT, "invalid arguments (%d, %d)", l, m);
    }
#endif
    norm = NormYReal(l, abs(m));
    leg  = AssocLegendre(l, abs(m), cos(theta));
    if (m > 0) {
        return M_SQRT2 * norm * cos(chi * (double)m) * leg;
    } else if (m < 0) {
        return M_SQRT2 * norm * sin(chi * (double)(-m)) * leg;
    } else {
        return norm * leg;
    }
}
#endif

/*******************/
double ExpIntE1(double x)
{
    if (x <= 1.0) {
        return -log(x) - 0.57721566 +
               x * (0.99999193 + x * (-0.24991055 +
                                      x * (0.05519968 + x * (-0.00976004 +
                                                             x * 0.00107875))));
    } else {
        return exp(-x) *
               ((0.2677737343 +
                 x * (8.6347608925 +
                      x * (18.0590169730 + x * (8.5733287401 + x)))) /
                (3.9584969228 +
                 x * (21.0996530827 +
                      x * (25.6329561486 + x * (9.5733223454 + x))))) /
               x;
    }
}

/************************************************
 *                  integration                 *
 ************************************************/

double Integrate(int first, int n, const double *x, const double *y,
                 enum t_quadrature qt, int equidist, const double *q)
{
    const double *pq = q + first, *px = x + first, *py = y + first;
    double        r = 0.0;
    int           i = n;
#if defined(DEBUG)
    if (n < 0)
        Error(E_ERROR, POR_AT, "invalid number of points n=%d", n);
    if (!x && !q)
        Error(E_ERROR, POR_AT, "x-array not allocated");
    if (!y)
        Error(E_ERROR, POR_AT, "y-array not allocated");
    if (!q && (qt < Q_TRAPEZOIDAL || qt > Q_TRAPEZOIDAL))
        Error(E_ERROR, POR_AT, "unknown quadrature type");
#endif
    if (n < 2)
        return 0.0;
    /* quadrature is known */
    if (q) {
        while (--i >= 0)
            r += (*pq++) * (*py++);
        return r;
    }
    /* quadrature is unknown */
    if (n == 2)
        return 0.5 * (*(px + 1) - *px) * (*py + *(py + 1));
    /* equidistantly spaced ordinates? */
    if (equidist) {
        if (Q_TRAPEZOIDAL == qt) {
            const double d = *(px + 1) - *px, *pylast = py + n - 2;
            r += 0.5 * (*py + *(pylast + 1)) * d;
            do
                r += d * (*(++py));
            while (py < pylast);
        }
        return r;
    } else {
        if (Q_TRAPEZOIDAL == qt) {
            const double *xlast = px + n - 1, *pylast = py + n - 2;
            r += 0.5 * ((*(px + 1) - *px) * (*py) +
                        (*xlast - *(xlast - 1)) * (*(pylast + 1)));
            ++px;
            do {
                ++px;
                ++py;
                r += 0.5 * (*px - *(px - 2)) * (*py);
            } while (py < pylast);
            return r;
        }
    }
    /* we should not be here */
#if defined(DEBUG)
    Error(E_ERROR, POR_AT, "internal error");
#endif
    return 0.0;
}

void mat_GauLeg(int n, double x1, double x2, double *x, double *w)
{
    int    i, j, m;
    double eps = 2.0 * DBL_EPSILON;
    double p1, p2, p3, pp, xl, xm, z, z1;
#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid number of points: %d", n);
    if (!x)
        Error(E_ERROR, POR_AT, "array of ordinates not initialized");
    if (!w)
        Error(E_ERROR, POR_AT, "array of weights not initialized");
#endif
    if (((n + 1) / 2) * 2 != n + 1)
        m = (n + 2) / 2;
    else
        m = (n + 1) / 2;
    xm = 0.5 * (x2 + x1);
    xl = 0.5 * (x2 - x1);
    for (i = 1; i <= m; i++) {
        z = cos(M_PI * ((double)i - 0.25) / ((double)n + 0.5));
        do {
            p1 = 1.;
            p2 = 0.0;
            for (j = 1; j <= n; j++) {
                p3 = p2;
                p2 = p1;
                p1 = ((2.0 * (double)j - 1.0) * z * p2 -
                      ((double)j - 1.0) * p3) /
                     (double)j;
            }
            pp = (double)n * (z * p1 - p2) / (z * z - 1.0);
            z1 = z;
            z  = z1 - p1 / pp;
        } while (fabs(z - z1) > eps);
        x[i - 1]         = xm - xl * z;
        x[n + 1 - i - 1] = xm + xl * z;
        w[i - 1]         = 2.0 * xl / ((1.0 - z * z) * pp * pp);
        w[n + 1 - i - 1] = w[i - 1];
    }
}

int Integrate2D(double x1, double y1, double x2, double y2,
                double (*f)(double, double), double err, double *val)
{
    static const int s[7] = {3, 7, 15, 31, 63, 127, 255};
    double           er, v1, v2;
    int              gi = 0, ng, i, j;
    static double    gx[255], gy[255], wx[255], wy[255];
    gi = 0;
    ng = s[gi];
    mat_GauLeg(ng, x1, x2, &gx[0], &wx[0]);
    mat_GauLeg(ng, y1, y2, &gy[0], &wy[0]);
    v1 = v2 = 0.0;
    for (i = 0; i < ng; i++) {
        for (j = 0; j < ng; j++) {
            v1 += f(gx[i], gy[j]) * wx[i] * wy[j];
        }
    }
    gi++;
    er = 2.0 * err;
    while (er > err && gi < 7) {
        ng = s[gi];
        mat_GauLeg(ng, x1, x2, &gx[0], &wx[0]);
        mat_GauLeg(ng, y1, y2, &gy[0], &wy[0]);
        v2 = 0.0;
        for (i = 0; i < ng; i++) {
            for (j = 0; j < ng; j++) {
                v2 += f(gx[i], gy[j]) * wx[i] * wy[j];
            }
        }
        er = fabs(v2 - v1);
        v1 = v2;
        gi++;
    }
    if (er <= err) {
        *val = v2;
        return s[gi - 1];
    } else {
        Error(E_WARNING, POR_AT,
              "insufficient accuracy in the 2d integral (%e)", er);
        return 0;
    }
}

/*
 * Racah algebra
 */

#define COU_MIN(a, b)  ((a) < (b) ? (a) : (b))
#define COU_MAX(a, b)  ((a) > (b) ? (a) : (b))
#define COU_IS_ODD(n)  ((n)&1)
#define FACT_TABLE_MAX 170

static const double FACTORIAL_TABLE[FACT_TABLE_MAX + 1] = {
    1.0,
    1.0,
    2.0,
    6.0,
    24.0,
    120.0,
    720.0,
    5040.0,
    40320.0,
    362880.0,
    3628800.0,
    39916800.0,
    479001600.0,
    6227020800.0,
    87178291200.0,
    1307674368000.0,
    20922789888000.0,
    355687428096000.0,
    6402373705728000.0,
    121645100408832000.0,
    2432902008176640000.0,
    51090942171709440000.0,
    1124000727777607680000.0,
    25852016738884976640000.0,
    620448401733239439360000.0,
    15511210043330985984000000.0,
    403291461126605635584000000.0,
    10888869450418352160768000000.0,
    304888344611713860501504000000.0,
    8841761993739701954543616000000.0,
    265252859812191058636308480000000.0,
    8222838654177922817725562880000000.0,
    263130836933693530167218012160000000.0,
    8683317618811886495518194401280000000.0,
    2.95232799039604140847618609644e38,
    1.03331479663861449296666513375e40,
    3.71993326789901217467999448151e41,
    1.37637530912263450463159795816e43,
    5.23022617466601111760007224100e44,
    2.03978820811974433586402817399e46,
    8.15915283247897734345611269600e47,
    3.34525266131638071081700620534e49,
    1.40500611775287989854314260624e51,
    6.04152630633738356373551320685e52,
    2.65827157478844876804362581101e54,
    1.19622220865480194561963161496e56,
    5.50262215981208894985030542880e57,
    2.58623241511168180642964355154e59,
    1.24139155925360726708622890474e61,
    6.08281864034267560872252163321e62,
    3.04140932017133780436126081661e64,
    1.55111875328738228022424301647e66,
    8.06581751709438785716606368564e67,
    4.27488328406002556429801375339e69,
    2.30843697339241380472092742683e71,
    1.26964033536582759259651008476e73,
    7.10998587804863451854045647464e74,
    4.05269195048772167556806019054e76,
    2.35056133128287857182947491052e78,
    1.38683118545689835737939019720e80,
    8.32098711274139014427634118320e81,
    5.07580213877224798800856812177e83,
    3.14699732603879375256531223550e85,
    1.982608315404440064116146708360e87,
    1.268869321858841641034333893350e89,
    8.247650592082470666723170306800e90,
    5.443449390774430640037292402480e92,
    3.647111091818868528824985909660e94,
    2.480035542436830599600990418570e96,
    1.711224524281413113724683388810e98,
    1.197857166996989179607278372170e100,
    8.504785885678623175211676442400e101,
    6.123445837688608686152407038530e103,
    4.470115461512684340891257138130e105,
    3.307885441519386412259530282210e107,
    2.480914081139539809194647711660e109,
    1.885494701666050254987932260860e111,
    1.451830920282858696340707840860e113,
    1.132428117820629783145752115870e115,
    8.946182130782975286851441715400e116,
    7.156945704626380229481153372320e118,
    5.797126020747367985879734231580e120,
    4.753643337012841748421382069890e122,
    3.945523969720658651189747118010e124,
    3.314240134565353266999387579130e126,
    2.817104114380550276949479442260e128,
    2.422709538367273238176552320340e130,
    2.107757298379527717213600518700e132,
    1.854826422573984391147968456460e134,
    1.650795516090846108121691926250e136,
    1.485715964481761497309522733620e138,
    1.352001527678402962551665687590e140,
    1.243841405464130725547532432590e142,
    1.156772507081641574759205162310e144,
    1.087366156656743080273652852570e146,
    1.032997848823905926259970209940e148,
    9.916779348709496892095714015400e149,
    9.619275968248211985332842594960e151,
    9.426890448883247745626185743100e153,
    9.332621544394415268169923885600e155,
    9.33262154439441526816992388563e157,
    9.42594775983835942085162312450e159,
    9.61446671503512660926865558700e161,
    9.90290071648618040754671525458e163,
    1.02990167451456276238485838648e166,
    1.08139675824029090050410130580e168,
    1.146280563734708354534347384148e170,
    1.226520203196137939351751701040e172,
    1.324641819451828974499891837120e174,
    1.443859583202493582204882102460e176,
    1.588245541522742940425370312710e178,
    1.762952551090244663872161047110e180,
    1.974506857221074023536820372760e182,
    2.231192748659813646596607021220e184,
    2.543559733472187557120132004190e186,
    2.925093693493015690688151804820e188,
    3.393108684451898201198256093590e190,
    3.96993716080872089540195962950e192,
    4.68452584975429065657431236281e194,
    5.57458576120760588132343171174e196,
    6.68950291344912705758811805409e198,
    8.09429852527344373968162284545e200,
    9.87504420083360136241157987140e202,
    1.21463043670253296757662432419e205,
    1.50614174151114087979501416199e207,
    1.88267717688892609974376770249e209,
    2.37217324288004688567714730514e211,
    3.01266001845765954480997707753e213,
    3.85620482362580421735677065923e215,
    4.97450422247728744039023415041e217,
    6.46685548922047367250730439554e219,
    8.47158069087882051098456875820e221,
    1.11824865119600430744996307608e224,
    1.48727070609068572890845089118e226,
    1.99294274616151887673732419418e228,
    2.69047270731805048359538766215e230,
    3.65904288195254865768972722052e232,
    5.01288874827499166103492629211e234,
    6.91778647261948849222819828311e236,
    9.61572319694108900419719561353e238,
    1.34620124757175246058760738589e241,
    1.89814375907617096942852641411e243,
    2.69536413788816277658850750804e245,
    3.85437071718007277052156573649e247,
    5.55029383273930478955105466055e249,
    8.04792605747199194484902925780e251,
    1.17499720439091082394795827164e254,
    1.72724589045463891120349865931e256,
    2.55632391787286558858117801578e258,
    3.80892263763056972698595524351e260,
    5.71338395644585459047893286526e262,
    8.62720977423324043162318862650e264,
    1.31133588568345254560672467123e267,
    2.00634390509568239477828874699e269,
    3.08976961384735088795856467036e271,
    4.78914290146339387633577523906e273,
    7.47106292628289444708380937294e275,
    1.17295687942641442819215807155e278,
    1.85327186949373479654360975305e280,
    2.94670227249503832650433950735e282,
    4.71472363599206132240694321176e284,
    7.59070505394721872907517857094e286,
    1.22969421873944943411017892849e289,
    2.00440157654530257759959165344e291,
    3.28721858553429622726333031164e293,
    5.42391066613158877498449501421e295,
    9.00369170577843736647426172359e297,
    1.50361651486499904020120170784e300,
    2.52607574497319838753801886917e302,
    4.26906800900470527493925188890e304,
    7.25741561530799896739672821113e306,
};

double mat_Factorial(int n)
{
#if defined(DEBUG)
    if ((n < 0) || (n > FACT_TABLE_MAX))
        Error(E_ERROR, POR_AT, "factorial of %d cannot be computed", n);
#endif
    return FACTORIAL_TABLE[n];
}

static double BinomCoef(int n, int m)
{
    if (m > n || m < 0 || n < 0) {
        Error(E_ERROR, POR_AT, "domain error");
        return 0;
    } else if ((m == n) || (0 == m))
        return 1.0;
    else if (n <= FACT_TABLE_MAX)
        return (FACTORIAL_TABLE[n] / FACTORIAL_TABLE[m]) /
               FACTORIAL_TABLE[n - m];
    else {
        if (m * 2 < n)
            m = n - m;
        if (n - m < 64) {
            double       prod = 1.0;
            unsigned int k;
            for (k = n; k >= m + 1; k--) {
                double tk = (double)k / (double)(k - m);
                if (tk > DBL_MAX / prod)
                    Error(E_ERROR, POR_AT, "overflow error");
                prod *= tk;
            }
            return prod;
        } else {
            Error(E_ERROR, POR_AT, "overflow error");
            return 0;
        }
    }
}

static int LocMax3(int a, int b, int c)
{
    int d = COU_MAX(a, b);
    return COU_MAX(d, c);
}

static int LocMin3(int a, int b, int c)
{
    int d = COU_MIN(a, b);
    return COU_MIN(d, c);
}

static int LocMin5(int a, int b, int c, int d, int e)
{
    int f = COU_MIN(a, b);
    int g = COU_MIN(c, d);
    int h = COU_MIN(f, g);
    return COU_MIN(e, h);
}

static double Delta(int ta, int tb, int tc)
{
    double f1, f2, f3, f4;
    f1 = mat_Factorial((ta + tb - tc) / 2);
    f2 = mat_Factorial((ta + tc - tb) / 2);
    f3 = mat_Factorial((tb + tc - ta) / 2);
    f4 = mat_Factorial((ta + tb + tc) / 2 + 1);
    return f1 * f2 * f3 / f4;
}

static int TriangleSelectionFails(int two_ja, int two_jb, int two_jc)
{
    return ((two_jb < abs(two_ja - two_jc)) || (two_jb > two_ja + two_jc));
}

static int MSelectionFails(int two_ja, int two_jb, int two_jc, int two_ma,
                           int two_mb, int two_mc)
{
    return (abs(two_ma) > two_ja || abs(two_mb) > two_jb ||
            abs(two_mc) > two_jc || COU_IS_ODD(two_ja + two_ma) ||
            COU_IS_ODD(two_jb + two_mb) || COU_IS_ODD(two_jc + two_mc) ||
            (two_ma + two_mb + two_mc) != 0);
}

double mat_Cou3j(int two_ja, int two_jb, int two_jc, int two_ma, int two_mb,
                 int two_mc)
{
    double  rval;
    double *val;

    if (!stripB6D_set) {
        stripB6D_set = 1;
        B6D.min      = INT_MAX;
        B6D.max      = INT_MIN;
        B6D.s        = NULL;
        B6D.s0       = NULL;
    }

    if ((val = elem6D(two_ja, two_jb, two_jc, two_ma, two_mb, two_mc, B6D)))
        return *val;

#if defined(DEBUG)
    if (two_ja < 0 || two_jb < 0 || two_jc < 0)
        Error(E_ERROR, POR_AT,
              "angular momentum must be > 0 "
              "(%d,%d,%d)",
              two_ja, two_jb, two_jc);
#endif
    if (TriangleSelectionFails(two_ja, two_jb, two_jc) ||
        MSelectionFails(two_ja, two_jb, two_jc, two_ma, two_mb, two_mc))
        return 0.0;
    else {
        int jca = (-two_ja + two_jb + two_jc) / 2,
            jcb = (two_ja - two_jb + two_jc) / 2,
            jcc = (two_ja + two_jb - two_jc) / 2, jmma = (two_ja - two_ma) / 2,
            jmmb = (two_jb - two_mb) / 2, jmmc = (two_jc - two_mc) / 2,
            jpma = (two_ja + two_ma) / 2, jpmb = (two_jb + two_mb) / 2,
            jpmc = (two_jc + two_mc) / 2, jsum = (two_ja + two_jb + two_jc) / 2,
            kmin       = LocMax3(0, jpmb - jmmc, jmma - jpmc),
            kmax       = LocMin3(jcc, jmma, jpmb), k,
            sign       = COU_IS_ODD(kmin - jpma + jmmb) ? -1 : 1;
        double sum_pos = 0.0, sum_neg = 0.0, norm, term;
        double bc1, bc2, bc3, bcn1, bcn2, bcd1, bcd2, bcd3, bcd4;
        bcn1 = BinomCoef(two_ja, jcc);
        bcn2 = BinomCoef(two_jb, jcc);
        bcd1 = BinomCoef(jsum + 1, jcc);
        bcd2 = BinomCoef(two_ja, jmma);
        bcd3 = BinomCoef(two_jb, jmmb);
        bcd4 = BinomCoef(two_jc, jpmc);
        norm = sqrt(bcn1 * bcn2) /
               sqrt(bcd1 * bcd2 * bcd3 * bcd4 * ((double)two_jc + 1.0));
        for (k = kmin; k <= kmax; k++) {
            bc1  = BinomCoef(jcc, k);
            bc2  = BinomCoef(jcb, jmma - k);
            bc3  = BinomCoef(jca, jpmb - k);
            term = bc1 * bc2 * bc3;
            if (sign < 0)
                sum_neg += norm * term;
            else
                sum_pos += norm * term;
            sign = -sign;
        }
        rval = sum_pos - sum_neg;
        insert6D(two_ja, two_jb, two_jc, two_ma, two_mb, two_mc, rval, &B6D);
        return rval;
    }
}

void mat_free_strip6D()
{
    if (stripA6D_set) {
        mem_free_6D(&A6D);
        stripA6D_set = 0;
    }
    if (stripB6D_set) {
        mem_free_6D(&B6D);
        stripB6D_set = 0;
    }
}

double mat_Cou6j(int two_ja, int two_jb, int two_jc, int two_jd, int two_je,
                 int two_jf)
{
    double  rval;
    double *val;

    if (!stripA6D_set) {
        stripA6D_set = 1;
        A6D.min      = INT_MAX;
        A6D.max      = INT_MIN;
        A6D.s        = NULL;
        A6D.s0       = NULL;
    }

    if ((val = elem6D(two_ja, two_jb, two_jc, two_jd, two_je, two_jf, A6D)))
        return *val;

#if defined(DEBUG)
    if (two_ja < 0 || two_jb < 0 || two_jc < 0 || two_jd < 0 || two_je < 0 ||
        two_jf < 0)
        Error(E_ERROR, POR_AT,
              "angular momentum must be > 0\n"
              "\t(%d,%d,%d)\n\t(%d,%d,%d)",
              two_ja, two_jb, two_jc, two_jd, two_je, two_jf);
#endif
    if (TriangleSelectionFails(two_ja, two_jb, two_jc) ||
        TriangleSelectionFails(two_ja, two_je, two_jf) ||
        TriangleSelectionFails(two_jb, two_jd, two_jf) ||
        TriangleSelectionFails(two_je, two_jd, two_jc))
        return 0.0;
    else {
        double n1;
        double d1, d2, d3, d4, d5, d6;
        double norm;
        int    tk, tkmin, tkmax;
        double phase;
        double sum_pos = 0.0;
        double sum_neg = 0.0;
        d1             = Delta(two_ja, two_jb, two_jc);
        d2             = Delta(two_ja, two_je, two_jf);
        d3             = Delta(two_jb, two_jd, two_jf);
        d4             = Delta(two_je, two_jd, two_jc);
        norm           = sqrt(d1) * sqrt(d2) * sqrt(d3) * sqrt(d4);
        tkmin          = LocMax3(0, two_ja + two_jd - two_jc - two_jf,
                        two_jb + two_je - two_jc - two_jf);
        tkmax          = LocMin5(two_ja + two_jb + two_je + two_jd + 2,
                        two_ja + two_jb - two_jc, two_je + two_jd - two_jc,
                        two_ja + two_je - two_jf, two_jb + two_jd - two_jf);
        phase =
            (COU_IS_ODD((two_ja + two_jb + two_je + two_jd + tkmin) / 2) ? -1.0
                                                                         : 1.0);
        for (tk = tkmin; tk <= tkmax; tk += 2) {
            double term;
            double den_1, den_2;
            double d1_a, d1_b;
            n1 =
                mat_Factorial((two_ja + two_jb + two_je + two_jd - tk) / 2 + 1);
            d1_a  = mat_Factorial(tk / 2);
            d1_b  = mat_Factorial((two_jc + two_jf - two_ja - two_jd + tk) / 2);
            d2    = mat_Factorial((two_jc + two_jf - two_jb - two_je + tk) / 2);
            d3    = mat_Factorial((two_ja + two_jb - two_jc - tk) / 2);
            d4    = mat_Factorial((two_je + two_jd - two_jc - tk) / 2);
            d5    = mat_Factorial((two_ja + two_je - two_jf - tk) / 2);
            d6    = mat_Factorial((two_jb + two_jd - two_jf - tk) / 2);
            d1    = d1_a * d1_b;
            den_1 = d1 * d2 * d3;
            den_2 = d4 * d5 * d6;
            term  = phase * n1 / den_1 / den_2;
            phase = -phase;
            if (term >= 0.0)
                sum_pos += norm * term;
            else
                sum_neg -= norm * term;
        }

        rval = sum_pos - sum_neg;
        insert6D(two_ja, two_jb, two_jc, two_jd, two_je, two_jf, rval, &A6D);
        return rval;
    }
}

double cou_RacahW(int two_ja, int two_jb, int two_jc, int two_jd, int two_je,
                  int two_jf)
{
    double res = mat_Cou6j(two_ja, two_jb, two_je, two_jd, two_jc, two_jf);
    int    phase_sum = (two_ja + two_jb + two_jc + two_jd) / 2;
    return res * (COU_IS_ODD(phase_sum) ? -1.0 : 1.0);
}

double mat_Cou9j(int two_ja, int two_jb, int two_jc, int two_jd, int two_je,
                 int two_jf, int two_jg, int two_jh, int two_ji)
{
#if defined(DEBUG)
    if (two_ja < 0 || two_jb < 0 || two_jc < 0 || two_jd < 0 || two_je < 0 ||
        two_jf < 0 || two_jg < 0 || two_jh < 0 || two_ji < 0)
        Error(E_ERROR, POR_AT,
              "angular momentum must be > 0\n"
              "\t(%d,%d,%d)\n\t(%d,%d,%d)\n\t(%d,%d,%d)",
              two_ja, two_jb, two_jc, two_jd, two_je, two_jf, two_jg, two_jh,
              two_ji);
#endif
    if (TriangleSelectionFails(two_ja, two_jb, two_jc) ||
        TriangleSelectionFails(two_jd, two_je, two_jf) ||
        TriangleSelectionFails(two_jg, two_jh, two_ji) ||
        TriangleSelectionFails(two_ja, two_jd, two_jg) ||
        TriangleSelectionFails(two_jb, two_je, two_jh) ||
        TriangleSelectionFails(two_jc, two_jf, two_ji))
        return 0.0;
    else {
        int tk;
        int tkmin = LocMax3(abs(two_ja - two_ji), abs(two_jh - two_jd),
                            abs(two_jb - two_jf));
        int tkmax = LocMin3(two_ja + two_ji, two_jh + two_jd, two_jb + two_jf);
        double sum_pos = 0.0;
        double sum_neg = 0.0;
        double phase;
        for (tk = tkmin; tk <= tkmax; tk += 2) {
            double s1, s2, s3;
            double term;
            s1   = mat_Cou6j(two_ja, two_ji, tk, two_jh, two_jd, two_jg);
            s2   = mat_Cou6j(two_jb, two_jf, tk, two_jd, two_jh, two_je);
            s3   = mat_Cou6j(two_ja, two_ji, tk, two_jf, two_jb, two_jc);
            term = s1 * s2 * s3;
            if (term >= 0.0)
                sum_pos += (tk + 1) * term;
            else
                sum_neg -= (tk + 1) * term;
        }
        phase = (COU_IS_ODD(tkmin) ? -1.0 : 1.0);
        return phase * (sum_pos - sum_neg);
    }
}

#undef COU_MIN
#undef COU_MAX
#undef COU_IS_ODD
#undef FACT_TABLE_MAX

void T20I(double theta, double chi, t_complex *T)
{
    double c;
    c     = cos(theta);
    T->re = 3.5355339059327376e-1 * (3.0 * c * c - 1.0);
    T->im = 0.0;
}

void T21I(double theta, double chi, t_complex *T)
{
    double s, c, cc, sc, a;
    s     = sin(theta);
    c     = cos(theta);
    cc    = cos(chi);
    sc    = sin(chi);
    a     = -8.6602540378443865e-1 * s * c;
    T->re = a * cc;
    T->im = a * sc;
}

void T22I(double theta, double chi, t_complex *T)
{
    double s, cc, sc, a;
    s     = sin(theta);
    cc    = cos(2.0 * chi);
    sc    = sin(2.0 * chi);
    a     = 4.3301270189221932e-1 * s * s;
    T->re = a * cc;
    T->im = a * sc;
}

void T20Q(double theta, double chi, t_complex *T)
{
    double s;
    s     = sin(theta);
    T->re = -1.0606601717798213 * s * s;
    T->im = 0.0;
}

void T21Q(double theta, double chi, t_complex *T)
{
    double s, c, cc, sc, a;
    s     = sin(theta);
    c     = cos(theta);
    cc    = cos(chi);
    sc    = sin(chi);
    a     = -8.6602540378443865e-1 * s * c;
    T->re = a * cc;
    T->im = a * sc;
}

void T22Q(double theta, double chi, t_complex *T)
{
    double c, cc, sc, a;
    c     = cos(theta);
    cc    = cos(2.0 * chi);
    sc    = sin(2.0 * chi);
    a     = -4.3301270189221932e-1 * (1.0 + c * c);
    T->re = a * cc;
    T->im = a * sc;
}

void T21U(double theta, double chi, t_complex *T)
{
    double s, cc, sc, a;
    s     = sin(theta);
    cc    = cos(chi);
    sc    = sin(chi);
    a     = -8.6602540378443865e-1 * s;
    T->re = -a * sc;
    T->im = a * cc;
}

void T22U(double theta, double chi, t_complex *T)
{
    double c, cc, sc, a;
    c     = cos(theta);
    cc    = cos(2.0 * chi);
    sc    = sin(2.0 * chi);
    a     = 4.3301270189221932e-1 * c;
    T->re = a * sc;
    T->im = -a * cc;
}

void T10V(double theta, double chi, t_complex *T)
{
    T->re = 1.2247448713915890 * cos(theta);
    T->im = 0.0;
}

void T11V(double theta, double chi, t_complex *T)
{
    double s, cc, sc, a;
    s     = sin(theta);
    cc    = cos(chi);
    sc    = sin(chi);
    a     = -0.86602540378443865 * s;
    T->re = a * cc;
    T->im = a * sc;
}

/************************************************
 *                linear algebra                *
 ************************************************/

void Complex2RealLinSystem(int n, t_complex **ac, t_complex *bc, double **ar,
                           double *br)
{
    int i, j;

    for (i = 0; i < n; i++) {
        br[i]     = bc[i].re;
        br[i + n] = bc[i].im;
        for (j = 0; j < n; j++) {
            ar[i][j]         = ac[i][j].re;
            ar[i][j + n]     = -ac[i][j].im;
            ar[i + n][j]     = ac[i][j].im;
            ar[i + n][j + n] = ac[i][j].re;
        }
    }
}

#define SVD_MAXITERS 100
#define DSIGN(a, b)  ((b) >= 0.0 ? fabs((a)) : -fabs((a)))
#define DMAX(a, b)                                                             \
    (maxarg1 = (a), maxarg2 = (b),                                             \
     (maxarg1) > (maxarg2) ? (maxarg1) : (maxarg2))
#define DPYTHAG(a, b)                                                          \
    ((at = fabs((a))) > (bt = fabs((b)))                                       \
         ? (ct = bt / at, at * sqrt((double)(ct * ct + 1)))                    \
         : ((bt != 0) ? (ct = at / bt, bt * sqrt((double)(ct * ct + 1))) : 0))

static int Rsvd(int n, double **a, double *w, double **v)
{
    int            flag, i, its, j, jj, k, ii = 0, nm = 0;
    int            m = n;
    double         c, f, h, s, x, y, z;
    double         anorm = 0.0, g = 0.0, scale = 0.0;
    double         at, bt, ct;
    double         maxarg1, maxarg2;
    static int     n_prev = -1;
    static double *rv1    = NULL;

    if (n != n_prev) {
        free(rv1);
        rv1    = ArrayNew(n);
        n_prev = n;
    }

    n--;
    m--;
    for (i = 0; i <= n; i++) {
        ii     = i + 1;
        rv1[i] = scale * g;
        g = s = scale = 0.0;
        if (i <= m) {
            for (k = i; k <= m; k++)
                scale += fabs(a[k][i]);
            if (scale) {
                for (k = i; k <= m; k++) {
                    a[k][i] /= scale;
                    s += a[k][i] * a[k][i];
                }
                f       = a[i][i];
                g       = -DSIGN(sqrt((double)s), f);
                h       = f * g - s;
                a[i][i] = f - g;
                if (i != n) {
                    for (j = ii; j <= n; j++) {
                        for (s = 0.0, k = i; k <= m; k++)
                            s += a[k][i] * a[k][j];
                        f = s / h;
                        for (k = i; k <= m; k++)
                            a[k][j] += f * a[k][i];
                    }
                }
                for (k = i; k <= m; k++)
                    a[k][i] *= scale;
            }
        }
        w[i] = scale * g;
        g = s = scale = 0.0;
        if (i <= m && i != n) {
            for (k = ii; k <= n; k++)
                scale += fabs(a[i][k]);
            if (scale) {
                for (k = ii; k <= n; k++) {
                    a[i][k] /= scale;
                    s += a[i][k] * a[i][k];
                }
                f        = a[i][ii];
                g        = -DSIGN(sqrt((double)s), f);
                h        = f * g - s;
                a[i][ii] = f - g;
                for (k = ii; k <= n; k++)
                    rv1[k] = a[i][k] / h;
                if (i != m) {
                    for (j = ii; j <= m; j++) {
                        for (s = 0.0, k = ii; k <= n; k++)
                            s += a[j][k] * a[i][k];
                        for (k = ii; k <= n; k++)
                            a[j][k] += s * rv1[k];
                    }
                }
                for (k = ii; k <= n; k++)
                    a[i][k] *= scale;
            }
        }
        anorm = DMAX(anorm, (fabs(w[i]) + fabs(rv1[i])));
    }
    for (i = n; i >= 0; i--) {
        if (i < n) {
            if (g) {
                for (j = ii; j <= n; j++)
                    v[j][i] = (a[i][j] / a[i][ii]) / g;
                for (j = ii; j <= n; j++) {
                    for (s = 0.0, k = ii; k <= n; k++)
                        s += a[i][k] * v[k][j];
                    for (k = ii; k <= n; k++)
                        v[k][j] += s * v[k][i];
                }
            }
            for (j = ii; j <= n; j++)
                v[i][j] = v[j][i] = 0.0;
        }
        v[i][i] = 1.0;
        g       = rv1[i];
        ii      = i;
    }
    for (i = n; i >= 0; i--) {
        ii = i + 1;
        g  = w[i];
        if (i < n)
            for (j = ii; j <= n; j++)
                a[i][j] = 0.0;
        if (g) {
            g = 1.0 / g;
            if (i != n) {
                for (j = ii; j <= n; j++) {
                    for (s = 0.0, k = ii; k <= m; k++)
                        s += a[k][i] * a[k][j];
                    f = (s / a[i][i]) * g;
                    for (k = i; k <= m; k++)
                        a[k][j] += f * a[k][i];
                }
            }
            for (j = i; j <= m; j++)
                a[j][i] *= g;
        } else {
            for (j = i; j <= m; j++)
                a[j][i] = 0.0;
        }
        ++a[i][i];
    }
    for (k = n; k >= 0; k--) {
        for (its = 1; its <= SVD_MAXITERS; its++) {
            flag = 1;
            for (ii = k; ii >= 0; ii--) {
                nm = ii - 1;
                if (fabs(rv1[ii]) + anorm == anorm) {
                    flag = 0;
                    break;
                }
                if (fabs(w[nm]) + anorm == anorm)
                    break;
            }
            if (flag) {
                c = 0.0;
                s = 1.0;
                for (i = ii; i <= k; i++) {
                    f = s * rv1[i];
                    if (fabs(f) + anorm != anorm) {
                        g    = w[i];
                        h    = DPYTHAG(f, g);
                        w[i] = h;
                        h    = 1.0 / h;
                        c    = g * h;
                        s    = (-f * h);
                        for (j = 0; j <= m; j++) {
                            y        = a[j][nm];
                            z        = a[j][i];
                            a[j][nm] = y * c + z * s;
                            a[j][i]  = z * c - y * s;
                        }
                    }
                }
            }
            z = w[k];
            if (ii == k) {
                if (z < 0.0) {
                    w[k] = -z;
                    for (j = 0; j <= n; j++)
                        v[j][k] = (-v[j][k]);
                }
                break;
            }
            if (its == SVD_MAXITERS)
                return -2;
            x  = w[ii];
            nm = k - 1;
            y  = w[nm];
            g  = rv1[nm];
            h  = rv1[k];
            f  = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
            g  = DPYTHAG(f, 1.0);
            f  = ((x - z) * (x + z) + h * ((y / (f + DSIGN(g, f))) - h)) / x;
            c = s = 1.0;
            for (j = ii; j <= nm; j++) {
                i      = j + 1;
                g      = rv1[i];
                y      = w[i];
                h      = s * g;
                g      = c * g;
                z      = DPYTHAG(f, h);
                rv1[j] = z;
                c      = f / z;
                s      = h / z;
                f      = x * c + g * s;
                g      = g * c - x * s;
                h      = y * s;
                y      = y * c;
                for (jj = 0; jj <= n; jj++) {
                    x        = v[jj][j];
                    z        = v[jj][i];
                    v[jj][j] = x * c + z * s;
                    v[jj][i] = z * c - x * s;
                }
                z    = DPYTHAG(f, h);
                w[j] = z;
                if (z) {
                    z = 1.0 / z;
                    c = f * z;
                    s = h * z;
                }
                f = (c * g) + (s * y);
                x = (c * y) - (s * g);
                for (jj = 0; jj <= m; jj++) {
                    y        = a[jj][j];
                    z        = a[jj][i];
                    a[jj][j] = y * c + z * s;
                    a[jj][i] = z * c - y * s;
                }
            }
            rv1[ii] = 0.0;
            rv1[k]  = f;
            w[k]    = x;
        }
    }
    return 0;
}

#undef DSIGN
#undef DMAX
#undef DPYTHAG

#define TOL (1.0e-12)

static double Rsvelim(int n, double *w)
{
    int    i;
    double thresh;
    int    singular = 0;
    double max = w[0], min = max;
    for (i = 0; i < n; i++) {
        if (w[i] < 0.0)
            Error(E_ERROR, POR_AT, "negative w[%d]=%e", i, w[i]);
    }
    thresh = 0.0;
    for (i = 0; i < n; i++) {
        if (w[i] > thresh)
            thresh = w[i];
        if (w[i] < min)
            min = w[i];
    }
    max = thresh;
    thresh *= TOL;
    for (i = 0; i < n; i++) {
        if (w[i] < thresh) {
            w[i]     = 0.0;
            singular = 1;
        }
    }
    if (singular)
        return (max / min);
    else
        return 0.0;
}

#undef TOL

static int Rsvbksb(int n, double **u, double *w, double **v, double *b,
                   double *x)
{
    int            j, i, m = n;
    double         s;
    static double *tmp    = NULL;
    static int     n_prev = -1;

    if (n != n_prev) {
        free(tmp);
        tmp    = ArrayNew(n);
        n_prev = n;
    }
    for (j = 0; j < n; j++) {
        s = 0.0;
        if (w[j]) {
            for (i = 0; i < m; i++)
                s += u[i][j] * b[i];
            s /= w[j];
        }
        tmp[j] = s;
    }
    for (j = 0; j < n; j++) {
        s = 0.0;
        for (i = 0; i < n; i++)
            s += v[j][i] * tmp[i];
        x[j] = s;
    }
    return 0;
}

double SvdSolve(int n, double **a, double *b)
{
    int             rowsize = n * DBLSIZE;
    double          singular;
    static double * x      = NULL;
    static double * w      = NULL;
    static double * B      = NULL;
    static double **v      = NULL;
    static double **A      = NULL;
    static int      n_prev = 0;

#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid dimmension matrix: n=%d", n);
    if (!a)
        Error(E_ERROR, POR_AT, "undefined matrix");
    if (!b)
        Error(E_ERROR, POR_AT, "undefined RHS");
#endif

    if (n != n_prev) {
        free(x);
        free(w);
        free(B);
        MatrixFree(n_prev, n_prev, v);
        MatrixFree(n_prev, n_prev, A);
        x      = ArrayNew(n);
        w      = ArrayNew(n);
        B      = ArrayNew(n);
        v      = MatrixNew(n, n);
        A      = MatrixNew(n, n);
        n_prev = n;
    }

    MatrixCopy(n, n, a, A);
    Memcpy(B, b, rowsize);
    if (-2 == Rsvd(n, A, w, v))
        Error(E_WARNING, POR_AT, "no convergence in %d SVD iterations",
              SVD_MAXITERS);
    singular = Rsvelim(n, w);
    Rsvbksb(n, A, w, v, B, x);
    Memcpy(b, x, rowsize);
    return singular;
}

#undef SVD_MAXITERS

static int Cludcmp(int n, double **a, int *indx, double *d)
{
    int            i, imax = 1, j, k, i1, j1, k1;
    double         cdum, sum;
    double         big, dum, temp;
    static double *vv     = NULL;
    static int     n_last = 0;

    if (n != n_last) {
        free(vv);
        vv     = ArrayNew(n + 1);
        n_last = n;
    }
    *d = 1.0;
    for (i = 0; i < n; i++) {
        big = 0.0;
        for (j = 0; j < n; j++)
            if ((temp = fabs(a[i][j])) > big)
                big = temp;
        if (0.0 == big)
            Error(E_ERROR, POR_AT, "singular matrix");
        vv[i + 1] = 1.0 / big;
    }
    for (j = 1; j <= n; j++) {
        j1 = j - 1;
        for (i = 1; i < j; i++) {
            i1  = i - 1;
            sum = a[i1][j1];
            for (k = 1; k < i; k++)
                sum -= a[i1][k - 1] * a[k - 1][j1];
            a[i1][j1] = sum;
        }
        big = 0.0;
        for (i = j; i <= n; i++) {
            i1  = i - 1;
            sum = a[i1][j1];
            for (k = 1; k < j; k++)
                sum -= a[i1][k - 1] * a[k - 1][j1];
            a[i1][j1] = sum;
            if ((dum = vv[i] * fabs(sum)) >= big) {
                big  = dum;
                imax = i;
            }
        }
        if (j != imax) {
            for (k = 1; k <= n; k++) {
                k1              = k - 1;
                cdum            = a[imax - 1][k1];
                a[imax - 1][k1] = a[j1][k1];
                a[j1][k1]       = cdum;
            }
            *d       = -(*d);
            vv[imax] = vv[j];
        }
        indx[j1] = imax;
        if (fabs(a[j1][j1]) == 0.0)
            a[j1][j1] = 1e-20;
        if (j != n) {
            cdum = 1.0 / a[j1][j1];
            for (i = j + 1; i <= n; i++)
                a[i - 1][j - 1] = a[i - 1][j - 1] * cdum;
        }
    }
    return 0;
}

static void Clubksb(int n, double **a, int *indx, double *b)
{
    int    i, i1, ii = 0, ip, j;
    double sum;
    for (i = 1; i <= n; i++) {
        i1        = i - 1;
        ip        = indx[i1];
        sum       = b[ip - 1];
        b[ip - 1] = b[i1];
        if (ii)
            for (j = ii; j <= i1; j++)
                sum -= a[i1][j - 1] * b[j - 1];
        else if (fabs(sum) != 0.0)
            ii = i;
        b[i1] = sum;
    }
    for (i = n; i > 0; i--) {
        i1  = i - 1;
        sum = b[i1];
        for (j = i + 1; j <= n; j++)
            sum -= a[i1][j - 1] * b[j - 1];
        b[i1] = sum / a[i1][i1];
    }
}

void LudSolve(int n, double **a, double *b, int corr)
{
    static int *    indx   = NULL;
    static int      n_last = 0;
    double          d;
    static double **a_cpy = NULL;
    static double * b_cpy = NULL;
    static double * resid = NULL;

#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid dimension matrix: n=%d", n);
    if (!a)
        Error(E_ERROR, POR_AT, "undefined matrix");
    if (!b)
        Error(E_ERROR, POR_AT, "undefined RHS");
#endif

    if (n != n_last) {
        free(indx);
        MatrixFree(n_last, n_last, a_cpy);
        free(b_cpy);
        free(resid);
        indx   = (int *)Malloc(n * INTSIZE);
        a_cpy  = MatrixNew(n, n);
        b_cpy  = ArrayNew(n);
        resid  = ArrayNew(n);
        n_last = n;
    }

    if (corr) {
        Memcpy(b_cpy, b, n * DBLSIZE);
        MatrixCopy(n, n, a, a_cpy);
    }

    Cludcmp(n, a, indx, &d);
    Clubksb(n, a, indx, b);

    if (corr) {
        int i;
        for (i = 0; i < n; i++) {
            int j;
            resid[i] = b_cpy[i];
            for (j = 0; j < n; j++) {
                resid[i] -= a_cpy[i][j] * b[j];
            }
        }
        Clubksb(n, a, indx, resid);
        for (i = 0; i < n; i++) {
            b[i] += resid[i];
        }
    }
}

void LudInverse(int n, double **a, double **a_inv)
{
    int            j, i;
    static int *   indx   = NULL;
    static double *col    = NULL;
    static int     n_last = 0;
    double         d;

#if defined(DEBUG)
    if (n < 1)
        Error(E_ERROR, POR_AT, "invalid dimension matrix: n=%d", n);
    if (!a)
        Error(E_ERROR, POR_AT, "undefined matrix");
    if (!a_inv)
        Error(E_ERROR, POR_AT, "undefined inversion matrix");
#endif

    if (n != n_last) {
        free(indx);
        free(col);
        indx   = (int *)Malloc(n * INTSIZE);
        col    = ArrayNew(n);
        n_last = n;
    }
    Cludcmp(n, a, indx, &d);
    for (j = 0; j < n; j++) {
        for (i = 0; i < n; i++)
            col[i] = 0.0;
        col[j] = 1.0;
        Clubksb(n, a, indx, col);
        for (i = 0; i < n; i++)
            a_inv[i][j] = col[i];
    }
}

/*
 * interpolation
 */

void mat_LinearInterpolation(int nmod, double x, double *y, double x1,
                             double x2, const double *y1, const double *y2)
{
    int    i;
    double t = 0.0;
#ifdef DEBUG
    if (x2 < x1)
        IERR;
#endif
    if (x2 > x1)
        t = (x - x1) / (x2 - x1);
    for (i = 0; i < nmod; i++) {
        y[i] = (1.0 - t) * y1[i] + t * y2[i];
    }
}

void mat_BilinearInterpolation(int nmod, double x, double y, double *z,
                               double x_min, double x_max, double y_min,
                               double y_max, const double *z_xmin_ymin,
                               const double *z_xmax_ymin,
                               const double *z_xmin_ymax,
                               const double *z_xmax_ymax)
{
    int    i;
    double tx = 0.0, ty = 0.0;
#ifdef DEBUG
    if (x_min - x > 0 && x_min - x < 3.0 * DBL_EPSILON * (x_max - x_min))
        x = x_min;
    if (y_min - y > 0 && y_min - y < 3.0 * DBL_EPSILON * (y_max - y_min))
        y = y_min;
    if (x - x_max > 0 && x - x_max < 3.0 * DBL_EPSILON * (x_max - x_min))
        x = x_max;
    if (y - y_max > 0 && y - y_max < 3.0 * DBL_EPSILON * (y_max - y_min))
        y = y_max;

    if (x_min > x_max || y_min > y_max)
        IERR;
    if (x < x_min || x > x_max || y < y_min || y > y_max) {
        PrintfErr("   x,y: : [%8.8e,%8.8e]\n", x, y);
        PrintfErr("   x min-max: (%8.8e , %8.8e)\n", x_min, x_max);
        PrintfErr("   y_min-max: (%8.8e , %8.8e)\n", y_min, y_max);
        Error(E_ERROR, POR_AT, "point out of bilinear interpolation rectangle");
    }
#endif
    if (x_max > x_min)
        tx = (x - x_min) / (x_max - x_min);
    if (y_max > y_min)
        ty = (y - y_min) / (y_max - y_min);
    /* correction to numerical inaccuracy: */
    if (tx < 0.0)
        tx = 0.0;
    if (tx > 1.0)
        tx = 1.0;
    if (ty < 0.0)
        ty = 0.0;
    if (ty > 1.0)
        ty = 1.0;
    for (i = 0; i < nmod; i++) {
        double v_ymin = (z_xmax_ymin[i] - z_xmin_ymin[i]) * tx + z_xmin_ymin[i];
        double v_ymax = (z_xmax_ymax[i] - z_xmin_ymax[i]) * tx + z_xmin_ymax[i];
        z[i]          = (v_ymax - v_ymin) * ty + v_ymin;
    }
}

double QBezier(double h0, double h1, double ym, double yo, double yp, double x)
{
    double dm, dp, dd, a, yder, co, u;

    a  = (1.0 + h1 / (h1 + h0)) / 3.0;
    dm = (yo - ym) / h0;
    dp = (yp - yo) / h1;
    dd = dm * dp;

    yder = 0.0;
    if (dd > 0.0)
        yder = dd / (a * (dp - dm) + dm);
    /* Bezier control point is in the middle of the m-o interval */
    co = yo - 0.5 * h0 * yder;
    /* in very rare ocasions a spurious extremum may appear in M-O => use the
     * linear interpolation instead */
    /*if ((co<ym && co<yo) || (co>ym && co>yo)) co = 0.5 * (ym + yo);*/ /* THIS
                                                                           CAUSES
                                                                           DISCONTINUITIES
                                                                           IN
                                                                           SOLUTIONS
                                                                           =>
                                                                           NUMERICAL
                                                                           NOISE
                                                                         */
    /* spurious extrema in O-P do not need to be checked */
    u = 1.0 + x / h0;
    return ym * (1.0 - u) * (1.0 - u) + yo * u * u + co * 2.0 * u * (1.0 - u);
}

#define YBETWAB(y, a, b)                                                       \
    (((a <= b && y >= a && y <= b) || (a >= b && y <= a && y >= b)) ? 1 : 0)
#define MINAB(a, b) (a <= b ? a : b)
#define MAXAB(a, b) (a >= b ? a : b)
double CorrectYAB(double y, double a, double b)
{
    double min = MINAB(a, b), max = MAXAB(a, b);
    if (y < min)
        return min;
    else if (y > max)
        return max;
    else
        return y;
}
double mat_QBezierC0(double h0, double h1, double ym, double yo,
                     double yp) /* ~ 40 ns @ Xeon */
{
    double dm, dp, yder, c0, c1;
    int    cond0, cond1;

    /*dm = (yo - ym) / h0;
    dp = (yp - yo) / h1;*/
    if (h0 > 0.0 && h1 > 0.0) {
        dm = (yo - ym) / h0;
        dp = (yp - yo) / h1;
    } else {
        c0 = yo;
        return c0;
    }

    if (dm * dp <= 0) {
        c0 = yo;
        return c0;
    }

    /*a = 1.0/3.0*(1+h1/(h0+h1));
  yder = dm*dp/(a*dp+(1.0-a)*dm);*/
    yder = (h0 * dp + h1 * dm) / (h0 + h1);
    c0   = yo - 0.5 * h0 * yder;
    c1   = yo + 0.5 * h1 * yder;

    cond0 = YBETWAB(c0, ym, yo);
    cond1 = YBETWAB(c1, yo, yp);
    if (cond0 && cond1) {
        /*PrintfErr("!");*/
        return c0;
    } else if (!cond0) {
        /*PrintfErr("?");*/
        c0 = CorrectYAB(c0, ym, yo);
    } else if (!cond1) {
        /*PrintfErr("*");*/
        c1   = CorrectYAB(c1, yo, yp);
        yder = 2.0 * (c1 - yo) / h1;
        c0   = yo - 0.5 * h0 * yder;
        if (!YBETWAB(c0, ym, yo)) {
            c0 = CorrectYAB(c0, ym, yo);
        }
    } else {
        IERR;
    }

    if (!YBETWAB(c0, ym, yo)) {
        PrintfErr("ym=%e, yo=%e, c0=%e, h0=%e, h1=%e\n", ym, yo, c0, h0, h1);
        IERR;
    }

    return c0;
}
#undef YBETWAB
#undef MINAB
#undef MAXAB

void CBezierC01(double h0, double h1, double ym, double yo, double yp,
                double *c0, double *c1)
{
    double dm, dp, dd, a, y1der;

    a  = (1.0 + h1 / (h1 + h0)) / 3.0;
    dm = (yo - ym) / h0;
    dp = (yp - yo) / h1;
    dd = dm * dp;

    y1der = 0;
    if (dd > 0.0)
        y1der = dd / (a * dp + (1.0 - a) * dm);

    *c0 = ym + h0 / 3.0 * dm;
    *c1 = yo - h0 / 3.0 * y1der;

#ifdef DEBUG
    if (ym >= yo && (*c0 > ym || *c1 > ym || *c0 < yo || *c1 < yo))
        PrintfErr("A: ym=%e yo=%e yp=%e, c0=%e c1=%e\n", ym, yo, yp, *c0, *c1);
    if (ym <= yo && (*c0 < ym || *c1 < ym || *c0 > yo || *c1 > yo))
        PrintfErr("B: ym=%e yo=%e yp=%e, c0=%e c1=%e\n", ym, yo, yp, *c0, *c1);
#endif
}

double mat_PlaneInterpolation(int nx, int ny, double *xc, double *yc, double dx,
                              double dy, double **z, int is_periodic, double x,
                              double y)
{
    int    ix, iy;
    double zint;
    if (!is_periodic) {
        if (x < xc[0] || x > xc[nx - 1] || y < yc[0] || y > yc[ny - 1])
            Error(E_ERROR, POR_AT, "point (%e,%e) out of domain", x, y);
        ix = mat_ArraySearch(nx, xc, x);
        iy = mat_ArraySearch(ny, yc, y);
        mat_BilinearInterpolation(1, x, y, &zint, xc[ix], xc[ix + 1], yc[iy],
                                  yc[iy + 1], &z[iy][ix], &z[iy][ix + 1],
                                  &z[iy + 1][ix], &z[iy + 1][ix + 1]);
    } else {
        while (x < xc[0])
            x += dx;
        while (x >= xc[0] + dx)
            x -= dx;
        while (y < yc[0])
            y += dy;
        while (y >= yc[0] + dy)
            y -= dy;
        if (x < xc[nx - 1])
            ix = mat_ArraySearch(nx, xc, x);
        else
            ix = nx - 1;
        if (y < yc[ny - 1])
            iy = mat_ArraySearch(ny, yc, y);
        else
            iy = ny - 1;
        mat_BilinearInterpolation(
            1, x, y, &zint, xc[ix], (ix < nx - 1 ? xc[ix + 1] : xc[0] + dx),
            yc[iy], (iy < ny - 1 ? yc[iy + 1] : yc[0] + dy), &z[iy][ix],
            &z[iy][(ix + 1) % nx], &z[(iy + 1) % ny][ix],
            &z[(iy + 1) % ny][(ix + 1) % nx]);
    }
    return zint;
}
