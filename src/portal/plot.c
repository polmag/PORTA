/* Encapsulated postscript output via the Cgraph library */

#include "plot.h"
#include "error.h"
#include "global.h"
#include "matika.h"
#include "mem.h"
#include "modules.h"
#include "stack.h"
#include "topology.h"
#include <math.h>
#include <stdio.h>

#define EPS_XSIZE 400
#define EPS_YSIZE 300

#define XMID (0.5 * ((double)EPS_XSIZE) / 72.0)
#define YMID (0.5 * ((double)EPS_YSIZE) / 72.0)

void plt_InitScatterPlot(t_scatter_plot *sp)
{
    if (!sp) {
        Error(E_ERROR, POR_AT, "unallocated scatter plot structure");
    }
    sp->x = sp->y = NULL;
    sp->data      = NULL;
    sp->n_points = sp->n_data = 0;
}

/* frees memory allocated for a scatter plot (not the structure itself) */
void plt_ResetScatterPlot(t_scatter_plot *sp)
{
    if (!sp) {
        Error(E_ERROR, POR_AT, "uninitialized scatter plot");
    }
    free(sp->x);
    free(sp->y);
    MatrixFree(sp->n_points, sp->n_data, sp->data);
    plt_InitScatterPlot(sp);
}

void plt_AddScatterPoint(t_scatter_plot *sp, double x, double y, int n_data,
                         double *data)
{
    if (!sp)
        Error(E_ERROR, POR_AT, "uninitialized scatter plot");
    if (!data)
        Error(E_ERROR, POR_AT, "uninitialized data");
    if (0 == sp->n_data) {
        sp->n_data = n_data;
    } else {
        if (sp->n_data != n_data)
            Error(E_ERROR, POR_AT,
                  "inconsitent lengths of the data arrays (%d, %d)", sp->n_data,
                  n_data);
    }
    if (!(sp->x = (double *)realloc(sp->x, DBLSIZE * (sp->n_points + 1))))
        Error(E_ERROR, POR_AT, "cannot allocate memory for x-array");
    sp->x[sp->n_points] = x;
    if (!(sp->y = (double *)realloc(sp->y, DBLSIZE * (sp->n_points + 1))))
        Error(E_ERROR, POR_AT, "cannot allocate memory for y-array");
    sp->y[sp->n_points] = y;
    if (!(sp->data = (double **)realloc(sp->data,
                                        sizeof(double *) * (sp->n_points + 1))))
        Error(E_ERROR, POR_AT, "cannot allocate memory for data-array");
    sp->data[sp->n_points] = (double *)Malloc(DBLSIZE * sp->n_data);
    Memcpy(sp->data[sp->n_points], data, DBLSIZE * sp->n_data);
    sp->n_points++;
}

int plt_ScatterPlotSave(t_scatter_plot *sp, int index, const char *fname)
{
    FILE *f;
    int   i;

    if (!sp) {
        Error(E_ERROR, POR_AT, "uninitialized scatter plot");
        return 0;
    }
    if (!sp->n_points) {
        Error(E_ERROR, POR_AT, "no data in the scatter plot");
        return 0;
    }
    if (index < 0 || index >= sp->n_data) {
        Error(E_ERROR, POR_AT, "index data out of bounds");
        return 0;
    }
    if (!(f = fopen(fname, "wt"))) {
        Error(E_ERROR, POR_AT, "cannot open file %s", fname);
        return 0;
    }

    for (i = 0; i < sp->n_points; i++) {
        fprintf(f, "%13.13e\t%13.13e\t%13.13e\n", sp->x[i] / 1e5,
                sp->y[i] / 1e5, sp->data[i][index]);
    }

    if (fclose(f)) {
        Error(E_ERROR, POR_AT, "cannot close file %s", fname);
        return 0;
    }
    return 1;
}

#ifdef UNDEF

static int OpenEPS(char *filename, char *title)
{
    char  velik[128];
    FILE *f;
    cg_use_stdout(0);
    cg_launch_preview(0);
    cg_set_output_filename(filename);
    sprintf(&velik[0], "%s %d.%d.%d", POR_LABEL, POR_VER, POR_SUBVER,
            POR_PATCH);
    cg_setcreator(velik);
    cg_settitle(title);
    cg_useflexcolor(2); /* let the device capability dictate color/monochrome */
    sprintf(velik, "0 0 %d %d", EPS_XSIZE, EPS_YSIZE);
    cg_setboundingbox(velik);
    /* Cgraph's error handling is miserable => try to create the file by hand */
    f = fopen(filename, "w");
    if (!f) {
        Error(E_ERROR, POR_AT, "file %s cannot be open", filename);
        return 0;
    } else {
        fclose(f);
    }
    cg_init(1, 1, 1.0);
    cg_move(0.0, 0.0);
    return 1;
}

static void CloseEPS(void) { cg_showpage(); }

/* X: central point of camera (l from the viewpoint)
 * o: observer
 * p: point of interest
 * u, v: unit orthodonal vectors in the camera plane
 * scale: conversion from cm to pixels
 * ix, iy: pixel coordinates of the projection
 */
static void GetRPixels(t_vector_3d X, t_vector_3d o, t_vector_3d p,
                       t_vector_3d u, t_vector_3d v, double scale, double *ix,
                       double *iy)
{
    double  a, b;
    double *mtx[3], rhs[3];

    mtx[0] = (double *)Malloc(DBLSIZE * 3);
    mtx[1] = (double *)Malloc(DBLSIZE * 3);
    mtx[2] = (double *)Malloc(DBLSIZE * 3);

    mtx[0][0] = o[0] - p[0];
    mtx[0][1] = -u[0];
    mtx[0][2] = -v[0];
    rhs[0]    = X[0] - p[0];
    mtx[1][0] = o[1] - p[1];
    mtx[1][1] = -u[1];
    mtx[1][2] = -v[1];
    rhs[1]    = X[1] - p[1];
    mtx[2][0] = o[2] - p[2];
    mtx[2][1] = -u[2];
    mtx[2][2] = -v[2];
    rhs[2]    = X[2] - p[2];

    LudSolve(3, mtx, rhs, 0);
    a = rhs[1];
    b = rhs[2];

    *ix = scale * a + XMID;
    *iy = scale * b + YMID;

    free(mtx[0]);
    free(mtx[1]);
    free(mtx[2]);
}

int PlotMeshEPS(char *filename, t_vector_3d viewpoint, t_vector_3d lookat,
                double perspective)
{
    t_vector_3d           u, v, n, X;
    double                Rl, R, l, width, ux2, uy2, zx, zy, scale;
    int                   i, j;
    extern t_global_vars *g_global;
    double                minthick = 0.2, maxthick = 1.0;
    double                maxdist, mindist;
    t_vector_3d           w[4];
    int                   f[3];
    t_vector_3d           po, pox, poy, poz;
    double                prodl, ox, oy;

    stk_Add(1, POR_AT, "plotting the grid mesh in %s", filename);
    if (!filename) {
        Error(E_ERROR, POR_AT, "null file name");
        return 0;
    }

    if (!OpenEPS(filename, "Mesh Plot"))
        return 0;

    if (!g_global->queue.nodes) {
        Error(E_ERROR, POR_AT, "queue not initialized");
        CloseEPS();
        return 0;
    }

    u[0] = viewpoint[0] - lookat[0];
    u[1] = viewpoint[1] - lookat[1];
    u[2] = viewpoint[2] - lookat[2];
    Rl   = VEC_LEN_3D(u);
    l    = Rl * 0.25;
    R    = Rl - l;
    n[0] = u[0];
    n[1] = u[1];
    n[2] = u[2];
    VEC_NORMALIZE_3D(n);
    width = perspective * l;
    X[0]  = lookat[0] + n[0] * R;
    X[1]  = lookat[1] + n[1] * R;
    X[2]  = lookat[2] + n[2] * R;

    if (n[2] == 1.0)
        Error(E_ERROR, POR_AT, "sorry, can't work with vertical LOS");

    /* unit vectors in the projection plane */
    ux2 = n[1] * n[1] / (n[0] * n[0] + n[1] * n[1]);
    uy2 = n[0] * n[0] / (n[0] * n[0] + n[1] * n[1]);
    if (n[1] < 0)
        zx = 1.0;
    else
        zx = -1.0;
    if (n[0] < 0)
        zy = -1.0;
    else
        zy = 1.0;
    u[0]  = sqrt(ux2) * zx;
    u[1]  = sqrt(uy2) * zy;
    u[2]  = 0.0;
    v[0]  = n[1] * u[2] - n[2] * u[1];
    v[1]  = n[2] * u[0] - n[0] * u[2];
    v[2]  = n[0] * u[1] - n[1] * u[0];
    scale = 2.0 * XMID / width;

    maxdist = 0.0;
    mindist = 1e300;
    for (i = 0; i < g_global->n_nodes; i++) {
        t_node *    p = g_global->queue.nodes[i];
        t_vector_3d ve1;
        ve1[0] = p->r[0];
        ve1[1] = p->r[1];
        ve1[2] = p->r[2];
        for (j = 0; j < 6; j++) {
            t_vector_3d ve2, stred;
            double      dist;
            if (!p->p_neighbors[j])
                continue;
            ve2[0]   = p->p_neighbors[j]->r[0];
            ve2[1]   = p->p_neighbors[j]->r[1];
            ve2[2]   = p->p_neighbors[j]->r[2];
            stred[0] = 0.5 * (ve1[0] + ve2[0]);
            stred[1] = 0.5 * (ve1[1] + ve2[1]);
            stred[2] = 0.5 * (ve1[2] + ve2[2]);
            stred[0] -= viewpoint[0];
            stred[1] -= viewpoint[1];
            stred[2] -= viewpoint[2];
            dist = VEC_LEN_3D(stred);
            if (dist < mindist)
                mindist = dist;
            if (dist > maxdist)
                maxdist = dist;
        }
    }

    cg_dash(0, 1.0);
    cg_linewidth(1.0);

    /* plot axes */
    cg_dash(0, 1.0);
    cg_linewidth((float)maxthick * 1.5f);
    cg_rgbcolor(1.0f, 0.0f, 0.0f);
    prodl  = 1.2;
    po[0]  = g_global->cells->p_nodes[OCT_LBD]->r[0];
    po[1]  = g_global->cells->p_nodes[OCT_LBD]->r[1];
    po[2]  = g_global->cells->p_nodes[OCT_LBD]->r[2];
    pox[0] = po[0] + prodl * g_global->domain_size[0];
    pox[1] = po[1];
    pox[2] = po[2];
    poy[0] = po[0];
    poy[1] = po[1] + prodl * g_global->domain_size[1];
    poy[2] = po[2];
    poz[0] = po[0];
    poz[1] = po[1];
    poz[2] = po[2] + prodl * g_global->domain_size[2];
    /* x */
    GetRPixels(X, viewpoint, po, u, v, scale, &ox, &oy);
    cg_move((float)ox, (float)oy);
    GetRPixels(X, viewpoint, pox, u, v, scale, &ox, &oy);
    cg_line((float)ox, (float)oy);
    cg_stroke();
    /* y */
    GetRPixels(X, viewpoint, po, u, v, scale, &ox, &oy);
    cg_move((float)ox, (float)oy);
    GetRPixels(X, viewpoint, poy, u, v, scale, &ox, &oy);
    cg_line((float)ox, (float)oy);
    cg_stroke();
    /* z */
    GetRPixels(X, viewpoint, po, u, v, scale, &ox, &oy);
    cg_move((float)ox, (float)oy);
    GetRPixels(X, viewpoint, poz, u, v, scale, &ox, &oy);
    cg_line((float)ox, (float)oy);
    cg_stroke();

    /* plot the boundaries */
    if (n[0] >= 0 && n[1] >= 0) {
        f[0] = 1;
        f[1] = 3;
    } else if (n[0] <= 0 && n[1] >= 0) {
        f[0] = 0;
        f[1] = 3;
    } else if (n[0] <= 0 && n[1] <= 0) {
        f[0] = 0;
        f[1] = 2;
    } else if (n[0] >= 0 && n[1] <= 0) {
        f[0] = 1;
        f[1] = 2;
    } else
        Error(E_ERROR, POR_AT, "internal error");
    if (n[2] > 0)
        f[2] = 5;
    else
        f[2] = 4;
    for (i = 0; i < 3; i++) {
        int    j;
        double ix, iy;
        double ss;
        for (j = 0; j < 4; j++) {
            t_node *pn = g_global->cells->p_nodes[g_c_cube_face[f[i]][j]];
            w[j][0]    = pn->r[0];
            w[j][1]    = pn->r[1];
            w[j][2]    = pn->r[2];
        }
        /* grayness is a function of face normal vector versus n */
        if (f[i] == 0 || f[i] == 1)
            ss = fabs(n[0]);
        else if (f[i] == 2 || f[i] == 3)
            ss = fabs(n[1]);
        else
            ss = fabs(n[2]);
        cg_gray(0.5f + (float)(ss * 0.5 - 0.05));
        GetRPixels(X, viewpoint, w[0], u, v, scale, &ix, &iy);
        cg_move((float)ix, (float)iy);
        GetRPixels(X, viewpoint, w[1], u, v, scale, &ix, &iy);
        cg_line((float)ix, (float)iy);
        GetRPixels(X, viewpoint, w[2], u, v, scale, &ix, &iy);
        cg_line((float)ix, (float)iy);
        GetRPixels(X, viewpoint, w[3], u, v, scale, &ix, &iy);
        cg_line((float)ix, (float)iy);
        cg_closepath();
        cg_fill();
        cg_stroke();
    }

    /* plot mesh */
    cg_gray(0.0f);
    for (i = 0; i < g_global->n_nodes; i++) {
        t_node *p = g_global->queue.nodes[i];
        for (j = 0; j < 6; j++) {
            t_vector_3d ve1;
            double      x1, y1;
            ve1[0] = p->r[0];
            ve1[1] = p->r[1];
            ve1[2] = p->r[2];
            GetRPixels(X, viewpoint, ve1, u, v, scale, &x1, &y1);
            if (p->p_neighbors[j] && j % 2) {
                t_vector_3d ve2;
                double      x2, y2;
                t_vector_3d stred;
                double      dist;
                float       thi;
                ve2[0] = p->p_neighbors[j]->r[0];
                ve2[1] = p->p_neighbors[j]->r[1];
                ve2[2] = p->p_neighbors[j]->r[2];
                GetRPixels(X, viewpoint, ve2, u, v, scale, &x2, &y2);
                stred[0] = 0.5 * (ve1[0] + ve2[0]);
                stred[1] = 0.5 * (ve1[1] + ve2[1]);
                stred[2] = 0.5 * (ve1[2] + ve2[2]);
                stred[0] -= viewpoint[0];
                stred[1] -= viewpoint[1];
                stred[2] -= viewpoint[2];
                dist = VEC_LEN_3D(stred);
                thi =
                    (float)(maxthick -
                            (maxthick - minthick) / (mindist - maxdist) *
                                mindist +
                            dist * (maxthick - minthick) / (mindist - maxdist));
                cg_linewidth(thi);
                cg_move((float)x1, (float)y1);
                cg_line((float)x2, (float)y2);
                cg_stroke();
            }
        }
    }

    CloseEPS();

    return 1;
}

#endif

#ifdef UNDEF
int plt_J00_3D_(int transition_id, char file_name[])
{
    int   i, j, k, nn;
    FILE *f;
    if (!mod_IsModuleLoaded()) {
        Error(E_ERROR, POR_AT, "no module loaded");
        return 0;
    }
    if (!(f = fopen(file_name, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open file %s", file_name);
        return 0;
    }
    nn = 12;
    fwrite(&nn, INTSIZE, 1, f);
    fwrite(&g_global->grid[0].nx, INTSIZE, 1, f);
    fwrite(&g_global->grid[0].ny, INTSIZE, 1, f);
    fwrite(&g_global->grid[0].nz, INTSIZE, 1, f);
    fwrite(&nn, INTSIZE, 1, f);
    nn = 4 * g_global->grid[0].nx * g_global->grid[0].ny * g_global->grid[0].nz;
    fwrite(&nn, INTSIZE, 1, f);
    for (k = 0; k < g_global->grid[0].nz; k++) {
        for (j = 0; j < g_global->grid[0].ny; j++) {
            for (i = 0; i < g_global->grid[0].nx; i++) {
                int     ndata;
                t_node *pn  = &g_global->grid[0].node[k][j][i];
                double *j00 = g_global->afunc.F_GetRadiArray(pn, &ndata);
                float   da  = (float)j00[transition_id];
                fwrite(&da, sizeof(float), 1, f);
                free(j00);
            }
        }
    }
    fwrite(&nn, INTSIZE, 1, f);
    if (!fclose(f)) {
        Error(E_ERROR, POR_AT, "cannot save file %s", file_name);
        return 0;
    }
    return 1;
}
#endif
