#include "pstream.h"
#include <memory.h>

/*******************************************************************************/
t_pstream pst_NewStream(int length, char *data, int writeable)
{
    t_pstream stream;
    stream.writeable = writeable;
    stream.buffer    = data;
    stream.offset    = stream.buffer;
    stream.length    = length;
    return stream;
}

/*******************************************************************************/
int pst_Read(t_pstream *stream, int type, void *result)
{
    if (!result)
        return 0;
    if (!stream || stream->writeable)
        return 0;

    switch (type) {
    case PSTREAM_BYTE:
        if (stream->length - (stream->offset - stream->buffer) < 1)
            return 0;
        memcpy(result, stream->offset, 1);
        stream->offset++;
        break;

    case PSTREAM_INT:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(int))
            return 0;
        memcpy(result, stream->offset, sizeof(int));
        stream->offset += sizeof(int);
        break;

    case PSTREAM_DOUBLE:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(double))
            return 0;
        memcpy(result, stream->offset, sizeof(double));
        stream->offset += sizeof(double);
        break;

    case PSTREAM_FLOAT:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(float))
            return 0;
        memcpy(result, stream->offset, sizeof(float));
        stream->offset += sizeof(float);
        break;

    default:
        return 0;
        break;
    }

    return 1;
}

/*******************************************************************************/
int pst_Write(t_pstream *stream, int type, void *src)
{
    if (!src)
        return 0;
    if (!stream || !stream->writeable)
        return 0;

    switch (type) {
    case PSTREAM_BYTE:
        if (stream->length - (stream->offset - stream->buffer) < 1)
            return 0;
        memcpy(stream->offset, src, 1);
        stream->offset++;
        break;

    case PSTREAM_INT:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(int))
            return 0;
        memcpy(stream->offset, src, sizeof(int));
        stream->offset += sizeof(int);
        break;

    case PSTREAM_DOUBLE:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(double))
            return 0;
        memcpy(stream->offset, src, sizeof(double));
        stream->offset += sizeof(double);
        break;

    case PSTREAM_FLOAT:
        if (stream->length - (stream->offset - stream->buffer) < sizeof(float))
            return 0;
        memcpy(stream->offset, src, sizeof(float));
        stream->offset += sizeof(float);
        break;

    default:
        return 0;
        break;
    }

    return 1;
}
