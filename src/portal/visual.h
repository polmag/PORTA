/*
 * Visualization functions of Porta.
 *
 * These functions are to be used on a converged non-LTE model
 * and to be run on a L=M=1 decompositions (only master + 1 slave node).
 *
 * Memory requirements of huge models are overcome by dynamical
 * loading of the required grid planes from the pmd file.
 *
 * The files in which the functions are implemented are
 * mentioned in their header comments.
 *
 * Some of the functions are provided with pointer to the pmd file
 * from which they load the required data.
 *
 * These functions should be accessed only via process.h functions
 * in which all the necessary tests are performed.
 *
 */

#ifndef VISUAL_H_
#define VISUAL_H_

#include "def.h"

/* load the nodes' data of grid plane iz
 * (io.c)
 */
extern void vis_LoadZPlane(FILE *pmd, int iz);

#endif /* VISUAL_H_ */
