/* grid creation and management */

#ifndef GRID_H_
#define GRID_H_

#include "def.h"

extern double grd_cycle_x(const t_grid *g, double x);
extern double grd_cycle_y(const t_grid *g, double y);

/* x-coordinate in the infinite periodic space; for any index ix */
extern double grd_XCycl(const t_grid *g, int ix);

/* y-coordinate in the infinite periodic space; for any index iy */
extern double grd_YCycl(const t_grid *g, int iy);

/* gives x-index between 0 and g->nx-1 from arbitrary ix index in the
 * horizontally infinite medium; g: pointer to grid */
#define grd_XIndex(g, ix)                                                      \
    ((((ix) >= 0) ? (ix) : ((g)->nx - ((-(ix)) % (g)->nx))) % (g)->nx)

/* gives y-index between 0 and g->ny-1 from arbitrary iy index in the
 * horizontally infinite medium; g: pointer to grid */
#define grd_YIndex(g, iy)                                                      \
    ((((iy) >= 0) ? (iy) : ((g)->ny - ((-(iy)) % (g)->ny))) % (g)->ny)

/* sets the domain dimensions and position and scales all the allocated grids
 * accordingly */
extern void grd_SetDomainGeometry(t_vec_3d origin, t_vec_3d dim);

/* finds intersection inters of a ray starting in p_node in direction with the
 * closest Cartesian plane if horizontal!=0 then the first horizontal
 * intersection is found cross_x(y): returned number of crossed vertical planes;
 * cross_x=# of crossed YZ planes (zero if no uw prolongation has been
 * performed) WARNING: cannot be used for rays parallel to any of the XYZ axes
 */
extern void grd_FindIntersection(const t_node *p_node, t_vec_3d *dir,
                                 int horizontal, t_intersect *inters,
                                 int *cross_x, int *cross_y);

/* create a new rectilinear grid and initialized the nodes p_data structure
 * g_global->pbc_maxgen is ignored
 * size: dimensions in 3 axes
 * center: central point of the grid
 * nx, ny, nz: number of nodes along axes; set equidistant
 * NOTE: in the master process, no nodes are allocated
 *
 * the domain origin and dimensions must be set before this function is called!
 */
extern void grd_NewGrid(int grid_id, int nx, int ny, int nz);

/* like grd_NewGrid but sets the nodes coordinates stored in the arrays x,y,z
 *
 * domain size is set accordingly
 */
extern void grd_NewGridC(int grid_id, int nx, int ny, int nz, double x[],
                         double y[], double z[]);

/* release one grid */
extern void grd_ReleaseGrid(int ig);

/* free memory allocated for the all grids (also atomic data are released
 * (modules is unlinked), PBC and the priority queue freed) */
extern void grd_ReleaseDomain(void);

#endif /* GRID_H_ */
