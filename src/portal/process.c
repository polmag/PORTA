#include "process.h"
#include "def.h"
#include "directions.h"
#include "error.h"
#include "global.h"
#include "grid.h"
#include "modules.h"
#include <mpi.h>

/*************************/
/* topological functions */
/*************************/

/*******************************************************************************/
void pro_PrintProcessInfo(void)
{
    Printf("=======================================================\n");
    Printf("Number of slave processes: %d\n", G_MPI_NPROCS - 1);
    Printf("#L=%d, #M=%d\n", G_MPI_L, G_MPI_M);
    if (G_MY_MPI_RANK == 0) {
        Printf("Process rank 0 (master)\n");
    } else {
        Printf("Process rank %d (slave)\n", G_MY_MPI_RANK);
        Printf("L=%d, M=%d\n", pro_Domain_Z_ID(G_MY_MPI_RANK),
               pro_MyFreqBand_ID());
        if (mod_IsModuleLoaded()) {
            char *mod_name = mod_GetModuleName();
            int   min_iz, max_iz, min_ifreq, max_ifreq;
            Printf("Module \"%s\" loaded, #frequencies=%d\n", mod_name,
                   g_global->n_freq);
            free(mod_name);
            pro_MinMax_IZ(G_MY_MPI_RANK, g_global->grid[0].nz, &min_iz,
                          &max_iz);
            Printf("My z-interval: %d -- %d\n", min_iz, max_iz);
            pro_MinMaxIFreq(&min_ifreq, &max_ifreq);
            Printf("My frequency interval: %d -- %d\n", min_ifreq, max_ifreq);
        }
    }
}

/*******************************************************************************/
int pro_CanBeCoarsened(t_grid *g)
{
    int nx = g->nx, ny = g->ny, nz = g->nz;
    int m = pro_NSubdomainZNodes(nz), i;

    if (!m)
        return 0;
    if ((m - 1) * G_MPI_L != nz - 1)
        return 0;
    if (!(m % 2))
        return 0;
    if ((g_global->period[0] && (nx % 2)) ||
        (!g_global->period[0] && !(nx % 2)))
        return 0;
    if ((g_global->period[1] && (ny % 2)) ||
        (!g_global->period[1] && !(ny % 2)))
        return 0;
    for (i = 1; i <= G_MPI_L; i++) {
        int minc_iz, maxc_iz, minf_iz, maxf_iz;
        pro_MinMax_IZ(i, nz, &minf_iz, &maxf_iz);
        pro_MinMax_IZ(i, nz / 2 + 1, &minc_iz, &maxc_iz);
        if (minc_iz * 2 != minf_iz || maxc_iz * 2 != maxf_iz)
            return 0;
    }
    return 1;
}

/*******************************************************************************/
int pro_Domain_Z_ID(int rank)
{
    if (rank < 1 || rank >= G_MPI_NPROCS)
        return 0;
    else
        return ((rank - 1) % G_MPI_L) + 1;
}

/*******************************************************************************/
int pro_MyFreqBand_ID(void)
{
    if (!G_MY_MPI_RANK)
        return -1;
    else
        return (G_MY_MPI_RANK - 1) / G_MPI_L;
}

/*******************************************************************************/
int pro_NSubdomainZNodes(int nz)
{
    int m, n = nz - 1;
    if (n < G_MPI_L)
        return 0;
    if (n % G_MPI_L == 0)
        m = n / G_MPI_L; /* all domains have the same number of Z-planes */
    else
        m = n / G_MPI_L + 1;
    /* check possibility of such division */
    if (m * (G_MPI_L - 1) >= n)
        return 0;
    /* now m contains number of STEPS within the subdomain => convert to the
     * number of nodes per subdomain: */
    return m + 1;
}

/*******************************************************************************/
int pro_MinMax_IZ(int rank, int nz, int *min_iz, int *max_iz)
{
    int id, steps = nz - 1, a, rest;

    if (rank < 1 || rank >= G_MPI_NPROCS) {
        *min_iz = *max_iz = -1;
        return 0;
    }

    id   = pro_Domain_Z_ID(rank); /* id's start at 1 */
    a    = steps / G_MPI_L;
    rest = steps % G_MPI_L;

    if (id <= rest) {
        // these domains should have one step, so these will cover rest*(a+1)
        // steps
        *min_iz = (id - 1) * (a + 1);
        *max_iz = *min_iz + (a + 1);
    } else {
        // these domains will cover (G_MPI_L - rest)*a steps
        *min_iz = rest * (a + 1) + ((id - 1) - rest) * a;
        *max_iz = *min_iz + a;
    }

    return 1;
}

/*******************************************************************************/
void pro_GetIZIndices(int nz, int iz, int *idz1, int *idz2)
{
    int idz;
    if (iz < 0 || iz >= nz)
        IERR;
    *idz1 = *idz2 = 0;
    for (idz = 1; idz <= G_MPI_L; idz++) {
        int min_iz, max_iz;
        pro_MinMax_IZ(idz, nz, &min_iz, &max_iz);
        if (min_iz == iz)
            *idz2 = idz;
        if (max_iz == iz)
            *idz1 = idz;
        if (iz > min_iz && iz < max_iz)
            *idz1 = idz;
    }
    if (*idz1 == 0 && *idz2 == 0)
        IERR;
}

/*******************************************************************************/
void pro_MinMaxIRange(int min_range, int max_range, int *min_i, int *max_i)
{
    int iband   = (G_MY_MPI_RANK - 1) / G_MPI_L, a, rest;
    int n_range = max_range - min_range + 1; /* number of items in range */

    if (!G_MY_MPI_RANK || !g_global)
        IERR;
    if (n_range < G_MPI_M) {
        Error(E_WARNING, POR_AT,
              "number of items in range: %d, is smaller than number of "
              "frequency bands: %d. Some processes will be idle",
              n_range, G_MPI_M);
    }

    a    = n_range / G_MPI_M;
    rest = n_range % G_MPI_M;

    if (iband < rest) {
        // these bands should have one more item, so these will cover
        // rest*(a+1) items
        *min_i = iband * (a + 1);
        *max_i = *min_i + a;
    } else {
        // these bands will cover (G_MPI_M - rest)*a items
        *min_i = rest * (a + 1) + (iband - rest) * a;
        *max_i = *min_i + a - 1;
    }

    // Shift by min_range, in case the range is not starting at 0
    *min_i += min_range;
    *max_i += min_range;

#ifdef DEBUG
    if (*max_i >= n_range || *min_i < 0)
        IERR;
#endif
}

/*******************************************************************************/
void pro_MinMaxIFreqRank(int rank, int *min_ifreq, int *max_ifreq)
{
    int iband = (rank - 1) / G_MPI_L, a, rest;

    if (!rank || !g_global)
        IERR;
    if (g_global->n_freq < G_MPI_M) {
        Error(
            E_ERROR, POR_AT,
            "total number of frequencies (%d) cannot be smaller than number of "
            "frequency bands (%d)",
            g_global->n_freq, G_MPI_M);
    }

    a    = g_global->n_freq / G_MPI_M;
    rest = g_global->n_freq % G_MPI_M;

    if (iband < rest) {
        // these bands should have one frequency more, so these will cover
        // rest*(a+1) frequencies
        *min_ifreq = iband * (a + 1);
        *max_ifreq = *min_ifreq + a;
    } else {
        // these bands will cover (G_MPI_M - rest)*a frequencies
        *min_ifreq = rest * (a + 1) + (iband - rest) * a;
        *max_ifreq = *min_ifreq + a - 1;
    }

#ifdef DEBUG
    if (*max_ifreq >= g_global->n_freq || *min_ifreq < 0)
        IERR;
#endif
}

/*******************************************************************************/
void pro_MinMaxIFreq(int *min_ifreq, int *max_ifreq)
{
    pro_MinMaxIFreqRank(G_MY_MPI_RANK, min_ifreq, max_ifreq);
}

/*******************************************************************************/
int pro_MinMax_ese(int rank, int nx, int ny, int *min_ese, int *max_ese)
{
    int id, nxy = nx * ny, a, rest;

    if (rank < 1 || rank >= G_MPI_NPROCS) {
        *min_ese = *max_ese = -1;
        return 0;
    }

    id   = pro_MyFreqBand_ID(); /* id's start at 0 */
    a    = nxy / G_MPI_M;
    rest = nxy % G_MPI_M;

    if (id < rest) {
        // these domains will compute cover rest*(a+1) points
        *min_ese = id * (a + 1);
        *max_ese = *min_ese + (a);
    } else {
        // these domains will cover (G_MPI_M - rest)*a steps
        *min_ese = rest * (a + 1) + (id - rest) * a;
        *max_ese = *min_ese + (a - 1);
    }

    return 1;
}

/*******************************************************************************/
/* pro_ese_gv_displ_counts
 *
 * local function. It calculates the right displacements (displ) and receive
 *  counts (recv_counts) to be used in the mpi_allgatherv call in
 * function pro_CollectDM
 */
int pro_ese_gv_displ_counts(int nx, int ny, int nrdim, int *displ,
                            int *recv_counts)
{
    int id, min_ese, max_ese, nxy = nx * ny, a, rest;

    a    = nxy / G_MPI_M;
    rest = nxy % G_MPI_M;

    for (id = 0; id < G_MPI_M; id++) {
        // mimics pro_MinMax_ese but fills displ and recv_counts for all
        // frequency bands

        if (id < rest) {
            min_ese = id * (a + 1);
            max_ese = min_ese + (a);
        } else {
            min_ese = rest * (a + 1) + (id - rest) * a;
            max_ese = min_ese + (a - 1);
        }

        displ[id]       = min_ese * nrdim;
        recv_counts[id] = (max_ese - min_ese + 1) * nrdim;
    }

    return 1;
}

/*************/
/* signaling */
/*************/

/*******************************************************************************/
int pro_SignalToAllSlaves(int signal)
{
    int i;
    if (G_MY_MPI_RANK)
        return 0;
    for (i = 1; i < G_MPI_NPROCS; i++) {
        if (MPI_SUCCESS !=
            MPI_Send((void *)&signal, 1, MPI_INT, i, 0, MPI_COMM_WORLD)) {
            Error(E_ERROR, POR_AT, "failed sending signal %d process %d",
                  signal, i);
            return 0;
        }
    }
    return 1;
}

/*******************************************************************************/
int pro_ReceiveSignalFromMaster(void)
{
    int        signal, master = 0;
    MPI_Status status;
    if (!G_MY_MPI_RANK)
        return SIG_ERROR;
    if (MPI_SUCCESS == MPI_Recv((void *)&signal, 1, MPI_INT, master, 0,
                                MPI_COMM_WORLD, &status)) {
        return signal;
    } else {
        return SIG_ERROR;
    }
}

/***************************/
/* data transfer functions */
/***************************/

/*******************************************************************************/
int pro_M2S_SendDirs(void)
{
    int i, data[2];
    if (G_MY_MPI_RANK)
        return 0;
    data[0] = g_global->dirs.n_incl_oct;
    data[1] = g_global->dirs.n_azim_oct;
    for (i = 1; i < G_MPI_NPROCS; i++) {
        if (MPI_SUCCESS != MPI_Send(data, 2, MPI_INT, i, 0, MPI_COMM_WORLD)) {
            Error(E_ERROR, POR_AT,
                  "failed sending direction quadrature data to process %d", i);
            return 0;
        }
    }
    return 1;
}

/*******************************************************************************/
int pro_S_RecvDirs(void)
{
    int        data[2], master = 0;
    MPI_Status status;
    if (!G_MY_MPI_RANK)
        return 0;
    if (MPI_SUCCESS !=
        MPI_Recv(data, 2, MPI_INT, master, 0, MPI_COMM_WORLD, &status)) {
        return 0;
    } else {
        if (data[1] == -1) {
            dir_MakeDirectionsQuadSet(data[0], &g_global->dirs);
        } else {
            dir_MakeDirections(data[0], data[1], &g_global->dirs);
        };
        return 1;
    }
}

/*******************************************************************************/
int pro_M2S_SendGridGeometry(int process_id)
{
    double vec[6], ord[MAX_N_ORDINATES];
    int    integers[7];
    /* domain size and origin */
    if (G_MY_MPI_RANK)
        return 0;
    vec[0] = g_global->domain_size.x;
    vec[1] = g_global->domain_size.y;
    vec[2] = g_global->domain_size.z;
    vec[3] = g_global->domain_origin.x;
    vec[4] = g_global->domain_origin.y;
    vec[5] = g_global->domain_origin.z;
    if (MPI_SUCCESS !=
        MPI_Send(vec, 6, MPI_DOUBLE, process_id, 0, MPI_COMM_WORLD)) {
        Error(E_ERROR, POR_AT,
              "failed sending domain size and origin to process %d",
              process_id);
        return 0;
    }
    /* the integer parameters */
    integers[0] = g_global->period[0];
    integers[1] = g_global->period[1];
    integers[2] = g_global->max_n_grids;
    integers[3] = g_global->current_n_grids;
    integers[4] = g_global->grid[0].nx;
    integers[5] = g_global->grid[0].ny;
    integers[6] = g_global->grid[0].nz;
    if (MPI_SUCCESS !=
        MPI_Send(integers, 7, MPI_INT, process_id, 0, MPI_COMM_WORLD)) {
        Error(E_ERROR, POR_AT, "failed sending to process %d", process_id);
        return 0;
    }
    /* coordinates */
    Memcpy(ord, g_global->grid[0].x, sizeof(double) * MAX_N_ORDINATES);
    if (MPI_SUCCESS != MPI_Send(ord, MAX_N_ORDINATES, MPI_DOUBLE, process_id, 0,
                                MPI_COMM_WORLD)) {
        Error(E_ERROR, POR_AT, "failed sending X coordinates to process %d",
              process_id);
        return 0;
    }
    Memcpy(ord, g_global->grid[0].y, sizeof(double) * MAX_N_ORDINATES);
    if (MPI_SUCCESS != MPI_Send(ord, MAX_N_ORDINATES, MPI_DOUBLE, process_id, 0,
                                MPI_COMM_WORLD)) {
        Error(E_ERROR, POR_AT, "failed sending Y coordinates to process %d",
              process_id);
        return 0;
    }
    Memcpy(ord, g_global->grid[0].z, sizeof(double) * MAX_N_ORDINATES);
    if (MPI_SUCCESS != MPI_Send(ord, MAX_N_ORDINATES, MPI_DOUBLE, process_id, 0,
                                MPI_COMM_WORLD)) {
        Error(E_ERROR, POR_AT, "failed sending Z coordinates to process %d",
              process_id);
        return 0;
    }
    return 1;
}

/*******************************************************************************/
int pro_S_ReceiveGridGeometry(void)
{
    double size_ori[6], x[MAX_N_ORDINATES], y[MAX_N_ORDINATES],
        z[MAX_N_ORDINATES];
    t_vec_3d   size, origin;
    int        integers[7], master = 0;
    MPI_Status status;

    if (!G_MY_MPI_RANK)
        return 0;
    if (g_global->current_n_grids != 0) {
        Error(E_ERROR, POR_AT, "grid has already been initialized");
        return 0;
    }

    /* domain size and origin */
    if (MPI_SUCCESS !=
        MPI_Recv(size_ori, 6, MPI_DOUBLE, master, 0, MPI_COMM_WORLD, &status))
        IERR;
    size.x   = size_ori[0];
    size.y   = size_ori[1];
    size.z   = size_ori[2];
    origin.x = size_ori[3];
    origin.y = size_ori[4];
    origin.z = size_ori[5];
    grd_SetDomainGeometry(origin, size);

    /* the integer parameters */
    if (MPI_SUCCESS !=
        MPI_Recv(integers, 7, MPI_INT, master, 0, MPI_COMM_WORLD, &status))
        IERR;

    /* the ordinates */
    if (MPI_SUCCESS != MPI_Recv(x, MAX_N_ORDINATES, MPI_DOUBLE, master, 0,
                                MPI_COMM_WORLD, &status))
        IERR;
    if (MPI_SUCCESS != MPI_Recv(y, MAX_N_ORDINATES, MPI_DOUBLE, master, 0,
                                MPI_COMM_WORLD, &status))
        IERR;
    if (MPI_SUCCESS != MPI_Recv(z, MAX_N_ORDINATES, MPI_DOUBLE, master, 0,
                                MPI_COMM_WORLD, &status))
        IERR;

    g_global->period[0]   = integers[0];
    g_global->period[1]   = integers[1];
    g_global->max_n_grids = integers[2];
    /* g_global->current_n_grids == 0 */
    grd_NewGridC(0, integers[4], integers[5], integers[6], x, y, z);

    return 1;
}

/*******************************************************************************/
int pro_SendRadiationBuffer(int len, double *buffer, int to_rank)
{
    if (!buffer || len <= 0)
        IERR;
    if (to_rank < 0 || to_rank >= G_MPI_NPROCS)
        IERR;
    /* MPI implementation can avoid buffering data when using Ssend =>
     * performance gain: */
    if (MPI_SUCCESS !=
        MPI_Ssend(buffer, len, MPI_DOUBLE, to_rank, 0, MPI_COMM_WORLD)) {
        CERR;
        return 0;
    }
    return 1;
}

/*******************************************************************************/
int pro_ReceiveRadiationBuffer(int len, double *buffer, int from_rank)
{
    MPI_Status status;
    if (!buffer || len <= 0)
        IERR;
    if (from_rank < 0 || from_rank >= G_MPI_NPROCS)
        IERR;
    if (MPI_SUCCESS != MPI_Recv(buffer, len, MPI_DOUBLE, from_rank, 0,
                                MPI_COMM_WORLD, &status)) {
        CERR;
        return 0;
    }
    return 1;
}

/*******************************************************************************/
void pro_CollectRadiationField(t_grid *g)
{
    int        nrlen;
    int        min_iz, max_iz, iz, iy, ix, m;
    double *   buf = NULL, *cbuf = NULL, *p;
    MPI_Status status;

    stk_Add(0, POR_AT, "collecting radiation field from the processes");
    if (!G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized())
        IERR;
    if (G_MPI_M == 1)
        return;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    m   = pro_MyFreqBand_ID();
    buf = g_global->afunc.F_GetRadiation(&g->node[min_iz][0][0],
                                         &nrlen); /* to obtain nrlen */
    free(buf);
    buf  = (double *)Malloc(nrlen * g->nx * g->ny * DBLSIZE);
    cbuf = (double *)Malloc(nrlen * g->nx * g->ny * DBLSIZE);

    for (iz = min_iz; iz <= max_iz; iz++) {
        p = buf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                double *tmp = g_global->afunc.F_GetRadiation(
                    &g->node[iz][iy][ix], &nrlen);
                Memcpy(p, tmp, nrlen * DBLSIZE);
                free(tmp);
                p += nrlen;
            }
        }

        if (MPI_SUCCESS != MPI_Allreduce(buf, cbuf, g->nx * g->ny * nrlen,
                                         MPI_DOUBLE, MPI_SUM, zband_comm))
            CERR;

        p = cbuf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_SetRadiation(&g->node[iz][iy][ix], p);
                p += nrlen;
            }
        }
    }
    free(buf);
    free(cbuf);
}

/*******************************************************************************/
void pro_CollectDM(t_grid *g, int min_ese, int max_ese)
{
    int     nrdim, min_iz, max_iz, iz, iy, ix, n_ese;
    double *buf = NULL, *cbuf = NULL, *p;
    int *   displs, *recv_counts;

    stk_Add(0, POR_AT, "collecting density matrix from the processes");
    if (!G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized())
        IERR;
    if (G_MPI_M == 1)
        return;

    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    nrdim = g_global->afunc.F_GetNDM();
    n_ese = max_ese - min_ese + 1;

    buf  = (double *)Malloc(nrdim * g->nx * g->ny * DBLSIZE);
    cbuf = (double *)Malloc(nrdim * g->nx * g->ny * DBLSIZE);

    displs      = (int *)Malloc(G_MPI_M * INTSIZE);
    recv_counts = (int *)Malloc(G_MPI_M * INTSIZE);
    pro_ese_gv_displ_counts(g->nx, g->ny, nrdim, displs, recv_counts);

    for (iz = min_iz; iz <= max_iz; iz++) {
        p = buf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_GetDM(&g->node[iz][iy][ix], p);
                p += nrdim;
            }
        }

        // the number of grid points calculated by each process is not equal, so
        // we use mpi_allgatherv, calculating the right displacements and
        // receive counts with the function pro_ese_gv_displ_counts
        if (MPI_SUCCESS != MPI_Allgatherv(&buf[min_ese * nrdim], n_ese * nrdim,
                                          MPI_DOUBLE, cbuf, recv_counts, displs,
                                          MPI_DOUBLE, zband_comm))
            CERR;

        // in our chunk we don't really need to the the density matrix, as we
        // already hold that information, but for simplicity we leave it like
        // this
        p = cbuf;
        for (iy = 0; iy < g->ny; iy++) {
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_SetDM(&g->node[iz][iy][ix], p);
                p += nrdim;
            }
        }
    }

    free(buf);
    free(cbuf);
    free(displs);
    free(recv_counts);
}

/*******************************************************************************/
void pro_SynchronizeOverlappingDM(t_grid *g)
{
    int         min_iz, max_iz, idz, ndm, ix, iy;
    double *    buf_s1, *buf_s2, *buf_r, *p;
    MPI_Status  status;
    MPI_Request req1, req2;

    if (!G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized())
        IERR;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    idz    = pro_Domain_Z_ID(G_MY_MPI_RANK);
    ndm    = g_global->afunc.F_GetNDM();
    buf_s1 = ArrayNew(ndm * g->nx); /* synchronization by individual X-rows */
    buf_s2 = ArrayNew(ndm * g->nx);
    buf_r  = ArrayNew(ndm * g->nx);
    for (iy = 0; iy < g->ny; iy++) {
        /* send down */
        if (idz > 1) {
            p = buf_s1;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_GetDM(&g->node[min_iz + 1][iy][ix], p);
                p += ndm;
            }
            if (MPI_SUCCESS != MPI_Isend(buf_s1, ndm * g->nx, MPI_DOUBLE,
                                         G_MY_MPI_RANK - 1, 0, MPI_COMM_WORLD,
                                         &req1))
                CERR;
        }
        /* send up */
        if (idz < G_MPI_L) {
            p = buf_s2;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_GetDM(&g->node[max_iz - 1][iy][ix], p);
                p += ndm;
            }
            if (MPI_SUCCESS != MPI_Isend(buf_s2, ndm * g->nx, MPI_DOUBLE,
                                         G_MY_MPI_RANK + 1, 0, MPI_COMM_WORLD,
                                         &req2))
                CERR;
        }
        /* recv from below */
        if (idz > 1) {
            if (MPI_SUCCESS != MPI_Recv(buf_r, ndm * g->nx, MPI_DOUBLE,
                                        G_MY_MPI_RANK - 1, 0, MPI_COMM_WORLD,
                                        &status))
                CERR;
            p = buf_r;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_SetDM(&g->node[min_iz - 1][iy][ix], p);
                p += ndm;
            }
        }
        /* recv from above */
        if (idz < G_MPI_L) {
            if (MPI_SUCCESS != MPI_Recv(buf_r, ndm * g->nx, MPI_DOUBLE,
                                        G_MY_MPI_RANK + 1, 0, MPI_COMM_WORLD,
                                        &status))
                CERR;
            p = buf_r;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_SetDM(&g->node[max_iz + 1][iy][ix], p);
                p += ndm;
            }
        }
        /* sync */
        if (idz > 1)
            MPI_Wait(&req1, MPI_STATUS_IGNORE);
        if (idz < G_MPI_L)
            MPI_Wait(&req2, MPI_STATUS_IGNORE);
    }
    free(buf_s1);
    free(buf_s2);
    free(buf_r);
}

/*******************************************************************************/
void pro_Sync_DM(t_grid *g)
{
    int         min_iz, max_iz, idz, ndm, ix, iy;
    double *    buf_s1, *buf_s2, *buf_r, *p;
    MPI_Status  status;
    MPI_Request req1;

    if (!G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized())
        IERR;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    idz    = pro_Domain_Z_ID(G_MY_MPI_RANK);
    ndm    = g_global->afunc.F_GetNDM();
    buf_s1 = ArrayNew(ndm * g->nx); /* synchronization by individual X-rows */
    buf_s2 = ArrayNew(ndm * g->nx);
    buf_r  = ArrayNew(ndm * g->nx);
    for (iy = 0; iy < g->ny; iy++) {
        /* send down */
        if (idz > 1) {
            p = buf_s1;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_GetDM(&g->node[min_iz][iy][ix], p);
                p += ndm;
            }
            if (MPI_SUCCESS != MPI_Isend(buf_s1, ndm * g->nx, MPI_DOUBLE,
                                         G_MY_MPI_RANK - 1, 0, MPI_COMM_WORLD,
                                         &req1))
                CERR;
        }

        /* recv from above */
        if (idz < G_MPI_L) {
            if (MPI_SUCCESS != MPI_Recv(buf_r, ndm * g->nx, MPI_DOUBLE,
                                        G_MY_MPI_RANK + 1, 0, MPI_COMM_WORLD,
                                        &status))
                CERR;
            p = buf_r;
            for (ix = 0; ix < g->nx; ix++) {
                g_global->afunc.F_SetDM(&g->node[max_iz][iy][ix], p);
                p += ndm;
            }
        }
        /* sync */
        if (idz > 1)
            MPI_Wait(&req1, MPI_STATUS_IGNORE);
    }
    free(buf_s1);
    free(buf_s2);
    free(buf_r);
}

/*******************************************************************************/
void pro_SynchronizeOverlappingNodes(t_grid *g)
{
    int         min_iz, max_iz, idz, ndat, ix, iy;
    char *      buf_s1, *buf_s2, *buf_r, *p;
    MPI_Status  status;
    MPI_Request req1, req2;

    if (!G_MY_MPI_RANK)
        IERR;
    if (!PorMeshInitialized())
        IERR;
    pro_MinMax_IZ(G_MY_MPI_RANK, g->nz, &min_iz, &max_iz);
    idz    = pro_Domain_Z_ID(G_MY_MPI_RANK);
    ndat   = g_global->afunc.F_GetNodeDataSize();
    buf_s1 = Malloc(ndat * g->nx); /* synchronization by individual X-rows */
    buf_s2 = Malloc(ndat * g->nx); /* synchronization by individual X-rows */
    buf_r  = Malloc(ndat * g->nx);
    for (iy = 0; iy < g->ny; iy++) {
        /* send down */
        if (idz > 1) {
            p = buf_s1;
            for (ix = 0; ix < g->nx; ix++) {
                char *tp = (char *)g_global->afunc.F_EncodeNodeData(
                    g->node[min_iz + 1][iy][ix].p_data);
                Memcpy(p, tp, ndat);
                free(tp);
                p += ndat;
            }
            if (MPI_SUCCESS != MPI_Isend(buf_s1, ndat * g->nx, MPI_BYTE,
                                         G_MY_MPI_RANK - 1, 0, MPI_COMM_WORLD,
                                         &req1))
                CERR;
        }
        /* send up */
        if (idz < G_MPI_L) {
            p = buf_s2;
            for (ix = 0; ix < g->nx; ix++) {
                char *tp = (char *)g_global->afunc.F_EncodeNodeData(
                    g->node[max_iz - 1][iy][ix].p_data);
                Memcpy(p, tp, ndat);
                free(tp);
                p += ndat;
            }
            if (MPI_SUCCESS != MPI_Isend(buf_s2, ndat * g->nx, MPI_BYTE,
                                         G_MY_MPI_RANK + 1, 0, MPI_COMM_WORLD,
                                         &req2))
                CERR;
        }
        /* recv from below */
        if (idz > 1) {
            if (MPI_SUCCESS != MPI_Recv(buf_r, ndat * g->nx, MPI_BYTE,
                                        G_MY_MPI_RANK - 1, 0, MPI_COMM_WORLD,
                                        &status))
                CERR;
            p = buf_r;
            for (ix = 0; ix < g->nx; ix++) {
                /*
                g_global->afunc.F_Free_p_data(g->node[min_iz-1][iy][ix].p_data);
                g->node[min_iz-1][iy][ix].p_data =
                g_global->afunc.F_DuplicateNodeData((void*)p); p += ndat;
                */
                g_global->afunc.F_DecodeNodeData(p,
                                                 &g->node[min_iz - 1][iy][ix]);
                /*Memcpy(g->node[min_iz-1][iy][ix].p_data, p, ndat);*/
                p += ndat;
            }
        }
        /* recv from above */
        if (idz < G_MPI_L) {
            if (MPI_SUCCESS != MPI_Recv(buf_r, ndat * g->nx, MPI_BYTE,
                                        G_MY_MPI_RANK + 1, 0, MPI_COMM_WORLD,
                                        &status))
                CERR;
            p = buf_r;
            for (ix = 0; ix < g->nx; ix++) {
                /*
                g_global->afunc.F_Free_p_data(g->node[max_iz+1][iy][ix].p_data);
                g->node[max_iz+1][iy][ix].p_data =
                g_global->afunc.F_DuplicateNodeData((void*)p); p += ndat;
                */
                g_global->afunc.F_DecodeNodeData(p,
                                                 &g->node[max_iz + 1][iy][ix]);
                /*Memcpy(g->node[max_iz+1][iy][ix].p_data, p, ndat);*/
                p += ndat;
            }
        }
        /* sync */
        if (idz > 1)
            MPI_Wait(&req1, MPI_STATUS_IGNORE);
        if (idz < G_MPI_L)
            MPI_Wait(&req2, MPI_STATUS_IGNORE);
    }
    free(buf_s1);
    free(buf_s2);
    free(buf_r);
}

/*****************************************************************************/
/**
 * @brief Synchronize Maximum value for a given variable
 *
 *****************************************************************************/
double pro_SynchronizeMaxQuantity(double my_quantity)
{
    MPI_Status status;
    double     maxq;

    if (!G_MY_MPI_RANK)
        IERR;

    if (MPI_SUCCESS !=
        MPI_Allreduce(&my_quantity, &maxq, 1, MPI_DOUBLE, MPI_MAX, slaves_comm))
        CERR;

    return maxq;
}
