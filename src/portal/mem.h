#ifndef MEM_H_
#define MEM_H_

#include <stdlib.h>

extern void *Malloc(size_t size);

extern void *Realloc(void *ptr, size_t size);

extern void *Memcpy(void *dst, const void *src, size_t size);

#endif /* MEM_H_ */
