#include "tools.h"
#include "fs.h"
#include "global.h"
#include "grid.h"
#include "hdf5.h"
#include "hdf5_hl.h"
#include "modules.h"
#include "portal.h"
#include "process.h"
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

#define WERR                                                                   \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot write to the file");                    \
        fclose(f);                                                             \
        return 0;                                                              \
    }
#define WERR_H5                                                                \
    {                                                                          \
        Error(E_ERROR, POR_AT, "cannot write to the file");                    \
        H5Fclose(f_id);                                                        \
        return 0;                                                              \
    }

/* pointer function to either too_SurfaceMap or too_SurfaceMap_h5 */
int (*too_SurfaceMap_fp)(double theta_los, double chi_los, int ifreq_min,
                         int ifreq_max, char fname[], char comment[]);

int too_SurfaceMap(double theta_los, double chi_los, int ifreq_min,
                   int ifreq_max, char fname[], char comment[])
{
    FILE *   f;
    char     cmt[IO_PSP_COMMENT_LEN + 1];
    int      nwl = ifreq_max - ifreq_min + 1, i, nx, ny, ver = IO_PSP_VERSION;
    long int start_dat = 0;
    double * buf; /* buffer containing monochromatic radiation */

    if (G_MY_MPI_RANK)
        IERR;

    if (theta_los < 0.0 || theta_los > 90.0) {
        Error(E_ERROR, POR_AT,
              "only theta from 0 to 90 degrees currently possible");
        return 0;
    }
    if (!(f = fopen(fname, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open %s for writing", fname);
        return 0;
    }

    /* HEADER: */

    /* magic */
    if (fwrite(IO_PSP_MAGIC, 1, IO_PSP_MAGIC_LEN, f) != IO_PSP_MAGIC_LEN)
        WERR;
    start_dat += IO_PSP_MAGIC_LEN;

    /* version */
    if (!fwrite(&ver, sizeof(int), 1, f))
        WERR;
    start_dat += sizeof(int);

    /* comment */
    memset(cmt, 0, IO_PSP_COMMENT_LEN + 1);
    if (strlen(comment) > IO_PSP_COMMENT_LEN)
        memcpy(cmt, comment, IO_PSP_COMMENT_LEN);
    else
        memcpy(cmt, comment, strlen(comment));
    if (fwrite(cmt, 1, IO_PSP_COMMENT_LEN, f) != IO_PSP_COMMENT_LEN)
        WERR;
    start_dat += IO_PSP_COMMENT_LEN;

    /* line of sight angles */
    if (!fwrite(&theta_los, sizeof(double), 1, f))
        WERR;
    if (!fwrite(&chi_los, sizeof(double), 1, f))
        WERR;
    start_dat += 2 * sizeof(double);

    /* number of wavelengths */
    if (!fwrite(&nwl, sizeof(int), 1, f))
        WERR;
    start_dat += sizeof(int);

    /* array of wavelengths */
    for (i = ifreq_min; i <= ifreq_max; i++) {
        double wl = 1.0e8 * C_C / PorFreqs()[i];
        if (!fwrite(&wl, sizeof(double), 1, f))
            WERR;
    }
    start_dat += (ifreq_max - ifreq_min + 1) * sizeof(double);

    /* orientation of the atmospheric surface */
    i = 2 * ORI_XY + 1; /* max Z surface */
    if (!fwrite(&i, sizeof(int), 1, f))
        WERR;
    start_dat += sizeof(int);

    /* domain X and Y dimensions (in cm) */
    if (!fwrite(&g_global->domain_size.x, sizeof(double), 1, f))
        WERR;
    if (!fwrite(&g_global->domain_size.y, sizeof(double), 1, f))
        WERR;
    start_dat += 2 * sizeof(double);

    /* periodicity in X and Y axes */
    if (fwrite(g_global->period, 1, 2, f) != 2)
        WERR;
    start_dat += 2;

    /* number of axes ordinates and the arrays of ordinates */
    nx = g_global->grid[0].nx;
    ny = g_global->grid[0].ny;
    if (!fwrite(&nx, sizeof(int), 1, f))
        WERR;
    if (!fwrite(&ny, sizeof(int), 1, f))
        WERR;
    start_dat += 2 * sizeof(int);

    if (fwrite(g_global->grid[0].x, sizeof(double), nx, f) != nx)
        WERR;
    start_dat += nx * sizeof(double);

    if (fwrite(g_global->grid[0].y, sizeof(double), ny, f) != ny)
        WERR;
    start_dat += ny * sizeof(double);

    /* SPECTRA: get the spectra from the processes treating the required
     * frequencies */

    buf = ArrayNew(nx * ny * NSTOKES);

    for (int fr_i = ifreq_min; fr_i <= ifreq_max; fr_i++) {
        MPI_Status status;
        int        ix, iy, rfr;

        if (MPI_SUCCESS != MPI_Recv(buf, nx * ny * NSTOKES, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;

        rfr = status.MPI_TAG - ifreq_min;
        if (fseek(f, start_dat + (rfr * nx * ny * NSTOKES * FLTSIZE), SEEK_SET))
            WERR;

        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                float ii = 0, qq = 0, uu = 0, vv = 0;
                ii = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_I];
#ifdef STOKES_Q
                qq = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_Q];
#endif
#ifdef STOKES_U
                uu = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_U];
#endif
#ifdef STOKES_V
                vv = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_V];
#endif
                if (!fwrite(&ii, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&qq, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&uu, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&vv, FLTSIZE, 1, f))
                    WERR;
            }
        }
    }

    free(buf);
    return (!fclose(f));
}

int too_SurfaceMap_h5(double theta_los, double chi_los, int ifreq_min,
                      int ifreq_max, char fname[], char comment[])
{
    hid_t   f_id;
    hsize_t dims[3];
    herr_t  h_status;
    hid_t dataspace_id, memspace_id, dataset_I, dataset_Q, dataset_U, dataset_V;
    hsize_t count[3], offset[3], stride[3], block[3];

    int     nwl = ifreq_max - ifreq_min + 1, ver = IO_PSP_VERSION, i, nx, ny;
    double *buf;    /* buffer containing monochromatic radiation */
    float * buf_h5; /* buffer to keep all radiation for a frequency */
    double *wls;
    double  d2[2];
    int     i2[2];

    if (G_MY_MPI_RANK)
        IERR;

    if (theta_los < 0.0 || theta_los > 90.0) {
        Error(E_ERROR, POR_AT,
              "only theta from 0 to 90 degrees currently possible");
        return 0;
    }

    f_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    if (f_id < 0) {
        fprintf(stderr, "cannot open HDF5 file %s for writing\n", fname);
        return 0;
    }

    /* HEADER: */
    if ((h_status = H5LTset_attribute_string(f_id, "/", "IO_PSP_MAGIC",
                                             IO_PSP_MAGIC)) < 0)
        WERR_H5;
    if (H5LTset_attribute_int(f_id, "/", "VERSION", &ver, 1) < 0)
        WERR_H5;
    if (H5LTset_attribute_string(f_id, "/", "COMMENT", comment) < 0)
        WERR_H5;

    /* line of sight angles */
    if (H5LTset_attribute_double(f_id, "/", "LOS_THETA", &theta_los, 1) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "LOS_CHI", &chi_los, 1) < 0)
        WERR_H5;

    /* number of wavelengths */
    if (H5LTset_attribute_int(f_id, "/", "NUMBER_WAVELENGTHS", &nwl, 1) < 0)
        WERR_H5;

    /* array of wavelengths */
    wls = Malloc(nwl * sizeof(double));
    for (i = ifreq_min; i <= ifreq_max; i++)
        wls[i - ifreq_min] = 1.0e8 * C_C / PorFreqs()[i];
    if (H5LTset_attribute_double(f_id, "/", "WAVELENGTHS", wls, nwl) < 0)
        WERR_H5;
    free(wls);

    /* orientation of the atmospheric surface */
    i = 2 * ORI_XY + 1; /* max Z surface */
    if (H5LTset_attribute_int(f_id, "/", "ORIENTATION_ATMOSPHERIC_SURFACE", &i,
                              1) < 0)
        WERR_H5;

    /* domain X and Y dimensions (in cm) */
    d2[0] = g_global->domain_size.x;
    d2[1] = g_global->domain_size.y;
    if (H5LTset_attribute_double(f_id, "/", "DOMAIN_SIZE", d2, 2) < 0)
        WERR_H5;

    /* periodicity in X and Y axes */
    i2[0] = (int)g_global->period[0];
    i2[1] = (int)g_global->period[1];
    if (H5LTset_attribute_int(f_id, "/", "PERIODICITY", i2, 2) < 0)
        WERR_H5;

    /* number of axes ordinates and the arrays of ordinates */
    nx = i2[0] = g_global->grid[0].nx;
    ny = i2[1] = g_global->grid[0].ny;
    if (H5LTset_attribute_int(f_id, "/", "GRID_DIMENSIONS", i2, 2) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "X_AXIS", g_global->grid[0].x, nx) <
        0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "Y_AXIS", g_global->grid[0].x, ny) <
        0)
        WERR_H5;

    /* SPECTRA: get the spectra from the processes treating the required
     * frequencies */
    buf = ArrayNew(nx * ny * NSTOKES);

    dims[0]      = nwl;
    dims[1]      = ny;
    dims[2]      = nx;
    dataspace_id = H5Screate_simple(3, dims, NULL);

    dataset_I = H5Dcreate(f_id, "/stokes_I", H5T_NATIVE_FLOAT, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_Q = H5Dcreate(f_id, "/stokes_Q", H5T_NATIVE_FLOAT, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_U = H5Dcreate(f_id, "/stokes_U", H5T_NATIVE_FLOAT, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_V = H5Dcreate(f_id, "/stokes_V", H5T_NATIVE_FLOAT, dataspace_id,
                          H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    count[0]    = 1;
    count[1]    = ny;
    count[2]    = nx;
    memspace_id = H5Screate_simple(3, count, NULL);

    stride[0] = stride[1] = stride[2] = 1;
    block[0] = block[1] = block[2] = 1;

    /* To mimic the I/O in non-HDF5, we create the file always as if working
       on SMODE_FULL mode, but setting to 0.0 the values of the STOKES profiles
       which are not defined. We could just write datasets for the Stokes
       profiles that are defined, but for the moment we leave it like this, to
       avoid problems with post-processing scripts that relied on having the
       four Stokes profiles defined */
    buf_h5 = Malloc(nx * ny * T_NS * sizeof(float));

    for (int fr_i = ifreq_min; fr_i <= ifreq_max; fr_i++) {
        MPI_Status status;
        int        ix, iy, rfr;

        if (MPI_SUCCESS != MPI_Recv(buf, nx * ny * NSTOKES, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;
        rfr = status.MPI_TAG - ifreq_min;

        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                float ii = 0, qq = 0, uu = 0, vv = 0;
                ii = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_I];
#ifdef STOKES_Q
                qq = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_Q];
#endif
#ifdef STOKES_U
                uu = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_U];
#endif
#ifdef STOKES_V
                vv = (float)buf[(iy * nx + ix) * NSTOKES + STOKES_V];
#endif
                buf_h5[T_SI * ny * nx + iy * nx + ix] = ii;
                buf_h5[T_SQ * ny * nx + iy * nx + ix] = qq;
                buf_h5[T_SU * ny * nx + iy * nx + ix] = uu;
                buf_h5[T_SV * ny * nx + iy * nx + ix] = vv;
            }
        }

        offset[0] = rfr;
        offset[1] = 0;
        offset[2] = 0;
        h_status  = H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset,
                                       stride, count, block);
        h_status  = H5Dwrite(dataset_I, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, &buf_h5[T_SI * ny * nx]);
        h_status  = H5Dwrite(dataset_Q, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, &buf_h5[T_SQ * ny * nx]);
        h_status  = H5Dwrite(dataset_U, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, &buf_h5[T_SU * ny * nx]);
        h_status  = H5Dwrite(dataset_V, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, &buf_h5[T_SV * ny * nx]);
    }

    free(buf);
    free(buf_h5);
    h_status = H5Sclose(memspace_id);
    h_status = H5Sclose(dataspace_id);
    h_status = H5Dclose(dataset_I);
    h_status = H5Dclose(dataset_Q);
    h_status = H5Dclose(dataset_U);
    h_status = H5Dclose(dataset_V);

    if (H5Fclose(f_id) < 0) {
        return 0;
    }
    return 1;
}

int too_CLVFreq(double chi, double mu_min, double mu_max, int N_ray, int id_fr,
                char fname[])
{
    FILE *  f;
    double *buf =
        NULL; /* buffer containing all the Stokes data at the surface */
    int        i, nx, ny, ix, iy, idir, ridir;
    double     mu, ii, qq, uu, vv;
    MPI_Status status;
    double *   results;

    if (G_MY_MPI_RANK)
        IERR;

    if (!(f = fopen(fname, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open %s for writing", fname);
        return 0;
    }

    results = Malloc(N_ray * 5 *
                     sizeof(double)); /* [mu,ii,qq,uu,vv] for each direction */

    nx  = g_global->grid[0].nx;
    ny  = g_global->grid[0].ny;
    buf = ArrayNew(nx * ny * NSTOKES);

    for (idir = 0; idir < N_ray; idir++) {
        if (MPI_SUCCESS != MPI_Recv(buf, nx * ny * NSTOKES, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;
        ridir = status.MPI_TAG;

        if (N_ray > 1)
            mu = mu_min + (mu_max - mu_min) * ridir / (N_ray - 1);
        else
            mu = mu_min;

        /* spatial average: */
        ii = qq = uu = vv = 0.0;
        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                ii += buf[(iy * nx + ix) * NSTOKES + STOKES_I];
#ifdef STOKES_Q
                qq += buf[(iy * nx + ix) * NSTOKES + STOKES_Q];
#endif
#ifdef STOKES_U
                uu += buf[(iy * nx + ix) * NSTOKES + STOKES_U];
#endif
#ifdef STOKES_V
                vv += buf[(iy * nx + ix) * NSTOKES + STOKES_V];
#endif
            }
        }
        ii /= nx * ny;
        qq /= nx * ny;
        uu /= nx * ny;
        vv /= nx * ny;

        results[ridir * 5 + 0] = mu;
        results[ridir * 5 + 1] = ii;
        results[ridir * 5 + 2] = qq;
        results[ridir * 5 + 3] = uu;
        results[ridir * 5 + 4] = vv;
    }

    /* Write to file */
    for (idir = 0; idir < N_ray; idir++) {
        mu = results[idir * 5 + 0];
        ii = results[idir * 5 + 1];
        qq = results[idir * 5 + 2];
        uu = results[idir * 5 + 3];
        vv = results[idir * 5 + 4];

        fprintf(f, "%12.12e\t%12.12e\t%12.12e\t%12.12e\t%12.12e\n", mu, ii,
                -100.0 * qq / (ii != 0 ? ii : 1),
                -100.0 * uu / (ii != 0 ? ii : 1),
                100.0 * vv / (ii != 0 ? ii : 1));
    }

    free(buf);
    free(results);
    return (!fclose(f));
}

/*******************************************************************************/

int too_CLVFreqAV(double mu_min, double mu_max, int N_ray, int N_azim,
                  int id_fr, char fname[])
{
    FILE *  f;
    double *buf =
        NULL; /* buffer containing all the Stokes data at the surface */
    int        i, nx, ny, ix, iy;
    double     mu, ii, qq, uu, vv;
    MPI_Status status;
    double *   results;

    if (G_MY_MPI_RANK)
        IERR;

    if (!(f = fopen(fname, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open %s for writing", fname);
        return 0;
    }

    results = Malloc(N_ray * N_azim * 5 *
                     sizeof(double)); /* [mu,ii,qq,uu,vv] for each direction */
    for (int i = 0; i < N_ray * N_azim * 5; i++)
        results[i] = 0;

    nx  = g_global->grid[0].nx;
    ny  = g_global->grid[0].ny;
    buf = ArrayNew(nx * ny * NSTOKES);

    for (int i = 0; i < N_ray * N_azim; i++) {
        int i_inc, i_az, r_i;

        if (MPI_SUCCESS != MPI_Recv(buf, nx * ny * NSTOKES, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;
        r_i = status.MPI_TAG;

        i_inc = r_i / N_azim;
        i_az  = r_i % N_azim;

        if (N_ray > 1)
            mu = mu_min + (mu_max - mu_min) * i_inc / (N_ray - 1);
        else
            mu = mu_min;

        ii = qq = uu = vv = 0.0;

        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                ii += buf[(iy * nx + ix) * NSTOKES + STOKES_I];
#ifdef STOKES_Q
                qq += buf[(iy * nx + ix) * NSTOKES + STOKES_Q];
#endif
#ifdef STOKES_U
                uu += buf[(iy * nx + ix) * NSTOKES + STOKES_U];
#endif
#ifdef STOKES_V
                vv += buf[(iy * nx + ix) * NSTOKES + STOKES_V];
#endif
            }
        }

        results[i_inc * N_azim * 5 + i_az * 5 + 0] = mu;
        results[i_inc * N_azim * 5 + i_az * 5 + 1] += ii;
        results[i_inc * N_azim * 5 + i_az * 5 + 2] += qq;
        results[i_inc * N_azim * 5 + i_az * 5 + 3] += uu;
        results[i_inc * N_azim * 5 + i_az * 5 + 4] += vv;
    }

    /* Write to file */
    for (int i = 0; i < N_ray; i++) {
        mu = results[i * N_azim * 5];

        ii = qq = uu = vv = 0;

        for (int j = 0; j < N_azim; j++) {
            ii += results[i * N_azim * 5 + j * 5 + 1];
            qq += results[i * N_azim * 5 + j * 5 + 2];
            uu += results[i * N_azim * 5 + j * 5 + 3];
            vv += results[i * N_azim * 5 + j * 5 + 4];
        }

        ii /= nx * ny * N_azim;
        qq /= nx * ny * N_azim;
        uu /= nx * ny * N_azim;
        vv /= nx * ny * N_azim;

        fprintf(f, "%12.12e\t%12.12e\t%12.12e\t%12.12e\t%12.12e\n", mu, ii,
                -100.0 * qq / (ii != 0 ? ii : 1),
                -100.0 * uu / (ii != 0 ? ii : 1),
                100.0 * vv / (ii != 0 ? ii : 1));
    }

    free(buf);
    free(results);
    return (!fclose(f));
}

/*******************************************************************************/

int too_FSAZAV(double theta_los, int N_azim, int ifreq_min, int ifreq_max,
               char fname[])
{
    FILE *     f;
    int        nwl = ifreq_max - ifreq_min + 1, i, j, nx, ny, ix, iy;
    double *   buf; /* buffer containing monochromatic radiation */
    double     stokes[4][MAXNFREQ];
    MPI_Status status;

    if (G_MY_MPI_RANK)
        IERR;

    if (theta_los < 0.0 || theta_los > 90.0) {
        Error(E_ERROR, POR_AT,
              "only theta from 0 to 90 degrees currently possible");
        return 0;
    }
    if (!(f = fopen(fname, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open %s for writing", fname);
        return 0;
    }

    for (j = 0; j < 4; j++)
        for (i = 0; i < nwl; i++)
            stokes[j][i] = 0.0;

    nx  = g_global->grid[0].nx;
    ny  = g_global->grid[0].ny;
    buf = ArrayNew(nx * ny * NSTOKES);

    for (int i = 0; i < nwl * N_azim; i++) {
        int ifreq, i_az, r_i;

        if (MPI_SUCCESS != MPI_Recv(buf, nx * ny * NSTOKES, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;
        r_i = status.MPI_TAG;

        i_az  = r_i / nwl;
        ifreq = (r_i % nwl);

        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                stokes[0][ifreq] += buf[(iy * nx + ix) * NSTOKES + STOKES_I];
#ifdef STOKES_Q
                stokes[1][ifreq] += buf[(iy * nx + ix) * NSTOKES + STOKES_Q];
#endif
#ifdef STOKES_U
                stokes[2][ifreq] += buf[(iy * nx + ix) * NSTOKES + STOKES_U];
#endif
#ifdef STOKES_V
                stokes[3][ifreq] += buf[(iy * nx + ix) * NSTOKES + STOKES_V];
#endif
            }
        }
    }

    for (j = 0; j < 4; j++)
        for (i = 0; i < nwl; i++)
            stokes[j][i] /= N_azim * nx * ny;

    for (i = ifreq_max; i >= ifreq_min; i--) {
        fprintf(f, "%12.12e\t%12.12e\t%12.12e\t%12.12e\t%12.12e\n",
                C_C * 1e8 / PorFreqs()[i], stokes[0][i - ifreq_min],
                stokes[1][i - ifreq_min], stokes[2][i - ifreq_min],
                stokes[3][i - ifreq_min]);
    }

    free(buf);
    return (!fclose(f));
}

/*******************************************************************************/

/* pointer function to either too_SurfaceTau or too_SurfaceTau_h5 */
int (*too_SurfaceTau_fp)(double tau, double theta_los, double chi_los,
                         int ifreq_min, int ifreq_max, char fname[],
                         char comment[]);

int too_SurfaceTau(double tau, double theta_los, double chi_los, int ifreq_min,
                   int ifreq_max, char fname[], char comment[])
{
    FILE *   f;
    char     cmt[IO_TAU_COMMENT_LEN + 1];
    int      nwl = ifreq_max - ifreq_min + 1, fr, nx, ny, ver = IO_TAU_VERSION;
    long int start_dat = 0;
    double * tau_xyz_buf, *tau_buf, *xyz_buf;

    if (G_MY_MPI_RANK)
        IERR;

    if (theta_los < 0.0 || theta_los > 90.0) {
        Error(E_ERROR, POR_AT,
              "only theta from 0 to 90 degrees currently possible");
        return 0;
    }

    if (!(f = fopen(fname, "wb"))) {
        Error(E_ERROR, POR_AT, "cannot open %s for writing", fname);
        return 0;
    }

    /* HEADER: */

    /* magic */
    if (fwrite(IO_TAU_MAGIC, 1, IO_TAU_MAGIC_LEN, f) != IO_TAU_MAGIC_LEN)
        WERR;
    start_dat += IO_TAU_MAGIC_LEN;

    /* version */
    if (!fwrite(&ver, sizeof(int), 1, f))
        WERR;
    start_dat += sizeof(int);

    /* comment */
    memset(cmt, 0, IO_TAU_COMMENT_LEN + 1);
    if (strlen(comment) > IO_TAU_COMMENT_LEN)
        memcpy(cmt, comment, IO_PSP_COMMENT_LEN);
    else
        memcpy(cmt, cmt, strlen(comment));
    if (fwrite(cmt, 1, IO_TAU_COMMENT_LEN, f) != IO_TAU_COMMENT_LEN)
        WERR;
    start_dat += IO_TAU_COMMENT_LEN;

    /* line of sight angles */
    if (!fwrite(&theta_los, sizeof(double), 1, f))
        WERR;
    if (!fwrite(&chi_los, sizeof(double), 1, f))
        WERR;
    start_dat += 2 * sizeof(double);

    /* number of wavelengths */
    if (!fwrite(&nwl, sizeof(int), 1, f))
        WERR;
    start_dat += sizeof(int);

    /* array of wavelengths */
    for (fr = ifreq_min; fr <= ifreq_max; fr++) {
        double wl = 1.0e8 * C_C / PorFreqs()[fr];
        if (!fwrite(&wl, sizeof(double), 1, f))
            WERR;
    }
    start_dat += (ifreq_max - ifreq_min + 1) * sizeof(double);

    /* domain X and Y dimensions (in cm) */
    if (!fwrite(&g_global->domain_size.x, sizeof(double), 1, f))
        WERR;
    if (!fwrite(&g_global->domain_size.y, sizeof(double), 1, f))
        WERR;
    start_dat += 2 * sizeof(double);

    /* periodicity in X and Y axes */
    if (fwrite(g_global->period, 1, 2, f) != 2)
        WERR;
    start_dat += 2;

    /* number of axes ordinates and the arrays of ordinates */
    nx = g_global->grid[0].nx;
    ny = g_global->grid[0].ny;
    if (!fwrite(&nx, sizeof(int), 1, f))
        WERR;
    if (!fwrite(&ny, sizeof(int), 1, f))
        WERR;
    start_dat += 2 * sizeof(int);

    if (fwrite(g_global->grid[0].x, sizeof(double), nx, f) != nx)
        WERR;
    start_dat += nx * sizeof(double);

    if (fwrite(g_global->grid[0].y, sizeof(double), ny, f) != ny)
        WERR;
    start_dat += ny * sizeof(double);

    /* go through all [iy][ix] point and calculate the tau's point coordinates
     */

    /*
        Algorithm:
        (1) For each surface node [iy][ix]:
            (a) go through grid topology calculating the intersection of the ray
       inside each z-domain (b) calculate opacities at the intersections and
       check if tau>=desired_tau at each step (c) send the tau_map and last
       intersection to the next z-domain (2) If all the [iy][ix] points have the
       desired tau, stop and send tau_map to the master process
    */

    // Receiving the data from the process who send them to the master process
    // and write them to the file

    tau_xyz_buf = ArrayNew(nx * ny * 4);
    tau_buf =
        &tau_xyz_buf[0]; // tau values stored in the first part of tau_xyz_buf
    xyz_buf = &tau_xyz_buf[nx * ny]; // xyz values stored as x,y,z triplets
                                     // after tau values

    for (fr = ifreq_min; fr <= ifreq_max; fr++) {
        MPI_Status status;
        int        ix, iy, rfr;

        if (MPI_SUCCESS != MPI_Recv(tau_xyz_buf, nx * ny * 4, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;

        rfr = status.MPI_TAG - ifreq_min;

        if (fseek(f, start_dat + (rfr * nx * ny * 4 * FLTSIZE), SEEK_SET))
            WERR;

        /* printf("I'm 0 and I'm receiving data of ifreq=%d from a process\n",
         * fr);
         */
        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                float xx, yy, zz, tau;
                tau = (float)tau_buf[iy * nx + ix];
                xx  = (float)xyz_buf[iy * nx * 3 + ix * 3 + 0];
                yy  = (float)xyz_buf[iy * nx * 3 + ix * 3 + 1];
                zz  = (float)xyz_buf[iy * nx * 3 + ix * 3 + 2];

                if (!fwrite(&tau, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&xx, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&yy, FLTSIZE, 1, f))
                    WERR;
                if (!fwrite(&zz, FLTSIZE, 1, f))
                    WERR;
            }
        }
    }
    free(tau_xyz_buf);
    return (!fclose(f));
}

int too_SurfaceTau_h5(double tau, double theta_los, double chi_los,
                      int ifreq_min, int ifreq_max, char fname[],
                      char comment[])
{
    hid_t   f_id;
    hsize_t dims[3];
    herr_t  h_status;
    hid_t   dataspace_id, memspace_id, dataset_tau, dataset_xx, dataset_yy,
        dataset_zz;
    hsize_t count[3], offset[3], stride[3], block[3];

    int nwl = ifreq_max - ifreq_min + 1, ver = IO_TAU_VERSION, fr, i, nx, ny;
    double *wls;
    double  d2[2];
    int     i2[2];

    // For efficiency, we allocate tau_xyz_buf to contain tau and xyz values in
    // a "4D" array, and tau_buf and xyz_buf will point inside it.
    double *tau_xyz_buf, *tau_buf, *xyz_buf;
    float * tau_buf_h5;
    float * xx_buf_h5;
    float * yy_buf_h5;
    float * zz_buf_h5;

    if (G_MY_MPI_RANK)
        IERR;

    if (theta_los < 0.0 || theta_los > 90.0) {
        Error(E_ERROR, POR_AT,
              "only theta from 0 to 90 degrees currently possible");
        return 0;
    }

    f_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
    if (f_id < 0) {
        fprintf(stderr, "cannot open HDF5 file %s for writing\n", fname);
        return 0;
    }

    /* HEADER: */
    if ((h_status = H5LTset_attribute_string(f_id, "/", "IO_TAU_MAGIC",
                                             IO_TAU_MAGIC)) < 0)
        WERR_H5;
    if (H5LTset_attribute_int(f_id, "/", "VERSION", &ver, 1) < 0)
        WERR_H5;
    if (H5LTset_attribute_string(f_id, "/", "COMMENT", comment) < 0)
        WERR_H5;

    /* line of sight angles */
    if (H5LTset_attribute_double(f_id, "/", "LOS_THETA", &theta_los, 1) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "LOS_CHI", &chi_los, 1) < 0)
        WERR_H5;

    /* number of wavelengths */
    if (H5LTset_attribute_int(f_id, "/", "NUMBER_WAVELENGTHS", &nwl, 1) < 0)
        WERR_H5;

    /* array of wavelengths */
    wls = Malloc(nwl * sizeof(double));
    for (i = ifreq_min; i <= ifreq_max; i++)
        wls[i - ifreq_min] = 1.0e8 * C_C / PorFreqs()[i];
    if (H5LTset_attribute_double(f_id, "/", "WAVELENGTHS", wls, nwl) < 0)
        WERR_H5;
    free(wls);

    /* domain X and Y dimensions (in cm) */
    d2[0] = g_global->domain_size.x;
    d2[1] = g_global->domain_size.y;
    if (H5LTset_attribute_double(f_id, "/", "DOMAIN_SIZE", d2, 2) < 0)
        WERR_H5;

    /* periodicity in X and Y axes */
    i2[0] = (int)g_global->period[0];
    i2[1] = (int)g_global->period[1];
    if (H5LTset_attribute_int(f_id, "/", "PERIODICITY", i2, 2) < 0)
        WERR_H5;

    /* number of axes ordinates and the arrays of ordinates */
    nx = i2[0] = g_global->grid[0].nx;
    ny = i2[1] = g_global->grid[0].ny;
    if (H5LTset_attribute_int(f_id, "/", "GRID_DIMENSIONS", i2, 2) < 0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "X_AXIS", g_global->grid[0].x, nx) <
        0)
        WERR_H5;
    if (H5LTset_attribute_double(f_id, "/", "Y_AXIS", g_global->grid[0].y, ny) <
        0)
        WERR_H5;

    /* go through all [iy][ix] point and calculate the tau's point coordinates
     */

    /*
      Algorithm:
      (1) For each surface node [iy][ix]:
      (a) go through grid topology calculating the intersection of the ray
      inside each z-domain (b) calculate opacities at the intersections and
      check if tau>=desired_tau at each step (c) send the tau_map and last
      intersection to the next z-domain (2) If all the [iy][ix] points have the
      desired tau, stop and send tau_map to the master process
    */

    // Receiving the data from the process who send them to the master process
    // and write them to the file

    tau_xyz_buf = ArrayNew(nx * ny * 4);
    tau_buf =
        &tau_xyz_buf[0]; // tau values stored in the first part of tau_xyz_buf
    xyz_buf = &tau_xyz_buf[nx * ny]; // xyz values stored as x,y,z triplets
                                     // after tau values

    dims[0]      = nwl;
    dims[1]      = ny;
    dims[2]      = nx;
    dataspace_id = H5Screate_simple(3, dims, NULL);

    dataset_tau = H5Dcreate(f_id, "/tau", H5T_NATIVE_FLOAT, dataspace_id,
                            H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_xx  = H5Dcreate(f_id, "/xx", H5T_NATIVE_FLOAT, dataspace_id,
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_yy  = H5Dcreate(f_id, "/yy", H5T_NATIVE_FLOAT, dataspace_id,
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    dataset_zz  = H5Dcreate(f_id, "/zz", H5T_NATIVE_FLOAT, dataspace_id,
                           H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

    count[0]    = 1;
    count[1]    = ny;
    count[2]    = nx;
    memspace_id = H5Screate_simple(3, count, NULL);

    stride[0] = stride[1] = stride[2] = 1;
    block[0] = block[1] = block[2] = 1;

    tau_buf_h5 = Malloc(nx * ny * sizeof(float));
    xx_buf_h5  = Malloc(nx * ny * sizeof(float));
    yy_buf_h5  = Malloc(nx * ny * sizeof(float));
    zz_buf_h5  = Malloc(nx * ny * sizeof(float));

    for (fr = ifreq_min; fr <= ifreq_max; fr++) {
        MPI_Status status;
        int        ix, iy, rfr;

        if (MPI_SUCCESS != MPI_Recv(tau_xyz_buf, nx * ny * 4, MPI_DOUBLE,
                                    MPI_ANY_SOURCE, MPI_ANY_TAG, MPI_COMM_WORLD,
                                    &status))
            CERR;
        rfr = status.MPI_TAG - ifreq_min;

        /* printf("I'm 0 and I'm receiving data of ifreq=%d from a process\n",
         * fr);
         */
        for (iy = 0; iy < ny; iy++) {
            for (ix = 0; ix < nx; ix++) {
                float xx, yy, zz, tau;

                tau_buf_h5[iy * nx + ix] = (float)tau_buf[iy * nx + ix];
                xx_buf_h5[iy * nx + ix] =
                    (float)xyz_buf[iy * nx * 3 + ix * 3 + 0];
                yy_buf_h5[iy * nx + ix] =
                    (float)xyz_buf[iy * nx * 3 + ix * 3 + 1];
                zz_buf_h5[iy * nx + ix] =
                    (float)xyz_buf[iy * nx * 3 + ix * 3 + 2];
            }
        }

        offset[0] = rfr;
        offset[1] = 0;
        offset[2] = 0;
        h_status  = H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset,
                                       stride, count, block);
        h_status  = H5Dwrite(dataset_tau, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, tau_buf_h5);
        h_status  = H5Dwrite(dataset_xx, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, xx_buf_h5);
        h_status  = H5Dwrite(dataset_yy, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, yy_buf_h5);
        h_status  = H5Dwrite(dataset_zz, H5T_NATIVE_FLOAT, memspace_id,
                            dataspace_id, H5P_DEFAULT, zz_buf_h5);
    }

    free(tau_xyz_buf);
    free(tau_buf_h5);
    free(xx_buf_h5);
    free(yy_buf_h5);
    free(zz_buf_h5);

    h_status = H5Sclose(memspace_id);
    h_status = H5Sclose(dataspace_id);
    h_status = H5Dclose(dataset_tau);
    h_status = H5Dclose(dataset_xx);
    h_status = H5Dclose(dataset_yy);
    h_status = H5Dclose(dataset_zz);

    if (H5Fclose(f_id) < 0) {
        return 0;
    }
    return 1;
}

#undef WERR
#undef WERR_H5
