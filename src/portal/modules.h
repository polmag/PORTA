/* management of the external loadable modules */

#ifndef MODULES_H_
#define MODULES_H_

#include "def.h"

/* returns 1 if a module is loaded, 0 otherwise */
extern int mod_IsModuleLoaded(void);

/* get name of the loaded module (this array must be freed by the caller) */
extern char *mod_GetModuleName(void);

/* load a module at a given file system location and initializes it via module's
 * (int)Init(t_afunc *p_afunc) function (modules.c) if f != NULL then the module
 * must load the header data from the file pointer; if f==NULL then a default
 * module initialization is to be performed output: node_pmd_size: size in bytes
 * of one grid node data in the PMD file
 */
extern int mod_LoadModule(const char *m_name, FILE *file, int *node_pmd_size);

/* load a module at a given file system location and initializes it via module's
 * (int)Init(t_afunc *p_afunc) function (modules.c) - HDF5 version
 */
extern int mod_LoadModule_h5(const char *m_name, hid_t file_id,
                             int *node_pmd_size);

/* runs the CloseModule() function of the currently loaded module unlinks it */
/* don't forget to free the atomic memory in all nodes using
 * module.F_Free_p_data() before calling this function */
extern int mod_UnlinkModule(void);

#endif /* MODULES_H_ */
