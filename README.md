PORTA
=====

PORTA is a modular computer program for solving three-dimensional (3D),
non-equilibrium radiative transfer problems with massively parallel computers
(see [Štěpán, J. & Trujillo Bueno, J. 2013, Astronomy & Astrophysics, 557,
143](https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract)). The
present public version can be applied for modeling the spectral line
polarization produced by the scattering of anisotropic radiation and the Hanle
and Zeeman effects assuming complete frequency redistribution, either using
two-level or multilevel atomic models. The numerical method of solution used to
find the self-consistent values of the atomic density matrix at each point of
the model’s Cartesian grid is based on Jacobi iterative scheme and on a
short-characteristics formal solver of the Stokes-vector transfer equation that
uses monotonic Bézier interpolation. The code can also be applied for computing
the linear polarization of the continuum radiation caused by Rayleigh and
Thomson scattering in 3D models of stellar atmospheres. It can also be applied
to solve the simpler 3D radiative transfer problem of unpolarized radiation in
multilevel systems. The present public version of PORTA comes with the HDF5
input/output, an advanced graphical user interface, and is released within the
framework of the POLMAG Advanced Grant project funded by the European Research
Council (see http://research.iac.es/proyecto/polmag/).

The documentation is available at https://polmag.gitlab.io/PORTA

