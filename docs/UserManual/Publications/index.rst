.. _publications:

Publications
==============

- `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143 <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_
- `Štěpán, J., and Heinzel, P. 2013, ApJL, 778, L6 <https://ui.adsabs.harvard.edu/abs/2013ApJ...778L...6S/abstract>`_
- `Štěpán, J. 2014, ASPC, 489, 243 <https://ui.adsabs.harvard.edu/abs/2014ASPC..489..243S/abstract>`_
- `Štěpán, J., Trujillo Bueno, J., Leenaarts, J., and Carlsson, M. 2015, ApJ, 803, 65 <https://ui.adsabs.harvard.edu/abs/2015ApJ...803...65S/abstract>`_
- `Tichý, A., Štěpán, J., Trujillo Bueno, J., and Kubát, J. 2015, IAUS, 305, 401 <https://ui.adsabs.harvard.edu/abs/2015IAUS..305..401T/abstract>`_
- `Štěpán, J. 2015, IAUS, 305, 360 <https://ui.adsabs.harvard.edu/abs/2015IAUS..305..360S/abstract>`_
- `Štěpán, J., and Trujillo Bueno, J. 2016, ApJL, 826, L10 <https://ui.adsabs.harvard.edu/abs/2016ApJ...826L..10S/abstract>`_
- `Trujillo Bueno, J., Landi Degl'Innocenti, E. and Belluzzi, L. 2017 SSRv, 210, 183 <https://ui.adsabs.harvard.edu/abs/2017SSRv..210..183T/abstract>`_
- `del Pino Alemán, T., Trujillo Bueno, J., Štěpán, J., and Shchukina, N. 2018, ApJ, 863, 164 <https://ui.adsabs.harvard.edu/abs/2018ApJ...863..164D/abstract>`_
- `Trujillo Bueno, J., et al. 2018, ApJL, 866, L15 <https://ui.adsabs.harvard.edu/abs/2018ApJ...866L..15T/abstract>`_
- `Jurčák, J., Štěpán, J., Trujillo Bueno, J., and Bianda, M. 2018, A&A, 619, A60 <https://ui.adsabs.harvard.edu/abs/2018A%26A...619A..60J/abstract>`_
- `Tichý, A., and Kubát, J. 2019, JQSRT, 225, 249 <https://ui.adsabs.harvard.edu/abs/2019JQSRT.225..249T/abstract>`_
