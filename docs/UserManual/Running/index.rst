.. _running_porta:

Running the code
================

Running PORTA (after a successful :ref:`installation_porta`) is very simple: we
simply run a command like the following one in the directory where the *porta*
executable is located:

.. code-block:: bash

   mpirun -n (1+L*M) ./porta (L) test.por

+ where *L* represents the number of vertical blocks we want to divide the
  atmosphere in, and *M* is the number of frequency bands we would like to divide
  the frequencies into. This is discussed in more detail in section
  `Parallelization in PORTA`_

+ *test.por* is a text file where we specify the different commands we want
  PORTA to run, such as loading an atmospheric model, applying a number of Jacobi
  iterations, etc. A description of all the possible PORTA commands and their
  syntax is given in section `PORTA commands`_

.. note::    
   If running PORTA in a sumpercomputer (like *LaPalma* or *PizDaint*), you will
   most likely have to prepare a submission script to prepare the environment
   and submit the job to the queueing system. For further details, get in touch
   with the technical support team of the supercomputer.
   
  
.. _parallelization_porta:
  
Parallelization in PORTA
-------------------------

As already explained in section :ref:`porta_intro`, PORTA solves the non-LTE
radiative transfer equations in a rectilinear 3D Cartesian grid, and it can be
parallelized by:

+ dividing the spatial domain in a number of blocks (L) in the vertical dimension,
+ dividing the number of radiation frequencies in a number of frequency bands
  (M).

Given these divisions, the number of *slave* processes that PORTA will use is
:math:`L*M`. PORTA also runs a *master* process and, therefore, the total number
of processes that PORTA will require is :math:`1 + L*M`. (*For details about the
subdivision of work amongst the processes, see* Štěpán, J., and Trujillo
Bueno, J. 2013 [1]_).

The parallelization in the spatial domain can be seen in the following image,
where a 3D model with a grid of 10 points in the vertical dimension (*Z*) has
been divided in three blocks. 
  
.. figure:: ../_static/img/paral.png
   :width: 40%

   PORTA spatial domain parallelization
	   
At the same time, the radiation frequencies can be divided in a number of
frequency bands. Thus, the computations required for a spatial domain block can
be further subdivided and performed by a number of different processes, each
taking care of a disjoint set of frequencies.

Sample executions
.................

For example, in the provided test for the *twolevel* module (see section
:ref:`twoleveltest`), the atmosphere is given by a 3x3x70 grid and the default
number of frequencies is 101. Thus, several example possibilities for running
this test with PORTA could be:

+ Run the test with only 1 *slave* process (i.e., no parallelization in the
  spatial domain nor the radiation frequencies)
  
.. code-block:: bash

   mpirun -n 2 ./porta 1 test.por

+ Parallelize only in the spatial dimension (i.e., each process will be in
  charge of the computations for all frequencies in its given domain block). In
  the following example, we divide the domain in 7 blocks:

.. code-block:: bash

   mpirun -n 8 ./porta 7 test.por

+ Parallelize in both the spatial dimension and the frequencies. If keeping the
  7 vertical blocks as above, but also subdividing the frequencies in 10 bands,
  then we would run PORTA with 70 *slave* processes:

.. code-block:: bash

   mpirun -n 71 ./porta 7 test.por

.. todo::
   Run some basic benchmarks on possible divisions and give here? some hints on
   the best way to parallelize PORTA

.. _porta_commands:   

PORTA commands
--------------

In the examples above, the file *test.por* is a text file where we specify the
different commands that we want PORTA to run. A sample commands file could be
something like:

.. code-block:: none

   # Sample PORTA commands file
   #
   loadmodel -f pmd_files/twolevel.h5
   solve_jacobi -i 10 -c 1.0e-4
   savemodel -f pmd_files/twolevel_iter10.h5

   loadmodel -f pmd_files/twolevel_iter10.h5
   fs_surface -c sc -i 0 -a 0 -m 0 -M 100 -f psp_files/twolevel_iter10.h5
   

.. note::
   Lines starting with the character *#* are treated as comments and ignored by
   PORTA.

   
The complete list of available PORTA commands is given in the table below and
further details for each command are given in the ensuing sections. Details
about the PORTA file formats are available in section :ref:`PORTA_file_formats`.


.. tabularcolumns:: |l|p{13cm}|

+-----------------------+-------------------------------------------------------------+
| Name                  | Description                                                 |
+=======================+=============================================================+
| `help`_               | Prints the list of the available commands and variables.    | 
|                       | Followed by one of the command names below, it provides     |
|                       | detailed information about that command                     |
+-----------------------+-------------------------------------------------------------+
| `version`_            | Prints the PORTA version information                        |
+-----------------------+-------------------------------------------------------------+
| `echo`_               | Prints ('echoes') some arbitrary text                       | 
+-----------------------+-------------------------------------------------------------+
| `exit`_               | Stops processing the commands script                        |
+-----------------------+-------------------------------------------------------------+
| `get`_                | Prints the value of some variables (number of angles,       |
|                       | wavelengths, etc.)                                          | 
+-----------------------+-------------------------------------------------------------+
| `set`_                | Sets the value of some variables (number of angles, etc.)   | 
+-----------------------+-------------------------------------------------------------+
| `loadmodel`_          | Loads a model from a file                                   |
+-----------------------+-------------------------------------------------------------+
| `savemodel`_          | Saves the curent model into a file                          |
+-----------------------+-------------------------------------------------------------+
| `solve_jacobi`_       | Solves the non-LTE problem using Jacobi iteration           |
+-----------------------+-------------------------------------------------------------+
| `fs_surface`_         | Performs the formal solution along a general ray            |
|                       | direction for all/some frequencies and stores the emergent  |
|                       | radiation in a file.                                        |
|                       | It uses the *short characteristics* method.                 |
+-----------------------+-------------------------------------------------------------+
| `fs_clv_freq`_        | Performs the formal solution for the calculation            |
|                       | of the center-to-limb variation at a given discrete         |
|                       | frequency of radiation, spatially averaged.                 |
+-----------------------+-------------------------------------------------------------+
| `fs_clv_freq_av`_     | Performs the formal solution for the calculation            |
|                       | of the center-to-limb variation at a given discrete         |
|                       | frequency of radiation, spatially and azimuth averaged.     | 
+-----------------------+-------------------------------------------------------------+
| `fs_azaver`_          | Performs the formal solution, averaged over all grid        |
|                       | points and all azimuths. It results in a single Stokes      |
|                       | profile                                                     |
+-----------------------+-------------------------------------------------------------+
| `get_tau_pos`_        | For a general ray direction calculates geometrical          |
|                       | coordinates of points with a given tau distance from the    | 
|                       | surface nodes.                                              |
+-----------------------+-------------------------------------------------------------+


.. note::
   The sections below describe the options common to all modules, but each
   module can also define specific options for some/all the commands. These
   options (if available) are described, for each publicly available module, in
   section :ref:`public-modules`. The module specific options (if any) must come
   after the common options and separated from these by "\-\-". As an example,
   the command below would run the `get_tau_pos` command with both common and
   module-specific (in this case the option `--fast`) options.
   
.. code-block:: none

   get_tau_pos -t 1.0 -i 0.0 -a 0.0 -m 101 -M 160 -f CaII_8542.tau -- --fast

The `help` information can also be obtained by running PORTA with the
"-h|\-\-help" argument and optionally giving a command name. For example:

.. code-block:: bash

   ./porta --help

or    

.. code-block:: bash

   ./porta -h get_tau_pos


   
		

help
.............

+ **Syntax**: help

+ **Description**: Prints the list of the available commands and
  options. Followed by one of the command names below, it provides detailed
  information about that command.  
  
  
version
.............

+ **Syntax**: version

+ **Description**: Prints the PORTA version and copyright information

  
echo
.............

+ **Syntax**: echo *text*

+ **Description**: Prints ('echoes') some arbitrary *text*


exit
.............

+ **Syntax**: exit

+ **Description**: Stops processing the commands script


get
.............

+ **Syntax**: get [options]

+ **Description**: Prints the value of some internal variables. The variables
  that can be queried with this command are given by the following options,
  which do not require arguments.

  + \-\-n_incl|-t:  number of inclination angles per octant
  + \-\-n_azim|-a:  number of azimuthal angles per octant
  + \-\-stack|-s:   stack output
  + \-\-n_freq|-f:  number of radiation frequencies
  + \-\-n_nodes|-n: total number of grid nodes in the grid
  + \-\-domain|-d:  domain origin and dimensions
  + \-\-period|-p:  horizontal periodicity of the domain on/off
  + \-\-wls|-w:     print all the radiation wavelengths to the standard output
  + \-\-quad|-q:    near optimal angular quadrature set
    
.. todo::
   does stack_out work?
    
set
.............

+ **Syntax**: set [options]

+ **Description**: Sets the value of some variables (number of angles,
  etc.). The variables representing the domain (ori\_ and dim\_) must be given all
  or none.

  + \-\-n_incl|-t: number of inclination angles per octant
  + \-\-n_azim|-a: number of azimuthal angles per octant
  + \-\-stack|-s:  stack output
  + \-\-quad|-q:   near optimal angular quadrature set
  + \-\-ori_x|-x:  domain origin (X)
  + \-\-ori_y|-y:  domain origin (Y)
  + \-\-ori_z|-z:  domain origin (Z)
  + \-\-dim_x|-i:  domain dimension (X)
  + \-\-dim_y|-j:  domain dimension (Y)
  + \-\-dim_z|-k:  domain dimension (Z)

.. todo::
   set period 0|1 is accepted, but for the moment we restrict PORTA to work with
   boundary conditions, so we do not include set period description here, since
   it doesn't make much sense. Add later on when checked with non-periodic
   boundaries.

.. todo::
   what does setting stack_out do?

.. todo::
   does setting the domain geometry work?
   
loadmodel
...............

+ **Syntax**: loadmodel [options]

+ **Description**: Loads a PORTA MODEL DATA (PMD) file.

  + \-\-io|-o:       format of the PMD file. Values: [pmd|h5]  (Default: h5)
  + \-\-file|-f:     input file (PORTA PMD file)

  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`. 

    
savemodel
................

+ **Syntax**: savemodel [options]

+ **Description**: Saves the curent model into a PMD file.

  + \-\-io|-o:       format of the generated PMD file. Values: [pmd|h5]  (Default: h5)
  + \-\-file|-f:     output file (PORTA PMD file)
  
  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`. 

.. note::
   Conversion between the original PORTA binary format and the newer HDF5 file
   format can be performed with PORTA by running commands like the following:

.. code-block:: none

   loadmodel -o pmd test.pmd
   savemodel test.h5

   
solve_jacobi
.............

+ **Syntax**: solve_jacobi [options]

+ **Description**: Solves the non-LTE problem using Jacobi iteration. PORTA will
  continue performing Jacobi iterations until one of the criteria below (max_it
  or max_rc) is reached, at which point PORTA will stop.
  
  + \-\-max_it|-i: maximum number of Jacobi iterations to perform
  + \-\-max_rc|-c: threshold for the relative change of atomic populations between
    consecutive iterations
    

fs_surface
................

+ **Syntax**: fs_surface [options]

+ **Description**: Performs the formal solution along a general ray direction
  for all/some frequencies and stores in a file the emergent Stokes profiles for
  each point of the model's surface. It uses monotonic Bézier interpolation.

  + \-\-io|-o:       format of the generated PSP file. Values: [psp|h5]  (Default: h5)
  + \-\-method|-c:   characteristics method to use.    Values: [sc|lc]   (Default: lc)
  + \-\-incl|-i:     inclination angle (in degrees), of the line of sight for which
    the emergent radiation will be computed
  + \-\-azim|-a:     azimuthal angle (in degrees) of the line of sight for which the
    emergent radiation will be computed
  + \-\-fr_i_min|-m: starting frequency index
  + \-\-fr_i_max|-M: ending frequency index
  + \-\-file|-f:     output file (PORTA PSP file)

.. note::
   Details about the *short characteristics* and the *long characteristics*
   methods are given in section :ref:`sc_vs_lc`.

   
fs_clv_freq
.............

+ **Syntax**: fs_clv_freq [options]

+ **Description**: Performs the formal solution for the calculation of the
  center-to-limb variation of the emergent Stokes profiles at a given discrete
  frequency of radiation and azimuth, spatially averaged.

  + \-\-method|-c: characteristics method to use. Values: [sc|lc] (Default: sc)
  + \-\-mu_min|-m: minimum :math:`\mu` value (:math:`\mu = 0` being the limb and
    :math:`\mu = 1` the disk center)
  + \-\-mu_max|-M: maximum :math:`\mu` value
  + \-\-n_dirs|-d: number of directions to take, equally spaced, in the range of
    *mu_min* :math:`\le \mu \le` *mu_max*
  + \-\-azim|-a:   azimuth angle (in degrees)
  + \-\-freq_i|-i: frequency **index** for which the center-to-limb variation will be computed
  + \-\-file|-f: output file. The center-to-limb variation results will be stored
    as a plain text file, with n_dirs lines, where each one will have five
    values: the :math:`\mu` value and the four Stokes values, where Q,U and V
    are given as a percentage over I. That, is, each line in the file will be
    given by:

    + :math:`\mu` *I Q/I[%] U/I[%] V/I[%]*
  
.. warning::
   Only for horizontally equidistant grids.

    
fs_clv_freq_av
................

+ **Syntax**: fs_clv_freq_av [options]

+ **Description**: Performs the formal solution for the calculation of the
  center-to-limb variation of the emergent Stokes profiles at a given discrete
  frequency of radiation, spatially and azimuth averaged. Similar to
  `fs_clv_freq`, but instead of specifying a particular azimuth angle, equally
  spaced *n_azim* azimuth angles will be taken by PORTA for doing the average.

  + \-\-method|-c: characteristics method to use. Values: [sc|lc] (Default: lc)
  + \-\-mu_min|-m: minimum :math:`\mu` value (:math:`\mu = 0` being the limb and
    :math:`\mu = 1` the disk center)
  + \-\-mu_max|-M: maximum :math:`\mu` value
  + \-\-n_dirs|-d: number of directions to take, equally spaced, in the range of
    *mu_min* :math:`\le \mu \le` *mu_max*
  + \-\-n_azim|-a: number of equally spaced azimuth angles to be considered for
    the calculation
  + \-\-freq_i|-i: frequency **index** for which the center-to-limb variation will be computed
  + \-\-file|-f: output file. The center-to-limb variation results will be stored
    as a plain text file, with n_dirs lines, where each one will have five
    values: the :math:`\mu` value and the four Stokes values, where Q,U and V
    are given as a percentage over I. That, is, each line in the file will be
    given by:

    + :math:`\mu` *I Q/I[%] U/I[%] V/I[%]*
  
.. warning::
   Only for horizontally equidistant grids.

.. note::
   Details about the *short characteristics* and the *long characteristics*
   methods are given in section :ref:`sc_vs_lc`.


fs_azaver
.............

+ **Syntax**: fs_azaver [options]

+ **Description**: Performs the formal solution, and provides the emergent
  Stokes profiles averaged over all grid points and all azimuths.

  + \-\-method|-c:   characteristics method to use. Values: [sc|lc] (Default: lc) 
  + \-\-incl|-t:  inclination angle (in degrees), of the line of sight.
  + \-\-n_azim|-a: number of equally spaced azimuth angles to be considered for
    the calculation.
  + \-\-fr_i_min|-m: starting frequency **index**
  + \-\-fr_i_max|-M: ending frequency **index**
  + \-\-file|-f: output file. The output will be stored as a plain text file,
    where each line will have five values: the :math:`\lambda` frequency value
    and the four Stokes values. That, is, each line in the file will be given
    by:
    
    + :math:`\lambda` *I Q U V*
  
.. todo::
   Verify description OK?


.. _get_tau_pos:

get_tau_pos
.................

+ **Syntax**: get_tau_pos [options]

+ **Description**: For a general ray direction calculates geometrical
  coordinates of points with a given :math:`\tau` distance from the surface
  nodes. It stores the data in a TAU file.  

  + \-\-io|-o:       format of the generated TAU file. Values: [tau|h5] (Default: h5)
  + \-\-tau|-t:      optical distance :math:`\tau from the surface nodes
  + \-\-incl|-i:     inclination angle (in degrees) of the line of sight for which the
    coordinates of :math:`\tau =` *tau* are to be calculated.
  + \-\-azim|-a:     azimuthal angle (in degrees) of the line of sight for which the
    coordinates of :math:`\tau =` *tau* are to be calculated.
  + \-\-fr_i_min|-m: starting frequency **index** for which the tau coordinates are to be calculated.
  + \-\-fr_i_max|-M: ending frequency **index** for which the tau coordinates are to be calculated.
  + \-\-file|-f:     output file (PORTA TAU file), which will store, for each
    wavelength, the actual :math:`\tau` value and the 3D coordinates for
    :math:`\tau =` *tau*.

  Further details about the PORTA file formats are available in section
  :ref:`PORTA_file_formats`.  

.. todo::
   Should the GUI be able to display these TAU files somehow?
       
.. _PORTA_file_formats:

PORTA files
-------------------

PORTA uses three different types of files:

+ PORTA MODEL DATA (PMD) files. Used with commands `loadmodel`_ and
  `savemodel`_ to read or write an atmosphere model.
+ PORTA SPECTRUM PROFILE (PSP) files. Used with command `fs_surface`_ to store
  the emergent radiation.
+ PORTA TAU (TAU) files. Used with command `get_tau_pos`_ to store the
  geometrical coordinates of points with a given :math:`\tau` distance from the
  surface.

Each of these files can be formatted using the original PORTA binary formats or
the newer HDF5 file format.

The technical details of each format for each file type is given in Appendix
:ref:`PORTA_files_formats`. The directories for the tests for the three different
modules include *Python* scripts that generate PMD files (both in "binary" and
in HDF5 formats), which can help as starting point for the generation of new PMD
files by PORTA users. Details of the mentioned tests and how to run them are in
section :ref:`public-tests`.

PSP and TAU files are generated by PORTA as output of the above-mentioned
commands.  

The included PORTA GUI visualization tool (see section
:ref:`visualization_PORTA`) can help with the visualization of PMD files (for
the three modules in this version of PORTA) and PSP files.

.. todo::
   Fill in the appendix with the technical details of each file type

Sample run
-----------

Once PORTA has been compiled and we have a PMD atmosphere file, we can run PORTA
with a commands file. In section :ref:`public-tests` we will show the
steps in more detail, but below we can see a possible commands script to test
the multilevel module:

.. code-block:: none

   loadmodel -f test.h5
   solve_jacobi -i 10 -c 1.0e-4
   savemodel -f test_j10.h5

   loadmodel -f test_j10.h5
   fs_surface -i 0 -a 0 -m 0 -M 100 -f test_psp.h5

When PORTA runs with this script, we should get two new files in the same directory where we
run the code: *test_j10.h5* and *test_psp.h5*, and we should get output
information similar to the following:

.. code-block:: none

   $ mpirun -np 5 ./porta 4 testh5.por
   [0] ## Porta 0.1.0  (build Dec 10 2019, 16:52:43)
   [0] ## Running 4 slave processes (4 subdomains, 1 frequency bands)
   [0] ## initializing global variables
   [1] ## initializing global variables
   [0] ## generating directions quadrature [1 x 1] per octant
   [1] ## generating directions quadrature [1 x 1] per octant
   [0] ## loading a PMD (HDF5) model from test.h5
   [1] ## generating directions quadrature [4 x 2] per octant
   [0] ## generating directions quadrature [4 x 2] per octant
   [0] ## creating a new grid with 3 x 3 x 70 nodes
   [1] ## Subdomain [1] z-indices: 0--18 (19 in total)
   [2] ## Subdomain [2] z-indices: 18--35 (18 in total)
   [0] ## loading the module multilevel
   [4] ## Subdomain [4] z-indices: 52--69 (18 in total)
   [3] ## Subdomain [3] z-indices: 35--52 (18 in total)
   [1] ## domain dimensions: 3.479145e+03 x 3.479145e+03 x 2.319430e+03 km^3
   [0] ## Loaded: libmultilevel.so
   [0] ## setting 502 frequencies
   [1] ## setting 502 frequencies
   [0] ## loading nodes data for 3x3x70 nodes
   [0] ## test.h5
   [1] STARTING JACOBI ITERATION WITH 10 ITERS. AND MAX. REL. CHANGE 1.000000e-04, \
       T = 0.000000e+00 sec
   [1]    1	7.000000e+00	2.684213e-02
   [1]    2	1.500000e+01	1.035172e-02
   [1]    3	2.200000e+01	1.483056e-03
   [1]    4	3.000000e+01	6.878365e-04
   [1]    5	3.700000e+01	4.897980e-04
   [1]    6	4.400000e+01	3.713853e-04
   [1]    7	5.200000e+01	3.515242e-04
   [1]    8	5.900000e+01	3.337930e-04
   [1]    9	6.700000e+01	3.149104e-04
   [1]   10	7.400000e+01	2.949745e-04
   [0] Jacobi iteration finished. Maximum relative change of population: 2.949745e-04.
   [0] ## saving a PMD model to the file test_j10.h5
   [0] ## loading a PMD (HDF5) model from test_j10.h5
   [0] ## releasing grid 0
   [0] ## unlinking the module multilevel
   [0] ## generating directions quadrature [4 x 2] per octant
   [1] ## generating directions quadrature [4 x 2] per octant
   [0] ## creating a new grid with 3 x 3 x 70 nodes
   [1] ## Subdomain [1] z-indices: 0--18 (19 in total)
   [2] ## Subdomain [2] z-indices: 18--35 (18 in total)
   [4] ## Subdomain [4] z-indices: 52--69 (18 in total)
   [0] ## loading the module multilevel
   [3] ## Subdomain [3] z-indices: 35--52 (18 in total)
   [1] ## domain dimensions: 3.479145e+03 x 3.479145e+03 x 2.319430e+03 km^3
   [0] ## Loaded: libmultilevel.so
   [0] ## setting 502 frequencies
   [1] ## setting 502 frequencies
   [0] ## loading nodes data for 3x3x70 nodes
   [0] ## test_j10.h5
   [0] ## calculating formal solution at the model surface...
   [0] ## exiting Porta
   [0] ## releasing grid 0
   [0] ## unlinking the module multilevel
   [0] ## bye
   $

		
For each run, PORTA also generates a more detailed *porta.log* file, which can be
useful for debugging purposes. The first few lines of this file for this
run were:

.. code-block:: none

   [1] @ Tue Dec 10 16:54:25 2019: initializing global variables
   [0] @ Tue Dec 10 16:54:25 2019: initializing global variables
   [0] @ Tue Dec 10 16:54:25 2019: generating directions quadrature [1 x 1] per octant
   [1] @ Tue Dec 10 16:54:25 2019: generating directions quadrature [1 x 1] per octant
   [1] @ Tue Dec 10 16:54:25 2019: process [1] received signal 4001 from the \
       master process
   [4] @ Tue Dec 10 16:54:25 2019: process [4] received signal 4001 from the \
       master process
   [2] @ Tue Dec 10 16:54:25 2019: process [2] received signal 4001 from the \
       master process
   [3] @ Tue Dec 10 16:54:25 2019: process [3] received signal 4001 from the \
       master process
   [0] @ Tue Dec 10 16:54:25 2019: loading a PMD (HDF5) model from test.h5
   [1] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [2] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [3] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [4] @ Tue Dec 10 16:54:25 2019: loading a PMD model
   [0] @ Tue Dec 10 16:54:25 2019: domain size: (3.479145000000e+03, \
       3.479145000000e+03, 2.319430000000e+03) km
   [0] @ Tue Dec 10 16:54:25 2019: domain origin: (0.000000000000e+00, \
       0.000000000000e+00, -1.000000000000e+02) km
   [0] @ Tue Dec 10 16:54:25 2019: (nx,ny,nz): (3,3,70)
   [0] @ Tue Dec 10 16:54:25 2019: mix(x), max(x): 0.000000000000e+00, \
       2.319430000000e+03 km
   [0] @ Tue Dec 10 16:54:25 2019: mix(y), max(y): 0.000000000000e+00, \
       2.319430000000e+03 km
   [0] @ Tue Dec 10 16:54:25 2019: mix(z), max(z): -1.000000000000e+02, \
       2.219430000000e+03 km


.. note::
   The numbers in square brackets at the beginning of each line for the output
   and log files generated by PORTA represent the MPI rank of the process
   generating that output information, which can also be useful for debugging.

.. [1] `Štěpán, J., and Trujillo Bueno, J. 2013, A&A, 557, A143 <https://ui.adsabs.harvard.edu/abs/2013A%26A...557A.143S/abstract>`_






  

   
